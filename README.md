SPRING BOOT VERSION: 2.7.14

JAVA VERSION: Java 19

GRADLE VERSION: Gradle 8.5

RUN SONAR WITH: docker run --name sonar -m 4g --cpus=2 -p 9001:9000 -itd sonarqube

COMMAND FOR RUN SONAR IN MICROSERVICE: gradle vials-motors-technical:sonar   -Dsonar.projectKey=vials-motors-technical   -Dsonar.projectName='vials-motors-technical'   -Dsonar.host.url=http://localhost:9001   -Dsonar.token=sqp_39255ba55b9526a08e08d9ea8219a4fa4e762514 -Dsonar.gradle.skipCompile=true --warning-mode all

NOTE: Remember that since it is a local sonar, you must create the projects, once created, copy the sonar token and replace the one in the command

SONAR_ADMIN:  ./gradlew vials-motors-admin:sonar -Dsonar.token=9f49724865df20ba9fa223efcabf1f63124de5c7
SONAR_CUSTOMER:  ./gradlew vials-motors-customer:sonar -Dsonar.login=f8a86f2372f9d4fe40992b656aa1174658b57436
