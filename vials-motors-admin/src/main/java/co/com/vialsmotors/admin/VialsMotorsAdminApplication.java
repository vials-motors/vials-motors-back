package co.com.vialsmotors.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class VialsMotorsAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(VialsMotorsAdminApplication.class, args);
	}

}
