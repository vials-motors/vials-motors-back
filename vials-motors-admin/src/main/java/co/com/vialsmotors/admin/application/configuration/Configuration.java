package co.com.vialsmotors.admin.application.configuration;

import co.com.vialsmotors.admin.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.admin.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.admin.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.admin.domain.port.manager.*;
import co.com.vialsmotors.admin.domain.service.delete.DeleteManagerImplService;
import co.com.vialsmotors.admin.domain.service.get.GetManagerByCustomerIdImplService;
import co.com.vialsmotors.admin.domain.service.identificationtype.delete.DeleteIdentificationTypeImplService;
import co.com.vialsmotors.admin.domain.service.identificationtype.list.GetIdentificationTypeImplService;
import co.com.vialsmotors.admin.domain.service.identificationtype.save.SaveIdentificationTypeImplService;
import co.com.vialsmotors.admin.domain.service.identificationtype.update.UpdateIdentificationTypeImplService;
import co.com.vialsmotors.admin.domain.service.list.GetListManagerImplService;
import co.com.vialsmotors.admin.domain.service.register.RegisterManagerImplService;
import co.com.vialsmotors.admin.domain.service.update.UpdateManagerImplService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public RegisterManagerImplService registerManagerImplService(RegisterManagerRepository registerManagerRepository,
                                                                 RegisterCredentialPort registerCredentialPort,
                                                                 GetManagerByIdentificationNumberOrEmailAndStatusRepository getManagerByIdentificationNumberOrEmailAndStatusRepository){
        return new RegisterManagerImplService(registerManagerRepository, registerCredentialPort, getManagerByIdentificationNumberOrEmailAndStatusRepository);
    }

    @Bean
    public GetListManagerImplService getListManagerImplService(GetListManagerRepository getListManagerRepository){
        return new GetListManagerImplService(getListManagerRepository);
    }

    @Bean
    public GetIdentificationTypeImplService getIdentificationTypeImplService(GetListIdentificationTypeRepository getListIdentificationTypeRepository){
        return new GetIdentificationTypeImplService(getListIdentificationTypeRepository);
    }

    @Bean
    public UpdateManagerImplService updateManagerImplService(RegisterManagerRepository registerManagerRepository, UpdateCredentialPort updateCredentialPort){
        return new UpdateManagerImplService(registerManagerRepository, updateCredentialPort);
    }

    @Bean
    public DeleteManagerImplService deleteManagerImplService(DeleteCredentialPort deleteCredentialPort, RegisterManagerRepository registerManagerRepository){
        return new DeleteManagerImplService(deleteCredentialPort, registerManagerRepository);
    }

    @Bean
    public SaveIdentificationTypeImplService saveIdentificationTypeImplService(SaveIdentificationTypeRepository saveIdentificationTypeRepository){
        return new SaveIdentificationTypeImplService(saveIdentificationTypeRepository);
    }

    @Bean
    public GetManagerByCustomerIdImplService getManagerByCustomerIdImplService(GetManagerByCustomerIdRepository getManagerByCustomerIdRepository){
        return new GetManagerByCustomerIdImplService(getManagerByCustomerIdRepository);
    }

    @Bean
    public UpdateIdentificationTypeImplService updateIdentificationTypeImplService(SaveIdentificationTypeRepository saveIdentificationTypeRepository,
                                                                                   GetIdentificationTypeRepository getIdentificationTypeRepository){
        return new UpdateIdentificationTypeImplService(saveIdentificationTypeRepository, getIdentificationTypeRepository);
    }

    @Bean
    public DeleteIdentificationTypeImplService deleteIdentificationTypeImplService(DeleteIdentificationTypeRepository deleteIdentificationTypeRepository,
                                                                                   GetIdentificationTypeRepository getIdentificationTypeRepository){
        return new DeleteIdentificationTypeImplService(deleteIdentificationTypeRepository, getIdentificationTypeRepository);
    }
}
