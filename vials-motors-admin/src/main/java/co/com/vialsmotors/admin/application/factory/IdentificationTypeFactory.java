package co.com.vialsmotors.admin.application.factory;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.domain.model.IdentificationType;
import org.springframework.stereotype.Component;

@Component
public class IdentificationTypeFactory {

    public IdentificationType execute(IdentificationTypeCommand identificationTypeCommand){
        return new IdentificationType(identificationTypeCommand.getIdIdentificationType(), identificationTypeCommand.getDescription());
    }
}
