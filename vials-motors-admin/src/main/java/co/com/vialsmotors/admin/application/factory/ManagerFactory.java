package co.com.vialsmotors.admin.application.factory;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.model.Manager;
import co.com.vialsmotors.admin.domain.request.UpdateManagerRequest;
import org.springframework.stereotype.Component;

@Component
public class ManagerFactory {

    public Manager buildManager(ManagerCommand managerCommand){
        return new Manager(managerCommand.getFirstName(), managerCommand.getSecondName(), managerCommand.getFirstLastName(),
                managerCommand.getSecondLastName(), managerCommand.getPhone(), managerCommand.getIdentificationNumber(),
                buildIdentificationType(managerCommand.getIdentificationType()), managerCommand.getPassword(), managerCommand.getEmail());
    }

    public UpdateManagerRequest updateManagerRequest(ManagerCommand managerCommand){
        return new UpdateManagerRequest(managerCommand.getFirstName(), managerCommand.getSecondName(),
                managerCommand.getFirstLastName(), managerCommand.getSecondLastName(), managerCommand.getPhone(),
                managerCommand.getIdentificationNumber(), managerCommand.getEmail());
    }

    private IdentificationType buildIdentificationType(IdentificationTypeCommand identificationTypeCommand){
        return identificationTypeCommand == null ?
                null:new IdentificationType(identificationTypeCommand.getIdIdentificationType(), identificationTypeCommand.getDescription());
    }
}
