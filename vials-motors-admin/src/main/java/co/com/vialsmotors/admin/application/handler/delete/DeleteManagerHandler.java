package co.com.vialsmotors.admin.application.handler.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteManagerHandler {

    ResponseEntity<GenericResponse> deleteManager(String identificationNumber, String email);
}
