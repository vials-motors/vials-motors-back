package co.com.vialsmotors.admin.application.handler.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.delete.DeleteManagerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteManagerImplHandler implements DeleteManagerHandler {

    private static final Logger LOGGER = Logger.getLogger(DeleteManagerImplHandler.class);
    private final DeleteManagerService deleteManagerService;

    @Override
    public ResponseEntity<GenericResponse> deleteManager(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-ADMIN: Delete manager in handler with identification Number: " + identificationNumber);
        return deleteManagerService.deleteManager(identificationNumber, email );
    }
}
