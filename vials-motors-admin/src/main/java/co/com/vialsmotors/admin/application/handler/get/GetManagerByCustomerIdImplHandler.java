package co.com.vialsmotors.admin.application.handler.get;

import co.com.vialsmotors.admin.domain.response.ManagerResponse;
import co.com.vialsmotors.admin.domain.service.get.GetManagerByCustomerIdService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetManagerByCustomerIdImplHandler implements GetManagerByCustomerIdHandler{

    private static final Logger LOGGER = Logger.getLogger(GetManagerByCustomerIdImplHandler.class);
    private final GetManagerByCustomerIdService getManagerByCustomerIdService;

    @Override
    public ResponseEntity<ManagerResponse> getManager(String customerId) {
        LOGGER.info("INFO-MS-ADMIN: Get manager in handler with customer id: " + customerId);
        return getManagerByCustomerIdService.getManager(customerId);
    }
}
