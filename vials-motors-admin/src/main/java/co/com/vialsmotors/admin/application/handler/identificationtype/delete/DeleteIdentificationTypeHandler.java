package co.com.vialsmotors.admin.application.handler.identificationtype.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteIdentificationTypeHandler {

    ResponseEntity<GenericResponse> deleteIdentificationType(Long id);
}
