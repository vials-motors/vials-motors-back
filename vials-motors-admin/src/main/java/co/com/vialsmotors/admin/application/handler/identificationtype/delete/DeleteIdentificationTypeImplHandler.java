package co.com.vialsmotors.admin.application.handler.identificationtype.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.identificationtype.delete.DeleteIdentificationTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteIdentificationTypeImplHandler implements DeleteIdentificationTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteIdentificationTypeImplHandler.class);
    private final DeleteIdentificationTypeService deleteIdentificationTypeService;

    @Override
    public ResponseEntity<GenericResponse> deleteIdentificationType(Long id) {
        LOGGER.info("ERROR-MS-ADMIN: Delete identification type with id: " + id);
        return deleteIdentificationTypeService.deleteIdentificationType(id);
    }
}
