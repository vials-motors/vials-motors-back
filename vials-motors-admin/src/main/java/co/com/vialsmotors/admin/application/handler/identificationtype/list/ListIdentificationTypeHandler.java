package co.com.vialsmotors.admin.application.handler.identificationtype.list;

import co.com.vialsmotors.admin.domain.response.ListIdentificationTypeResponse;
import org.springframework.http.ResponseEntity;

public interface ListIdentificationTypeHandler {

    ResponseEntity<ListIdentificationTypeResponse> getIdentificationType();
}
