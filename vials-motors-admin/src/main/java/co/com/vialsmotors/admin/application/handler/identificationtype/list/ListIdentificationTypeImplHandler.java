package co.com.vialsmotors.admin.application.handler.identificationtype.list;

import co.com.vialsmotors.admin.domain.response.ListIdentificationTypeResponse;
import co.com.vialsmotors.admin.domain.service.identificationtype.list.GetIdentificationTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ListIdentificationTypeImplHandler implements ListIdentificationTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(ListIdentificationTypeImplHandler.class);

    private final GetIdentificationTypeService getIdentificationTypeService;

    @Override
    public ResponseEntity<ListIdentificationTypeResponse> getIdentificationType() {
        LOGGER.info("INFO-MS-ADMIN: Get Identification Type in ListIdentificationTypeImplHandler");
        return getIdentificationTypeService.getIdentificationType();
    }
}
