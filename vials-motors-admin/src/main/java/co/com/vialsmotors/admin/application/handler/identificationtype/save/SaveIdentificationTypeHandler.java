package co.com.vialsmotors.admin.application.handler.identificationtype.save;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveIdentificationTypeHandler {

    ResponseEntity<GenericResponse> saveIdentificationType(IdentificationTypeCommand identificationTypeCommand);
}
