package co.com.vialsmotors.admin.application.handler.identificationtype.save;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.application.factory.IdentificationTypeFactory;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.identificationtype.save.SaveIdentificationTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveIdentificationTypeImplHandler implements SaveIdentificationTypeHandler{

    private final SaveIdentificationTypeService saveIdentificationTypeService;
    private final IdentificationTypeFactory identificationTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> saveIdentificationType(IdentificationTypeCommand identificationTypeCommand) {
        return saveIdentificationTypeService.saveIdentificationType(identificationTypeFactory.execute(identificationTypeCommand));
    }
}
