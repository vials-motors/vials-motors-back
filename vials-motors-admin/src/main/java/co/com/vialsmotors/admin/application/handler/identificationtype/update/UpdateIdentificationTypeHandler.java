package co.com.vialsmotors.admin.application.handler.identificationtype.update;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateIdentificationTypeHandler {

    ResponseEntity<GenericResponse> updateIdentificationType(IdentificationTypeCommand identificationTypeCommand, Long id);
}
