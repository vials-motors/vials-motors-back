package co.com.vialsmotors.admin.application.handler.identificationtype.update;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.application.factory.IdentificationTypeFactory;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.identificationtype.update.UpdateIdentificationTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateIdentificationTypeImplHandler implements UpdateIdentificationTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateIdentificationTypeImplHandler.class);
    private final UpdateIdentificationTypeService updateIdentificationTypeService;
    private final IdentificationTypeFactory identificationTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> updateIdentificationType(IdentificationTypeCommand identificationTypeCommand, Long id) {
        LOGGER.info("INFO-MS-ADMIN: Update identification type with id: " + id);
        return this.updateIdentificationTypeService.updateIdentificationType(identificationTypeFactory.execute(identificationTypeCommand), id);
    }
}
