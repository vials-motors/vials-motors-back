package co.com.vialsmotors.admin.application.handler.list;

import co.com.vialsmotors.admin.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListManagerHandler {

    ResponseEntity<ListGenericResponse> getListManager();
}
