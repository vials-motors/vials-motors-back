package co.com.vialsmotors.admin.application.handler.list;

import co.com.vialsmotors.admin.domain.response.ListGenericResponse;
import co.com.vialsmotors.admin.domain.service.list.GetListManagerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListManagerImplHandler implements GetListManagerHandler{

    private static final Logger LOGGER = Logger.getLogger(GetListManagerImplHandler.class);

    private final GetListManagerService getListManagerService;

    @Override
    public ResponseEntity<ListGenericResponse> getListManager() {
        LOGGER.info("INFO-MS-ADMIN: Get list in getListManager for class " + GetListManagerImplHandler.class.getName());
        return getListManagerService.getListManager();
    }
}
