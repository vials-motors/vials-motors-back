package co.com.vialsmotors.admin.application.handler.register;

import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterManagerHandler {

    ResponseEntity<GenericResponse> registerManager(ManagerCommand managerCommand);
}
