package co.com.vialsmotors.admin.application.handler.register;

import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.application.factory.ManagerFactory;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.register.RegisterManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RegisterManagerImplHandler implements RegisterManagerHandler{

    private final RegisterManagerService registerManagerService;
    private final ManagerFactory managerFactory;

    @Override
    public ResponseEntity<GenericResponse> registerManager(ManagerCommand managerCommand) {
        return registerManagerService.registerManager(managerFactory.buildManager(managerCommand));
    }
}
