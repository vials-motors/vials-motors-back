package co.com.vialsmotors.admin.application.handler.update;

import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateManagerHandler {

    ResponseEntity<GenericResponse> updateManager(ManagerCommand managerCommand, String identificationNumber, String email);
}
