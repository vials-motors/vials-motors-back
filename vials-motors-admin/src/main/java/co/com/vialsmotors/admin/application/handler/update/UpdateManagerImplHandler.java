package co.com.vialsmotors.admin.application.handler.update;

import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.application.factory.ManagerFactory;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.service.update.UpdateManagerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateManagerImplHandler implements UpdateManagerHandler {

    private static final Logger LOGGER = Logger.getLogger(UpdateManagerImplHandler.class);

    private final UpdateManagerService updateManagerService;
    private final ManagerFactory managerFactory;

    @Override
    public ResponseEntity<GenericResponse> updateManager(ManagerCommand managerCommand, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-ADMIN: Update manager in handler with identification number " + identificationNumber);
        return updateManagerService.updateManager(managerFactory.updateManagerRequest(managerCommand), identificationNumber, email);
    }
}
