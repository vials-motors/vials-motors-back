package co.com.vialsmotors.admin.domain.dto;

public record IdentificationTypeDTO(Long idIdentificationType, String description) {

}
