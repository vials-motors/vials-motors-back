package co.com.vialsmotors.admin.domain.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    REGISTER_MANAGER_SUCCESSFULLY("SUCC-00", "Administrador registrado correctamente"),
    GET_LIST_SUCCESSFULLY("SUCC-01", "Lista obtenida correctamente"),
    UPDATE_MANAGER_SUCCESS("SUCC-02", "Actualización de administrador correcta"),
    DELETE_MANAGER_SUCCESS("SUCC-03", "Administrador eliminado correctamente"),
    REGISTER_IDENTIFICATION_TYPE_SUCCESS("SUCC-04", "Tipo de identificacion guardada"),
    GET_MANAGER_SUCCESSFULLY("SUCC-05", "Administrador obtenido correctamente"),
    UPDATE_IDENTIFICATION_TYPE_SUCCESS("SUCC-05", "Tipo de identificacion actualizado"),
    DELETE_IDENTIFICATION_TYPE_SUCCESS("SUCC-06", "Tipo de identification eliminada"),
    ERROR_LIST_MANAGER("ERR-02", "Error obteniendo lista de administrador"),
    MESSAGE_ERROR_REGISTER("ERR-00", "Ocurrio un error mientras se registraba"),
    MESSAGE_ERROR_IDENTIFICATION_NUMBER("ERR-01", "El administrador ya existe"),
    EMPTY_LIST_MANAGER("ERR-03", "La lista esta vacia"),
    EMPTY_LIST_IDENTIFICATION_TYPE("ERR-04", "La lista para tipo de identificación es vacia"),
    MANAGER_NOT_EXIST("ERR-05", "El administrador no existe"),
    IDENTIFICATION_TYPE_NOT_EXIST("ERR-06", "El tipo de identificacion no existe"),
    FIELD_EMAIL_ERROR("ERR-07", "El correo es requerido"),
    FIELD_PASSWORD_ERROR("ERR-08", "La contraseña es requerida"),
    FIELD_CUSTOMER_ID("ERR-09", "El customer id es requerido"),
    IDENTIFICATION_NUMBER_ERROR("ERR-10", "El numero de identificacion ya existe"),
    EMAIL_ERROR("ERR-11", "El correo ya existe"),
    FIELD_ID_IDENTIFICATION_TYPE_ERROR("ERR-12", "El id del tipo de identificacion es nulo"),
    INTERNAL_ERROR("ERR-500", "Error interno de servidor");

    private final String code;
    private final String message;
}
