package co.com.vialsmotors.admin.domain.exception;

import lombok.Getter;

@Getter
public class ListIdentificationTypeException extends RuntimeException{

    private final String status;

    public ListIdentificationTypeException(String message, String status) {
        super(message);
        this.status = status;
    }
}
