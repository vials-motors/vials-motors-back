package co.com.vialsmotors.admin.domain.exception;

import lombok.Getter;

@Getter
public class ListManagerException extends RuntimeException{

    private final String status;

    public ListManagerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
