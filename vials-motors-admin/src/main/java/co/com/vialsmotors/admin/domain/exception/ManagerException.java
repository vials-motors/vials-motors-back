package co.com.vialsmotors.admin.domain.exception;

import lombok.Getter;

@Getter
public class ManagerException extends RuntimeException{

    private final String status;

    public ManagerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
