package co.com.vialsmotors.admin.domain.model;

import co.com.vialsmotors.admin.domain.validate.ValidateArgument;
import co.com.vialsmotors.commons.domain.enums.EFieldMessage;

public record IdentificationType(Long idIdentificationType, String description) {

    public IdentificationType{
        ValidateArgument.validateLongField(idIdentificationType, EFieldMessage.FIELD_ID_MESSAGE.getCode(), EFieldMessage.FIELD_ID_MESSAGE.getMessage());
        ValidateArgument.validateStringField(description, EFieldMessage.FIELD_DESCRIPTION_MESSAGE.getCode(), EFieldMessage.FIELD_DESCRIPTION_MESSAGE.getMessage());
    }
}
