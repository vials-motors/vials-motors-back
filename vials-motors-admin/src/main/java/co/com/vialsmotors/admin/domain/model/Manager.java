package co.com.vialsmotors.admin.domain.model;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.validate.ValidateArgument;
import co.com.vialsmotors.commons.domain.enums.EFieldMessage;

public record Manager(String firstName, String secondName, String firstLastName, String secondLastName, String phone,
                      String identificationNumber, IdentificationType identificationType, String password, String email) {

    public Manager{
        ValidateArgument.validateStringField(firstName, EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getCode(), EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getMessage());
        ValidateArgument.validateStringField(firstLastName, EFieldMessage.FIELD_FIRST_LAST_NAME_MESSAGE.getCode(), EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getMessage());
        ValidateArgument.validateStringField(phone, EFieldMessage.FIELD_PHONE_MESSAGE.getCode(), EFieldMessage.FIELD_PHONE_MESSAGE.getMessage());
        ValidateArgument.validateStringField(identificationNumber, EFieldMessage.FIELD_IDENTIFICATION_NUMBER.getCode(), EFieldMessage.FIELD_IDENTIFICATION_NUMBER.getMessage());
        ValidateArgument.validateStringField(email, EResponse.FIELD_EMAIL_ERROR.getCode(), EResponse.FIELD_EMAIL_ERROR.getMessage());
        ValidateArgument.validateStringField(password, EResponse.FIELD_PASSWORD_ERROR.getCode(), EResponse.FIELD_PASSWORD_ERROR.getMessage());
        ValidateArgument.validateIdentificationTypeField(identificationType, EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getCode(), EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getMessage());
    }
}
