package co.com.vialsmotors.admin.domain.port.credential;

public interface DeleteCredentialPort {

    Boolean deleteCredential(String customerId);
}
