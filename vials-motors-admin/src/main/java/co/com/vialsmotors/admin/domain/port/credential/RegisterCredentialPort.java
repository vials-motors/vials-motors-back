package co.com.vialsmotors.admin.domain.port.credential;


import co.com.vialsmotors.admin.infrastructure.apirest.dto.CredentialDTO;

public interface RegisterCredentialPort {

    Boolean registerCredential(CredentialDTO credentialDTO, Integer userType);
}
