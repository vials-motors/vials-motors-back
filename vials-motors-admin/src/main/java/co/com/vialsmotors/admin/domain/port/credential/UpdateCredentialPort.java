package co.com.vialsmotors.admin.domain.port.credential;

public interface UpdateCredentialPort {

    Boolean updateCredential(String username, String customerID);
}
