package co.com.vialsmotors.admin.domain.port.manager;

public interface DeleteIdentificationTypeRepository {

    void deleteIdentificationType(Long id);
}
