package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;

import java.util.Optional;

public interface GetIdentificationTypeRepository {

    Optional<IdentificationTypeEntity> getIdentificationType(Long id);
}
