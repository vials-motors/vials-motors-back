package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;

import java.util.List;

public interface GetListIdentificationTypeRepository {

    List<IdentificationTypeEntity> getListIdentificationType();
}
