package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;

import java.util.List;

public interface GetListManagerRepository {

    List<ManagerEntity> getListManager();
}
