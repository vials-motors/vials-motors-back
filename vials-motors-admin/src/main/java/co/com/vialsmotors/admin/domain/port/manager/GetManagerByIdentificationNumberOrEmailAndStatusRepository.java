package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;

import java.util.Optional;

public interface GetManagerByIdentificationNumberOrEmailAndStatusRepository {

    Optional<ManagerEntity> getManagerWithIdentificationNumber(String identificationNumber);
    Optional<ManagerEntity> getManagerWithEmail(String email);
}
