package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;

import java.util.Optional;

public interface RegisterManagerRepository {

    void registerManager(ManagerEntity managerEntity);
    Optional<ManagerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email);
}
