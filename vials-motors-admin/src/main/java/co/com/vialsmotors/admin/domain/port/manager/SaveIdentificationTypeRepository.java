package co.com.vialsmotors.admin.domain.port.manager;

import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;

public interface SaveIdentificationTypeRepository {

    void saveIdentificationType(IdentificationTypeEntity identificationType);
}
