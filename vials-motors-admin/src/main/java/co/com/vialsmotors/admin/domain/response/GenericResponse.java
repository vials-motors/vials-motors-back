package co.com.vialsmotors.admin.domain.response;

import java.time.ZonedDateTime;

public record GenericResponse(String code, String message, ZonedDateTime timestamp) {
}
