package co.com.vialsmotors.admin.domain.response;

import co.com.vialsmotors.admin.domain.dto.ManagerDTO;

import java.time.ZonedDateTime;
import java.util.List;

public record ListGenericResponse(String status, String message, List<ManagerDTO> data, ZonedDateTime timestamp) {
}
