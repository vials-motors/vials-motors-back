package co.com.vialsmotors.admin.domain.response;

import co.com.vialsmotors.admin.domain.dto.IdentificationTypeDTO;

import java.time.ZonedDateTime;
import java.util.List;

public record ListIdentificationTypeResponse(String status, String message, List<IdentificationTypeDTO> data, ZonedDateTime timestamp) {
}
