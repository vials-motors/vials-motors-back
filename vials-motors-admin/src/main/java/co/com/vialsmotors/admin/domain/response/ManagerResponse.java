package co.com.vialsmotors.admin.domain.response;

import co.com.vialsmotors.admin.domain.dto.ManagerDTO;

import java.time.ZonedDateTime;

public record ManagerResponse(String code, String message, ManagerDTO data, ZonedDateTime timestamp) {
}
