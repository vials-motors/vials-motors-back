package co.com.vialsmotors.admin.domain.service.delete;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.admin.domain.exception.ManagerException;
import co.com.vialsmotors.admin.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.admin.domain.port.manager.RegisterManagerRepository;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteManagerImplService implements DeleteManagerService{

    private static final Logger LOGGER = Logger.getLogger(DeleteManagerImplService.class);
    private static final Boolean STATUS_TRUE_FOR_MANAGER = true;
    private static final Boolean STATUS_FALSE_FOR_MANAGER = false;
    private final DeleteCredentialPort deleteCredentialPort;
    private final RegisterManagerRepository registerManagerRepository;

    public DeleteManagerImplService(DeleteCredentialPort deleteCredentialPort, RegisterManagerRepository registerManagerRepository) {
        this.deleteCredentialPort = deleteCredentialPort;
        this.registerManagerRepository = registerManagerRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteManager(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-ADMIN: Delete credential with identification number: " + identificationNumber);
        this.registerManagerRepository.registerManager(callDeleteCredential(identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_MANAGER_SUCCESS.getCode(), EResponse.DELETE_MANAGER_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isEmpty())
            throw new IdentificationNumberException(EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
    }

    private void validateEmail(String email){
        if (email == null || email.isEmpty()){
            throw new FieldException(EResponse.FIELD_EMAIL_ERROR.getMessage(), EResponse.FIELD_EMAIL_ERROR.getCode());
        }
    }

    private ManagerEntity getManagerEntity(String identificationNumber, String email){
        this.validateIdentificationNumber(identificationNumber);
        this.validateEmail(email);
        Optional<ManagerEntity> managerEntity = this.registerManagerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_TRUE_FOR_MANAGER, email );
        if (managerEntity.isPresent())
            return managerEntity.get();
        throw new ManagerException(EResponse.MANAGER_NOT_EXIST.getMessage(), EResponse.MANAGER_NOT_EXIST.getCode());
    }

    private ManagerEntity callDeleteCredential(String identificationNumber, String email){
        ManagerEntity managerEntity = getManagerEntity(identificationNumber, email);
        Boolean callCredential = this.deleteCredentialPort.deleteCredential(managerEntity.getCustomerID());
        if (Boolean.FALSE.equals(callCredential)){
            LOGGER.error("ERROR-MS-ADMIN: Error call delete credential with callCredential: " + false);
            throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL_MESSAGE.getCode());
        }
        return changeStatus(managerEntity);
    }

    private ManagerEntity changeStatus(ManagerEntity managerEntity){
        return managerEntity.withStatus(STATUS_FALSE_FOR_MANAGER);
    }
}
