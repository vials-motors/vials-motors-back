package co.com.vialsmotors.admin.domain.service.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteManagerService {

    ResponseEntity<GenericResponse> deleteManager(String identificationNumber, String email);
}
