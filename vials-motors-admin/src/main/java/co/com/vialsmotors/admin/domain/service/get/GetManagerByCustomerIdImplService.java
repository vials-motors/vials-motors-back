package co.com.vialsmotors.admin.domain.service.get;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ManagerException;
import co.com.vialsmotors.admin.domain.port.manager.GetManagerByCustomerIdRepository;
import co.com.vialsmotors.admin.domain.response.ManagerResponse;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.mapper.ManagerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class GetManagerByCustomerIdImplService implements GetManagerByCustomerIdService{

    private static final Logger LOGGER = Logger.getLogger(GetManagerByCustomerIdImplService.class);
    private final GetManagerByCustomerIdRepository getManagerByCustomerIdRepository;

    public GetManagerByCustomerIdImplService(GetManagerByCustomerIdRepository getManagerByCustomerIdRepository) {
        this.getManagerByCustomerIdRepository = getManagerByCustomerIdRepository;
    }

    @Override
    public ResponseEntity<ManagerResponse> getManager(String customerId) {
        LOGGER.info("INFO-MS-ADMIN: Get manager in service with customer id: " + customerId);
        this.validateCustomerId(customerId);
        return ResponseEntity.ok(new ManagerResponse(EResponse.GET_MANAGER_SUCCESSFULLY.getCode(),
                EResponse.GET_MANAGER_SUCCESSFULLY.getMessage(), ManagerMapper.convertEntityToDto(getManagerInOptional(customerId)),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty()){
            LOGGER.error("ERROR-MS-ADMIN: Error because customer id is null");
            throw new ManagerException(EResponse.FIELD_CUSTOMER_ID.getMessage(), EResponse.FIELD_CUSTOMER_ID.getCode());
        }
    }

    private ManagerEntity getManagerInOptional(String customerId){
        Optional<ManagerEntity> managerEntity = this.getManagerByCustomerIdRepository.getManager(customerId);
        if (managerEntity.isEmpty()){
            LOGGER.error("ERROR-MS-ADMIN: Error because manager entity is empty");
            throw new ManagerException(EResponse.MANAGER_NOT_EXIST.getMessage(), EResponse.MANAGER_NOT_EXIST.getCode());
        }
        return managerEntity.get();
    }
}
