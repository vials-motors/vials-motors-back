package co.com.vialsmotors.admin.domain.service.get;

import co.com.vialsmotors.admin.domain.response.ManagerResponse;
import org.springframework.http.ResponseEntity;

public interface GetManagerByCustomerIdService {

    ResponseEntity<ManagerResponse> getManager(String customerId);
}
