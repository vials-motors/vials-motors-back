package co.com.vialsmotors.admin.domain.service.identificationtype.delete;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ListIdentificationTypeException;
import co.com.vialsmotors.admin.domain.port.manager.DeleteIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.port.manager.GetIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteIdentificationTypeImplService implements DeleteIdentificationTypeService{

    private static final Logger LOGGER = Logger.getLogger(DeleteIdentificationTypeImplService.class);
    private final DeleteIdentificationTypeRepository deleteIdentificationTypeRepository;
    private final GetIdentificationTypeRepository getIdentificationTypeRepository;

    public DeleteIdentificationTypeImplService(DeleteIdentificationTypeRepository deleteIdentificationTypeRepository, GetIdentificationTypeRepository getIdentificationTypeRepository) {
        this.deleteIdentificationTypeRepository = deleteIdentificationTypeRepository;
        this.getIdentificationTypeRepository = getIdentificationTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteIdentificationType(Long id) {
        this.validateId(id);
        this.validateIdentificationType(id);
        this.deleteIdentificationTypeRepository.deleteIdentificationType(id);
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_IDENTIFICATION_TYPE_SUCCESS.getCode(),
                EResponse.DELETE_IDENTIFICATION_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-ADMIN: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_IDENTIFICATION_TYPE_ERROR.getMessage(), EResponse.FIELD_ID_IDENTIFICATION_TYPE_ERROR.getCode());
        }
    }

    private void validateIdentificationType(Long id){
        Optional<IdentificationTypeEntity> identificationTypeEntity = this.getIdentificationTypeRepository.getIdentificationType(id);
        if (identificationTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-ADMIN: Error because identification type not exist");
            throw new ListIdentificationTypeException(EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getMessage(), EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getCode());
        }
    }
}
