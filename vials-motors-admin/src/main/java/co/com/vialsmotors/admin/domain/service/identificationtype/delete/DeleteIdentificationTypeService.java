package co.com.vialsmotors.admin.domain.service.identificationtype.delete;

import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteIdentificationTypeService {

    ResponseEntity<GenericResponse> deleteIdentificationType(Long id);
}
