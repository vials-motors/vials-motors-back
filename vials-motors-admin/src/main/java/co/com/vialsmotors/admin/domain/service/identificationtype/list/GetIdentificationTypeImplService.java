package co.com.vialsmotors.admin.domain.service.identificationtype.list;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ListIdentificationTypeException;
import co.com.vialsmotors.admin.domain.port.manager.GetListIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.response.ListIdentificationTypeResponse;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.admin.infrastructure.mapper.IdentificationTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetIdentificationTypeImplService implements GetIdentificationTypeService{

    private static final Logger LOGGER = Logger.getLogger(GetIdentificationTypeImplService.class);

    private final GetListIdentificationTypeRepository getListIdentificationTypeRepository;

    public GetIdentificationTypeImplService(GetListIdentificationTypeRepository getListIdentificationTypeRepository) {
        this.getListIdentificationTypeRepository = getListIdentificationTypeRepository;
    }

    @Override
    public ResponseEntity<ListIdentificationTypeResponse> getIdentificationType() {
        LOGGER.info("INFO-MS-ADMIN: Get identification type in GetListIdentificationTypeImplService");
        List<IdentificationTypeEntity> data = getListIdentificationTypeRepository.getListIdentificationType();
        validateList(data);
        return ResponseEntity.ok(new ListIdentificationTypeResponse(EResponse.GET_LIST_SUCCESSFULLY.getCode(),
                EResponse.GET_LIST_SUCCESSFULLY.getMessage(), IdentificationTypeMapper.convertListEntityToModel(data),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateList(List<IdentificationTypeEntity> data){
        LOGGER.info("INFO-MS-ADMIN: Validate Data List");
        if (data.isEmpty()){
            LOGGER.error("ERROR-MS-ADMIN: Error for data empty for get identification type");
            throw new ListIdentificationTypeException(EResponse.EMPTY_LIST_IDENTIFICATION_TYPE.getMessage(), EResponse.EMPTY_LIST_IDENTIFICATION_TYPE.getCode());
        }

    }
}
