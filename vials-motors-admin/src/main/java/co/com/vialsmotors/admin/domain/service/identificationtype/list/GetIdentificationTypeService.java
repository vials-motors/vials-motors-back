package co.com.vialsmotors.admin.domain.service.identificationtype.list;

import co.com.vialsmotors.admin.domain.response.ListIdentificationTypeResponse;
import org.springframework.http.ResponseEntity;

public interface GetIdentificationTypeService {

    ResponseEntity<ListIdentificationTypeResponse> getIdentificationType();
}
