package co.com.vialsmotors.admin.domain.service.identificationtype.save;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.port.manager.SaveIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.mapper.IdentificationTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.TimeZone;

public class SaveIdentificationTypeImplService implements SaveIdentificationTypeService{

    private static final Logger LOGGER = Logger.getLogger(SaveIdentificationTypeImplService.class);
    private final SaveIdentificationTypeRepository saveIdentificationTypeRepository;

    public SaveIdentificationTypeImplService(SaveIdentificationTypeRepository saveIdentificationTypeRepository) {
        this.saveIdentificationTypeRepository = saveIdentificationTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveIdentificationType(IdentificationType identificationType) {
        LOGGER.info("INFO-MS-ADMIN: Save identification type in service");
        this.saveIdentificationTypeRepository.saveIdentificationType(IdentificationTypeMapper.convertModelToEntity(identificationType));
        return ResponseEntity.ok(new GenericResponse(EResponse.REGISTER_IDENTIFICATION_TYPE_SUCCESS.getCode(),
                EResponse.REGISTER_IDENTIFICATION_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }
}
