package co.com.vialsmotors.admin.domain.service.identificationtype.save;

import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveIdentificationTypeService {

    ResponseEntity<GenericResponse> saveIdentificationType(IdentificationType identificationType);
}
