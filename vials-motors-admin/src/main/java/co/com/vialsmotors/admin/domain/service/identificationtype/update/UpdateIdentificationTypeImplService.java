package co.com.vialsmotors.admin.domain.service.identificationtype.update;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ListIdentificationTypeException;
import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.port.manager.GetIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.port.manager.SaveIdentificationTypeRepository;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateIdentificationTypeImplService implements UpdateIdentificationTypeService{

    private static final Logger LOGGER = Logger.getLogger(UpdateIdentificationTypeImplService.class);
    private final SaveIdentificationTypeRepository saveIdentificationTypeRepository;
    private final GetIdentificationTypeRepository getIdentificationTypeRepository;

    public UpdateIdentificationTypeImplService(SaveIdentificationTypeRepository saveIdentificationTypeRepository, GetIdentificationTypeRepository getIdentificationTypeRepository) {
        this.saveIdentificationTypeRepository = saveIdentificationTypeRepository;
        this.getIdentificationTypeRepository = getIdentificationTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateIdentificationType(IdentificationType newIdentificationType, Long id) {
        LOGGER.info("INFO-MS-ADMIN: Update identification type with id: " + id);
        this.validateId(id);
        this.saveIdentificationTypeRepository.saveIdentificationType(buildIdentificationType(newIdentificationType, id));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_IDENTIFICATION_TYPE_SUCCESS.getCode(),
                EResponse.UPDATE_IDENTIFICATION_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null ){
            LOGGER.error("ERROR-MS-ADMIN: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_IDENTIFICATION_TYPE_ERROR.getMessage(), EResponse.FIELD_ID_IDENTIFICATION_TYPE_ERROR.getMessage());
        }
    }

    private IdentificationTypeEntity getIdentificationType(Long id){
        Optional<IdentificationTypeEntity> identificationTypeEntity = this.getIdentificationTypeRepository.getIdentificationType(id);
        if (identificationTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-ADMIN: Error because identification type not exist");
            throw new ListIdentificationTypeException(EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getMessage(), EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getCode());
        }
        return identificationTypeEntity.get();
    }

    private IdentificationTypeEntity buildIdentificationType(IdentificationType identificationType, Long id){
        return getIdentificationType(id).withDescription(identificationType.description());
    }
}
