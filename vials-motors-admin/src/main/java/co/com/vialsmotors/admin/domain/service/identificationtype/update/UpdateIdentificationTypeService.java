package co.com.vialsmotors.admin.domain.service.identificationtype.update;

import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateIdentificationTypeService {

    ResponseEntity<GenericResponse> updateIdentificationType(IdentificationType newIdentificationType, Long id);
}
