package co.com.vialsmotors.admin.domain.service.list;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ListManagerException;
import co.com.vialsmotors.admin.domain.port.manager.GetListManagerRepository;
import co.com.vialsmotors.admin.domain.response.ListGenericResponse;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.mapper.ManagerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListManagerImplService implements GetListManagerService {

    private static final Logger LOGGER = Logger.getLogger(GetListManagerImplService.class);

    private final GetListManagerRepository getListManagerRepository;

    public GetListManagerImplService(GetListManagerRepository getListManagerRepository) {
        this.getListManagerRepository = getListManagerRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse> getListManager() {
        LOGGER.info("INFO-MS-ADMIN: Get list in getListManager for class " + GetListManagerImplService.class.getName());
        List<ManagerEntity> managerEntityList = getListManagerRepository.getListManager();
        validateListEmpty(managerEntityList);
        return ResponseEntity.ok(new ListGenericResponse(EResponse.GET_LIST_SUCCESSFULLY.getCode(), EResponse.GET_LIST_SUCCESSFULLY.getMessage(),
                ManagerMapper.convertListEntityInDto(managerEntityList),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));

    }

    private void validateListEmpty(List<ManagerEntity> entityList) {
        if (entityList.isEmpty()) {
            LOGGER.info("INFO-MS-ADMIN: The entity list is empty in method validateListEmpty for class " + GetListManagerImplService.class.getName());
            throw new ListManagerException(EResponse.EMPTY_LIST_MANAGER.getMessage(), EResponse.EMPTY_LIST_MANAGER.getCode());
        }
    }
}
