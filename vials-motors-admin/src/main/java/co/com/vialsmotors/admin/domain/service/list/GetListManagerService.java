package co.com.vialsmotors.admin.domain.service.list;

import co.com.vialsmotors.admin.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListManagerService {

    ResponseEntity<ListGenericResponse> getListManager();
}
