package co.com.vialsmotors.admin.domain.service.register;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.ManagerException;
import co.com.vialsmotors.admin.domain.model.Manager;
import co.com.vialsmotors.admin.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.admin.domain.port.manager.GetManagerByIdentificationNumberOrEmailAndStatusRepository;
import co.com.vialsmotors.admin.domain.port.manager.RegisterManagerRepository;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.apirest.dto.CredentialDTO;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.mapper.ManagerMapper;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class RegisterManagerImplService implements RegisterManagerService{

    private static final Integer NUMBER_FOR_USER_TYPE = 1;
    private static final Boolean STATUS_FOR_MANAGER_ACTIVE = true;
    private static final Boolean STATUS_FOR_MANAGER_INACTIVE = false;
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final RegisterManagerRepository registerManagerRepository;
    private final RegisterCredentialPort registerCredentialPort;
    private final GetManagerByIdentificationNumberOrEmailAndStatusRepository getManagerByIdentificationNumberOrEmailAndStatusRepository;

    public RegisterManagerImplService(RegisterManagerRepository registerManagerRepository, RegisterCredentialPort registerCredentialPort, GetManagerByIdentificationNumberOrEmailAndStatusRepository getManagerByIdentificationNumberOrEmailAndStatusRepository) {
        this.registerManagerRepository = registerManagerRepository;
        this.registerCredentialPort = registerCredentialPort;
        this.getManagerByIdentificationNumberOrEmailAndStatusRepository = getManagerByIdentificationNumberOrEmailAndStatusRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> registerManager(Manager manager) {
        this.validateIdentificationNumberRepository(manager.identificationNumber());
        this.validateEmailRepository(manager.email());
        ManagerEntity managerEntity = ManagerMapper.convertModelToEntity(manager, STATUS_FOR_MANAGER_ACTIVE);
        this.validateIdentificationNumberAndRegister(managerEntity, manager);
        return ResponseEntity.ok(new GenericResponse(EResponse.REGISTER_MANAGER_SUCCESSFULLY.getCode(),
                EResponse.REGISTER_MANAGER_SUCCESSFULLY.getMessage(),
                        TIMESTAMP_RESPONSE));
    }

    private CredentialDTO buildCredentialDto(Manager manager, String customerID){
        return new CredentialDTO(manager.email(),
                manager.password(), customerID);
    }

    private void validateIdentificationNumberAndRegister(ManagerEntity newManagerEntity, Manager manager){
        Optional<ManagerEntity> managerEntity = registerManagerRepository.findByIdentificationNumberAndStatusAndEmail(manager.identificationNumber(), STATUS_FOR_MANAGER_ACTIVE, manager.email());
        Optional<ManagerEntity> managerEntityInactive = registerManagerRepository.findByIdentificationNumberAndStatusAndEmail(manager.identificationNumber(), STATUS_FOR_MANAGER_INACTIVE, manager.email());
        if (managerEntity.isPresent()){
            throw new ManagerException(EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
        } else if (managerEntityInactive.isPresent()) {
            callRegisterCredential(manager, managerEntityInactive.get().getCustomerID());
            registerManagerRepository.registerManager(buildManagerEntity(newManagerEntity, managerEntityInactive.get()));
        }else{
            callRegisterCredential(manager, newManagerEntity.getCustomerID());
            registerManagerRepository.registerManager(newManagerEntity);
        }
    }

    private void callRegisterCredential(Manager manager, String customerId){
        Boolean statusCall = registerCredentialPort.registerCredential(buildCredentialDto(manager, customerId), NUMBER_FOR_USER_TYPE);
        if (Boolean.FALSE.equals(statusCall)){
            throw new CallCredentialException(EResponse.MESSAGE_ERROR_REGISTER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
        }
    }

    private ManagerEntity buildManagerEntity(ManagerEntity newManagerEntity, ManagerEntity previusManagerEntity){
        return previusManagerEntity.withFirstName(newManagerEntity.getFirstName())
                .withSecondName(newManagerEntity.getSecondName())
                .withFirstLastName(newManagerEntity.getFirstLastName())
                .withSecondLastName(newManagerEntity.getSecondLastName())
                .withPhone(newManagerEntity.getPhone())
                .withIdentificationNumber(newManagerEntity.getIdentificationNumber())
                .withIdentificationType(newManagerEntity.getIdentificationType())
                .withEmail(newManagerEntity.getEmail())
                .withStatus(true);
    }

    private void validateIdentificationNumberRepository(String identificationNumber){
        Optional<ManagerEntity> managerEntity = this.getManagerByIdentificationNumberOrEmailAndStatusRepository.getManagerWithIdentificationNumber(identificationNumber);
        if (managerEntity.isPresent()){
            throw new ManagerException(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getCode());
        }
    }

    private void validateEmailRepository(String email){
        Optional<ManagerEntity> managerEntity = this.getManagerByIdentificationNumberOrEmailAndStatusRepository.getManagerWithEmail(email);
        if (managerEntity.isPresent()){
            throw new ManagerException(EResponse.EMAIL_ERROR.getMessage(), EResponse.EMAIL_ERROR.getCode());
        }
    }
}
