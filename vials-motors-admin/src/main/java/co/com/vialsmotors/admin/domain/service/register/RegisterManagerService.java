package co.com.vialsmotors.admin.domain.service.register;

import co.com.vialsmotors.admin.domain.model.Manager;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterManagerService {

    ResponseEntity<GenericResponse> registerManager(Manager manager);
}
