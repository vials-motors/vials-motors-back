package co.com.vialsmotors.admin.domain.service.update;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.admin.domain.exception.ManagerException;
import co.com.vialsmotors.admin.domain.port.manager.RegisterManagerRepository;
import co.com.vialsmotors.admin.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.admin.domain.request.UpdateManagerRequest;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateManagerImplService implements UpdateManagerService{

    private static final Logger LOGGER = Logger.getLogger(UpdateManagerImplService.class);
    private static final Boolean STATUS_FOR_MANAGER_ACTIVE = true;
    private final RegisterManagerRepository registerManagerRepository;
    private final UpdateCredentialPort updateCredentialPort;

    public UpdateManagerImplService(RegisterManagerRepository registerManagerRepository, UpdateCredentialPort updateCredentialPort) {
        this.registerManagerRepository = registerManagerRepository;
        this.updateCredentialPort = updateCredentialPort;
    }

    @Override
    public ResponseEntity<GenericResponse> updateManager(UpdateManagerRequest manager, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-MANAGER: Update manager in service with identification number: " + identificationNumber);
        this.validateIdentificationNumber(identificationNumber);
        registerManagerRepository.registerManager(buildNewManagerEntity(manager, identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_MANAGER_SUCCESS.getCode(), EResponse.UPDATE_MANAGER_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private ManagerEntity getManagerEntity(String identificationNumber, String email){
        Optional<ManagerEntity> managerEntity = registerManagerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_FOR_MANAGER_ACTIVE, email);
        return managerEntity.orElse(null);
    }

    private ManagerEntity buildNewManagerEntity(UpdateManagerRequest manager, String identificationNumber, String email){
        ManagerEntity managerEntity = getManagerEntity(identificationNumber, email);
        this.validateManagerEntity(managerEntity);
        this.validateAndCall(manager, managerEntity);
        return managerEntity.withFirstName(manager.getFirstName())
                .withSecondName(manager.getSecondName())
                .withFirstLastName(manager.getFirstLastName())
                .withSecondLastName(manager.getSecondLastName())
                .withPhone(manager.getPhone())
                .withEmail(manager.getEmail())
                .withIdentificationNumber(manager.getIdentificationNumber());
    }

    private void validateManagerEntity(ManagerEntity managerEntity){
        if (managerEntity == null){
            throw new ManagerException(EResponse.MANAGER_NOT_EXIST.getMessage(), EResponse.MANAGER_NOT_EXIST.getCode());
        }
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isEmpty()) {
            throw new IdentificationNumberException(EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getMessage(), EResponse.IDENTIFICATION_TYPE_NOT_EXIST.getCode());
        }
    }

    private Boolean callUpdateCredential(UpdateManagerRequest manager, String customerId){
        return updateCredentialPort.updateCredential(manager.getEmail(), customerId);
    }

    private Boolean validateCreateUsername(UpdateManagerRequest manager, ManagerEntity managerEntity){
        return !managerEntity.getEmail().equals(manager.getEmail());
    }

    private void validateAndCall(UpdateManagerRequest manager, ManagerEntity managerEntity){
        if (Boolean.TRUE.equals(validateCreateUsername(manager, managerEntity))){
            LOGGER.info("INFO-MS-MANAGER: Update credential with first name: " + manager.getFirstName());
            if (Boolean.FALSE.equals(callUpdateCredential(manager, managerEntity.getCustomerID()))){
                LOGGER.error("ERROR-MS-MANAGER: Error call update credential");
                throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL.getCode(), ECallMessage.ERROR_CALL_CREDENTIAL.getMessage());
            }
        }
    }
}
