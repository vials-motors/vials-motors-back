package co.com.vialsmotors.admin.domain.service.update;

import co.com.vialsmotors.admin.domain.request.UpdateManagerRequest;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateManagerService {

    ResponseEntity<GenericResponse> updateManager(UpdateManagerRequest manager, String identificationNumber, String email);
}
