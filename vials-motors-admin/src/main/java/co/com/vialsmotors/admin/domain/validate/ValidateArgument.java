package co.com.vialsmotors.admin.domain.validate;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.commons.domain.exception.IdentificationTypeException;

public class ValidateArgument {

    private static final String NOT_INSTANTIABLE = "The class ValidateArgument is not instantiable";

    private ValidateArgument (){
        throw new IllegalCallerException(NOT_INSTANTIABLE);
    }

    public static void validateStringField(String field, String message, String status){
        if (field == null || field.isEmpty())
            throw new FieldException(message, status);
    }

    public static void validateIdentificationTypeField(Object object, String message, String status){
        if (object == null)
            throw new IdentificationTypeException(message, status);
    }

    public static void validateLongField(Long field, String message, String status){
        if (field == null)
            throw new FieldException(message, status);
    }
}
