package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.DeleteIdentificationTypeRepository;
import co.com.vialsmotors.admin.infrastructure.jpa.IdentificationTypeSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteIdentificationTypeImplRepository implements DeleteIdentificationTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteIdentificationTypeImplRepository.class);
    private final IdentificationTypeSqlServerRepository identificationTypeSqlServerRepository;

    @Override
    public void deleteIdentificationType(Long id) {
        try {
            LOGGER.info("INFO-MS-ADMIN: Delete identification type with id: " + id);
            this.identificationTypeSqlServerRepository.deleteById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
