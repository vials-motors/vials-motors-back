package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.GetIdentificationTypeRepository;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.IdentificationTypeSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetIdentificationTypeImplRepository implements GetIdentificationTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetIdentificationTypeImplRepository.class);
    private final IdentificationTypeSqlServerRepository identificationTypeSqlServerRepository;

    @Override
    public Optional<IdentificationTypeEntity> getIdentificationType(Long id) {
        try {
            LOGGER.info("INFO-MS-ADMIN: Get identification type with id: " + id);
            return this.identificationTypeSqlServerRepository.findById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
