package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.GetListIdentificationTypeRepository;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.IdentificationTypeSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListIdentificationTypeImplRepository implements GetListIdentificationTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListIdentificationTypeImplRepository.class);
    private final IdentificationTypeSqlServerRepository identificationTypeSqlServerRepository;

    @Override
    public List<IdentificationTypeEntity> getListIdentificationType() {
        try {
            LOGGER.info("INFO-MS-ADMIN: Get list identifier type");
            return identificationTypeSqlServerRepository.findAll();
        }catch (Exception e) {
            LOGGER.error("ERROR-MS-ADMIN: Error get list identification type with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
