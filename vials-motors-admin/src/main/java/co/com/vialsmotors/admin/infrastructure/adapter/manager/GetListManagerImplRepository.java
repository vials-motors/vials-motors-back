package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.port.manager.GetListManagerRepository;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.ManagerSqlServerRepository;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListManagerImplRepository implements GetListManagerRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListManagerImplRepository.class);

    private final ManagerSqlServerRepository managerSqlServerRepository;

    @Override
    public List<ManagerEntity> getListManager() {
        try {
            return managerSqlServerRepository.findAll();
        }catch (Exception exception){
            LOGGER.error("ERR-MS-ADMIN: Error in " + GetListManagerImplRepository.class.getName() + " in method getListManager with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(EResponse.ERROR_LIST_MANAGER.getMessage(), EResponse.ERROR_LIST_MANAGER.getCode());
        }
    }
}
