package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.GetManagerByCustomerIdRepository;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.ManagerSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetManagerByCustomerIdImplRepository implements GetManagerByCustomerIdRepository {

    private static final Logger LOGGER = Logger.getLogger(GetManagerByCustomerIdImplRepository.class);
    private final ManagerSqlServerRepository managerSqlServerRepository;

    @Override
    public Optional<ManagerEntity> getManager(String customerId) {
        try{
            LOGGER.info("INFO-MS-ADMIN: Get manager with customer id: " + customerId);
            return managerSqlServerRepository.findByCustomerId(customerId);
        }catch (Exception exception){
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
