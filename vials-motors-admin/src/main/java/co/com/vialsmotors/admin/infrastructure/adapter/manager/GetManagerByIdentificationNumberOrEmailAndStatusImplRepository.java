package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.GetManagerByIdentificationNumberOrEmailAndStatusRepository;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.ManagerSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetManagerByIdentificationNumberOrEmailAndStatusImplRepository implements GetManagerByIdentificationNumberOrEmailAndStatusRepository {

    private static final Logger LOGGER = Logger.getLogger(GetManagerByIdentificationNumberOrEmailAndStatusImplRepository.class);
    private static final Boolean STATUS = Boolean.TRUE;
    private final ManagerSqlServerRepository managerSqlServerRepository;

    @Override
    public Optional<ManagerEntity> getManagerWithIdentificationNumber(String identificationNumber) {
        try {
            LOGGER.info("INFO-MS-ADMIN: Get admin with identification number: " + identificationNumber);
            return managerSqlServerRepository.findByIdentificationNumberAndStatus(identificationNumber, STATUS);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<ManagerEntity> getManagerWithEmail(String email) {
        try {
            LOGGER.info("INFO-MS-ADMIN: Get admin with email: " + email);
            return managerSqlServerRepository.findByEmailAndStatus(email, STATUS);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
