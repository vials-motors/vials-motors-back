package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.RegisterManagerRepository;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.ManagerSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class RegisterManagerImplRepository implements RegisterManagerRepository {

    private final ManagerSqlServerRepository managerSqlServerRepository;

    @Override
    public void registerManager(ManagerEntity managerEntity) {
        try {
            managerSqlServerRepository.save(managerEntity);
        }catch (Exception exception){
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<ManagerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email) {
        return managerSqlServerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, status, email);
    }
}
