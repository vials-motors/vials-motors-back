package co.com.vialsmotors.admin.infrastructure.adapter.manager;

import co.com.vialsmotors.admin.domain.port.manager.SaveIdentificationTypeRepository;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import co.com.vialsmotors.admin.infrastructure.jpa.IdentificationTypeSqlServerRepository;
import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SaveIdentificationTypeImplRepository implements SaveIdentificationTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveIdentificationTypeImplRepository.class);
    private final IdentificationTypeSqlServerRepository identificationTypeSqlServerRepository;

    @Override
    public void saveIdentificationType(IdentificationTypeEntity identificationType) {
        try {
            this.identificationTypeSqlServerRepository.save(identificationType);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
