package co.com.vialsmotors.admin.infrastructure.apirest.client;

import co.com.vialsmotors.admin.infrastructure.apirest.dto.CredentialDTO;
import co.com.vialsmotors.admin.infrastructure.apirest.dto.FeignResponse;
import co.com.vialsmotors.admin.infrastructure.apirest.response.GetCredentialResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "feignClientCredential", url = "${external.service.credential}")
public interface CredentialService {

    @PostMapping("/login/register-credential")
    ResponseEntity<FeignResponse> registerCredential(@RequestHeader Map<String, Object> headers, @RequestBody CredentialDTO credentialDTO);

    @GetMapping("/login/get-credential")
    ResponseEntity<GetCredentialResponse> getCredential(@RequestHeader Map<String, Object> headers);

    @PutMapping("/login/update")
    ResponseEntity<FeignResponse> updateCredential(@RequestHeader Map<String, Object> headers);

    @DeleteMapping("/login/delete")
    ResponseEntity<FeignResponse> deleteCredential(@RequestHeader Map<String, Object> headers);
}
