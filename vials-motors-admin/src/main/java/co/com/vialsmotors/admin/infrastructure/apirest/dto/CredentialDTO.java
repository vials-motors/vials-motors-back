package co.com.vialsmotors.admin.infrastructure.apirest.dto;

public record CredentialDTO(String email, String password,  String customerID) {
}
