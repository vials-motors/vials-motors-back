package co.com.vialsmotors.admin.infrastructure.apirest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeignResponse {

    private String code;
    private String message;
    private ZonedDateTime timestamp;
}
