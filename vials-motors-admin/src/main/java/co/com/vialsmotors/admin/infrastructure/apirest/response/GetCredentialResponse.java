package co.com.vialsmotors.admin.infrastructure.apirest.response;

import co.com.vialsmotors.commons.domain.model.RoleEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class GetCredentialResponse {

    private String username;
    private String email;
    private Set<RoleEntity> roles;
}

