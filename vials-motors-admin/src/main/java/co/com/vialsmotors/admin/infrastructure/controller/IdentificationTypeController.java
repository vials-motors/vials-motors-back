package co.com.vialsmotors.admin.infrastructure.controller;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.application.handler.identificationtype.delete.DeleteIdentificationTypeHandler;
import co.com.vialsmotors.admin.application.handler.identificationtype.list.ListIdentificationTypeHandler;
import co.com.vialsmotors.admin.application.handler.identificationtype.save.SaveIdentificationTypeHandler;
import co.com.vialsmotors.admin.application.handler.identificationtype.update.UpdateIdentificationTypeHandler;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.response.ListIdentificationTypeResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/identification-type")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class IdentificationTypeController {

    private static final Logger LOGGER = Logger.getLogger(IdentificationTypeController.class);

    private final ListIdentificationTypeHandler listIdentificationTypeHandler;
    private final SaveIdentificationTypeHandler saveIdentificationTypeHandler;
    private final UpdateIdentificationTypeHandler updateIdentificationTypeHandler;
    private final DeleteIdentificationTypeHandler deleteIdentificationTypeHandler;
    private final ObjectMapper objectMapper;

    @GetMapping
    public ResponseEntity<ListIdentificationTypeResponse> getIdentificationType(){
        LOGGER.info("INFO-MS-ADMIN: Request received for get list identification type in IdentificationTypeController");
        return listIdentificationTypeHandler.getIdentificationType();
    }

    @PostMapping
    public ResponseEntity<GenericResponse> saveIdentificationType(@RequestBody IdentificationTypeCommand identificationTypeCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-ADMIN: Receiver request for save identification type with body: " + objectMapper.writeValueAsString(identificationTypeCommand));
        return saveIdentificationTypeHandler.saveIdentificationType(identificationTypeCommand);
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateIdentificationType(@RequestBody IdentificationTypeCommand identificationTypeCommand,
                                                                    @RequestParam("id") Long id) throws JsonProcessingException {
        LOGGER.info("INFO-MS-ADMIN: Receiver request for update identification type with body: " + objectMapper.writeValueAsString(identificationTypeCommand));
        LOGGER.info("INFO-MS-ADMIN: Receiver request for update identification type with id: " + id);
        return updateIdentificationTypeHandler.updateIdentificationType(identificationTypeCommand, id);
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse> deleteIdentificationType(@RequestParam("id") Long id){
        LOGGER.info("INFO-MS-ADMIN: Receiver request for delete identification type with id: " + id);
        return deleteIdentificationTypeHandler.deleteIdentificationType(id);
    }
}
