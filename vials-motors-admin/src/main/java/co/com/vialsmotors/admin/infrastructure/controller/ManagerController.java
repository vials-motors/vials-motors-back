package co.com.vialsmotors.admin.infrastructure.controller;

import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.application.handler.delete.DeleteManagerHandler;
import co.com.vialsmotors.admin.application.handler.get.GetManagerByCustomerIdHandler;
import co.com.vialsmotors.admin.application.handler.list.GetListManagerHandler;
import co.com.vialsmotors.admin.application.handler.register.RegisterManagerHandler;
import co.com.vialsmotors.admin.application.handler.update.UpdateManagerHandler;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.admin.domain.response.ListGenericResponse;
import co.com.vialsmotors.admin.domain.response.ManagerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manager")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class ManagerController {

    private static final Logger LOGGER = Logger.getLogger(ManagerController.class);

    private final RegisterManagerHandler registerManagerHandler;
    private final GetListManagerHandler getListManagerHandler;
    private final UpdateManagerHandler updateManagerHandler;
    private final DeleteManagerHandler deleteManagerHandler;
    private final GetManagerByCustomerIdHandler getManagerByCustomerIdHandler;
    private final ObjectMapper objectMapper;

    @PostMapping
    public ResponseEntity<GenericResponse> addManager(@RequestBody ManagerCommand managerCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-ADMIN: Received request for add manager with body " + objectMapper.writeValueAsString(managerCommand));
        return registerManagerHandler.registerManager(managerCommand);
    }

    @GetMapping
    public ResponseEntity<ListGenericResponse> listManager(){
        LOGGER.info("INFO-MS-ADMIN: Received request for get list Manager");
        return getListManagerHandler.getListManager();
    }

    @GetMapping(path = "/get-manager")
    public ResponseEntity<ManagerResponse> getManagerByCustomerID(@RequestParam("customerId") String customerId){
        LOGGER.info("INFO-MS-ADMIN: Receiver request for get manager with customer id: " +  customerId);
        return getManagerByCustomerIdHandler.getManager(customerId);
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateManager(@RequestBody ManagerCommand managerCommand,
                                                         @RequestParam("identificationNumber") String identificationNumber,
                                                         @RequestParam("email") String email) throws JsonProcessingException {
        LOGGER.info("INFO-MS-ADMIN: Received request for update manager with body: " + objectMapper.writeValueAsString(managerCommand));
        LOGGER.info("INFO-MS-ADMIN: Received request for update manager with identifier: " + identificationNumber);
        return updateManagerHandler.updateManager(managerCommand, identificationNumber, email);
    }

    @PutMapping("/delete")
    public ResponseEntity<GenericResponse> deleteManager(@RequestParam("identificationNumber") String identificationNumber, @RequestParam("email") String email){
        LOGGER.info("INFO-MS-ADMIN: Receiver request for delete manager with identification Number: " + identificationNumber);
        return deleteManagerHandler.deleteManager(identificationNumber, email);
    }
}
