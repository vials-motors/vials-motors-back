package co.com.vialsmotors.admin.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "identification_type", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id_identification_type")
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IdentificationTypeEntity {

    @Id
    @Column(name = "id_identification_type")
    private Long idIdentificationType;

    @Column(nullable = false)
    private String description;

    public IdentificationTypeEntity withDescription(String description){
        this.description = description;
        return this;
    }
}
