package co.com.vialsmotors.admin.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "manager", uniqueConstraints = {
        @UniqueConstraint(columnNames = "customer_ID"),
        @UniqueConstraint(columnNames = "identification_number")
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ManagerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_manager")
    private Long idCustomer;

    @Column(name = "firs_name", nullable = false, length = 250)
    private String firstName;

    @Column(name = "second_name", length = 250)
    private String secondName;

    @Column(name = "first_last_name", nullable = false, length = 250)
    private String firstLastName;

    @Column(name = "second_last_name", length = 250)
    private String secondLastName;

    @Column(length = 100, nullable = false)
    private String phone;

    @Column(name = "identification_number", nullable = false, unique = true, length = 250)
    private String identificationNumber;

    @Column(name = "customer_ID", nullable = false, unique = true, length = 250)
    private String customerID;

    @ManyToOne
    @JoinColumn(name = "id_identification_type")
    private IdentificationTypeEntity identificationType;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "status", nullable = false)
    private Boolean status;

    public ManagerEntity(String firstName, String secondName, String firstLastName, String secondLastName, String phone, String identificationNumber, String customerID, IdentificationTypeEntity identificationType, String email, Boolean status) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.phone = phone;
        this.identificationNumber = identificationNumber;
        this.customerID = customerID;
        this.identificationType = identificationType;
        this.email = email;
        this.status =  status;
    }

    public ManagerEntity withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ManagerEntity withSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public ManagerEntity withFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
        return this;
    }

    public ManagerEntity withSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
        return this;
    }

    public ManagerEntity withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public ManagerEntity withIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
        return this;
    }

    public ManagerEntity withEmail(String email){
        this.email = email;
        return this;
    }

    public ManagerEntity withIdentificationType(IdentificationTypeEntity identificationType){
        this.identificationType = identificationType;
        return this;
    }

    public ManagerEntity withStatus(Boolean status){
        this.status = status;
        return this;
    }
}
