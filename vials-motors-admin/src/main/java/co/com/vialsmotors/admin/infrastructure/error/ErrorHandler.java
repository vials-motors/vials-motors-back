package co.com.vialsmotors.admin.infrastructure.error;

import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.admin.domain.exception.ListIdentificationTypeException;
import co.com.vialsmotors.admin.domain.exception.ListManagerException;
import co.com.vialsmotors.admin.domain.exception.ManagerException;
import co.com.vialsmotors.admin.domain.response.GenericResponse;
import co.com.vialsmotors.commons.domain.exception.*;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = Logger.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: ";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler() {
        CODE_STATE.put(FieldException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(DecodedErrorTokenException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CallCredentialException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(GetUserNameTokenException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(IdentificationTypeException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(SqlServerDataBaseException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(ListManagerException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ManagerException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(IdentificationNumberException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(ListIdentificationTypeException.class.getSimpleName(), HttpStatus.OK.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleAllException(Exception exception){
        String nameException = exception.getClass().getSimpleName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof FieldException fieldException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(fieldException.getStatus(), fieldException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof DecodedErrorTokenException decodedErrorTokenException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(decodedErrorTokenException.getStatus(), decodedErrorTokenException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof CallCredentialException callCredentialException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(callCredentialException.getStatus(), callCredentialException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GetUserNameTokenException getUserNameTokenException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(getUserNameTokenException.getStatus(), getUserNameTokenException.getMessage(), TIMESTAMP_RESPONSE));
        } else if(code != null && exception instanceof IdentificationTypeException identificationTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(identificationTypeException.getStatus(), identificationTypeException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof SqlServerDataBaseException sqlServerDataBaseException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sqlServerDataBaseException.getStatus(), sqlServerDataBaseException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof ListManagerException listManagerException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(listManagerException.getStatus(), listManagerException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ManagerException managerException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(managerException.getStatus(), managerException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof IdentificationNumberException identificationNumberException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(identificationNumberException.getStatus(), identificationNumberException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ListIdentificationTypeException listIdentificationTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(listIdentificationTypeException.getStatus(), listIdentificationTypeException.getMessage(), TIMESTAMP_RESPONSE));
        }else {
            LOGGER.info(MESSAGE_ERROR_GENERIC + exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_ERROR.getCode(), EResponse.INTERNAL_ERROR.getMessage(), TIMESTAMP_RESPONSE));
        }
    }
}
