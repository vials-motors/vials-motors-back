package co.com.vialsmotors.admin.infrastructure.jpa;

import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentificationTypeSqlServerRepository extends JpaRepository<IdentificationTypeEntity, Long> {
}
