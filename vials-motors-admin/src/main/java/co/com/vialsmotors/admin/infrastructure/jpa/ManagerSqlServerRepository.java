package co.com.vialsmotors.admin.infrastructure.jpa;

import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ManagerSqlServerRepository extends JpaRepository<ManagerEntity, Long> {

    @Query(value = "SELECT * FROM manager WHERE identification_number = ?1 AND status = ?2 AND email =?3", nativeQuery = true)
    Optional<ManagerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email);

    @Query(value = "SELECT * FROM manager WHERE customer_ID =?1", nativeQuery = true)
    Optional<ManagerEntity> findByCustomerId(String customerId);

    @Query(value = "SELECT * FROM manager WHERE identification_number =?1 AND status =?2", nativeQuery = true)
    Optional<ManagerEntity> findByIdentificationNumberAndStatus(String identificationNumber, Boolean status);

    @Query(value = "SELECT * FROM manager WHERE email =?1 AND status =?2", nativeQuery = true)
    Optional<ManagerEntity> findByEmailAndStatus(String email, Boolean status);

}
