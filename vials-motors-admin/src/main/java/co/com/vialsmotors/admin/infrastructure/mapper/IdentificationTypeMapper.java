package co.com.vialsmotors.admin.infrastructure.mapper;


import co.com.vialsmotors.admin.domain.dto.IdentificationTypeDTO;
import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.infrastructure.entity.IdentificationTypeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class IdentificationTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class " + IdentificationTypeMapper.class.getName() + " is not instantiable";

    private IdentificationTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static IdentificationTypeEntity convertModelToEntity(IdentificationType identificationType){
        return new IdentificationTypeEntity(identificationType.idIdentificationType(), identificationType.description());
    }

    public static List<IdentificationTypeDTO> convertListEntityToModel(List<IdentificationTypeEntity> data){
        return data.stream()
                .map(dataTemporal -> new IdentificationTypeDTO(dataTemporal.getIdIdentificationType(),
                    dataTemporal.getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
