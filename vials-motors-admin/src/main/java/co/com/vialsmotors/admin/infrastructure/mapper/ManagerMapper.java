package co.com.vialsmotors.admin.infrastructure.mapper;

import co.com.vialsmotors.admin.domain.dto.ManagerDTO;
import co.com.vialsmotors.admin.domain.model.Manager;
import co.com.vialsmotors.admin.infrastructure.entity.ManagerEntity;
import co.com.vialsmotors.admin.infrastructure.utils.CreateCustomerIdUtils;
import co.com.vialsmotors.admin.infrastructure.utils.ValidateStatusUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ManagerMapper {

    private static final String NOT_INSTANTIABLE = "The class ManagerMapper is not instantiable";

    private ManagerMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ManagerEntity convertModelToEntity(Manager manager, Boolean status){
        return new ManagerEntity(manager.firstName(), manager.secondName(), manager.firstLastName(), manager.secondLastName(),
                manager.phone(), manager.identificationNumber(),
                CreateCustomerIdUtils.generateAlphanumericString(),
                IdentificationTypeMapper.convertModelToEntity(manager.identificationType()), manager.email(), status);
    }

    public static List<ManagerDTO> convertListEntityInDto(List<ManagerEntity> entityList){
        return entityList.stream()
                .map(entityTemporal -> new ManagerDTO(entityTemporal.getFirstName(),
                        entityTemporal.getSecondName(), entityTemporal.getFirstLastName(),
                        entityTemporal.getSecondLastName(), entityTemporal.getPhone(),
                        entityTemporal.getIdentificationNumber(), entityTemporal.getEmail(),
                        ValidateStatusUtils.validateStatus(entityTemporal.getStatus()),
                        entityTemporal.getCustomerID()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static ManagerDTO convertEntityToDto(ManagerEntity managerEntity){
        return new ManagerDTO(managerEntity.getFirstName(), managerEntity.getSecondName(),
                managerEntity.getFirstLastName(), managerEntity.getSecondLastName(),
                managerEntity.getPhone(), managerEntity.getIdentificationNumber(),
                managerEntity.getEmail(), ValidateStatusUtils.validateStatus(managerEntity.getStatus()),
                managerEntity.getCustomerID());
    }
}
