package co.com.vialsmotors.admin.infrastructure.security;

import co.com.vialsmotors.admin.infrastructure.security.jwt.AuthEntryPointJwt;
import co.com.vialsmotors.admin.infrastructure.security.jwt.AuthTokenFilter;
import co.com.vialsmotors.admin.infrastructure.security.jwt.JwtUtils;
import co.com.vialsmotors.admin.infrastructure.security.userdetails.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity()
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig {

    private static final String MANAGER = "/manager";
    public static final String IDENTIFICATION_TYPE = "/identification-type";
    public static final String ADMIN = "ADMIN";
    private final AuthEntryPointJwt unauthorizedHandler;
    private final JwtUtils jwtUtils;
    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter(){
        return new AuthTokenFilter(jwtUtils, userDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers(HttpMethod.POST, MANAGER).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.GET, MANAGER).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.PUT, MANAGER).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.PUT, "/manager/delete").hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.GET, "/manager/get-manager").permitAll()
                .antMatchers(HttpMethod.GET, IDENTIFICATION_TYPE).permitAll()
                .antMatchers(HttpMethod.POST, IDENTIFICATION_TYPE).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.PUT, IDENTIFICATION_TYPE).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, IDENTIFICATION_TYPE).hasAnyRole(ADMIN)
                .anyRequest().authenticated().and()
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class).build();
    }
}
