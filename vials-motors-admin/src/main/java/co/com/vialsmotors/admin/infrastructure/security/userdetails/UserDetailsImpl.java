package co.com.vialsmotors.admin.infrastructure.security.userdetails;

import co.com.vialsmotors.admin.infrastructure.apirest.response.GetCredentialResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private String username;
    private String email;
    private  Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl( String username, String email, Collection<? extends GrantedAuthority> authorities) {

        this.username = username;
        this.email = email;
        this.authorities = authorities;
    }

   public static UserDetailsImpl build(GetCredentialResponse getCredentialResponse){
        List<GrantedAuthority> authorities = getCredentialResponse.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
        return new UserDetailsImpl(
                getCredentialResponse.getUsername(),
                getCredentialResponse.getEmail(),
                authorities);
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }
    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) obj;
        return Objects.equals(username, user.username);

    }

}
