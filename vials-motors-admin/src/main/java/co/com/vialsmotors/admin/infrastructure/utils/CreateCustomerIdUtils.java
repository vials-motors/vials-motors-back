package co.com.vialsmotors.admin.infrastructure.utils;

import java.security.SecureRandom;

public class CreateCustomerIdUtils {

    private static final String ALPHANUMERIC_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final String NOT_INSTANTIABLE = "The class is not instantiable";
    private static final Integer STRING_LENGTH = 10;
    private static final String INITIAL_STRING = "ADMIN-";

    private CreateCustomerIdUtils() {
        throw new IllegalStateException(NOT_INSTANTIABLE);
    }

    public static String generateAlphanumericString() {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder(STRING_LENGTH);

        for (int i = 0; i < STRING_LENGTH; i++) {
            int randomIndex = random.nextInt(ALPHANUMERIC_CHARS.length());
            char randomChar = ALPHANUMERIC_CHARS.charAt(randomIndex);
            sb.append(randomChar);
        }

        return INITIAL_STRING + sb.toString();
    }
}
