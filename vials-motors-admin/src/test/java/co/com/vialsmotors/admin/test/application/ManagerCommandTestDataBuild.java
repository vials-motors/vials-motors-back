package co.com.vialsmotors.admin.test.application;

import co.com.vialsmotors.admin.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.admin.application.command.ManagerCommand;

public class ManagerCommandTestDataBuild {

    private static final String FIRST_NAME = "Maria";
    private static final String SECOND_NAME = "Alejandra";
    private static final String FIRST_LAST_NAME = "Agudelo";
    private static final String SECOND_LAST_NAME = "Mejia";
    private static final String PHONE = "11111111";
    private static final String IDENTIFICATION_NUMBER = "11111111";
    private static final IdentificationTypeCommand IDENTIFICATION_TYPE = new IdentificationTypeCommand(1L, "CC");
    private static final String PASSWORD = "sebas123";
    private static final String EMAIL = "maria@gmail.com";

    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    private String phone;
    private String identificationNumber;
    private IdentificationTypeCommand identificationType;
    private String password;
    private String email;

    public ManagerCommandTestDataBuild(){
        this.firstName = FIRST_NAME;
        this.secondName = SECOND_NAME;
        this.firstLastName = FIRST_LAST_NAME;
        this.secondLastName = SECOND_LAST_NAME;
        this.phone = PHONE;
        this.identificationNumber = IDENTIFICATION_NUMBER;
        this.identificationType = IDENTIFICATION_TYPE;
        this.password = PASSWORD;
        this.email = EMAIL;
    }

    public ManagerCommandTestDataBuild withIdentificationNumber(String identificationNumber){
        this.identificationNumber = identificationNumber;
        return this;
    }

    public ManagerCommandTestDataBuild withEmail(String email){
        this.email = email;
        return this;
    }

    public ManagerCommand build(){
        return new ManagerCommand(this.firstName, this.secondName, this.firstLastName, this.secondLastName, this.phone, this.identificationNumber,
                this.identificationType, this.password, this.email);
    }
}
