package co.com.vialsmotors.admin.test.domain.databuilder;

import co.com.vialsmotors.admin.domain.model.IdentificationType;
import co.com.vialsmotors.admin.domain.model.Manager;

public class ManagerTestDataBuild {

    private static final String FIRST_NAME = "Juan";
    private static final String SECOND_NAME = "Sebastian";
    private static final String FIRST_LAST_NAME = "Agudelo";
    private static final String SECOND_LAST_NAME = "Mejia";
    private static final String PHONE = "123456789";
    private static final String IDENTIFICATION_NUMBER = "123456789";
    private static final IdentificationType IDENTIFICATION_TYPE = new IdentificationType(1L, "CC");
    private static final String PASSWORD = "sebas123";
    private static final String EMAIL = "sebas@gmail.com";

    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    private String phone;
    private String identificationNumber;
    private IdentificationType identificationType;
    private String password;
    private String email;

    public ManagerTestDataBuild(){
        this.firstName = FIRST_NAME;
        this.secondName = SECOND_NAME;
        this.firstLastName = FIRST_LAST_NAME;
        this.secondLastName = SECOND_LAST_NAME;
        this.phone = PHONE;
        this.identificationNumber = IDENTIFICATION_NUMBER;
        this.identificationType = IDENTIFICATION_TYPE;
        this.password = PASSWORD;
        this.email = EMAIL;
    }

    public Manager build(){
        return new Manager(this.firstName, this.secondName, this.firstLastName,
                this.secondLastName, this.phone, this.identificationNumber, this.identificationType,
                this.password, this.email);
    }
}
