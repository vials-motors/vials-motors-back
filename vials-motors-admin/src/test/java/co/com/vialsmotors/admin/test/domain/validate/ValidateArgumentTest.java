package co.com.vialsmotors.admin.test.domain.validate;

import static org.junit.jupiter.api.Assertions.*;

import co.com.vialsmotors.admin.domain.validate.ValidateArgument;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.commons.domain.exception.IdentificationTypeException;
import org.junit.jupiter.api.Test;

class ValidateArgumentTest {

    @Test
    void validateStringFieldValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateStringField("Test", "message", "status"));
    }

    @Test
    void validateStringFieldInvalidTest(){
        FieldException exception = assertThrows(FieldException.class,
                () -> ValidateArgument.validateStringField(null, "message", "status"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }

    @Test
    void validateIdentificationTypeFieldValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateIdentificationTypeField(new Object(), "message", "status"));
    }

    @Test
    void validateIdentificationTypeFieldInvalidTest(){
        IdentificationTypeException exception = assertThrows(IdentificationTypeException.class,
                () -> ValidateArgument.validateIdentificationTypeField(null, "message", "status"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }

    @Test
    void validateLongFieldValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateLongField(1L, "Message", "status"));
    }

    @Test
    void validateLongFieldInvalidTest(){
        FieldException exception = assertThrows(FieldException.class,
                () -> ValidateArgument.validateLongField(null, "message", "status"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }
}
