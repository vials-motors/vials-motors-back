package co.com.vialsmotors.admin.test.infrastructure.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import co.com.vialsmotors.admin.VialsMotorsAdminApplication;
import co.com.vialsmotors.admin.application.command.ManagerCommand;
import co.com.vialsmotors.admin.application.handler.delete.DeleteManagerHandler;
import co.com.vialsmotors.admin.application.handler.get.GetManagerByCustomerIdHandler;
import co.com.vialsmotors.admin.application.handler.list.GetListManagerHandler;
import co.com.vialsmotors.admin.application.handler.register.RegisterManagerHandler;
import co.com.vialsmotors.admin.application.handler.update.UpdateManagerHandler;
import co.com.vialsmotors.admin.domain.enums.EResponse;
import co.com.vialsmotors.admin.domain.model.Manager;
import co.com.vialsmotors.admin.infrastructure.adapter.credential.DeleteCredentialAdapter;
import co.com.vialsmotors.admin.infrastructure.adapter.credential.RegisterCredentialAdapter;
import co.com.vialsmotors.admin.infrastructure.jpa.ManagerSqlServerRepository;
import co.com.vialsmotors.admin.infrastructure.mapper.ManagerMapper;
import co.com.vialsmotors.admin.test.application.ManagerCommandTestDataBuild;
import co.com.vialsmotors.admin.test.domain.databuilder.ManagerTestDataBuild;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

@SpringBootTest(classes = VialsMotorsAdminApplication.class)
@AutoConfigureMockMvc
@Transactional
class ManagerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private RegisterManagerHandler registerManagerHandler;
    @Autowired
    private ManagerSqlServerRepository managerSqlServerRepository;
    @MockBean
    private GetListManagerHandler getListManagerHandler;
    @MockBean
    private UpdateManagerHandler updateManagerHandler;
    @Autowired
    private DeleteManagerHandler deleteManagerHandler;
    @MockBean
    private GetManagerByCustomerIdHandler getManagerByCustomerIdHandler;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private RegisterCredentialAdapter registerCredentialAdapter;
    @MockBean
    private DeleteCredentialAdapter deleteCredentialAdapter;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        managerSqlServerRepository.save(ManagerMapper.convertModelToEntity(new ManagerTestDataBuild().build(), true));
    }

    @Test
    void registerManagerTest() throws Exception{
        ManagerCommand managerCommand = new ManagerCommandTestDataBuild().build();
        Mockito.when(registerCredentialAdapter.registerCredential(Mockito.any(), Mockito.any())).thenReturn(true);
        mockMvc.perform(post("http://localhost:9002/manager").content(objectMapper.writeValueAsString(managerCommand))
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is(EResponse.REGISTER_MANAGER_SUCCESSFULLY.getMessage())))
                .andExpect(jsonPath("$.code", is(EResponse.REGISTER_MANAGER_SUCCESSFULLY.getCode())));
    }

    @Test
    void registerManagerInactiveTest() throws Exception{
        Manager manager = new ManagerTestDataBuild().build();
        ManagerCommand managerCommand = new ManagerCommandTestDataBuild().withIdentificationNumber(manager.identificationNumber()).withEmail(manager.email()).build();
        Mockito.when(deleteCredentialAdapter.deleteCredential(Mockito.any())).thenReturn(true);
        deleteManagerHandler.deleteManager(manager.identificationNumber(), manager.email());
        Mockito.when(registerCredentialAdapter.registerCredential(Mockito.any(), Mockito.any())).thenReturn(true);

        mockMvc.perform(post("http://localhost:9002/manager").content(objectMapper.writeValueAsString(managerCommand))
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is(EResponse.REGISTER_MANAGER_SUCCESSFULLY.getCode())))
                .andExpect(jsonPath("$.message", is(EResponse.REGISTER_MANAGER_SUCCESSFULLY.getMessage())));
    }

    @Test
    void registerManagerInvalidTest() throws Exception{
        ManagerCommand managerCommand = new ManagerCommandTestDataBuild().withIdentificationNumber("123456789").build();
        mockMvc.perform(post("http://localhost:9002/manager").content(objectMapper.writeValueAsString(managerCommand))
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is(EResponse.IDENTIFICATION_NUMBER_ERROR.getCode())));
    }
}
