package co.com.vialsmotors.commons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VialsMotorsCommonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(VialsMotorsCommonsApplication.class, args);
	}

}
