package co.com.vialsmotors.commons.domain;

public class TestException extends RuntimeException{

    public TestException(String message) {
        super(message);
    }
}
