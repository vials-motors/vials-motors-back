package co.com.vialsmotors.commons.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ECallMessage {

    ERROR_CALL_CREDENTIAL("ERR-CALL-00", "Error iterno de comunicacion"),
    ERROR_CALL_CREDENTIAL_MESSAGE("ERR-CALL-01", "Se obtiene errores controlados");

    private final String code;
    private final String message;
}
