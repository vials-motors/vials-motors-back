package co.com.vialsmotors.commons.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EDatabaseResponse {

    ERROR_DATABASE("DB-ERR-00");

    private final String code;
}
