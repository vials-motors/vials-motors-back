package co.com.vialsmotors.commons.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EFieldMessage {

    IDENTIFICATION_TYPE_MESSAGE("FIELD-ERR-00", "El tipo de identificacion es requerido"),
    FIELD_FIRST_NAME_MESSAGE("FIELD-ERR-01", "El primer nombre es requerido"),
    FIELD_FIRST_LAST_NAME_MESSAGE("FIELD-ERR-02", "El primer apellido es requerido"),
    FIELD_PHONE_MESSAGE("FIELD-ERR-03","El telefono es requerido"),
    FIELD_IDENTIFICATION_NUMBER("FIELD-ERR-04", "El numero de identificacion es requerido"),
    FIELD_ID_MESSAGE("FIELD-ERR-05", "El id es requerido"),
    FIELD_DESCRIPTION_MESSAGE("FIELD-ERR-06", "La descripcion es requerida");

    private final String code;
    private final String message;
}
