package co.com.vialsmotors.commons.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EJwtMessage {

    GET_USERNAME_MESSAGE("JWT-ERR-00", "Error obteniendo el nombre de usuario"),
    DECODED_TOKEN_ERROR_MESSAGE("JWT-ERR-01", "Error decodificando el token");

    private final String code;
    private final String message;
}
