package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class CallCredentialException extends RuntimeException{

    private final String status;

    public CallCredentialException(String message, String status) {
        super(message);
        this.status = status;
    }
}
