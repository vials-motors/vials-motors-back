package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class DecodedErrorTokenException extends RuntimeException{

    private final String status;

    public DecodedErrorTokenException(String message, String status) {
        super(message);
        this.status = status;
    }
}
