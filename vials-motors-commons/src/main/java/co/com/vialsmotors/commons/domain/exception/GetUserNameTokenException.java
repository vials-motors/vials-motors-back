package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class GetUserNameTokenException extends RuntimeException{

    private final String status;

    public GetUserNameTokenException(String message, String status) {
        super(message);
        this.status = status;
    }
}
