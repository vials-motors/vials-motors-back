package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class IdentificationTypeException extends RuntimeException{

    private final String status;

    public IdentificationTypeException(String message, String status) {
        super(message);
        this.status = status;
    }
}
