package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class LoadUsernameException extends RuntimeException{

    private final String status;

    public LoadUsernameException(String message, String status) {
        super(message);
        this.status = status;
    }
}
