package co.com.vialsmotors.commons.domain.exception;

import lombok.Getter;

@Getter
public class SqlServerDataBaseException extends RuntimeException{

    private final String status;

    public SqlServerDataBaseException(String message, String status) {
        super(message);
        this.status = status;
    }
}
