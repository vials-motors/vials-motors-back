package co.com.vialsmotors.core.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DateCommand {

    private String day;
    private String hour;
    private String customerId;
    private ServiceCommand service;
    private VehicleCommand vehicle;
}
