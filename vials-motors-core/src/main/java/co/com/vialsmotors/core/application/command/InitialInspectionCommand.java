package co.com.vialsmotors.core.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InitialInspectionCommand {

    private String mileage;
    private String inspectionDescription;
    private String fullLevel;
    private String generalDescription;
}
