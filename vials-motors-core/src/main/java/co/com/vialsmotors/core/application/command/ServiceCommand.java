package co.com.vialsmotors.core.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ServiceCommand {

    private String codeService;
    private String description;
    private Double amount;
    private ServiceTypeCommand serviceType;
}
