package co.com.vialsmotors.core.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDateByTechnicalCommand {

    private InitialInspectionCommand initialInspection;
    private String noveltyDate;
    private String codeTechnical;
    private List<ProductCommand> product;
}
