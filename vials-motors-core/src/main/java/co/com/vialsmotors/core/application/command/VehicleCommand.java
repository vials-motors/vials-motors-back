package co.com.vialsmotors.core.application.command;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VehicleCommand {

    private String licensePlate;
    private String make;
    private String reference;
    private String model;
    private VehicleTypeCommand vehicleType;
}
