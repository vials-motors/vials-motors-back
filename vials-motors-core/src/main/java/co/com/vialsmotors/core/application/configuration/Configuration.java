package co.com.vialsmotors.core.application.configuration;

import co.com.vialsmotors.core.domain.port.date.GetAllReserveRepository;
import co.com.vialsmotors.core.domain.port.date.GetDateRepository;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.domain.port.product.DeleteProductRepository;
import co.com.vialsmotors.core.domain.port.product.GetListProductRepository;
import co.com.vialsmotors.core.domain.port.product.GetProductRepository;
import co.com.vialsmotors.core.domain.port.product.SaveProductRepository;
import co.com.vialsmotors.core.domain.port.producttype.delete.DeleteProductTypeRepository;
import co.com.vialsmotors.core.domain.port.producttype.get.GetProductTypeRepository;
import co.com.vialsmotors.core.domain.port.producttype.list.GetListProductTypeRepository;
import co.com.vialsmotors.core.domain.port.producttype.save.SaveProductTypeRepository;
import co.com.vialsmotors.core.domain.port.provideservice.DeleteServiceRepository;
import co.com.vialsmotors.core.domain.port.provideservice.GetAllServiceRepository;
import co.com.vialsmotors.core.domain.port.provideservice.GetServiceRepository;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.domain.port.servicetype.DeleteServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.GetAllServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.GetServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.SaveServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.DeleteVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.GetAllVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.GetVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.SaveVehicleTypeRepository;
import co.com.vialsmotors.core.domain.service.date.list.impl.*;
import co.com.vialsmotors.core.domain.service.date.save.SaveDateImplService;
import co.com.vialsmotors.core.domain.service.date.delete.DeleteDateImplService;
import co.com.vialsmotors.core.domain.service.date.scheduled.ChangeStatusDateScheduledService;
import co.com.vialsmotors.core.domain.service.date.update.UpdateDateByCustomerImplService;
import co.com.vialsmotors.core.domain.service.date.update.UpdateStatusByTechnicalImplService;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateSchedulingAvailabilityImplService;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateSchedulingAvailabilityService;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateViabilityNewStateImplService;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateViabilityNewStateService;
import co.com.vialsmotors.core.domain.service.product.delete.DeleteProductImplService;
import co.com.vialsmotors.core.domain.service.product.list.GetListProductImplService;
import co.com.vialsmotors.core.domain.service.product.save.SaveProductImplService;
import co.com.vialsmotors.core.domain.service.product.update.UpdateProductImplService;
import co.com.vialsmotors.core.domain.service.producttype.delete.DeleteProductTypeImplService;
import co.com.vialsmotors.core.domain.service.producttype.list.GetListProductTypeImplService;
import co.com.vialsmotors.core.domain.service.producttype.save.SaveProductTypeImplService;
import co.com.vialsmotors.core.domain.service.producttype.update.UpdateProductTypeImplService;
import co.com.vialsmotors.core.domain.service.provideservice.delete.DeleteServicesImplService;
import co.com.vialsmotors.core.domain.service.provideservice.list.GetAllServicesImplService;
import co.com.vialsmotors.core.domain.service.provideservice.save.SaveProvideImplService;
import co.com.vialsmotors.core.domain.service.provideservice.update.UpdateServicesImplService;
import co.com.vialsmotors.core.domain.service.servicetype.delete.DeleteServiceTypeImplService;
import co.com.vialsmotors.core.domain.service.servicetype.list.GetAllServiceTypeImplService;
import co.com.vialsmotors.core.domain.service.servicetype.save.SaveServiceTypeImplService;
import co.com.vialsmotors.core.domain.service.servicetype.update.UpdateServiceTypeImplService;
import co.com.vialsmotors.core.domain.service.vehicletype.delete.DeleteVehicleTypeImplService;
import co.com.vialsmotors.core.domain.service.vehicletype.list.GetAllVehicleTypeImplService;
import co.com.vialsmotors.core.domain.service.vehicletype.save.SaveVehicleTypeImplService;
import co.com.vialsmotors.core.domain.service.vehicletype.update.UpdateVehicleTypeImplService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public SaveProductImplService saveProductImplService(SaveProductRepository saveProductRepository){
        return new SaveProductImplService(saveProductRepository);
    }

    @Bean
    public GetListProductImplService getListProductImplService(GetListProductRepository getListProductRepository){
        return new GetListProductImplService(getListProductRepository);
    }

    @Bean
    public UpdateProductImplService updateProductImplService(SaveProductRepository saveProductRepository){
        return new UpdateProductImplService(saveProductRepository);
    }

    @Bean
    public DeleteProductImplService deleteProductImplService(DeleteProductRepository deleteProductRepository, SaveProductRepository saveProductRepository){
        return new DeleteProductImplService(deleteProductRepository, saveProductRepository);
    }

    @Bean
    public SaveProductTypeImplService saveProductTypeImplService(SaveProductTypeRepository saveProductTypeRepository){
        return new SaveProductTypeImplService(saveProductTypeRepository);
    }

    @Bean
    public GetListProductTypeImplService getListProductTypeImplService(GetListProductTypeRepository getListProductTypeRepository){
        return new GetListProductTypeImplService(getListProductTypeRepository);
    }

    @Bean
    public SaveProvideImplService saveProvideImplService(SaveServiceRepository saveServiceRepository){
        return new SaveProvideImplService(saveServiceRepository);
    }

    @Bean
    public SaveDateImplService saveDateImplService(SaveDateRepository saveDateRepository,
                                                   SaveServiceRepository saveServiceRepository,
                                                   ValidateSchedulingAvailabilityService validateSchedulingAvailabilityService){
        return new SaveDateImplService(saveDateRepository, saveServiceRepository, validateSchedulingAvailabilityService);
    }

    @Bean
    public ValidateSchedulingAvailabilityImplService validateSchedulingAvailabilityImplService(GetListDateRepository getListDateRepository){
        return new ValidateSchedulingAvailabilityImplService(getListDateRepository);
    }

    @Bean
    public DeleteDateImplService deleteDateImplService(SaveDateRepository saveDateRepository){
        return new DeleteDateImplService(saveDateRepository);
    }

    @Bean
    public GetAllListReserveImplService getAllListReserveImplService(GetAllReserveRepository getAllReserveRepository){
        return new GetAllListReserveImplService(getAllReserveRepository);
    }

    @Bean
    public GetListReserveActiveForAdminAndTechnicalImplService getListReserveActiveForAdminAndTechnicalImplService (GetListDateRepository getAllReserveRepository){
        return new GetListReserveActiveForAdminAndTechnicalImplService(getAllReserveRepository);
    }

    @Bean
    public GetListActiveByClientImplService getListActiveByClientImplService(GetListDateRepository getListDateRepository){
        return new GetListActiveByClientImplService(getListDateRepository);
    }

    @Bean
    public GetAllListReserveClientImplService getAllListReserveClientImplService(GetListDateRepository getListDateRepository){
        return new GetAllListReserveClientImplService(getListDateRepository);
    }

    @Bean
    public UpdateDateByCustomerImplService updateDateByCustomerImplService(SaveDateRepository saveDateRepository, SaveServiceRepository saveServiceRepository, GetDateRepository getDateRepository){
        return new UpdateDateByCustomerImplService(saveDateRepository, saveServiceRepository, getDateRepository);
    }

    @Bean
    public GetAllServicesImplService getAllServicesImplService(GetAllServiceRepository getAllServiceRepository){
        return new GetAllServicesImplService(getAllServiceRepository);
    }

    @Bean
    public GetAllServiceTypeImplService getAllServiceTypeImplService(GetAllServiceTypeRepository getAllServiceTypeRepository){
        return new GetAllServiceTypeImplService(getAllServiceTypeRepository);
    }

    @Bean
    public GetAllVehicleTypeImplService getAllVehicleTypeImplService(GetAllVehicleTypeRepository getAllVehicleTypeRepository){
        return new GetAllVehicleTypeImplService(getAllVehicleTypeRepository);
    }

    @Bean
    public ValidateViabilityNewStateImplService validateViabilityNewStateImplService(){
        return new ValidateViabilityNewStateImplService();
    }

    @Bean
    public UpdateStatusByTechnicalImplService updateStatusByTechnicalImplService(ValidateViabilityNewStateService validateViabilityNewStateService,
                                                                                 GetDateRepository getDateRepository,
                                                                                 SaveDateRepository saveDateRepository,
                                                                                 GetProductRepository getProductRepository){
        return new UpdateStatusByTechnicalImplService(validateViabilityNewStateService, getDateRepository, saveDateRepository, getProductRepository);
    }

    @Bean
    public GetAvailableHoursForDayImplService getAvailableHoursForDayImplService (GetListDateRepository getListDateRepository){
        return new GetAvailableHoursForDayImplService(getListDateRepository);
    }

    @Bean
    public DeleteServicesImplService deleteServicesImplService(DeleteServiceRepository deleteServiceRepository, GetServiceRepository getServiceRepository){
        return new DeleteServicesImplService(deleteServiceRepository, getServiceRepository);
    }

    @Bean
    public UpdateServicesImplService updateServicesImplService(SaveServiceRepository saveServiceRepository){
        return new UpdateServicesImplService(saveServiceRepository);
    }

    @Bean
    public SaveServiceTypeImplService saveServiceTypeImplService(GetServiceTypeRepository getServiceTypeRepository, SaveServiceTypeRepository saveServiceTypeRepository){
        return new SaveServiceTypeImplService(getServiceTypeRepository, saveServiceTypeRepository);
    }

    @Bean
    public DeleteServiceTypeImplService deleteServiceTypeImplService(DeleteServiceTypeRepository deleteServiceTypeRepository, GetServiceTypeRepository getServiceTypeRepository){
        return new DeleteServiceTypeImplService(deleteServiceTypeRepository, getServiceTypeRepository);
    }

    @Bean
    public UpdateServiceTypeImplService updateServiceTypeImplService(SaveServiceTypeRepository saveServiceTypeRepository, GetServiceTypeRepository getServiceTypeRepository){
        return new UpdateServiceTypeImplService(saveServiceTypeRepository, getServiceTypeRepository);
    }

    @Bean
    public SaveVehicleTypeImplService saveVehicleTypeImplService(GetVehicleTypeRepository getVehicleTypeRepository, SaveVehicleTypeRepository saveVehicleTypeRepository){
        return new SaveVehicleTypeImplService(getVehicleTypeRepository, saveVehicleTypeRepository);
    }

    @Bean
    public DeleteVehicleTypeImplService deleteVehicleTypeImplService(DeleteVehicleTypeRepository deleteVehicleTypeRepository, GetVehicleTypeRepository getVehicleTypeRepository){
        return new DeleteVehicleTypeImplService(deleteVehicleTypeRepository, getVehicleTypeRepository);
    }

    @Bean
    public UpdateVehicleTypeImplService updateVehicleTypeImplService(GetVehicleTypeRepository getVehicleTypeRepository, SaveVehicleTypeRepository saveVehicleTypeRepository){
        return new UpdateVehicleTypeImplService(getVehicleTypeRepository, saveVehicleTypeRepository);
    }

    @Bean
    public UpdateProductTypeImplService updateProductTypeImplService(SaveProductTypeRepository saveProductTypeRepository, GetProductTypeRepository getProductTypeRepository){
        return new UpdateProductTypeImplService(saveProductTypeRepository, getProductTypeRepository);
    }

    @Bean
    public DeleteProductTypeImplService deleteProductTypeImplService(DeleteProductTypeRepository deleteProductTypeRepository, GetProductTypeRepository getProductTypeRepository){
        return new DeleteProductTypeImplService(deleteProductTypeRepository, getProductTypeRepository);
    }
}
