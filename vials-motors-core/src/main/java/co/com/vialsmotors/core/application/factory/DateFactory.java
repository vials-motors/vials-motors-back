package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.DateCommand;
import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.application.command.UpdateDateCommand;
import co.com.vialsmotors.core.application.command.VehicleCommand;
import co.com.vialsmotors.core.domain.model.Date;
import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.model.UpdateDate;
import co.com.vialsmotors.core.domain.model.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DateFactory {

    private final ServiceFactory serviceFactory;
    private final VehicleFactory vehicleFactory;

    public Date execute(DateCommand dateCommand){
        return new Date(dateCommand.getDay(),
                dateCommand.getHour(),
                dateCommand.getCustomerId(),
                buildService(dateCommand.getService()),
                buildVehicle(dateCommand.getVehicle()));
    }

    public UpdateDate executeUpdate(UpdateDateCommand updateDateCommand){
        return new UpdateDate(updateDateCommand.getDay(), updateDateCommand.getHour(),
                buildService(updateDateCommand.getService()));
    }

    private Service buildService(ServiceCommand serviceCommand){
        return serviceCommand == null ? null:serviceFactory.execute(serviceCommand);
    }

    private Vehicle buildVehicle(VehicleCommand vehicleCommand){
        return vehicleCommand == null ? null:vehicleFactory.execute(vehicleCommand);
    }
}
