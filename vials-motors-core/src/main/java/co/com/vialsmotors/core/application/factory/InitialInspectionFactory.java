package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.InitialInspectionCommand;
import co.com.vialsmotors.core.domain.model.InitialInspection;
import org.springframework.stereotype.Component;

@Component
public class InitialInspectionFactory {

    public InitialInspection execute(InitialInspectionCommand initialInspectionCommand){
        return new InitialInspection(
                initialInspectionCommand.getMileage(),
                initialInspectionCommand.getInspectionDescription(),
                initialInspectionCommand.getFullLevel(),
                initialInspectionCommand.getGeneralDescription());
    }
}
