package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.domain.model.ProductType;
import org.springframework.stereotype.Component;

@Component
public class ProductFactory {

    public Product execute(ProductCommand productCommand){
        return new Product(productCommand.getProductCode(), productCommand.getDescription(), productCommand.getPrice(),
                buildProductType(productCommand.getProductType()));
    }

    private ProductType buildProductType(ProductTypeCommand productTypeCommand){
        return productTypeCommand == null ?
                null:new ProductType(productTypeCommand.getIdProductType(), productTypeCommand.getDescription());
    }
}
