package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.domain.model.ProductType;
import org.springframework.stereotype.Component;

@Component
public class ProductTypeFactory {

    public ProductType execute(ProductTypeCommand productTypeCommand){
        return new ProductType(productTypeCommand.getIdProductType(), productTypeCommand.getDescription());
    }
}
