package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.model.ServiceType;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactory {

    public Service execute(ServiceCommand serviceCommand){
        return new Service(serviceCommand.getCodeService(), serviceCommand.getDescription(),
                serviceCommand.getAmount(),
                buildServiceType(serviceCommand.getServiceType()));
    }

    private ServiceType buildServiceType(ServiceTypeCommand serviceTypeCommand){
        return serviceTypeCommand == null ? null:new ServiceType(serviceTypeCommand.getIdServiceType(),
                serviceTypeCommand.getDescription());
    }
}
