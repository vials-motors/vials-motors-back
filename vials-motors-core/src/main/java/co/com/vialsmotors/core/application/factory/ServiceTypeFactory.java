package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.domain.model.ServiceType;
import org.springframework.stereotype.Component;

@Component
public class ServiceTypeFactory {

    public ServiceType execute(ServiceTypeCommand serviceTypeCommand){
        return new ServiceType(serviceTypeCommand.getIdServiceType(), serviceTypeCommand.getDescription());
    }
}
