package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.VehicleCommand;
import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.domain.model.Vehicle;
import co.com.vialsmotors.core.domain.model.VehicleType;
import org.springframework.stereotype.Component;

@Component
public class VehicleFactory {

    public Vehicle execute(VehicleCommand vehicleCommand){
        return new Vehicle(vehicleCommand.getLicensePlate(), vehicleCommand.getMake(),
                vehicleCommand.getReference(), vehicleCommand.getModel(),
                buildVehicle(vehicleCommand.getVehicleType()));
    }

    private VehicleType buildVehicle(VehicleTypeCommand vehicleTypeCommand){
        return vehicleTypeCommand == null ?
                null:new VehicleType(vehicleTypeCommand.getIdVehicleType(),
                vehicleTypeCommand.getDescription());
    }
}
