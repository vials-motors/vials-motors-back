package co.com.vialsmotors.core.application.factory;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.domain.model.VehicleType;
import org.springframework.stereotype.Component;

@Component
public class VehicleTypeFactory {

    public VehicleType execute(VehicleTypeCommand vehicleTypeCommand){
        return new VehicleType(vehicleTypeCommand.getIdVehicleType(), vehicleTypeCommand.getDescription());
    }
}
