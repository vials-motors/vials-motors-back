package co.com.vialsmotors.core.application.handler.date.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteDateHandler {

    ResponseEntity<GenericResponse> deleteDate(Integer deleteRequest, String day, String hour, String customerId, String license);
}
