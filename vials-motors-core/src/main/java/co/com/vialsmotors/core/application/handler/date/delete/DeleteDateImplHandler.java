package co.com.vialsmotors.core.application.handler.date.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.delete.DeleteDateService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeleteDateImplHandler implements DeleteDateHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteDateImplHandler.class);
    private final DeleteDateService deleteDateService;

    @Override
    public ResponseEntity<GenericResponse> deleteDate(Integer deleteRequest, String day, String hour, String customerId, String license) {
        LOGGER.info("INFO-MS-CORE: Delete date in handler with day " + day + " and delete request: " + deleteRequest);
        return deleteDateService.deleteDate(deleteRequest, day, hour, customerId, license);
    }
}
