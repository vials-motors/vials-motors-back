package co.com.vialsmotors.core.application.handler.date.list;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllListReserveClientHandler {

    ResponseEntity<ListGenericResponse<DateDTO>> getList(String identificationNumber);
}
