package co.com.vialsmotors.core.application.handler.date.list;

import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAvailableHoursForDayHandler {

    ResponseEntity<ListGenericResponse<String>> getList(String day);
}
