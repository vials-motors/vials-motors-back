package co.com.vialsmotors.core.application.handler.date.list.impl;

import co.com.vialsmotors.core.application.handler.date.list.GetAllListReserveClientHandler;
import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAllListReserveClientService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAllListReserveClientImplHandler implements GetAllListReserveClientHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAllListReserveClientImplHandler.class);
    private final GetAllListReserveClientService getAllListReserveClientService;

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getList(String identificationNumber) {
        LOGGER.info("INFO-MS-CORE: Get list for client with identificationNumber " + identificationNumber);
        return getAllListReserveClientService.getAllList(identificationNumber);
    }
}
