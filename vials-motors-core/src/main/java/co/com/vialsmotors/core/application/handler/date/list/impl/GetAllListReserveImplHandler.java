package co.com.vialsmotors.core.application.handler.date.list.impl;

import co.com.vialsmotors.core.application.handler.date.list.GetAllListReserveHandler;
import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAllListReserveService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAllListReserveImplHandler implements GetAllListReserveHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAllListReserveImplHandler.class);
    private final GetAllListReserveService getAllListReserveService;

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getAllList(String day) {
        LOGGER.info("INFO-MS-CORE: Get data in handler");
        return getAllListReserveService.getAllList(day);
    }
}
