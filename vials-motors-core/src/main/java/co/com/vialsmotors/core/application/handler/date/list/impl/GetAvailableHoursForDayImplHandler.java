package co.com.vialsmotors.core.application.handler.date.list.impl;

import co.com.vialsmotors.core.application.handler.date.list.GetAvailableHoursForDayHandler;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAvailableHoursForDayService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAvailableHoursForDayImplHandler implements GetAvailableHoursForDayHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAvailableHoursForDayImplHandler.class);
    private final GetAvailableHoursForDayService getAvailableHoursForDayService;

    @Override
    public ResponseEntity<ListGenericResponse<String>> getList(String day) {
        LOGGER.info("INFO-MS-CORE: Get list for available hours for day: " + day);
        return getAvailableHoursForDayService.getList(day);
    }
}
