package co.com.vialsmotors.core.application.handler.date.list.impl;

import co.com.vialsmotors.core.application.handler.date.list.GetListActiveByClientHandler;
import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetListActiveByClientService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListActiveByClientImplHandler implements GetListActiveByClientHandler {

    private static final Logger LOGGER = Logger.getLogger(GetListActiveByClientImplHandler.class);
    private final GetListActiveByClientService getListActiveByClientService;

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getList(String customerId) {
        LOGGER.info("INFO-MS-CORE: Get list for client active: " + customerId);
        return getListActiveByClientService.getList(customerId);
    }
}
