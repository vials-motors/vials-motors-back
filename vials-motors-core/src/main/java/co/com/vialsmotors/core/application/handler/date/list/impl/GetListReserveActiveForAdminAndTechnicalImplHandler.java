package co.com.vialsmotors.core.application.handler.date.list.impl;

import co.com.vialsmotors.core.application.handler.date.list.GetListReserveActiveForAdminAndTechnicalHandler;
import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetListReserveActiveForAdminAndTechnicalService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListReserveActiveForAdminAndTechnicalImplHandler implements GetListReserveActiveForAdminAndTechnicalHandler {

    private static final Logger LOGGER = Logger.getLogger(GetListReserveActiveForAdminAndTechnicalImplHandler.class);
    private final GetListReserveActiveForAdminAndTechnicalService getListReserveActiveForAdminAndTechnicalService;

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getListReserve(String day) {
        LOGGER.info("INFO-MS-CORE: Get list reserve active");
        return this.getListReserveActiveForAdminAndTechnicalService.getListActive(day);
    }
}
