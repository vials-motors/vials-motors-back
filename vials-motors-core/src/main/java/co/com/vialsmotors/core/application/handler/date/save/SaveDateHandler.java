package co.com.vialsmotors.core.application.handler.date.save;

import co.com.vialsmotors.core.application.command.DateCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveDateHandler {

    ResponseEntity<GenericResponse> saveDate(DateCommand dateCommand);
}
