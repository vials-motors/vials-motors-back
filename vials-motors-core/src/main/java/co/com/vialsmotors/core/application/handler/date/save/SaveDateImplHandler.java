package co.com.vialsmotors.core.application.handler.date.save;

import co.com.vialsmotors.core.application.command.DateCommand;
import co.com.vialsmotors.core.application.factory.DateFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.save.SaveDateService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveDateImplHandler implements SaveDateHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveDateImplHandler.class);
    private final DateFactory dateFactory;
    private final SaveDateService saveDateService;

    @Override
    public ResponseEntity<GenericResponse> saveDate(DateCommand dateCommand) {
        LOGGER.info("INFO-MS-CORE: Add date in handler");
        return saveDateService.saveDate(dateFactory.execute(dateCommand));
    }
}
