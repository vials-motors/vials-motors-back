package co.com.vialsmotors.core.application.handler.date.update;

import co.com.vialsmotors.core.application.command.UpdateDateCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateDateByCustomerHandler {

    ResponseEntity<GenericResponse> updateHandler(String customerId, String day, String hour,  UpdateDateCommand updateDateCommand, String license);
}
