package co.com.vialsmotors.core.application.handler.date.update;

import co.com.vialsmotors.core.application.command.UpdateDateCommand;
import co.com.vialsmotors.core.application.factory.DateFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.update.UpdateDateByCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateDateByCustomerImplHandler implements UpdateDateByCustomerHandler {

    private static final Logger LOGGER = Logger.getLogger(UpdateDateByCustomerImplHandler.class);
    private final UpdateDateByCustomerService updateDateByCustomerService;
    private final DateFactory dateFactory;

    @Override
    public ResponseEntity<GenericResponse> updateHandler(String customerId, String day, String hour, UpdateDateCommand updateDateCommand, String license) {
        LOGGER.info("INFO-MS-CORE: Update date in handler with customer id: " + customerId);
        return updateDateByCustomerService.updateDate(customerId,day, hour, dateFactory.executeUpdate(updateDateCommand), license );
    }
}
