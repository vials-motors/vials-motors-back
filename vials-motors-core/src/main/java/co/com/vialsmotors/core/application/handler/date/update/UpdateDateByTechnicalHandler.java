package co.com.vialsmotors.core.application.handler.date.update;

import co.com.vialsmotors.core.application.command.UpdateDateByTechnicalCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateDateByTechnicalHandler {

    ResponseEntity<GenericResponse> updateDate(String customerId, String day, String hour, Long newState,
                                               UpdateDateByTechnicalCommand updateDateByTechnicalCommand, String license);
}
