package co.com.vialsmotors.core.application.handler.date.update;

import co.com.vialsmotors.core.application.command.UpdateDateByTechnicalCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.update.UpdateStatusByTechnicalService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateDateByTechnicalImplHandler implements UpdateDateByTechnicalHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateDateByTechnicalImplHandler.class);
    private final UpdateStatusByTechnicalService updateStatusByTechnicalService;

    @Override
    public ResponseEntity<GenericResponse> updateDate(String customerId, String day, String hour, Long newState,
                                                      UpdateDateByTechnicalCommand updateDateByTechnicalCommand, String license) {
        LOGGER.info("INFO-MS-CORE: Update state the date in handler");
        return updateStatusByTechnicalService.updateStatus(customerId, day, hour, newState, updateDateByTechnicalCommand, license);
    }
}
