package co.com.vialsmotors.core.application.handler.product.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteProductHandler {

    ResponseEntity<GenericResponse> deleteProduct(String productCode);
}
