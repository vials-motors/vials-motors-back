package co.com.vialsmotors.core.application.handler.product.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.product.delete.DeleteProductService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteProductImplHandler implements DeleteProductHandler{

    private final static Logger LOGGER = Logger.getLogger(DeleteProductImplHandler.class);
    private final DeleteProductService deleteProductService;

    @Override
    public ResponseEntity<GenericResponse> deleteProduct(String productCode) {
        LOGGER.info("INFO-MS-CORE: Delete product with code: " + productCode);
        return deleteProductService.deleteProduct(productCode);
    }
}
