package co.com.vialsmotors.core.application.handler.product.list;

import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListProductHandler {

    ResponseEntity<ListGenericResponse<ProductDTO>> getListProduct();
}
