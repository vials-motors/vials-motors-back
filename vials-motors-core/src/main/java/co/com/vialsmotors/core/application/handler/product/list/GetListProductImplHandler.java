package co.com.vialsmotors.core.application.handler.product.list;

import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.product.list.GetListProductService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

@Configuration
@RequiredArgsConstructor
public class GetListProductImplHandler implements GetListProductHandler{

    private static final Logger LOGGER = Logger.getLogger(GetListProductImplHandler.class);

    private final GetListProductService getListProductService;

    @Override
    public ResponseEntity<ListGenericResponse<ProductDTO>> getListProduct() {
        LOGGER.info("INFO-MS-CORE: Getting list product of GetListProductImplHandler");
        return getListProductService.getListProduct();
    }
}
