package co.com.vialsmotors.core.application.handler.product.save;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveProductHandler {

    ResponseEntity<GenericResponse> saveProduct(ProductCommand productCommand);
}
