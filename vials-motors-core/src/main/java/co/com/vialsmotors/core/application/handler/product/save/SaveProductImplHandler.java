package co.com.vialsmotors.core.application.handler.product.save;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.application.factory.ProductFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.product.save.SaveProductService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveProductImplHandler implements SaveProductHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveProductImplHandler.class);

    private final SaveProductService saveProductService;
    private final ProductFactory productFactory;

    @Override
    public ResponseEntity<GenericResponse> saveProduct(ProductCommand productCommand) {
        LOGGER.info("INFO-MS-CORE: Save product in Save product impl handler");
        return saveProductService.saveProduct(productFactory.execute(productCommand));
    }
}
