package co.com.vialsmotors.core.application.handler.product.update;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateProductHandler {

    ResponseEntity<GenericResponse> updateProduct(ProductCommand newProductCommand, String productCode);
}
