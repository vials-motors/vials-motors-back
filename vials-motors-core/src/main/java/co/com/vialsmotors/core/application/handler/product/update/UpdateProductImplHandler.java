package co.com.vialsmotors.core.application.handler.product.update;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.application.factory.ProductFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.product.update.UpdateProductService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateProductImplHandler implements UpdateProductHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateProductImplHandler.class);

    private final UpdateProductService updateProductService;
    private final ProductFactory productFactory;

    @Override
    public ResponseEntity<GenericResponse> updateProduct(ProductCommand newProductCommand, String productCode) {
        LOGGER.info("INFO-MS-CORE: Update product in UpdateProductImplHandler with productCode " + productCode);
        return this.updateProductService.updateProduct(productFactory.execute(newProductCommand), productCode);
    }
}
