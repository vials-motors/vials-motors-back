package co.com.vialsmotors.core.application.handler.producttype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteProductTypeHandler {

    ResponseEntity<GenericResponse> deleteProductType(Long id);
}
