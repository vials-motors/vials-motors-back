package co.com.vialsmotors.core.application.handler.producttype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.producttype.delete.DeleteProductTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteProductTypeImplHandler implements DeleteProductTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteProductTypeImplHandler.class);
    private final DeleteProductTypeService deleteProductTypeService;

    @Override
    public ResponseEntity<GenericResponse> deleteProductType(Long id) {
        LOGGER.info("INFO-MS-CORE: Delete product type with id: " + id);
        return deleteProductTypeService.deleteProductType(id);
    }
}
