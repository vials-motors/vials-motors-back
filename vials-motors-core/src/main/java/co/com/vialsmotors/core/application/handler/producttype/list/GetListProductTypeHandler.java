package co.com.vialsmotors.core.application.handler.producttype.list;

import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListProductTypeHandler {

    ResponseEntity<ListGenericResponse<ProductTypeDTO>> getListProductType();
}
