package co.com.vialsmotors.core.application.handler.producttype.list;

import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.producttype.list.GetListProductTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListProductTypeImplHandler implements GetListProductTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(GetListProductTypeImplHandler.class);
    private final GetListProductTypeService getListProductTypeService;

    @Override
    public ResponseEntity<ListGenericResponse<ProductTypeDTO>> getListProductType() {
        LOGGER.info("INFO-MS-CORE: Get product type in handler");
        return getListProductTypeService.getListProductType();
    }
}
