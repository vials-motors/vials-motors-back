package co.com.vialsmotors.core.application.handler.producttype.save;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveProductTypeHandler {

    ResponseEntity<GenericResponse> saveProductType(ProductTypeCommand productTypeCommand);
}
