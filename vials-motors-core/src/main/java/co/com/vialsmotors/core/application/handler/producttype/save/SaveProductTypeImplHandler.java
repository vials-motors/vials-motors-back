package co.com.vialsmotors.core.application.handler.producttype.save;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.application.factory.ProductTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.producttype.save.SaveProductTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveProductTypeImplHandler implements SaveProductTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveProductTypeImplHandler.class);
    private final SaveProductTypeService saveProductTypeService;
    private final ProductTypeFactory productTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> saveProductType(ProductTypeCommand productTypeCommand) {
        LOGGER.info("INFO-MS-CORE: Save product type in handler with id: " + productTypeCommand.getIdProductType());
        return saveProductTypeService.saveProductType(productTypeFactory.execute(productTypeCommand));
    }
}
