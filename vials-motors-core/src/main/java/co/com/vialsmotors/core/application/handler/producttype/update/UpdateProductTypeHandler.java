package co.com.vialsmotors.core.application.handler.producttype.update;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateProductTypeHandler {

    ResponseEntity<GenericResponse> updateProducType(ProductTypeCommand productTypeCommand, Long id);
}
