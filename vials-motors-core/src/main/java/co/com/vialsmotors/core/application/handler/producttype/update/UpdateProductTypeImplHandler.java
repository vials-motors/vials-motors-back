package co.com.vialsmotors.core.application.handler.producttype.update;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.application.factory.ProductTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.producttype.update.UpdateProductTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateProductTypeImplHandler implements UpdateProductTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateProductTypeImplHandler.class);
    private final UpdateProductTypeService productTypeService;
    private final ProductTypeFactory productTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> updateProducType(ProductTypeCommand productTypeCommand, Long id) {
        LOGGER.info("INFO-MS-CORE: Update product type with id: " + id);
        return productTypeService.updateProductType(productTypeFactory.execute(productTypeCommand), id);
    }
}
