package co.com.vialsmotors.core.application.handler.provideservice.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteServiceHandler {

    ResponseEntity<GenericResponse> deleteService(String codeService);
}
