package co.com.vialsmotors.core.application.handler.provideservice.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.provideservice.delete.DeleteServicesService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteServiceImplHandler implements DeleteServiceHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteServiceImplHandler.class);
    private final DeleteServicesService deleteServicesService;

    @Override
    public ResponseEntity<GenericResponse> deleteService(String codeService) {
        LOGGER.info("INFO-MS-CORE: Delete service with code service: " + codeService);
        return deleteServicesService.deleteService(codeService);
    }
}
