package co.com.vialsmotors.core.application.handler.provideservice.list;

import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllServicesHandler {

    ResponseEntity<ListGenericResponse<ServiceDTO>> getAll();
}
