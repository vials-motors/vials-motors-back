package co.com.vialsmotors.core.application.handler.provideservice.list;

import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.provideservice.list.GetAllServicesService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAllServicesImplHandler implements GetAllServicesHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAllServicesImplHandler.class);
    private final GetAllServicesService getAllServicesService;

    @Override
    public ResponseEntity<ListGenericResponse<ServiceDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Getting all services in handler");
        return getAllServicesService.getAll();
    }
}
