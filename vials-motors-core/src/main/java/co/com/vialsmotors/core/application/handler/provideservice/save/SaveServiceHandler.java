package co.com.vialsmotors.core.application.handler.provideservice.save;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveServiceHandler {

    ResponseEntity<GenericResponse> saveProvideService(ServiceCommand serviceCommand);
}
