package co.com.vialsmotors.core.application.handler.provideservice.save;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.application.factory.ServiceFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.provideservice.save.SaveService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveServiceImplHandler implements SaveServiceHandler {

    private static final Logger LOGGER = Logger.getLogger(SaveServiceImplHandler.class);
    private final SaveService saveService;
    private final ServiceFactory serviceFactory;

    @Override
    public ResponseEntity<GenericResponse> saveProvideService(ServiceCommand serviceCommand) {
        LOGGER.info("INFO-MS-CORE: Save service in handler");
        return saveService.saveNewService(serviceFactory.execute(serviceCommand));
    }
}
