package co.com.vialsmotors.core.application.handler.provideservice.update;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateServicesHandler {

    ResponseEntity<GenericResponse> updateService(ServiceCommand serviceCommand, String codeService);
}
