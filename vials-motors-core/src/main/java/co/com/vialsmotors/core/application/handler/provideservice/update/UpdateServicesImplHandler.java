package co.com.vialsmotors.core.application.handler.provideservice.update;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.application.factory.ServiceFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.provideservice.update.UpdateServicesService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateServicesImplHandler implements UpdateServicesHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateServicesImplHandler.class);
    private final ServiceFactory serviceFactory;
    private final UpdateServicesService updateServicesService;

    @Override
    public ResponseEntity<GenericResponse> updateService(ServiceCommand serviceCommand, String codeService) {
        LOGGER.info("INFO-MS-CORE: update service with code service: " + codeService);
        return updateServicesService.updateService(serviceFactory.execute(serviceCommand), codeService);
    }
}
