package co.com.vialsmotors.core.application.handler.servicetype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteServiceTypeHandler {

    ResponseEntity<GenericResponse> deleteServiceType(Long id);
}
