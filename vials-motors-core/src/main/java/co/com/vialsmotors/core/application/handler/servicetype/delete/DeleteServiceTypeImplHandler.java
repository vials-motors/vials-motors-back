package co.com.vialsmotors.core.application.handler.servicetype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.servicetype.delete.DeleteServiceTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteServiceTypeImplHandler implements DeleteServiceTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteServiceTypeImplHandler.class);
    private final DeleteServiceTypeService deleteServiceTypeService;

    @Override
    public ResponseEntity<GenericResponse> deleteServiceType(Long id) {
        LOGGER.info("INFO-MS-CORE: Delete service type with id: " + id);
        return deleteServiceTypeService.deleteService(id);
    }
}
