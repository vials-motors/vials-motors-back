package co.com.vialsmotors.core.application.handler.servicetype.list;

import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllServiceTypeHandler {

    ResponseEntity<ListGenericResponse<ServiceTypeDTO>> getAll();
}
