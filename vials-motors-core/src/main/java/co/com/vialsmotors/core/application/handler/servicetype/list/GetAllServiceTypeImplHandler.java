package co.com.vialsmotors.core.application.handler.servicetype.list;

import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.servicetype.list.GetAllServiceTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAllServiceTypeImplHandler implements GetAllServiceTypeHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAllServiceTypeImplHandler.class);
    private final GetAllServiceTypeService getAllServiceTypeService;

    @Override
    public ResponseEntity<ListGenericResponse<ServiceTypeDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Getting all data for service type in handler");
        return getAllServiceTypeService.getAll();
    }
}
