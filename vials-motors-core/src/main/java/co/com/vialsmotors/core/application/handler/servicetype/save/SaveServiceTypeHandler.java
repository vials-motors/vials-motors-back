package co.com.vialsmotors.core.application.handler.servicetype.save;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveServiceTypeHandler {

    ResponseEntity<GenericResponse> saveServiceType(ServiceTypeCommand serviceTypeCommand);
}
