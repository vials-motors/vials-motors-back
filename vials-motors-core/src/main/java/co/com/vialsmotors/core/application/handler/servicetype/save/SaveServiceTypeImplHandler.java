package co.com.vialsmotors.core.application.handler.servicetype.save;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.application.factory.ServiceTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.servicetype.save.SaveServiceTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveServiceTypeImplHandler implements SaveServiceTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveServiceTypeImplHandler.class);
    private final SaveServiceTypeService saveServiceTypeService;
    private final ServiceTypeFactory serviceTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> saveServiceType(ServiceTypeCommand serviceTypeCommand) {
        LOGGER.info("INFO-MS-CORE: Save service type in handler");
        return saveServiceTypeService.saveServiceType(serviceTypeFactory.execute(serviceTypeCommand));
    }
}
