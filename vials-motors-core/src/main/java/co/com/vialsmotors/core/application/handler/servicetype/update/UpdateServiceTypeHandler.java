package co.com.vialsmotors.core.application.handler.servicetype.update;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateServiceTypeHandler {

    ResponseEntity<GenericResponse> updateServiceType(ServiceTypeCommand serviceTypeCommand, Long id);
}
