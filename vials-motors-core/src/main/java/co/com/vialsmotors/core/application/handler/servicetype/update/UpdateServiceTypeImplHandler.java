package co.com.vialsmotors.core.application.handler.servicetype.update;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.application.factory.ServiceTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.servicetype.update.UpdateServiceTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateServiceTypeImplHandler implements UpdateServiceTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateServiceTypeImplHandler.class);
    private final ServiceTypeFactory serviceTypeFactory;
    private final UpdateServiceTypeService updateServiceTypeService;

    @Override
    public ResponseEntity<GenericResponse> updateServiceType(ServiceTypeCommand serviceTypeCommand, Long id) {
        LOGGER.info("INFO-MS-CORE: Update service type with id: " + id);
        return updateServiceTypeService.updateServiceType(serviceTypeFactory.execute(serviceTypeCommand), id);
    }
}
