package co.com.vialsmotors.core.application.handler.vehicletype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteVehicleTypeHandler {

    ResponseEntity<GenericResponse> deleteVehicleType(Long id);
}
