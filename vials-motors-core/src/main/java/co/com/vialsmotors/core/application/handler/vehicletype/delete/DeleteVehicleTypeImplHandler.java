package co.com.vialsmotors.core.application.handler.vehicletype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.vehicletype.delete.DeleteVehicleTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteVehicleTypeImplHandler implements DeleteVehicleTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteVehicleTypeImplHandler.class);
    private final DeleteVehicleTypeService deleteVehicleTypeService;

    @Override
    public ResponseEntity<GenericResponse> deleteVehicleType(Long id) {
        LOGGER.info("INFO-MS-CORE: Delete vehicle type with id: " + id);
        return deleteVehicleTypeService.deleteVehicleType(id);
    }
}
