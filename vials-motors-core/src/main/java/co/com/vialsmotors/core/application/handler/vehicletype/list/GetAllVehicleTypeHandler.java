package co.com.vialsmotors.core.application.handler.vehicletype.list;

import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllVehicleTypeHandler {

    ResponseEntity<ListGenericResponse<VehicleTypeDTO>> getAll();
}
