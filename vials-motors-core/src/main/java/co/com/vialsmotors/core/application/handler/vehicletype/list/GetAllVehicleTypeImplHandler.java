package co.com.vialsmotors.core.application.handler.vehicletype.list;

import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.vehicletype.list.GetAllVehicleTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAllVehicleTypeImplHandler implements GetAllVehicleTypeHandler {

    private static final Logger LOGGER = Logger.getLogger(GetAllVehicleTypeImplHandler.class);
    private final GetAllVehicleTypeService getAllVehicleTypeService;

    @Override
    public ResponseEntity<ListGenericResponse<VehicleTypeDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Get list of vehicle type in handler");
        return getAllVehicleTypeService.getAll();
    }
}
