package co.com.vialsmotors.core.application.handler.vehicletype.save;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveVehicleTypeHandler {

    ResponseEntity<GenericResponse> saveVehicleType(VehicleTypeCommand vehicleTypeCommand);
}
