package co.com.vialsmotors.core.application.handler.vehicletype.save;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.application.factory.VehicleTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.vehicletype.save.SaveVehicleTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveVehicleTypeImplHandler implements SaveVehicleTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveVehicleTypeImplHandler.class);
    private final SaveVehicleTypeService saveVehicleTypeService;
    private final VehicleTypeFactory vehicleTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> saveVehicleType(VehicleTypeCommand vehicleTypeCommand) {
        LOGGER.info("INFO-MS-CORE: Save vehicle type with id: " + vehicleTypeCommand.getIdVehicleType());
        return saveVehicleTypeService.saveVehicleType(vehicleTypeFactory.execute(vehicleTypeCommand));
    }
}
