package co.com.vialsmotors.core.application.handler.vehicletype.update;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateVehicleTypeHandler {

    ResponseEntity<GenericResponse> updateVehicleType(VehicleTypeCommand vehicleTypeCommand, Long id);
}
