package co.com.vialsmotors.core.application.handler.vehicletype.update;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.application.factory.VehicleTypeFactory;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.vehicletype.update.UpdateVehicleTypeService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateVehicleTypeImplHandler implements UpdateVehicleTypeHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateVehicleTypeImplHandler.class);
    private final UpdateVehicleTypeService updateVehicleTypeService;
    private final VehicleTypeFactory vehicleTypeFactory;

    @Override
    public ResponseEntity<GenericResponse> updateVehicleType(VehicleTypeCommand vehicleTypeCommand, Long id) {
        LOGGER.info("INFO-MS-CORE: Update vehicle type with id: " + id);
        return updateVehicleTypeService.updateVehicleType(vehicleTypeFactory.execute(vehicleTypeCommand), id);
    }
}
