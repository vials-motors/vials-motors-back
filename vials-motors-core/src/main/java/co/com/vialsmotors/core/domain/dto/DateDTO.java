package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class DateDTO {

    private String day;
    private String hour;
    private String customerId;
    private String licensePlate;
    private String service;
    private String status;
    private String descriptionStatus;
    private String make;
    private String reference;
    private String vehicleType;
    private List<NoveltyDateDTO> novelty;
}
