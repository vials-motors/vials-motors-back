package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
@AllArgsConstructor
public class NoveltyDateDTO {

    private String noveltyDate;
    private String technicalCode;
    private ZonedDateTime updateDate;
    private String statusDate;
}
