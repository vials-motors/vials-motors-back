package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProductDTO{

    private String productCode;
    private String description;
    private Double price;
    private ProductTypeDTO productType;
}
