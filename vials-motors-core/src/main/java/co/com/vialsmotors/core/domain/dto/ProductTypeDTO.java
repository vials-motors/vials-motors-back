package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProductTypeDTO {

    private Long idProductType;
    private String description;
}
