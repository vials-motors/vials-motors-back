package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ServiceDTO {

    private String codeService;
    private String description;
    private Double amount;
    private ServiceTypeDTO serviceType;
}
