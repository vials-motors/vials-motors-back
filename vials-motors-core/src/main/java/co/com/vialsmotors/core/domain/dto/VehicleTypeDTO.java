package co.com.vialsmotors.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class VehicleTypeDTO {

    private Long idVehicleType;
    private String description;
}
