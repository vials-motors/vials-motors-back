package co.com.vialsmotors.core.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EDateStatusEntity {

    STATUS_REGISTER(1L, "REGISTRADO"),
    STATUS_CANCEL(2L, "CANCELADO"),
    STATUS_RECEIVER(3L, "RECIBIDO"),
    STATUS_PROCESS(4L, "PROCESO"),
    STATUS_FINISH(5L, "FINALIZADO");

    private final Long id;
    private final String description;
}
