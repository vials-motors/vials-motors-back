package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class CallCustomerException extends RuntimeException{

    private final String status;

    public CallCustomerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
