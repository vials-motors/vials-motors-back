package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class CoreErrorException extends RuntimeException{

    private final String status;

    public CoreErrorException(String message, String status) {
        super(message);
        this.status = status;
    }
}
