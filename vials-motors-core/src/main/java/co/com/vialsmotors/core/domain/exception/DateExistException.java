package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class DateExistException extends RuntimeException{

    private final String status;

    public DateExistException(String message, String status) {
        super(message);
        this.status = status;
    }
}
