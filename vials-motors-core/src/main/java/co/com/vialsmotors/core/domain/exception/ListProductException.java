package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ListProductException extends RuntimeException {

    private final String status;

    public ListProductException(String message, String status) {
        super(message);
        this.status = status;
    }
}
