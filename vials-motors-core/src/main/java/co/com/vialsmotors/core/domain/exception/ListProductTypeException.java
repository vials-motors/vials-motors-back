package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ListProductTypeException extends RuntimeException{

    private final String status;

    public ListProductTypeException(String message, String status) {
        super(message);
        this.status = status;
    }
}
