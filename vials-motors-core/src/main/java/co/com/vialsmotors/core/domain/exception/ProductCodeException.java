package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ProductCodeException extends RuntimeException{

    private final String status;

    public ProductCodeException(String message, String status) {
        super(message);
        this.status = status;
    }
}
