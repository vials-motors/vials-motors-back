package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ProductException extends RuntimeException{

    private final String status;

    public ProductException(String message, String status) {
        super(message);
        this.status = status;
    }
}
