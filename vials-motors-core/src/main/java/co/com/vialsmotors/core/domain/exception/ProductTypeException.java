package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ProductTypeException extends RuntimeException{

    private final String status;

    public ProductTypeException(String message, String status) {
        super(message);
        this.status = status;
    }
}
