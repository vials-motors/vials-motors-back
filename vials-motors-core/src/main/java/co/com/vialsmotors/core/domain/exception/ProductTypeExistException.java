package co.com.vialsmotors.core.domain.exception;

import lombok.Getter;

@Getter
public class ProductTypeExistException extends RuntimeException{

    private final String status;

    public ProductTypeExistException(String message, String status) {
        super(message);
        this.status = status;
    }
}
