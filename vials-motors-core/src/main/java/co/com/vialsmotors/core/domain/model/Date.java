package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record Date (String day, String hour, String customerId, Service service, Vehicle vehicle){

    public Date {
        ValidateArgument.validateFieldString(day, EResponse.FIELD_DAY_MESSAGE.getMessage(), EResponse.FIELD_DAY_MESSAGE.getCode());
        ValidateArgument.validateFieldString(hour, EResponse.FIELD_HOUR_MESSAGE.getMessage(), EResponse.FIELD_HOUR_MESSAGE.getCode());
        ValidateArgument.validateFieldString(customerId, EResponse.FIELD_CUSTOMER_ID_MESSAGE.getMessage(), EResponse.FIELD_CUSTOMER_ID_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(service, EResponse.OBJECT_SERVICE_MESSAGE.getMessage(), EResponse.OBJECT_SERVICE_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(vehicle, EResponse.OBJECT_VEHICLE_IS_REQUIRED.getMessage(), EResponse.OBJECT_VEHICLE_IS_REQUIRED.getCode());
    }
}
