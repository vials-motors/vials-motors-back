package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record InitialInspection(String mileage, String inspectionDescription, String fullLevel, String generalDescription) {

    public InitialInspection {
        ValidateArgument.validateFieldString(mileage, EResponse.FIELD_MILEAGE_ERROR.getMessage(), EResponse.FIELD_MILEAGE_ERROR.getCode());
        ValidateArgument.validateFieldString(inspectionDescription, EResponse.FIELD_INSPECTION_DESCRIPTION_ERROR.getMessage(), EResponse.FIELD_INSPECTION_DESCRIPTION_ERROR.getCode());
        ValidateArgument.validateFieldString(fullLevel, EResponse.FIELD_FULL_LEVEL_ERROR.getMessage(), EResponse.FIELD_FULL_LEVEL_ERROR.getCode());
        ValidateArgument.validateFieldString(generalDescription, EResponse.FIELD_GENERAL_DESCRIPTION_ERROR.getMessage(), EResponse.FIELD_GENERAL_DESCRIPTION_ERROR.getCode());
    }
}
