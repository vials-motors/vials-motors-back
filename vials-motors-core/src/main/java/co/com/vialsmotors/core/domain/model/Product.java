package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record Product(String productCode, String description, Double price, ProductType productType) {

    public Product{
        ValidateArgument.validateFieldString(productCode, EResponse.FIELD_PRODUCT_CODE_MESSAGE.getMessage(), EResponse.FIELD_PRODUCT_CODE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(description, EResponse.FIELD_DESCRIPTION_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_MESSAGE.getCode());
        ValidateArgument.validateFieldDouble(price, EResponse.FIELD_PRICE_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(productType, EResponse.PRODUCT_TYPE_MESSAGE.getMessage(), EResponse.PRODUCT_TYPE_MESSAGE.getCode());
    }
}
