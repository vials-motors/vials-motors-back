package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record ProductType(Long idProductType, String description) {

    public ProductType{
        ValidateArgument.validateFieldLong(idProductType, EResponse.FIELD_ID_PRODUCT_TYPE_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_PRODUCT_TYPE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(description, EResponse.FIELD_DESCRIPTION_PRODUCT_TYPE_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_PRODUCT_TYPE_MESSAGE.getCode());
    }
}
