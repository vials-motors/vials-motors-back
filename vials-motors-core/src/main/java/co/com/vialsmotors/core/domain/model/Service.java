package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record Service(String codeService, String description, Double amount, ServiceType serviceType) {

    public Service {
        ValidateArgument.validateFieldString(codeService, EResponse.FIELD_CODE_SERVICE_ERROR.getMessage(), EResponse.FIELD_CODE_SERVICE_ERROR.getCode());
        ValidateArgument.validateFieldString(description, EResponse.FIELD_DESCRIPTION_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_MESSAGE.getCode());
        ValidateArgument.validateFieldDouble(amount, EResponse.FIELD_PRICE_MESSAGE.getMessage(), EResponse.FIELD_PRICE_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(serviceType, EResponse.SERVICE_TYPE_REQUIRED_ERROR.getMessage(), EResponse.SERVICE_TYPE_REQUIRED_ERROR.getCode());
    }
}
