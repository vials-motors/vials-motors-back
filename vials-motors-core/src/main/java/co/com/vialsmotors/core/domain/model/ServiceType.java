package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record ServiceType(Long idServiceType, String description) {

    public ServiceType {
        ValidateArgument.validateFieldLong(idServiceType, EResponse.FIELD_IS_SERVICE_TYPE.getMessage(), EResponse.FIELD_IS_SERVICE_TYPE.getCode());
        ValidateArgument.validateFieldString(description, EResponse.FIELD_DESCRIPTION_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_MESSAGE.getCode());
    }
}
