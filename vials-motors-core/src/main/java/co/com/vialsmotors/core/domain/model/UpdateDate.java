package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record UpdateDate(String day, String hour, Service service) {

    public UpdateDate {
        ValidateArgument.validateFieldString(day, EResponse.FIELD_DAY_MESSAGE.getMessage(), EResponse.FIELD_DAY_MESSAGE.getCode());
        ValidateArgument.validateFieldString(hour, EResponse.FIELD_HOUR_MESSAGE.getMessage(), EResponse.FIELD_HOUR_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(service, EResponse.OBJECT_SERVICE_MESSAGE.getMessage(), EResponse.OBJECT_SERVICE_MESSAGE.getCode());
    }
}
