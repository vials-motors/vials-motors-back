package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record Vehicle(String licensePlate, String make, String reference, String model, VehicleType vehicleType) {

    public Vehicle{
        ValidateArgument.validateFieldString(licensePlate, EResponse.FIELD_LICENSE_PLATE_MESSAGE.getMessage(), EResponse.FIELD_LICENSE_PLATE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(make, EResponse.FIELD_MAKE_MESSAGE.getMessage(), EResponse.FIELD_MAKE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(reference, EResponse.FIELD_REFERENCE_MESSAGE.getMessage(), EResponse.FIELD_REFERENCE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(model, EResponse.FIELD_MODEL_MESSAGE.getMessage(), EResponse.FIELD_MODEL_MESSAGE.getCode());
        ValidateArgument.validateFieldObject(vehicleType, EResponse.OBJECT_VEHICLE_TYPE_MESSAGE.getMessage(), EResponse.OBJECT_VEHICLE_TYPE_MESSAGE.getCode());
    }
}
