package co.com.vialsmotors.core.domain.model;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.validate.ValidateArgument;

public record VehicleType(Long idVehicleType, String description) {

    public VehicleType{
        ValidateArgument.validateFieldLong(idVehicleType, EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getMessage(), EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getCode());
        ValidateArgument.validateFieldString(description, EResponse.FIELD_DESCRIPTION_MESSAGE.getMessage(), EResponse.FIELD_DESCRIPTION_MESSAGE.getCode());
    }
}
