package co.com.vialsmotors.core.domain.port.date;

import co.com.vialsmotors.core.infrastructure.entity.DateEntity;

import java.util.List;

public interface GetAllReserveRepository {

    List<DateEntity> getList(String day);
}
