package co.com.vialsmotors.core.domain.port.date;

import co.com.vialsmotors.core.infrastructure.entity.DateEntity;

import java.util.Optional;

public interface GetDateRepository {

    Optional<DateEntity> getDate(String customerId, String day, String hour, String license);
}
