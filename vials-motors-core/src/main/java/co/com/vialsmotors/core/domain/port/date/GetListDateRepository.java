package co.com.vialsmotors.core.domain.port.date;

import co.com.vialsmotors.core.infrastructure.entity.DateEntity;

import java.util.List;

public interface GetListDateRepository {

    List<DateEntity> getListDateEntity(String day, String hour);

    List<DateEntity> getListActiveForAdminAndTechnical(String day, Boolean status);

    List<DateEntity> getListReserveActiveByClient(String customerId, Boolean status);

    List<DateEntity> getListReserveByClient(String customerId);

    List<DateEntity> getAllList();

}
