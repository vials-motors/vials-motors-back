package co.com.vialsmotors.core.domain.port.date;

import co.com.vialsmotors.core.infrastructure.entity.DateEntity;

import java.util.Optional;

public interface SaveDateRepository {

    void saveDate(DateEntity dateEntity);

    Optional<DateEntity> findDateByHourAndDayAndStatusAndCustomerId(String hour, String day, Boolean status, String customerId, String license);
}
