package co.com.vialsmotors.core.domain.port.product;

public interface DeleteProductRepository {

    void deleteProduct(Long id);
}
