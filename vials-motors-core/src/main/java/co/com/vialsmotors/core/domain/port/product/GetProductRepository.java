package co.com.vialsmotors.core.domain.port.product;

import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;

import java.util.List;

public interface GetProductRepository {

    List<ProductEntity> getProduct(List<String> productCode);
}
