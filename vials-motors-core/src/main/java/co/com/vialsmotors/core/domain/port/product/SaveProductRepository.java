package co.com.vialsmotors.core.domain.port.product;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface SaveProductRepository {

    ResponseEntity<GenericResponse> saveProduct(ProductEntity productEntity);
    Optional<ProductEntity> findByCodeProduct(String codeProduct);
}
