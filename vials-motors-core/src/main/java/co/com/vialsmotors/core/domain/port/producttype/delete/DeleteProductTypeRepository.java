package co.com.vialsmotors.core.domain.port.producttype.delete;

public interface DeleteProductTypeRepository {

    void deleteProductType(Long id);
}
