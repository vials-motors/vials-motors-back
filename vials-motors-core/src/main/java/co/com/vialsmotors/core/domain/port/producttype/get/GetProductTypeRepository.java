package co.com.vialsmotors.core.domain.port.producttype.get;

import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;

import java.util.Optional;

public interface GetProductTypeRepository {

    Optional<ProductTypeEntity> getProductType(Long id);
}
