package co.com.vialsmotors.core.domain.port.producttype.list;

import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;

import java.util.List;

public interface GetListProductTypeRepository {

    List<ProductTypeEntity> getListProductType();
}
