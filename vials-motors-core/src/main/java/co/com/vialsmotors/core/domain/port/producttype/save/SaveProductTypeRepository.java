package co.com.vialsmotors.core.domain.port.producttype.save;

import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;

import java.util.Optional;


public interface SaveProductTypeRepository {

    void saveProductType(ProductTypeEntity productTypeEntity);
    Optional<ProductTypeEntity> findById(Long id);
}
