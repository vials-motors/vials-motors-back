package co.com.vialsmotors.core.domain.port.provideservice;

public interface DeleteServiceRepository {

    void deleteService(Long id);
}
