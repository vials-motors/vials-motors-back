package co.com.vialsmotors.core.domain.port.provideservice;

import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;

import java.util.List;

public interface GetAllServiceRepository {

    List<ServiceEntity> getAll();
}
