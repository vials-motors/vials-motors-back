package co.com.vialsmotors.core.domain.port.provideservice;

import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;

import java.util.Optional;

public interface GetServiceRepository {

    Optional<ServiceEntity> getService(String codeService);
}
