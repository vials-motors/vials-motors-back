package co.com.vialsmotors.core.domain.port.provideservice;

import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;

import java.util.Optional;

public interface SaveServiceRepository {

    void saveService(ServiceEntity serviceEntity);
    Optional<ServiceEntity> findByCodeService(String codeService);
}
