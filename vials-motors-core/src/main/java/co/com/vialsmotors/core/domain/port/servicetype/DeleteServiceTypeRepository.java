package co.com.vialsmotors.core.domain.port.servicetype;

public interface DeleteServiceTypeRepository {

    void deleteServiceType(Long id);
}

