package co.com.vialsmotors.core.domain.port.servicetype;

import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;

import java.util.List;

public interface GetAllServiceTypeRepository {

    List<ServiceTypeEntity> getAll();
}
