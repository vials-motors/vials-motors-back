package co.com.vialsmotors.core.domain.port.servicetype;

import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;

import java.util.Optional;

public interface GetServiceTypeRepository {

    Optional<ServiceTypeEntity> getServiceType(Long id);
}
