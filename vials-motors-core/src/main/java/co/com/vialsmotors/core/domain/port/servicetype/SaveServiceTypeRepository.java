package co.com.vialsmotors.core.domain.port.servicetype;

import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;

public interface SaveServiceTypeRepository {

    void saveServiceType(ServiceTypeEntity serviceTypeEntity);
}
