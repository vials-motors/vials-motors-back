package co.com.vialsmotors.core.domain.port.vehicletype;

public interface DeleteVehicleTypeRepository {

    void deleteVehicleType(Long id);
}
