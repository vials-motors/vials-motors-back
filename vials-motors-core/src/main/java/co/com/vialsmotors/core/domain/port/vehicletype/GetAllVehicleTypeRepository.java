package co.com.vialsmotors.core.domain.port.vehicletype;

import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;

import java.util.List;

public interface GetAllVehicleTypeRepository {

    List<VehicleTypeEntity> getAll();
}
