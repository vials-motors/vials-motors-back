package co.com.vialsmotors.core.domain.port.vehicletype;

import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;

import java.util.Optional;

public interface GetVehicleTypeRepository {

    Optional<VehicleTypeEntity> getVehicleType(Long id);
}
