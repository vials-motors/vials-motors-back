package co.com.vialsmotors.core.domain.port.vehicletype;

import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;

public interface SaveVehicleTypeRepository {

    void saveVehicleType(VehicleTypeEntity vehicleTypeEntity);
}
