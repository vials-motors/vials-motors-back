package co.com.vialsmotors.core.domain.response;

import java.time.ZonedDateTime;

public record GenericResponse(String code, String message, ZonedDateTime timestamp) {
}
