package co.com.vialsmotors.core.domain.response;

import java.time.ZonedDateTime;
import java.util.List;

public record ListGenericResponse<T> (String status, String message, List<T> data, ZonedDateTime timestamp) {
}
