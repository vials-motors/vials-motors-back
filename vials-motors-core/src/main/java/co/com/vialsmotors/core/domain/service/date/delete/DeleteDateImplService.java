package co.com.vialsmotors.core.domain.service.date.delete;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.exception.DateExistException;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.AuditDateEntity;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.entity.DateStatusEntity;
import co.com.vialsmotors.core.infrastructure.entity.DetailsDateEntity;
import co.com.vialsmotors.core.infrastructure.utils.BuildDateUtils;
import co.com.vialsmotors.core.infrastructure.utils.ValidateDeleteDateUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteDateImplService implements DeleteDateService{

    private static final Logger LOGGER = Logger.getLogger(DeleteDateImplService.class);
    private static final Boolean STATUS_FOR_ACTIVE = true;
    private static final Boolean STATUS_FOR_DEACTIVATE = false;
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private static final DateStatusEntity NEW_STATUS = new DateStatusEntity(2L, "CANCELADO");
    private final SaveDateRepository saveDateRepository;

    public DeleteDateImplService(SaveDateRepository saveDateRepository) {
        this.saveDateRepository = saveDateRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteDate(Integer deleteRequest, String day, String hour, String customerId, String license) {
        this.saveDateRepository.saveDate(modifyDate(getDateEntity(day, hour, customerId, license), deleteRequest, hour, day));
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_CITE_SUCCESS.getCode(),
               EResponse.DELETE_CITE_SUCCESS.getMessage(), TIMESTAMP));
    }

    private DateEntity getDateEntity(String day, String hour, String customerId, String license){
        Optional<DateEntity> dateEntity = this.saveDateRepository.findDateByHourAndDayAndStatusAndCustomerId(hour, day, STATUS_FOR_ACTIVE, customerId, license);
        if (dateEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error for delete date because no exist");
            throw new DateExistException(EResponse.DATE_NOT_EXIST_ERROR.getMessage(), EResponse.DATE_NOT_EXIST_ERROR.getCode());
        }
        return dateEntity.get();
    }

    private DateEntity modifyDate(DateEntity dateEntity, Integer deleteRequest, String hour, String day){
        return dateEntity.withAuditEntity(buildAudit(dateEntity.getAuditDateEntity(), deleteRequest, hour, day))
                .withStatus(STATUS_FOR_DEACTIVATE);
    }

    private AuditDateEntity buildAudit(AuditDateEntity auditDateEntity, Integer deleteRequest, String hour, String day){
        List<DetailsDateEntity> newList = auditDateEntity.getDetailsDate();
        newList.add(buildNewDetail(deleteRequest, day, hour));
        return auditDateEntity.withDetailsDate(newList);
    }

    private DetailsDateEntity buildNewDetail(Integer deleteRequest, String day, String hour){
        String commentDeleted = ValidateDeleteDateUtils.validate(deleteRequest);
        this.validateHourByDeletedClient(hour, day, deleteRequest);
        return new DetailsDateEntity(commentDeleted, TIMESTAMP,
                null, this.buildNewStatus(), null);
    }

    private DateStatusEntity buildNewStatus(){
        return NEW_STATUS;
    }

    private void validateHourByDeletedClient(String hour, String day, Integer deleteRequest){
        if (deleteRequest == 2 && (TIMESTAMP.plusHours(2L).isAfter(BuildDateUtils.buildDate(day, hour)))){
                throw new CoreErrorException(EResponse.DELETE_NOT_VALID_FOR_CLIENT.getMessage(), EResponse.DELETE_NOT_VALID_FOR_CLIENT.getCode());
        }
    }
}
