package co.com.vialsmotors.core.domain.service.date.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteDateService {

    ResponseEntity<GenericResponse> deleteDate(Integer deleteRequest, String day, String hour, String customerId, String license);
}
