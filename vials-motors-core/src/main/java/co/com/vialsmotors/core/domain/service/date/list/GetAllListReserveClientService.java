package co.com.vialsmotors.core.domain.service.date.list;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllListReserveClientService {

    ResponseEntity<ListGenericResponse<DateDTO>> getAllList(String identificationNumber);
}
