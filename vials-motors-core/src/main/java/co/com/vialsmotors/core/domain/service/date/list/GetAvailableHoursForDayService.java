package co.com.vialsmotors.core.domain.service.date.list;

import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAvailableHoursForDayService {

    ResponseEntity<ListGenericResponse<String>> getList(String day);
}
