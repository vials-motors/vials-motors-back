package co.com.vialsmotors.core.domain.service.date.list.impl;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAllListReserveClientService;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.mapper.DateMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetAllListReserveClientImplService implements GetAllListReserveClientService {

    private static final Logger LOGGER = Logger.getLogger(GetAllListReserveClientImplService.class);
    private final GetListDateRepository getListDateRepository;

    public GetAllListReserveClientImplService(GetListDateRepository getListDateRepository) {
        this.getListDateRepository = getListDateRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getAllList(String identificationNumber) {
        this.validateIdentificationNumber(identificationNumber);
        List<DateEntity> dateEntityList = this.getListDateRepository.getListReserveByClient(identificationNumber);
        this.validateList(dateEntityList);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_DATE.getCode(), EResponse.GET_LIST_DATE.getMessage(),
                DateMapper.convertListEntityToDto(dateEntityList),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because identification number is null or empty");
            throw new CoreErrorException(EResponse.FIELD_IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.FIELD_IDENTIFICATION_NUMBER_ERROR.getCode());
        }
    }

    private void validateList(List<DateEntity> list){
        if (list.isEmpty())
            throw new CoreErrorException(EResponse.EMPTY_LIST_CLIENT.getMessage(), EResponse.EMPTY_LIST_CLIENT.getCode());
    }
}
