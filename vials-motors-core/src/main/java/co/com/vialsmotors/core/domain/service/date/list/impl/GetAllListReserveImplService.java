package co.com.vialsmotors.core.domain.service.date.list.impl;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetAllReserveRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAllListReserveService;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.mapper.DateMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetAllListReserveImplService implements GetAllListReserveService {

    private static final Logger LOGGER = Logger.getLogger(GetAllListReserveImplService.class);
    private final GetAllReserveRepository getAllReserveRepository;

    public GetAllListReserveImplService(GetAllReserveRepository getAllReserveRepository) {
        this.getAllReserveRepository = getAllReserveRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getAllList(String day) {
        LOGGER.info("INFO-MS-CORE: Get data ");
        this.validateDay(day);
        List<DateEntity> entityList = getAllReserveRepository.getList(day);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_DATE.getCode(),
                EResponse.GET_LIST_DATE.getMessage(), DateMapper.convertListEntityToDto(entityList),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateDay(String day){
        if (day == null || day.isEmpty()){
            throw new CoreErrorException(EResponse.DAY_NULL_MESSAGE.getMessage(), EResponse.DAY_NULL_MESSAGE.getCode());
        }
    }
}
