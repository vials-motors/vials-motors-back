package co.com.vialsmotors.core.domain.service.date.list.impl;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetAvailableHoursForDayService;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.utils.BuildListHoursAvailableUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

public class GetAvailableHoursForDayImplService implements GetAvailableHoursForDayService {

    private static final Logger LOGGER = Logger.getLogger(GetAvailableHoursForDayImplService.class);
    private final GetListDateRepository getListDateRepository;

    public GetAvailableHoursForDayImplService(GetListDateRepository getListDateRepository) {
        this.getListDateRepository = getListDateRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<String>> getList(String day) {
        LOGGER.info("INFO-MS-CORE: Get list for available hour");
        this.validateDay(day);
        Map<String, Long> dateForHour = this.getListDateRepository.getListActiveForAdminAndTechnical(day, true)
                .stream().collect(Collectors.groupingBy(DateEntity::getHour, Collectors.counting()));
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_AVAILABLE_HOUR.getCode(),
                EResponse.GET_AVAILABLE_HOUR.getMessage(), BuildListHoursAvailableUtils.generateList(dateForHour),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateDay(String day){
        if (day == null || day.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error for day null or empty");
            throw new CoreErrorException(EResponse.DAY_NULL_MESSAGE.getMessage(), EResponse.DAY_NULL_MESSAGE.getCode());
        }
    }
}
