package co.com.vialsmotors.core.domain.service.date.list.impl;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetListActiveByClientService;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.mapper.DateMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListActiveByClientImplService implements GetListActiveByClientService {

    private static final Logger LOGGER = Logger.getLogger(GetListActiveByClientImplService.class);
    private static final Boolean STATUS_ACTIVE = Boolean.TRUE;
    private final GetListDateRepository getListDateRepository;

    public GetListActiveByClientImplService(GetListDateRepository getListDateRepository) {
        this.getListDateRepository = getListDateRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getList(String customerId) {
        this.validateCustomerId(customerId);
        List<DateEntity> dateEntityList = this.getListDateRepository.getListReserveActiveByClient(customerId, STATUS_ACTIVE);
        this.validateList(dateEntityList);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_DATE.getCode(), EResponse.GET_LIST_DATE.getMessage(),
                DateMapper.convertListEntityToDto(dateEntityList),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: The customer id is null or empty");
            throw new CoreErrorException(EResponse.FIELD_CUSTOMER_ID_MESSAGE.getMessage(), EResponse.FIELD_CUSTOMER_ID_MESSAGE.getCode());
        }
    }

    private void validateList(List<DateEntity> list){
        if (list.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: The list is empty");
            throw new CoreErrorException(EResponse.EMPTY_LIST_CLIENT.getMessage(), EResponse.EMPTY_LIST_CLIENT.getCode());
        }
    }
}
