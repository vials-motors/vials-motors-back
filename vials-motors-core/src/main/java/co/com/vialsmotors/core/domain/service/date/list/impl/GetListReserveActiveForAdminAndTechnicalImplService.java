package co.com.vialsmotors.core.domain.service.date.list.impl;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.domain.service.date.list.GetListReserveActiveForAdminAndTechnicalService;
import co.com.vialsmotors.core.infrastructure.mapper.DateMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.TimeZone;

public class GetListReserveActiveForAdminAndTechnicalImplService implements GetListReserveActiveForAdminAndTechnicalService {

    private static final Logger LOGGER = Logger.getLogger(GetListReserveActiveForAdminAndTechnicalImplService.class);
    private static final Boolean STATUS_ACTIVE = Boolean.TRUE;
    private final GetListDateRepository getListDateRepository;

    public GetListReserveActiveForAdminAndTechnicalImplService(GetListDateRepository getListDateRepository) {
        this.getListDateRepository = getListDateRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<DateDTO>> getListActive(String day) {
        this.validateDay(day);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_DATE.getCode(), EResponse.GET_LIST_DATE.getMessage(),
                DateMapper.convertListEntityToDto(this.getListDateRepository.getListActiveForAdminAndTechnical(day, STATUS_ACTIVE)),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateDay(String day){
        if (day == null || day.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error for day invalid");
            throw new CoreErrorException(EResponse.DAY_NULL_MESSAGE.getMessage(), EResponse.DAY_NULL_MESSAGE.getCode());
        }
    }
}
