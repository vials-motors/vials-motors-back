package co.com.vialsmotors.core.domain.service.date.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.exception.DateExistException;
import co.com.vialsmotors.core.domain.exception.ProvideServiceException;
import co.com.vialsmotors.core.domain.model.Date;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateSchedulingAvailabilityService;
import co.com.vialsmotors.core.infrastructure.entity.*;
import co.com.vialsmotors.core.infrastructure.mapper.DateMapper;
import co.com.vialsmotors.core.infrastructure.utils.BuildDateUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

public class SaveDateImplService implements SaveDateService{

    private static final Logger LOGGER = Logger.getLogger(SaveDateImplService.class);
    private static final Long ID_STATUS_DATE = 1L;
    private static final String INITIAL_STATUS_DATE = "REGISTRADO";
    private static final String NOVELTY_INITIAL = "Cita Registrada";
    private static final String TIME_ZONE = "America/Bogota";
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone(TIME_ZONE).toZoneId());
    private static final Boolean STATUS_VALIDATE = true;
    private final SaveDateRepository saveDateRepository;
    private final SaveServiceRepository saveServiceRepository;
    private final ValidateSchedulingAvailabilityService validateSchedulingAvailabilityService;

    public SaveDateImplService(SaveDateRepository saveDateRepository, SaveServiceRepository saveServiceRepository, ValidateSchedulingAvailabilityService validateSchedulingAvailabilityService) {
        this.saveDateRepository = saveDateRepository;
        this.saveServiceRepository = saveServiceRepository;
        this.validateSchedulingAvailabilityService = validateSchedulingAvailabilityService;
    }

    @Override
    public ResponseEntity<GenericResponse> saveDate(Date date) {
        this.validateDateCorrect(date.day(), date.hour());
        this.validateExistDate(date.hour(), date.day(), date.customerId(), date.vehicle().licensePlate());
        this.validateSchedulingAvailabilityService.getListEntity(date.day(), date.hour());
        saveDateRepository.saveDate(DateMapper.convertModelToEntity(date, STATUS_VALIDATE, getService(date.service().codeService())).
                withAuditEntity(buildAudit(date.customerId())));
        return ResponseEntity.ok(new GenericResponse(EResponse.DATE_REGISTER_SUCCESS.getCode(),
                EResponse.DATE_REGISTER_SUCCESS.getMessage(), TIMESTAMP));
    }

    private void validateExistDate(String hour, String day, String customerId, String license){
        Optional<DateEntity> dateEntity = saveDateRepository.findDateByHourAndDayAndStatusAndCustomerId(hour, day, STATUS_VALIDATE, customerId, license);
        if (dateEntity.isPresent() && license.equals(dateEntity.get().getVehicleEntity().getLicensePlate())){
            LOGGER.error("ERROR-MS-CORE: Error because product exist with customer id: " + customerId);
            throw new DateExistException(EResponse.DATE_EXIST_ERROR.getMessage(), EResponse.DATE_EXIST_ERROR.getCode());
        }
    }

    private AuditDateEntity buildAudit(String customerId){
        return new AuditDateEntity(buildInitialInspection(customerId),
                buildInitialDetailsDate());
    }

    private InitialInspectionEntity buildInitialInspection(String customerId){
        return new InitialInspectionEntity(customerId);
    }

    private List<DetailsDateEntity> buildInitialDetailsDate(){
        List<DetailsDateEntity> list = new ArrayList<>();
        list.add(new DetailsDateEntity(NOVELTY_INITIAL,
                TIMESTAMP,null, statusInitial(), null));
        return list;
    }

    private DateStatusEntity statusInitial(){
        return new DateStatusEntity(ID_STATUS_DATE, INITIAL_STATUS_DATE);
    }

    private ServiceEntity getService(String serviceCode){
        Optional<ServiceEntity> serviceEntity = saveServiceRepository.findByCodeService(serviceCode);
        if (serviceEntity.isPresent())
            return serviceEntity.get();
        throw new ProvideServiceException(EResponse.SERVICE_NOT_EXIST_ERROR.getMessage(), EResponse.SERVICE_NOT_EXIST_ERROR.getCode());
    }

    private void validateDateCorrect(String day, String hour){
        if (TIMESTAMP.isAfter(BuildDateUtils.buildDate(day, hour))){
            throw new CoreErrorException(EResponse.REGISTER_DATE_ERROR.getMessage(), EResponse.REGISTER_DATE_ERROR.getCode());
        }
    }
}
