package co.com.vialsmotors.core.domain.service.date.save;

import co.com.vialsmotors.core.domain.model.Date;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveDateService {

    ResponseEntity<GenericResponse> saveDate(Date date);
}
