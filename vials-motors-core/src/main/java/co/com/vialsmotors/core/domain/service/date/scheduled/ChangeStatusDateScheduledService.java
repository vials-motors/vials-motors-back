package co.com.vialsmotors.core.domain.service.date.scheduled;

import co.com.vialsmotors.core.domain.enums.EDateStatusEntity;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.entity.DateStatusEntity;
import co.com.vialsmotors.core.infrastructure.entity.DetailsDateEntity;
import org.jboss.logging.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

@Service
public class ChangeStatusDateScheduledService {

    private static final Logger LOGGER = Logger.getLogger(ChangeStatusDateScheduledService.class);
    private static final String NOVELTY = "Cancelado por no asistir";
    private final GetListDateRepository getListDateRepository;
    private final SaveDateRepository saveDateRepository;

    public ChangeStatusDateScheduledService(GetListDateRepository getListDateRepository, SaveDateRepository saveDateRepository) {
        this.getListDateRepository = getListDateRepository;
        this.saveDateRepository = saveDateRepository;
    }

    @Scheduled(fixedRate = 10000)
    public void execute(){
        LOGGER.info("INFO-MS-CORE: Execute scheduled for change status date");
        this.changeStatus(this.getListDateRepository.getAllList()
                .stream()
                .filter(temporal -> temporal.getStatus()
                        .equals(true) && temporal.getAuditDateEntity().getDetailsDate().size() == 1L && temporal.getDayDate()
                        .isBefore(ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId()).plusHours(1L))).toList());
    }

    private void changeStatus(List<DateEntity> list){
        LOGGER.info("INFO-MS-CORE: Change status");
        list.forEach(temporal -> {
            temporal.setStatus(false);
            List<DetailsDateEntity> detailsDateEntities = temporal.getAuditDateEntity().getDetailsDate();
            detailsDateEntities.add(new DetailsDateEntity(NOVELTY, ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId()),
                    "null", new DateStatusEntity(EDateStatusEntity.STATUS_FINISH.getId(), EDateStatusEntity.STATUS_FINISH.getDescription()), null));
            saveDateRepository.saveDate(temporal);
        });
    }
}
