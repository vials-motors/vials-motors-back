package co.com.vialsmotors.core.domain.service.date.update;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.exception.ProvideServiceException;
import co.com.vialsmotors.core.domain.model.UpdateDate;
import co.com.vialsmotors.core.domain.port.date.GetDateRepository;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.utils.BuildDateUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateDateByCustomerImplService implements UpdateDateByCustomerService{

    private static final Logger LOGGER = Logger.getLogger(UpdateDateByCustomerImplService.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final SaveDateRepository saveDateRepository;
    private final SaveServiceRepository saveServiceRepository;
    private final GetDateRepository getDateRepository;

    public UpdateDateByCustomerImplService(SaveDateRepository saveDateRepository, SaveServiceRepository saveServiceRepository, GetDateRepository getDateRepository) {
        this.saveDateRepository = saveDateRepository;
        this.saveServiceRepository = saveServiceRepository;
        this.getDateRepository = getDateRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateDate(String customerId, String day, String hour, UpdateDate newDate, String license) {
        LOGGER.info("INFO-MS-CORE: Update date by customer in service with customer id: " + customerId);
        this.validateCustomerId(customerId);
        this.validateDateCorrect(newDate.day(), newDate.hour());
        this.saveDateRepository.saveDate(this.buildNewEntity(customerId, day, hour, newDate, license));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_DATE_SUCCESS.getCode(), EResponse.UPDATE_DATE_SUCCESS.getMessage(), TIMESTAMP));
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty())
            throw new FieldException(EResponse.FIELD_CUSTOMER_ID_MESSAGE.getMessage(), EResponse.FIELD_CUSTOMER_ID_MESSAGE.getCode());
    }

    private DateEntity getDate(String customerId, String day, String hour, String license){
        Optional<DateEntity> dateEntity = this.getDateRepository.getDate(customerId, day, hour, license);
        if (dateEntity.isEmpty())
            throw new CoreErrorException(EResponse.DATE_NOT_EXIST_ERROR.getMessage(), EResponse.DATE_NOT_EXIST_ERROR.getCode());
        return dateEntity.get();
    }

    private DateEntity buildNewEntity(String customerId, String day, String hour, UpdateDate newDate, String license){
        return this.getDate(customerId, day, hour, license)
                .withDay(newDate.day())
                .withHour(newDate.hour())
                .withService(validateService(newDate.service().codeService()));
    }

    private void validateDateCorrect(String day, String hour){
        if (TIMESTAMP.isAfter(BuildDateUtils.buildDate(day, hour))){
            LOGGER.error("ERROR-MS-CORE: Error for date incorrect");
            throw new CoreErrorException(EResponse.REGISTER_DATE_ERROR.getMessage(), EResponse.REGISTER_DATE_ERROR.getCode());
        }
    }

    private ServiceEntity validateService(String serviceCode){
        Optional<ServiceEntity> serviceEntity = saveServiceRepository.findByCodeService(serviceCode);
        if (serviceEntity.isEmpty())
            throw new ProvideServiceException(EResponse.SERVICE_NOT_EXIST_ERROR.getMessage(), EResponse.SERVICE_NOT_EXIST_ERROR.getCode());
        return serviceEntity.get();
    }

}
