package co.com.vialsmotors.core.domain.service.date.update;

import co.com.vialsmotors.core.domain.model.UpdateDate;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateDateByCustomerService {

    ResponseEntity<GenericResponse> updateDate(String customerId, String day, String hour, UpdateDate newDate, String license);
}
