package co.com.vialsmotors.core.domain.service.date.update;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.application.command.InitialInspectionCommand;
import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.application.command.UpdateDateByTechnicalCommand;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.date.GetDateRepository;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.domain.port.product.GetProductRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.service.date.validate.ValidateViabilityNewStateService;
import co.com.vialsmotors.core.infrastructure.entity.*;
import co.com.vialsmotors.core.infrastructure.utils.NewStatusUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.*;

public class UpdateStatusByTechnicalImplService implements UpdateStatusByTechnicalService{

    private static final Logger LOGGER = Logger.getLogger(UpdateStatusByTechnicalImplService.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private static final String SET_NOVELTY_DATE = "Ninguna novedad";
    private final ValidateViabilityNewStateService validateViabilityNewStateService;
    private final GetDateRepository getDateRepository;
    private final SaveDateRepository saveDateRepository;
    private final GetProductRepository getProductRepository;

    public UpdateStatusByTechnicalImplService(ValidateViabilityNewStateService validateViabilityNewStateService,
                                              GetDateRepository getDateRepository, SaveDateRepository saveDateRepository,
                                              GetProductRepository getProductRepository) {
        this.validateViabilityNewStateService = validateViabilityNewStateService;
        this.getDateRepository = getDateRepository;
        this.saveDateRepository = saveDateRepository;
        this.getProductRepository = getProductRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateStatus(String customerId, String day, String hour,
                                                        Long newState, UpdateDateByTechnicalCommand updateDateByTechnicalCommand, String license) {
        this.validateCodeTechnical(updateDateByTechnicalCommand.getCodeTechnical());
        this.validateCustomerId(customerId);
        this.validateDay(day);
        this.validateHour(hour);
        this.validateNewState(newState);
        DateEntity dateEntity = getDateEntity(customerId, day, hour, license);
        this.validateViabilityNewStateService.validateStateViability(newState, updateDateByTechnicalCommand.getInitialInspection(),
                getLatestStatus(dateEntity).getIdDateStatus());
        this.saveDateEntityUpdate(buildDateEntity(newState, dateEntity,
                updateDateByTechnicalCommand.getInitialInspection(),
                updateDateByTechnicalCommand));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_DATE_BY_TECHNICAL_SUCCESS.getCode(),
                EResponse.UPDATE_DATE_BY_TECHNICAL_SUCCESS.getMessage(), TIMESTAMP));
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because customer id invalid");
            throw new FieldException(EResponse.FIELD_CUSTOMER_ID_MESSAGE.getMessage(), EResponse.FIELD_CUSTOMER_ID_MESSAGE.getCode());
        }
    }

    private void validateDay(String day){
        if(day == null || day.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because day invalid");
            throw new FieldException(EResponse.DAY_NULL_MESSAGE.getMessage(), EResponse.DAY_NULL_MESSAGE.getCode());
        }
    }

    private void validateHour(String hour){
        if (hour == null || hour.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because hour invalid");
            throw new FieldException(EResponse.FIELD_HOUR_MESSAGE.getMessage(), EResponse.FIELD_HOUR_MESSAGE.getCode());
        }
    }

    private void validateNewState(Long newState){
        if (newState == null){
            LOGGER.error("ERROR-MS-CORE: Error because new state invalid");
            throw new FieldException(EResponse.NEW_STATE_INVALID.getMessage(), EResponse.NEW_STATE_INVALID.getCode());
        }
    }

    private DateEntity getDateEntity(String customerId, String day, String hour, String license){
        Optional<DateEntity> dateEntity = this.getDateRepository.getDate(customerId, day, hour, license);
        if (dateEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because date entity not exist");
            throw new CoreErrorException(EResponse.DATE_NOT_EXIST_ERROR.getMessage(), EResponse.DATE_NOT_EXIST_ERROR.getCode());
        }
        return dateEntity.get();
    }

    private DateStatusEntity getLatestStatus(DateEntity dateEntity){
        LOGGER.info("INFO-MS-CORE: Get latest status");
        return dateEntity.getAuditDateEntity().getDetailsDate()
                .get(dateEntity.getAuditDateEntity().getDetailsDate().size() - 1)
                .getDateStatusEntity();
    }

    private void validateCodeTechnical(String codeTechnical){
        if (codeTechnical == null || codeTechnical.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because codeTechnical invalid");
            throw new FieldException(EResponse.FIELD_CODE_TECHNICAL_ERROR.getMessage(), EResponse.FIELD_CODE_TECHNICAL_ERROR.getCode());
        }
    }

    private String validateNoveltyDateField(String noveltyDate){
        LOGGER.info("INFO-MS-CORE: Set novelty date");
        return noveltyDate == null || noveltyDate.isEmpty() ? SET_NOVELTY_DATE:noveltyDate;
    }

    private void saveDateEntityUpdate(DateEntity dateEntity){
        LOGGER.info("INFO-MS-CORE: Save date entity");
        this.saveDateRepository.saveDate(dateEntity);
    }

    private DateStatusEntity setNewDateStatus(Long newState){
        LOGGER.info("INFO-MS-CORE: Set new date status");
        return NewStatusUtils.build(newState);
    }

    private List<ProductEntity> setListProduct(List<ProductCommand> productCommand){
        LOGGER.info("INFO-MS-CORE: Set list product");
        List<String> productsCode = productCommand.stream()
                .map(ProductCommand::getProductCode)
                .toList();
        return getProductRepository.getProduct(productsCode);
    }

    private List<DetailsDateEntity> setDetailsDateEntity(Long newState, List<DetailsDateEntity> detailsDateEntities,
                                                         UpdateDateByTechnicalCommand updateDateByTechnicalCommand){
        LOGGER.info("INFO-MS-CORE: Set details date list");
        detailsDateEntities.add(new DetailsDateEntity(validateNoveltyDateField(updateDateByTechnicalCommand.getNoveltyDate()),
                TIMESTAMP, updateDateByTechnicalCommand.getCodeTechnical(),
                setNewDateStatus(newState), setListProduct(updateDateByTechnicalCommand.getProduct())));
        return detailsDateEntities;
    }

    private InitialInspectionEntity buildInitialInspection(InitialInspectionCommand initialInspectionCommand, DateEntity dateEntity){
        LOGGER.info("INFO-MS-CORE: Build initial inspection");
        return initialInspectionCommand == null ? dateEntity.getAuditDateEntity().getInitialInspectionEntity():
                dateEntity.getAuditDateEntity().getInitialInspectionEntity().withMileage(initialInspectionCommand.getMileage())
                .withDateInspection(TIMESTAMP)
                .withInspectionDescription(initialInspectionCommand.getInspectionDescription())
                .withFullLevel(initialInspectionCommand.getFullLevel())
                .withGeneralDescription(initialInspectionCommand.getGeneralDescription()).
                        withTermsAndCondition();
    }

    private AuditDateEntity buildNewAudit(Long newState, AuditDateEntity auditDateEntity,
                                          InitialInspectionCommand initialInspectionCommand, DateEntity dateEntity,
                                          UpdateDateByTechnicalCommand updateDateByTechnicalCommand){
        LOGGER.info("INFO-MS-CORE: Build new audit entity");
        return auditDateEntity
                .withDetailsDate(setDetailsDateEntity(newState, auditDateEntity.getDetailsDate(), updateDateByTechnicalCommand))
                .withInitialInspection(buildInitialInspection(initialInspectionCommand, dateEntity));
    }

    private DateEntity buildDateEntity(Long newState,DateEntity dateEntity,
                                       InitialInspectionCommand initialInspectionCommand,
                                       UpdateDateByTechnicalCommand updateDateByTechnicalCommand){
        LOGGER.info("INFO-MS-CORE: Build new date entity");
        return dateEntity.withAuditEntity(buildNewAudit(newState,dateEntity.getAuditDateEntity(),
                initialInspectionCommand, dateEntity, updateDateByTechnicalCommand))
                .withStatus(setNewStatus(newState, dateEntity.getStatus()));
    }

    private Boolean setNewStatus(Long newState, Boolean actualState){
        return newState == 5L ? Boolean.FALSE:actualState;
    }
}
