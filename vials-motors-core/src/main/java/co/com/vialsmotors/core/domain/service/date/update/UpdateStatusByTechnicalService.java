package co.com.vialsmotors.core.domain.service.date.update;

import co.com.vialsmotors.core.application.command.UpdateDateByTechnicalCommand;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateStatusByTechnicalService {

    ResponseEntity<GenericResponse> updateStatus(String customerId, String day, String hour, Long newState,
                                                 UpdateDateByTechnicalCommand updateDateByTechnicalCommand, String license);
}
