package co.com.vialsmotors.core.domain.service.date.validate;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.DateExistException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public class ValidateSchedulingAvailabilityImplService implements ValidateSchedulingAvailabilityService{

    private static final Logger LOGGER = Logger.getLogger(ValidateSchedulingAvailabilityImplService.class);
    private final GetListDateRepository getListDateRepository;
    @Value("${total.number.of.cite}")
    protected Integer totalNumberOfCite;

    public ValidateSchedulingAvailabilityImplService(GetListDateRepository getListDateRepository) {
        this.getListDateRepository = getListDateRepository;
    }

    @Override
    public void getListEntity(String day, String hour) {
        LOGGER.info("INFO-MS-CORE: Get list for date entity in service validate");
        List<DateEntity> dateEntityList = getListDateRepository.getListDateEntity(day, hour);
        if (dateEntityList.size() >= totalNumberOfCite){
            LOGGER.error("ERROR-MS-CORE: Error because there are no longer appointments available for that hour: " + hour + " and day: " + day);
            throw new DateExistException(EResponse.CITE_NOT_AVAILABLE.getMessage(), EResponse.CITE_NOT_AVAILABLE.getCode());
        }
    }
}
