package co.com.vialsmotors.core.domain.service.date.validate;


public interface ValidateSchedulingAvailabilityService {

    void getListEntity(String day, String hour);
}
