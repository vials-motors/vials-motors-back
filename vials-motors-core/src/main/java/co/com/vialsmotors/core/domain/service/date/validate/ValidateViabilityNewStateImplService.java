package co.com.vialsmotors.core.domain.service.date.validate;

import co.com.vialsmotors.core.application.command.InitialInspectionCommand;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import org.jboss.logging.Logger;

import java.util.Objects;

public class ValidateViabilityNewStateImplService implements ValidateViabilityNewStateService{

    private static final Logger LOGGER = Logger.getLogger(ValidateViabilityNewStateImplService.class);
    private static final Long STATE_REGISTER = 1L;
    private static final Long STATE_CANCEL = 2L;
    private static final Long STATE_RECEIVED = 3L;
    private static final Long STATE_PROCESS = 4L;
    private static final Long STATE_FINISH = 5L;


    @Override
    public void validateStateViability(Long newState, InitialInspectionCommand initialInspectionCommand, Long actualState) {
        if (newState > STATE_REGISTER && newState <= STATE_FINISH){LOGGER.info("INFO-MS-CORE: Validating states");
           this.validateStateTwo(newState);
           this.validateStateThree(newState, actualState, initialInspectionCommand);
           this.validateStateFour(newState, actualState);
           this.validateStateFive(newState, actualState);
        }else {
            throw new CoreErrorException("ERROR", "ERROR");
        }
    }

    private void validateInitialCommand(InitialInspectionCommand initialInspectionCommand){
        if (initialInspectionCommand == null){
            LOGGER.error("ERROR-MS-CORE: Error because initial command null in validateInitialCommand Method");
            throw new CoreErrorException(EResponse.INITIAL_COMMAND_ERROR.getMessage(), EResponse.INITIAL_COMMAND_ERROR.getCode());
        }
    }

    private void validateStateTwo(Long newState){
        LOGGER.info("INFO-MS-CORE: Validate state two");
        if (Objects.equals(newState, STATE_CANCEL)){
            LOGGER.error("ERROR-MS-CORE: Error because state two is invalid in validateStateTwo method");
            throw new CoreErrorException(EResponse.STATE_TWO_ERROR.getMessage(), EResponse.STATE_TWO_ERROR.getCode());
        }
    }

    private void validateStateThree(Long newState, Long actualState, InitialInspectionCommand initialInspectionCommand){
        LOGGER.info("INFO-MS-CORE: Validate state three");
        if (Objects.equals(newState, STATE_RECEIVED) && !Objects.equals(actualState, STATE_REGISTER)){
            LOGGER.error("ERROR-MS-CORE: Error because state three is invalid in validateStateThree method");
            throw new CoreErrorException(EResponse.STATE_THREE_ERROR.getMessage(), EResponse.STATE_THREE_ERROR.getCode());
        }
        if(Objects.equals(newState, STATE_RECEIVED) && Objects.equals(actualState, STATE_REGISTER)){
            LOGGER.info("INFO-MS-CORE: Validate initial command with state three");
            this.validateInitialCommand(initialInspectionCommand);
        }
    }

    private void validateStateFour(Long newState, Long actualState){
        if (Objects.equals(newState, STATE_PROCESS) && !Objects.equals(actualState, STATE_RECEIVED) && (!Objects.equals(actualState, STATE_PROCESS))){
                LOGGER.error("ERROR-MS-CORE: Error because state four is invalid in validateStateFour method");
                throw new CoreErrorException(EResponse.STATE_FOUR_ERROR.getMessage(), EResponse.STATE_FOUR_ERROR.getCode());

        }
    }

    private void validateStateFive(Long newState, Long actualState){
        if (Objects.equals(newState, STATE_FINISH) && !Objects.equals(actualState, STATE_PROCESS)) {
            LOGGER.error("ERROR-MS-CORE: Error because state five is invalid in validateStateFive method");
            throw new CoreErrorException(EResponse.STATE_FIVE_ERROR.getMessage(), EResponse.STATE_FIVE_ERROR.getCode());
        }
    }
}
