package co.com.vialsmotors.core.domain.service.date.validate;

import co.com.vialsmotors.core.application.command.InitialInspectionCommand;

public interface ValidateViabilityNewStateService {

    void validateStateViability(Long newState, InitialInspectionCommand initialInspectionCommand, Long actualState);
}
