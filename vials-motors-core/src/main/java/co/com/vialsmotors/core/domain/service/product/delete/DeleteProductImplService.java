package co.com.vialsmotors.core.domain.service.product.delete;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ProductException;
import co.com.vialsmotors.core.domain.port.product.DeleteProductRepository;
import co.com.vialsmotors.core.domain.port.product.SaveProductRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteProductImplService implements DeleteProductService{

    private static final Logger LOGGER = Logger.getLogger(DeleteProductImplService.class);
    private final DeleteProductRepository deleteProductRepository;
    private final SaveProductRepository saveProductRepository;

    public DeleteProductImplService(DeleteProductRepository deleteProductRepository, SaveProductRepository saveProductRepository) {
        this.deleteProductRepository = deleteProductRepository;
        this.saveProductRepository = saveProductRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteProduct(String productCode) {
        LOGGER.info("INFO-MS-CORE: Delete product in service with product code: " + productCode);
        this.validateProductCode(productCode);
        this.deleteProductRepository.deleteProduct(validateProductExit(productCode).getIdProduct());
        return ResponseEntity.ok(new GenericResponse(EResponse.PRODUCT_DELETE_SUCCESS.getCode(),
                EResponse.PRODUCT_DELETE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private ProductEntity validateProductExit(String productCode){
        Optional<ProductEntity> getProduct = saveProductRepository.findByCodeProduct(productCode);
        if (getProduct.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error by product not exist");
            throw new ProductException(EResponse.NULL_PRODUCT_MESSAGE.getMessage(), EResponse.NULL_PRODUCT_MESSAGE.getCode());
        }
        return getProduct.get();
    }

    private void validateProductCode(String productCode){
        if (productCode == null || productCode.isEmpty())
            throw new FieldException(EResponse.FIELD_PRODUCT_CODE_MESSAGE.getMessage(), EResponse.FIELD_PRODUCT_CODE_MESSAGE.getCode());
    }
}
