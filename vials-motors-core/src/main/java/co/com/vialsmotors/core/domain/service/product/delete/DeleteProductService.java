package co.com.vialsmotors.core.domain.service.product.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteProductService {

    ResponseEntity<GenericResponse> deleteProduct(String productCode);
}
