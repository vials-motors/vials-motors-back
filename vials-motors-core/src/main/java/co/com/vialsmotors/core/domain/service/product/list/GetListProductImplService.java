package co.com.vialsmotors.core.domain.service.product.list;

import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ListProductException;
import co.com.vialsmotors.core.domain.port.product.GetListProductRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ProductMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListProductImplService implements GetListProductService{

    private static final Logger LOGGER = Logger.getLogger(GetListProductImplService.class);

    private final GetListProductRepository getListProductRepository;

    public GetListProductImplService(GetListProductRepository getListProductRepository) {
        this.getListProductRepository = getListProductRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<ProductDTO>> getListProduct() {
        LOGGER.info("INFO-MS-CORE: Getting list of products in GetListProductImplService");
        List<ProductEntity> products = getListProductRepository.getProduct();
        this.validateList(products);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.SUCCESS_LIST_PRODUCT_MESSAGES.getCode(),
                EResponse.SUCCESS_LIST_PRODUCT_MESSAGES.getCode(),
                ProductMapper.convertListEntityToDTO(products), ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateList(List<ProductEntity> data){
        if(data.isEmpty()){
            LOGGER.info("ERROR-MS-CORE: Error getting list of products");
            throw new ListProductException(EResponse.EMPTY_LIST_PRODUCT.getMessage(), EResponse.EMPTY_LIST_PRODUCT.getMessage());
        }
    }
}
