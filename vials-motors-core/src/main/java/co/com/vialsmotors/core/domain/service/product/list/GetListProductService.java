package co.com.vialsmotors.core.domain.service.product.list;

import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListProductService {

    ResponseEntity<ListGenericResponse<ProductDTO>> getListProduct();
}
