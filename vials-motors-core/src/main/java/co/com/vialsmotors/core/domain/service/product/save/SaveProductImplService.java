package co.com.vialsmotors.core.domain.service.product.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ProductException;
import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.domain.port.product.SaveProductRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.mapper.ProductMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

public class SaveProductImplService implements SaveProductService{

   private static final Logger LOGGER  = Logger.getLogger(SaveProductImplService.class);

   private final SaveProductRepository saveProductRepository;

    public SaveProductImplService(SaveProductRepository saveProductRepository) {
        this.saveProductRepository = saveProductRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveProduct(Product product) {
        LOGGER.info("INFO-MS-CORE: Save product in saveProductImplService");
        if (validateCodeProduct(product.productCode())){
            LOGGER.info("INFO-MS-CORE: The product with code " + product.productCode() + " Is exist");
            throw new ProductException(EResponse.EXIST_PRODUCT_CODE_MESSAGE.getMessage(), EResponse.EXIST_PRODUCT_CODE_MESSAGE.getCode());
        }
        return saveProductRepository.saveProduct(ProductMapper.convertModelToEntity(product));
    }

    private Boolean validateCodeProduct(String codeProduct){
        return saveProductRepository.findByCodeProduct(codeProduct).isPresent();
    }
}
