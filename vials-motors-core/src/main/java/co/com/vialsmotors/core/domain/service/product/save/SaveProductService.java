package co.com.vialsmotors.core.domain.service.product.save;

import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveProductService {

    ResponseEntity<GenericResponse> saveProduct(Product product);
}
