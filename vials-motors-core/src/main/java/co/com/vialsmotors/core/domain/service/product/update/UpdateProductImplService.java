package co.com.vialsmotors.core.domain.service.product.update;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ProductCodeException;
import co.com.vialsmotors.core.domain.exception.ProductException;
import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.domain.port.product.SaveProductRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ProductTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public class UpdateProductImplService implements UpdateProductService{

    private static final Logger LOGGER = Logger.getLogger(UpdateProductImplService.class);

    private final SaveProductRepository saveProductRepository;

    public UpdateProductImplService(SaveProductRepository saveProductRepository) {
        this.saveProductRepository = saveProductRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateProduct(Product newProduct, String productCode) {
        LOGGER.info("INFO-MS-CORE: Update product with code " + productCode);
        if (productCode == null || productCode.isEmpty()){
            throw new ProductCodeException(EResponse.FIELD_PRODUCT_CODE_MESSAGE.getMessage(), EResponse.FIELD_PRODUCT_CODE_MESSAGE.getCode());
        }
        return this.saveProductRepository.saveProduct(this.buildNewProductEntity(newProduct, productCode));
    }

    private ProductEntity getProductEntity(String productCode) {
        Optional<ProductEntity> productEntity = this.saveProductRepository.findByCodeProduct(productCode);
        if (productEntity.isEmpty()){
            throw new ProductException(EResponse.NULL_PRODUCT_MESSAGE.getMessage(), EResponse.NULL_PRODUCT_MESSAGE.getCode());
        }
        return productEntity.get();
    }

    private ProductEntity buildNewProductEntity(Product newProduct, String productCode){
        ProductEntity productEntity = this.getProductEntity(productCode);
        return productEntity.withProductCode(newProduct.productCode())
                .withDescription(newProduct.description())
                .withPrice(newProduct.price())
                .withProductTypeEntity(ProductTypeMapper.convertModelToEntity(newProduct.productType()));
    }
}
