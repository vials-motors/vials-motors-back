package co.com.vialsmotors.core.domain.service.product.update;

import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateProductService {

    ResponseEntity<GenericResponse> updateProduct(Product newProduct, String productCode);
}
