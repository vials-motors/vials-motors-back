package co.com.vialsmotors.core.domain.service.producttype.delete;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.producttype.delete.DeleteProductTypeRepository;
import co.com.vialsmotors.core.domain.port.producttype.get.GetProductTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteProductTypeImplService implements DeleteProductTypeService{

    private static final Logger LOGGER = Logger.getLogger(DeleteProductTypeImplService.class);
    private final DeleteProductTypeRepository productTypeRepository;
    private final GetProductTypeRepository getProductTypeRepository;

    public DeleteProductTypeImplService(DeleteProductTypeRepository productTypeRepository, GetProductTypeRepository getProductTypeRepository) {
        this.productTypeRepository = productTypeRepository;
        this.getProductTypeRepository = getProductTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteProductType(Long id) {
        LOGGER.info("INFO-MS-CORE: Delete product type with id: " + id);
        this.validateId(id);
        this.validateProductTypeEntity(id);
        this.productTypeRepository.deleteProductType(id);
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_PRODUCT_TYPE_SUCCESS.getCode(),
                EResponse.DELETE_PRODUCT_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_PRODUCT_TYPE_MESSAGE.getMessage(), EResponse.FIELD_ID_PRODUCT_TYPE_MESSAGE.getCode());
        }
    }

    private void validateProductTypeEntity(Long id){
        Optional<ProductTypeEntity> productType = this.getProductTypeRepository.getProductType(id);
        if (productType.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because product type not exist");
            throw new CoreErrorException(EResponse.PRODUCT_TYPE_NOT_EXIST.getMessage(), EResponse.PRODUCT_TYPE_NOT_EXIST.getCode());
        }
    }
}
