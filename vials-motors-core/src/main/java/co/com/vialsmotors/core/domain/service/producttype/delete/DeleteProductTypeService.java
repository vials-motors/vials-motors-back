package co.com.vialsmotors.core.domain.service.producttype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteProductTypeService {

    ResponseEntity<GenericResponse> deleteProductType(Long id);
}
