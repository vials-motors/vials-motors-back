package co.com.vialsmotors.core.domain.service.producttype.list;

import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ListProductTypeException;
import co.com.vialsmotors.core.domain.port.producttype.list.GetListProductTypeRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ProductTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListProductTypeImplService implements GetListProductTypeService{

    private static final Logger LOGGER = Logger.getLogger(GetListProductTypeImplService.class);
    private final GetListProductTypeRepository getListProductTypeRepository;

    public GetListProductTypeImplService(GetListProductTypeRepository getListProductTypeRepository) {
        this.getListProductTypeRepository = getListProductTypeRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<ProductTypeDTO>> getListProductType() {
        LOGGER.info("INFO-MS-CORE: Get list in service");
        List<ProductTypeEntity> entityList = this.getListProductTypeRepository.getListProductType();
        this.validateList(entityList);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.SUCCESS_LIST_PRODUCT_TYPE.getCode(),
                EResponse.SUCCESS_LIST_PRODUCT_TYPE.getMessage(),
                ProductTypeMapper.convertListEntityToDTO(entityList),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateList(List<ProductTypeEntity> productTypeEntity){
        if (productTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error in get list for list empty");
            throw new ListProductTypeException(EResponse.EMPTY_LIST_PRODUCT_TYPE.getMessage(), EResponse.EMPTY_LIST_PRODUCT_TYPE.getCode());
        }
    }
}
