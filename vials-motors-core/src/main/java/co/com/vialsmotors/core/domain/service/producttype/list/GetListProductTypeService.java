package co.com.vialsmotors.core.domain.service.producttype.list;

import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListProductTypeService {

    ResponseEntity<ListGenericResponse<ProductTypeDTO>> getListProductType();
}
