package co.com.vialsmotors.core.domain.service.producttype.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ProductTypeExistException;
import co.com.vialsmotors.core.domain.model.ProductType;
import co.com.vialsmotors.core.domain.port.producttype.save.SaveProductTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ProductTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class SaveProductTypeImplService implements SaveProductTypeService{

    private static final Logger LOGGER = Logger.getLogger(SaveProductTypeImplService.class);
    private final SaveProductTypeRepository saveProductTypeRepository;

    public SaveProductTypeImplService(SaveProductTypeRepository saveProductTypeRepository) {
        this.saveProductTypeRepository = saveProductTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveProductType(ProductType productType) {
        LOGGER.info("INFO-MS-CORE: Save product Type in service with product type id: " + productType.idProductType());
        this.validateExistProductType(productType.idProductType());
        this.saveProductTypeRepository.saveProductType(ProductTypeMapper.convertModelToEntity(productType));
        return ResponseEntity.ok(new GenericResponse(EResponse.PRODUCT_TYPE_SAVE_SUCCESS.getCode(), EResponse.PRODUCT_TYPE_SAVE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateExistProductType(Long id){
        Optional<ProductTypeEntity> productTypeEntity = this.saveProductTypeRepository.findById(id);
        if (productTypeEntity.isPresent())
            throw new ProductTypeExistException(EResponse.PRODUCT_TYPE_EXIST_ERROR.getMessage(), EResponse.PRODUCT_TYPE_EXIST_ERROR.getCode());
    }
}
