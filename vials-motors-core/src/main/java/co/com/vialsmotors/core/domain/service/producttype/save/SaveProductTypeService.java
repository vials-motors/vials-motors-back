package co.com.vialsmotors.core.domain.service.producttype.save;

import co.com.vialsmotors.core.domain.model.ProductType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveProductTypeService {

    ResponseEntity<GenericResponse> saveProductType(ProductType productType);
}
