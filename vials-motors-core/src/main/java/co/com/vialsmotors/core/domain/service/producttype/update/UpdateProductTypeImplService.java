package co.com.vialsmotors.core.domain.service.producttype.update;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.ProductType;
import co.com.vialsmotors.core.domain.port.producttype.get.GetProductTypeRepository;
import co.com.vialsmotors.core.domain.port.producttype.save.SaveProductTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateProductTypeImplService implements UpdateProductTypeService{

    private static final Logger LOGGER = Logger.getLogger(UpdateProductTypeImplService.class);
    private final SaveProductTypeRepository saveProductTypeRepository;
    private final GetProductTypeRepository getProductTypeRepository;

    public UpdateProductTypeImplService(SaveProductTypeRepository saveProductTypeRepository, GetProductTypeRepository getProductTypeRepository) {
        this.saveProductTypeRepository = saveProductTypeRepository;
        this.getProductTypeRepository = getProductTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateProductType(ProductType productType, Long id) {
        LOGGER.info("INFO-MS-CORE: Update product type with id: " + id);
        validateId(id);
        this.saveProductTypeRepository.saveProductType(buildProductType(productType, id));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_PRODUCT_TYPE_SUCCESS.getCode(),
                EResponse.UPDATE_PRODUCT_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_PRODUCT_TYPE_MESSAGE.getMessage(), EResponse.FIELD_ID_PRODUCT_TYPE_MESSAGE.getCode());
        }
    }

    private ProductTypeEntity getProductType(Long id){
        Optional<ProductTypeEntity> productType = this.getProductTypeRepository.getProductType(id);
        if (productType.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because product type not exist");
            throw new CoreErrorException(EResponse.PRODUCT_TYPE_NOT_EXIST.getMessage(), EResponse.PRODUCT_TYPE_NOT_EXIST.getCode());
        }
        return productType.get();
    }

    private ProductTypeEntity buildProductType(ProductType productType, Long id){
        return getProductType(id).withDescription(productType.description());
    }
}
