package co.com.vialsmotors.core.domain.service.producttype.update;

import co.com.vialsmotors.core.domain.model.ProductType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateProductTypeService {

    ResponseEntity<GenericResponse> updateProductType(ProductType productType, Long id);
}
