package co.com.vialsmotors.core.domain.service.provideservice.delete;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.provideservice.DeleteServiceRepository;
import co.com.vialsmotors.core.domain.port.provideservice.GetServiceRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteServicesImplService implements DeleteServicesService{

    private static final Logger LOGGER = Logger.getLogger(DeleteServicesImplService.class);
    private final DeleteServiceRepository deleteServiceRepository;
    private final GetServiceRepository getServiceRepository;

    public DeleteServicesImplService(DeleteServiceRepository deleteServiceRepository, GetServiceRepository getServiceRepository) {
        this.deleteServiceRepository = deleteServiceRepository;
        this.getServiceRepository = getServiceRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteService(String codeService) {
        LOGGER.info("INFO-MS-CORE: Delete service with code service: " + codeService);
        this.validateCodeService(codeService);
        this.deleteServiceRepository.deleteService(getIdOfServiceEntity(codeService));
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_SERVICE_SUCCESS.getCode(),
                EResponse.DELETE_SERVICE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private Long getIdOfServiceEntity(String codeService){
        Optional<ServiceEntity> serviceEntity = this.getServiceRepository.getService(codeService);
        if (serviceEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because service not exist");
            throw new CoreErrorException(EResponse.SERVICE_NOT_EXIST_ERROR.getMessage(), EResponse.SERVICE_NOT_EXIST_ERROR.getCode());
        }
        return serviceEntity.get().getIdService();
    }

    private void validateCodeService(String codeService){
        if (codeService == null || codeService.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because code service is null or empty");
            throw new FieldException(EResponse.FIELD_CODE_SERVICE_ERROR.getMessage(), EResponse.FIELD_CODE_SERVICE_ERROR.getCode());
        }
    }
}
