package co.com.vialsmotors.core.domain.service.provideservice.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteServicesService {

    ResponseEntity<GenericResponse> deleteService(String codeService);
}
