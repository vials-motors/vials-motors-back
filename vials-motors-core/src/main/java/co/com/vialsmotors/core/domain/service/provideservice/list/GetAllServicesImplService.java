package co.com.vialsmotors.core.domain.service.provideservice.list;

import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.port.provideservice.GetAllServiceRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.infrastructure.mapper.ServiceMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.TimeZone;

public class GetAllServicesImplService implements  GetAllServicesService{

    private static final Logger LOGGER = Logger.getLogger(GetAllServicesImplService.class);
    private final GetAllServiceRepository getAllServiceRepository;

    public GetAllServicesImplService(GetAllServiceRepository getAllServiceRepository) {
        this.getAllServiceRepository = getAllServiceRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<ServiceDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Getting service");
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_SERVICE_SUCCESS.getCode(),
                EResponse.GET_LIST_SERVICE_SUCCESS.getMessage(), ServiceMapper.convertListEntityToDto(getAllServiceRepository.getAll()),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }
}
