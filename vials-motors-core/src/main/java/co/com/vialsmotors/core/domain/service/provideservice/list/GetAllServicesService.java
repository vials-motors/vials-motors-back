package co.com.vialsmotors.core.domain.service.provideservice.list;

import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllServicesService {

    ResponseEntity<ListGenericResponse<ServiceDTO>> getAll();
}
