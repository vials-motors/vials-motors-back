package co.com.vialsmotors.core.domain.service.provideservice.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.ProvideServiceException;
import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ServiceMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class SaveProvideImplService implements SaveService{

    private static final Logger LOGGER = Logger.getLogger(SaveProvideImplService.class);
    private final SaveServiceRepository saveServiceRepository;

    public SaveProvideImplService(SaveServiceRepository saveServiceRepository) {
        this.saveServiceRepository = saveServiceRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveNewService(Service service) {
        LOGGER.info("INFO-MS-CORE: Save new provide Service in Impl Service");
        this.validateProvideService(service.codeService());
        this.saveServiceRepository.saveService(ServiceMapper.convertModelToEntity(service));
        return ResponseEntity.ok(new GenericResponse(EResponse.PROVIDE_SERVICE_SAVE_SUCCESS.getCode(),
                EResponse.PROVIDE_SERVICE_SAVE_SUCCESS.getMessage(), ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    public void validateProvideService (String codeService){
        Optional<ServiceEntity> serviceEntity = this.saveServiceRepository.findByCodeService(codeService);
        if (serviceEntity.isPresent()){
            LOGGER.error("ERROR-MS-CORE: Error add service for service exist");
            throw new ProvideServiceException(EResponse.PROVIDE_SERVICE_EXIST_ERROR.getMessage(), EResponse.PROVIDE_SERVICE_EXIST_ERROR.getCode());
        }
    }


}
