package co.com.vialsmotors.core.domain.service.provideservice.save;

import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveService {

    ResponseEntity<GenericResponse> saveNewService(Service service);
}
