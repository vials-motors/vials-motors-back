package co.com.vialsmotors.core.domain.service.provideservice.update;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ServiceTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateServicesImplService implements UpdateServicesService{

    private static final Logger LOGGER = Logger.getLogger(UpdateServicesImplService.class);
    private final SaveServiceRepository saveServiceRepository;

    public UpdateServicesImplService(SaveServiceRepository saveServiceRepository) {
        this.saveServiceRepository = saveServiceRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateService(Service newService, String codeService) {
        LOGGER.info("INFO-MS-CORE: Update service with code service: " + codeService);
        this.validateCodeService(codeService);
        this.saveServiceRepository.saveService(buildNewService(codeService, newService));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_SERVICE_SUCCESS.getCode(),
                EResponse.UPDATE_SERVICE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateCodeService(String codeService){
        if (codeService == null || codeService.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because code service is null or empty");
            throw new FieldException(EResponse.FIELD_CODE_SERVICE_ERROR.getMessage(), EResponse.FIELD_CODE_SERVICE_ERROR.getCode());
        }
    }

    private ServiceEntity getServiceEntity(String codeService, String codeNewService){
        Optional<ServiceEntity> serviceEntity = saveServiceRepository.findByCodeService(codeService);
        if (serviceEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because service entity not exist");
            throw new CoreErrorException(EResponse.SERVICE_NOT_EXIST_ERROR.getMessage(), EResponse.SERVICE_NOT_EXIST_ERROR.getCode());
        }
        this.validateNewService(codeNewService, serviceEntity.get().getCodeService());
        return serviceEntity.get();
    }

    private void validateNewService(String codeService, String beforeCodeService){
        Optional<ServiceEntity> serviceEntity = saveServiceRepository.findByCodeService(codeService);
        if (serviceEntity.isPresent() && !beforeCodeService.equals(codeService)){
            LOGGER.error("ERROR-MS-CORE: Error because new service exist");
            throw new CoreErrorException(EResponse.PROVIDE_SERVICE_EXIST_ERROR.getMessage(), EResponse.PROVIDE_SERVICE_EXIST_ERROR.getCode());
        }
    }

    private ServiceEntity buildNewService(String codeService, Service service){
        return this.getServiceEntity(codeService, service.codeService()).withCodeService(service.codeService())
                .withDescription(service.description()).withAmount(service.amount())
                .withServiceType(ServiceTypeMapper.convertModelToEntity(service.serviceType()));
    }
}
