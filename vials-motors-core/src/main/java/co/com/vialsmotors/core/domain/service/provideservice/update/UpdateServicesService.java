package co.com.vialsmotors.core.domain.service.provideservice.update;

import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateServicesService {

    ResponseEntity<GenericResponse> updateService(Service newService, String codeService);
}
