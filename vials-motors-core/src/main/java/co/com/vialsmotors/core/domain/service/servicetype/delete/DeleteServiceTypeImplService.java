package co.com.vialsmotors.core.domain.service.servicetype.delete;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.servicetype.DeleteServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.GetServiceTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteServiceTypeImplService implements DeleteServiceTypeService{

    private static final Logger LOGGER = Logger.getLogger(DeleteServiceTypeImplService.class);
    private final DeleteServiceTypeRepository deleteServiceTypeRepository;
    private final GetServiceTypeRepository getServiceTypeRepository;

    public DeleteServiceTypeImplService(DeleteServiceTypeRepository deleteServiceTypeRepository, GetServiceTypeRepository getServiceTypeRepository) {
        this.deleteServiceTypeRepository = deleteServiceTypeRepository;
        this.getServiceTypeRepository = getServiceTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteService(Long id) {
        LOGGER.info("INFO-MS-CORE: Delete service type with id: " + id);
        this.validateId(id);
        this.validateServiceType(id);
        this.deleteServiceTypeRepository.deleteServiceType(id);
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_SERVICE_TYPE_SUCCESS.getCode(),
                EResponse.DELETE_SERVICE_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null ){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new FieldException(EResponse.FIELD_IS_SERVICE_TYPE.getMessage(), EResponse.FIELD_IS_SERVICE_TYPE.getCode());
        }
    }

    private void validateServiceType(Long id){
        Optional<ServiceTypeEntity> serviceTypeEntity = this.getServiceTypeRepository.getServiceType(id);
        if (serviceTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because service type not exist");
            throw new CoreErrorException(EResponse.SERVICE_TYPE_NOT_EXIST.getMessage(), EResponse.SERVICE_TYPE_NOT_EXIST.getCode());
        }
    }
}
