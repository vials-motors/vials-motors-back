package co.com.vialsmotors.core.domain.service.servicetype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteServiceTypeService {

    ResponseEntity<GenericResponse> deleteService(Long id);
}
