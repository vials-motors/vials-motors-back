package co.com.vialsmotors.core.domain.service.servicetype.list;

import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.port.servicetype.GetAllServiceTypeRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.infrastructure.mapper.ServiceTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.TimeZone;

public class GetAllServiceTypeImplService implements GetAllServiceTypeService{

    private static final Logger LOGGER = Logger.getLogger(GetAllServiceTypeImplService.class);
    private final GetAllServiceTypeRepository getAllServiceTypeRepository;

    public GetAllServiceTypeImplService(GetAllServiceTypeRepository getAllServiceTypeRepository) {
        this.getAllServiceTypeRepository = getAllServiceTypeRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<ServiceTypeDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Getting all data for service type in service");
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_SERVICE_TYPE_SUCCESS.getCode(),
                EResponse.GET_LIST_SERVICE_TYPE_SUCCESS.getMessage(),
                ServiceTypeMapper.convertListEntityToDto(getAllServiceTypeRepository.getAll()),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }
}
