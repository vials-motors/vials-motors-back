package co.com.vialsmotors.core.domain.service.servicetype.list;

import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllServiceTypeService {

    ResponseEntity<ListGenericResponse<ServiceTypeDTO>> getAll();
}
