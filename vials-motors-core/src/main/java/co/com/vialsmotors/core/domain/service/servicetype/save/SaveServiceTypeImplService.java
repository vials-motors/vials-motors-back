package co.com.vialsmotors.core.domain.service.servicetype.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.ServiceType;
import co.com.vialsmotors.core.domain.port.servicetype.GetServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.SaveServiceTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import co.com.vialsmotors.core.infrastructure.mapper.ServiceTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class SaveServiceTypeImplService implements SaveServiceTypeService{

    private static final Logger LOGGER = Logger.getLogger(SaveServiceTypeImplService.class);
    private final GetServiceTypeRepository getServiceTypeRepository;
    private final SaveServiceTypeRepository saveServiceTypeRepository;

    public SaveServiceTypeImplService(GetServiceTypeRepository getServiceTypeRepository, SaveServiceTypeRepository saveServiceTypeRepository) {
        this.getServiceTypeRepository = getServiceTypeRepository;
        this.saveServiceTypeRepository = saveServiceTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveServiceType(ServiceType serviceType) {
        this.validateServiceType(serviceType.idServiceType());
        this.saveServiceTypeRepository.saveServiceType(ServiceTypeMapper.convertModelToEntity(serviceType));
        return ResponseEntity.ok(new GenericResponse(EResponse.SERVICE_TYPE_SAVE_SUCCESS.getCode(),
                EResponse.SERVICE_TYPE_SAVE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateServiceType(Long id){
        Optional<ServiceTypeEntity> serviceTypeEntity = getServiceTypeRepository.getServiceType(id);
        if (serviceTypeEntity.isPresent()){
            LOGGER.error("ERROR-MS-CORE: Error because service type exist");
            throw new CoreErrorException(EResponse.SERVICE_TYPE_EXIST.getMessage(), EResponse.SERVICE_TYPE_EXIST.getCode());
        }
    }
}
