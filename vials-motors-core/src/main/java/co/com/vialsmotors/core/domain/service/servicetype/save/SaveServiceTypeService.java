package co.com.vialsmotors.core.domain.service.servicetype.save;

import co.com.vialsmotors.core.domain.model.ServiceType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveServiceTypeService {

    ResponseEntity<GenericResponse> saveServiceType(ServiceType serviceType);
}
