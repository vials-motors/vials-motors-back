package co.com.vialsmotors.core.domain.service.servicetype.update;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.ServiceType;
import co.com.vialsmotors.core.domain.port.servicetype.GetServiceTypeRepository;
import co.com.vialsmotors.core.domain.port.servicetype.SaveServiceTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateServiceTypeImplService implements UpdateServiceTypeService{

    private static final Logger LOGGER = Logger.getLogger(UpdateServiceTypeImplService.class);
    private final SaveServiceTypeRepository saveServiceTypeRepository;
    private final GetServiceTypeRepository getServiceTypeRepository;

    public UpdateServiceTypeImplService(SaveServiceTypeRepository saveServiceTypeRepository, GetServiceTypeRepository getServiceTypeRepository) {
        this.saveServiceTypeRepository = saveServiceTypeRepository;
        this.getServiceTypeRepository = getServiceTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateServiceType(ServiceType newServiceType, Long id) {
        LOGGER.info("INFO-MS-CORE: Update service type with id: " + id);
        this.validateId(id);
        this.saveServiceTypeRepository.saveServiceType(buildServiceType(newServiceType, id));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_SERVICE_TYPE_SUCCESS.getCode(),
                EResponse.UPDATE_SERVICE_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new CoreErrorException(EResponse.FIELD_IS_SERVICE_TYPE.getMessage(), EResponse.FIELD_IS_SERVICE_TYPE.getCode());
        }
    }

    private ServiceTypeEntity getServiceType(Long id){
        Optional<ServiceTypeEntity> serviceTypeEntity = this.getServiceTypeRepository.getServiceType(id);
        if (serviceTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because service type not exist");
            throw new CoreErrorException(EResponse.SERVICE_TYPE_NOT_EXIST.getMessage(), EResponse.SERVICE_TYPE_NOT_EXIST.getCode());
        }
        return serviceTypeEntity.get();
    }

    private ServiceTypeEntity buildServiceType(ServiceType serviceType, Long id){
        return getServiceType(id).
                withDescription(serviceType.description());
    }
}
