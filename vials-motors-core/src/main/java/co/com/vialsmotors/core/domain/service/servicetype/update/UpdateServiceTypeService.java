package co.com.vialsmotors.core.domain.service.servicetype.update;

import co.com.vialsmotors.core.domain.model.ServiceType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateServiceTypeService {

    ResponseEntity<GenericResponse> updateServiceType(ServiceType newServiceType, Long id);
}
