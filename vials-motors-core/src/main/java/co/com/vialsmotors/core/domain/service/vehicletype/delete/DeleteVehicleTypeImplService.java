package co.com.vialsmotors.core.domain.service.vehicletype.delete;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.vehicletype.DeleteVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.GetVehicleTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteVehicleTypeImplService implements DeleteVehicleTypeService{

    private static final Logger LOGGER = Logger.getLogger(DeleteVehicleTypeImplService.class);
    private final DeleteVehicleTypeRepository deleteVehicleTypeRepository;
    private final GetVehicleTypeRepository getVehicleTypeRepository;

    public DeleteVehicleTypeImplService(DeleteVehicleTypeRepository deleteVehicleTypeRepository, GetVehicleTypeRepository getVehicleTypeRepository) {
        this.deleteVehicleTypeRepository = deleteVehicleTypeRepository;
        this.getVehicleTypeRepository = getVehicleTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteVehicleType(Long id) {
        this.validateId(id);
        this.validateVehicleType(id);
        this.deleteVehicleTypeRepository.deleteVehicleType(id);
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_SERVICE_TYPE_SUCCESS.getCode(),
                EResponse.DELETE_VEHICLE_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getMessage(), EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getCode());
        }
    }

    private void validateVehicleType(Long id){
        Optional<VehicleTypeEntity> vehicleTypeEntity = this.getVehicleTypeRepository.getVehicleType(id);
        if (vehicleTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because vehicle type not exist");
            throw new CoreErrorException(EResponse.VEHICLE_TYPE_NOT_EXIST.getMessage(), EResponse.VEHICLE_TYPE_NOT_EXIST.getCode());
        }
    }
}
