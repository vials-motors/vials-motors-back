package co.com.vialsmotors.core.domain.service.vehicletype.delete;

import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteVehicleTypeService {

    ResponseEntity<GenericResponse> deleteVehicleType(Long id);
}
