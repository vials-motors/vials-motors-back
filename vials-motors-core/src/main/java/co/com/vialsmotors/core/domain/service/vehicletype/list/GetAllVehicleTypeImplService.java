package co.com.vialsmotors.core.domain.service.vehicletype.list;

import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.port.vehicletype.GetAllVehicleTypeRepository;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import co.com.vialsmotors.core.infrastructure.mapper.VehicleTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.TimeZone;

public class GetAllVehicleTypeImplService implements GetAllVehicleTypeService{

    private static final Logger LOGGER = Logger.getLogger(GetAllVehicleTypeImplService.class);
    private final GetAllVehicleTypeRepository getAllVehicleTypeRepository;

    public GetAllVehicleTypeImplService(GetAllVehicleTypeRepository getAllVehicleTypeRepository) {
        this.getAllVehicleTypeRepository = getAllVehicleTypeRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<VehicleTypeDTO>> getAll() {
        LOGGER.info("INFO-MS-CORE: Get list vehicle type in service");
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_VEHICLE_TYPE_SUCCESS.getCode(),
                EResponse.GET_LIST_VEHICLE_TYPE_SUCCESS.getMessage(),
                VehicleTypeMapper.convertListEntityToDTO(getAllVehicleTypeRepository.getAll()),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }
}
