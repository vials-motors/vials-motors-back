package co.com.vialsmotors.core.domain.service.vehicletype.list;

import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetAllVehicleTypeService {

    ResponseEntity<ListGenericResponse<VehicleTypeDTO>> getAll();
}
