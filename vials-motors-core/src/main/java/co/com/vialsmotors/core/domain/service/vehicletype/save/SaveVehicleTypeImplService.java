package co.com.vialsmotors.core.domain.service.vehicletype.save;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.VehicleType;
import co.com.vialsmotors.core.domain.port.vehicletype.GetVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.SaveVehicleTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import co.com.vialsmotors.core.infrastructure.mapper.VehicleTypeMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class SaveVehicleTypeImplService implements SaveVehicleTypeService{

    private static final Logger LOGGER = Logger.getLogger(SaveVehicleTypeImplService.class);
    private final GetVehicleTypeRepository getVehicleTypeRepository;
    private final SaveVehicleTypeRepository saveVehicleTypeRepository;

    public SaveVehicleTypeImplService(GetVehicleTypeRepository getVehicleTypeRepository, SaveVehicleTypeRepository saveVehicleTypeRepository) {
        this.getVehicleTypeRepository = getVehicleTypeRepository;
        this.saveVehicleTypeRepository = saveVehicleTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> saveVehicleType(VehicleType vehicleType) {
        LOGGER.info("INFO-MS-CORE: Save vehicle type with id: " + vehicleType.idVehicleType());
        this.validateVehicleType(vehicleType.idVehicleType());
        this.saveVehicleTypeRepository.saveVehicleType(VehicleTypeMapper.convertModelToEntity(vehicleType));
        return ResponseEntity.ok(new GenericResponse(EResponse.SAVE_VEHICLE_TYPE.getCode(),
                EResponse.SAVE_VEHICLE_TYPE.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateVehicleType(Long id){
        Optional<VehicleTypeEntity> vehicleTypeEntity = this.getVehicleTypeRepository.getVehicleType(id);
        if (vehicleTypeEntity.isPresent()){
            LOGGER.error("ERROR-MS-CORE: Error because vehicle type exist");
            throw new CoreErrorException(EResponse.VEHICLE_TYPE_EXIST.getMessage(), EResponse.VEHICLE_TYPE_EXIST.getCode());
        }
    }
}
