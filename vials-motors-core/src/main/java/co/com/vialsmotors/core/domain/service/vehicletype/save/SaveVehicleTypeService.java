package co.com.vialsmotors.core.domain.service.vehicletype.save;

import co.com.vialsmotors.core.domain.model.VehicleType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveVehicleTypeService {

    ResponseEntity<GenericResponse> saveVehicleType(VehicleType vehicleType);
}
