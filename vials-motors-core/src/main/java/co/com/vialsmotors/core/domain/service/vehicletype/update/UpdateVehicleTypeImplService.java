package co.com.vialsmotors.core.domain.service.vehicletype.update;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.VehicleType;
import co.com.vialsmotors.core.domain.port.vehicletype.GetVehicleTypeRepository;
import co.com.vialsmotors.core.domain.port.vehicletype.SaveVehicleTypeRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateVehicleTypeImplService implements UpdateVehicleTypeService{

    private static final Logger LOGGER = Logger.getLogger(UpdateVehicleTypeImplService.class);
    private final GetVehicleTypeRepository getVehicleTypeRepository;
    private final SaveVehicleTypeRepository saveVehicleTypeRepository;

    public UpdateVehicleTypeImplService(GetVehicleTypeRepository getVehicleTypeRepository, SaveVehicleTypeRepository saveVehicleTypeRepository) {
        this.getVehicleTypeRepository = getVehicleTypeRepository;
        this.saveVehicleTypeRepository = saveVehicleTypeRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateVehicleType(VehicleType newVehicleType, Long id) {
        LOGGER.info("INFO-MS-CORE: Update vehicle type with id: " + id);
        this.validateId(id);
        this.saveVehicleTypeRepository.saveVehicleType(buildVehicleType(newVehicleType, id));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_VEHICLE_TYPE_SUCCESS.getCode(),
                EResponse.UPDATE_VEHICLE_TYPE_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateId(Long id){
        if (id == null){
            LOGGER.error("ERROR-MS-CORE: Error because id is null");
            throw new FieldException(EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getMessage(), EResponse.FIELD_ID_VEHICLE_TYPE_MESSAGE.getCode());
        }
    }

    private VehicleTypeEntity getVehicleType(Long id){
        Optional<VehicleTypeEntity> vehicleTypeEntity = this.getVehicleTypeRepository.getVehicleType(id);
        if (vehicleTypeEntity.isEmpty()){
            LOGGER.error("ERROR-MS-CORE: Error because vehicle type not exist");
            throw new CoreErrorException(EResponse.VEHICLE_TYPE_NOT_EXIST.getMessage(), EResponse.VEHICLE_TYPE_NOT_EXIST.getCode());
        }
        return vehicleTypeEntity.get();
    }

    private VehicleTypeEntity buildVehicleType(VehicleType newVehicleType, Long id){
        return getVehicleType(id).withDescription(newVehicleType.description());
    }
}
