package co.com.vialsmotors.core.domain.service.vehicletype.update;

import co.com.vialsmotors.core.domain.model.VehicleType;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateVehicleTypeService {

    ResponseEntity<GenericResponse> updateVehicleType(VehicleType newVehicleType, Long id);
}
