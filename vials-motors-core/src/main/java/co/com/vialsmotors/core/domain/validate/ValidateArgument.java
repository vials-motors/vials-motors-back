package co.com.vialsmotors.core.domain.validate;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.exception.ProductTypeException;

public class ValidateArgument {

    private static final String NOT_INSTANTIABLE = "The class Validate Argument is not instantiable";

    private ValidateArgument(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static void validateFieldString(String field, String message, String status){
        if (field == null || field.isBlank())
            throw new FieldException(message, status);
    }

    public static void validateFieldDouble(Double field, String message, String status){
        if (field == null || field <= 0.0)
            throw new FieldException(message, status);
    }

    public static void validateFieldLong(Long field, String message, String status){
        if (field == null)
            throw new FieldException(message, status);
    }

    public static void validateFieldObject(Object field, String message, String status){
        if (field == null)
            throw new ProductTypeException(message, status);
    }
}
