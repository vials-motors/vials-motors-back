package co.com.vialsmotors.core.infrastructure.adapter.date;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.date.GetAllReserveRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.jpa.DateSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@RequiredArgsConstructor
public class GetAllReserveImplRepository implements GetAllReserveRepository {

    private static final Logger LOGGER = Logger.getLogger(GetAllReserveImplRepository.class);
    private final DateSqlServerRepository dateSqlServerRepository;

    @Override
    public List<DateEntity> getList(String day) {
        try {
            return dateSqlServerRepository.getAllListDay(day);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CORE: Error with message " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
