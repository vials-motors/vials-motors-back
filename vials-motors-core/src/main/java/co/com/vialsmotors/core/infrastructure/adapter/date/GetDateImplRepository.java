package co.com.vialsmotors.core.infrastructure.adapter.date;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.date.GetDateRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.jpa.DateSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetDateImplRepository implements GetDateRepository {

    private static final Logger LOGGER = Logger.getLogger(GetDateImplRepository.class);
    private final DateSqlServerRepository dateSqlServerRepository;

    @Override
    public Optional<DateEntity> getDate(String customerId, String day, String hour, String license) {
        try {
            return dateSqlServerRepository.getDateEntityByCustomer(customerId, day, hour, true, license);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error get data for customer id: " + customerId + " With message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
