package co.com.vialsmotors.core.infrastructure.adapter.date;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.date.GetListDateRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.jpa.DateSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListDateImplRepository implements GetListDateRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListDateImplRepository.class);
    private final DateSqlServerRepository dateSqlServerRepository;

    @Override
    public List<DateEntity> getListDateEntity(String day, String hour) {
        try {
            return dateSqlServerRepository.getListByDayAndHourAndStatus(hour, day, true);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error getting list find by day and hour");
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public List<DateEntity> getListActiveForAdminAndTechnical(String day, Boolean status) {
        try {
            return dateSqlServerRepository.getAllListDayActiveByAdminAndTechnical(day, status);
        }catch (Exception exception){
            LOGGER.info("ERROR-MS-CORE: Error getting list find day and status active for admin and technical with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public List<DateEntity> getListReserveActiveByClient(String customerId, Boolean status) {
        try {
            return dateSqlServerRepository.getAllListActiveByClient(customerId, status);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CORE: Error getting list find customer id active for client with customer id: " + customerId);
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public List<DateEntity> getListReserveByClient(String customerId) {
        try{
            return dateSqlServerRepository.getAllListByClient(customerId);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error getting list find customer id with customer id " + customerId);
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public List<DateEntity> getAllList() {
        try {
            return dateSqlServerRepository.findAll();
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
