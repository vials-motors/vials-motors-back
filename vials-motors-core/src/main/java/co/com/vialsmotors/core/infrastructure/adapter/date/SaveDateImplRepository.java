package co.com.vialsmotors.core.infrastructure.adapter.date;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.date.SaveDateRepository;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.jpa.DateSqlServerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class SaveDateImplRepository implements SaveDateRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveDateImplRepository.class);
    private final DateSqlServerRepository dateSqlServerRepository;
    private final ObjectMapper objectMapper;

    @Override
    public void saveDate(DateEntity dateEntity) {
        try {
            LOGGER.info("INFO-MS-CORE: Info for save " + objectMapper.writeValueAsString(dateEntity));
            Object dateSave = this.dateSqlServerRepository.save(dateEntity);
            LOGGER.info("INFO-MS-CORE: Info guardada " + objectMapper.writeValueAsString(dateSave));
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error save Date in repository with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<DateEntity> findDateByHourAndDayAndStatusAndCustomerId(String hour, String day, Boolean status, String customerId, String license) {
        LOGGER.info("INFO-MS-CORE: Find Date Entity in repository");
        return dateSqlServerRepository.findByDayAndHourAndStatusAndCustomerId(hour, day, status, customerId, license);
    }
}
