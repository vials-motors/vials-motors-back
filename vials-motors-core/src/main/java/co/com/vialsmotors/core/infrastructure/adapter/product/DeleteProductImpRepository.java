package co.com.vialsmotors.core.infrastructure.adapter.product;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.product.DeleteProductRepository;
import co.com.vialsmotors.core.infrastructure.jpa.ProductSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteProductImpRepository implements DeleteProductRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteProductImpRepository.class);
    private final ProductSqlServerRepository productSqlServerRepository;

    @Override
    public void deleteProduct(Long id) {
        try {
            productSqlServerRepository.deleteById(id);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error Delete product in repository with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
