package co.com.vialsmotors.core.infrastructure.adapter.product;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.product.GetListProductRepository;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ProductSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListProductImplRepository implements GetListProductRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListProductImplRepository.class);

    private final ProductSqlServerRepository productSqlServerRepository;

    @Override
    public List<ProductEntity> getProduct() {
        try {
            return productSqlServerRepository.findAll();
        }catch (Exception e) {
            LOGGER.error("ERROR-MS-CORE: Error getting product in GetListProductImplRepository with message " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
