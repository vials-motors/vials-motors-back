package co.com.vialsmotors.core.infrastructure.adapter.product;

import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.port.product.GetProductRepository;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ProductSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class GetProductImplRepository implements GetProductRepository {

    private static final Logger LOGGER = Logger.getLogger(GetProductImplRepository.class);
    private final ProductSqlServerRepository productSqlServerRepository;

    @Override
    public List<ProductEntity> getProduct(List<String> productCode) {
        return productCode.stream()
                .map(temporal ->  {
                    Optional<ProductEntity> product = this.productSqlServerRepository.findByProductCode(temporal);
                    if (product.isEmpty()){
                        LOGGER.error("ERROR-MS-CORE: The product with code: " + temporal + " not exist");
                        throw new CoreErrorException(EResponse.PRODUCT_NOT_EXIST.getMessage(), EResponse.PRODUCT_NOT_EXIST.getCode());
                    }
                    return product.get();
                })
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
