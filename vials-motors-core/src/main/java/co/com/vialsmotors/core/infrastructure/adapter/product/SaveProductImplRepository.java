package co.com.vialsmotors.core.infrastructure.adapter.product;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.port.product.SaveProductRepository;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ProductSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

@Repository
@RequiredArgsConstructor
public class SaveProductImplRepository implements SaveProductRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveProductImplRepository.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    private final ProductSqlServerRepository productSqlServerRepository;

    @Override
    public ResponseEntity<GenericResponse> saveProduct(ProductEntity productEntity) {
        try {
            productSqlServerRepository.save(productEntity);

            return ResponseEntity.ok(new GenericResponse(EResponse.SUCCESS_PRODUCT_MESSAGE.getCode(),
                    EResponse.SUCCESS_PRODUCT_MESSAGE.getMessage(), TIMESTAMP));
        }catch (Exception e) {
            LOGGER.error("ERR-MS-CORE: Error callback to method save Product in Save product iml Repository class with message: "+ e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<ProductEntity> findByCodeProduct(String codeProduct) {
        try {
            return productSqlServerRepository.findByProductCode(codeProduct);
        }catch (Exception e) {
            LOGGER.error("ERR-MS-CORE: Error callback to method findByCodeProduct in Save product iml Repository class with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
