package co.com.vialsmotors.core.infrastructure.adapter.producttype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.producttype.delete.DeleteProductTypeRepository;
import co.com.vialsmotors.core.infrastructure.jpa.ProductTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteProductTypeImplRepository implements DeleteProductTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteProductTypeImplRepository.class);
    private final ProductTypeSqlServerRepository productTypeSqlServerRepository;

    @Override
    public void deleteProductType(Long id) {
        try {
            LOGGER.info("INFO-MS-CORE: Delete product type with id: " + id);
            this.productTypeSqlServerRepository.deleteById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
