package co.com.vialsmotors.core.infrastructure.adapter.producttype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.producttype.list.GetListProductTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ProductTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListProductTypeImplRepository implements GetListProductTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListProductTypeImplRepository.class);
    private final ProductTypeSqlServerRepository productTypeSqlServerRepository;

    @Override
    public List<ProductTypeEntity> getListProductType() {
        try {
            return this.productTypeSqlServerRepository.findAll();
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error call database for get list: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
