package co.com.vialsmotors.core.infrastructure.adapter.provideservice;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.provideservice.DeleteServiceRepository;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteServiceImplRepository implements DeleteServiceRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteServiceImplRepository.class);
    private final ServiceSqlServerRepository serviceSqlServerRepository;

    @Override
    public void deleteService(Long id) {
        try {
            LOGGER.info("Delete service with id: " + id);
            serviceSqlServerRepository.deleteById(id);
        }catch (Exception exception){
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
