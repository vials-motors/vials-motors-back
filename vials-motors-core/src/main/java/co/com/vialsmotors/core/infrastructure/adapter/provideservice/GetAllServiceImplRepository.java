package co.com.vialsmotors.core.infrastructure.adapter.provideservice;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.provideservice.GetAllServiceRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetAllServiceImplRepository implements GetAllServiceRepository {

    private static final Logger LOGGER = Logger.getLogger(GetAllServiceImplRepository.class);
    private final ServiceSqlServerRepository serviceSqlServerRepository;

    @Override
    public List<ServiceEntity> getAll() {
        try {
            return serviceSqlServerRepository.findAll();
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error getting service with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
