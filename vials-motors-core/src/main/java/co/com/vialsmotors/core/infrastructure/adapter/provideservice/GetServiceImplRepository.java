package co.com.vialsmotors.core.infrastructure.adapter.provideservice;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.provideservice.GetServiceRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetServiceImplRepository implements GetServiceRepository {

    private static final Logger LOGGER = Logger.getLogger(GetServiceImplRepository.class);
    private final ServiceSqlServerRepository serviceSqlServerRepository;

    @Override
    public Optional<ServiceEntity> getService(String codeService) {
        try {
            LOGGER.info("INFO-MS-CORE: Get service with code service: " + codeService);
            return serviceSqlServerRepository.findByCodeService(codeService);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
