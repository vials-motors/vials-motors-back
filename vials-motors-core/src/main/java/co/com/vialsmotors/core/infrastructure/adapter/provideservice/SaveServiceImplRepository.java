package co.com.vialsmotors.core.infrastructure.adapter.provideservice;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.provideservice.SaveServiceRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class SaveServiceImplRepository implements SaveServiceRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveServiceImplRepository.class);
    private final ServiceSqlServerRepository serviceSqlServerRepository;

    @Override
    public void saveService(ServiceEntity serviceEntity) {
        try {
            LOGGER.info("INFO-MS-CORE: Save service entity in repository");
            this.serviceSqlServerRepository.save(serviceEntity);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CORE: Error save new service in repository with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<ServiceEntity> findByCodeService(String codeService) {
        LOGGER.info("INFO-MS-CORE: Find Service entity by id: " + codeService);
        return serviceSqlServerRepository.findByCodeService(codeService);
    }
}
