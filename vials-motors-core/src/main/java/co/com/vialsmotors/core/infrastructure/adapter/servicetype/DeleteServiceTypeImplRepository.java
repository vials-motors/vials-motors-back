package co.com.vialsmotors.core.infrastructure.adapter.servicetype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.servicetype.DeleteServiceTypeRepository;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteServiceTypeImplRepository implements DeleteServiceTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteServiceTypeImplRepository.class);
    private final ServiceTypeSqlServerRepository serviceTypeSqlServerRepository;

    @Override
    public void deleteServiceType(Long id) {
        try {
            LOGGER.info("INFO-MS-CORE: Delete service type with id: " + id);
            this.serviceTypeSqlServerRepository.deleteById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
