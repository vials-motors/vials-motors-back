package co.com.vialsmotors.core.infrastructure.adapter.servicetype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.servicetype.GetAllServiceTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetAllServiceTypeImplRepository implements GetAllServiceTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetAllServiceTypeImplRepository.class);
    private final ServiceTypeSqlServerRepository serviceTypeSqlServerRepository;

    @Override
    public List<ServiceTypeEntity> getAll() {
        try {
            return serviceTypeSqlServerRepository.findAll();
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error getting all service type data with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
