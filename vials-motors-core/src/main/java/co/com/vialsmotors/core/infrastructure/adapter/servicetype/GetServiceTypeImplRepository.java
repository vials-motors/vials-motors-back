package co.com.vialsmotors.core.infrastructure.adapter.servicetype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.servicetype.GetServiceTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetServiceTypeImplRepository implements GetServiceTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetServiceTypeImplRepository.class);
    private final ServiceTypeSqlServerRepository serviceTypeSqlServerRepository;

    @Override
    public Optional<ServiceTypeEntity> getServiceType(Long id) {
        try {
            LOGGER.info("INFO-MS-CORE: Get service type with id: " + id);
            return serviceTypeSqlServerRepository.findById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
