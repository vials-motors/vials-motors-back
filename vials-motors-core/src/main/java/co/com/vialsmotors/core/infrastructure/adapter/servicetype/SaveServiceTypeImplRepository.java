package co.com.vialsmotors.core.infrastructure.adapter.servicetype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.servicetype.SaveServiceTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.ServiceTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SaveServiceTypeImplRepository implements SaveServiceTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveServiceTypeImplRepository.class);
    private final ServiceTypeSqlServerRepository serviceTypeSqlServerRepository;

    @Override
    public void saveServiceType(ServiceTypeEntity serviceTypeEntity) {
        try {
            LOGGER.info("INFO-MS-CORE: Save service type entity: " + serviceTypeEntity.getDescription());
            this.serviceTypeSqlServerRepository.save(serviceTypeEntity);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
