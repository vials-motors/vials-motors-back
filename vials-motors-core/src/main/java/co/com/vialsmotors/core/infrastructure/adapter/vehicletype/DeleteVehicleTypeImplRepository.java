package co.com.vialsmotors.core.infrastructure.adapter.vehicletype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.vehicletype.DeleteVehicleTypeRepository;
import co.com.vialsmotors.core.infrastructure.jpa.VehicleTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteVehicleTypeImplRepository implements DeleteVehicleTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteVehicleTypeImplRepository.class);
    private final VehicleTypeSqlServerRepository vehicleTypeSqlServerRepository;

    @Override
    public void deleteVehicleType(Long id) {
        try {
            LOGGER.info("INFO-MS-CORE: Delete vehicle type with id: " + id);
            this.vehicleTypeSqlServerRepository.deleteById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
