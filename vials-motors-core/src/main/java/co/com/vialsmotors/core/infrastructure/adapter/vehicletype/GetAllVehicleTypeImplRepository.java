package co.com.vialsmotors.core.infrastructure.adapter.vehicletype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.vehicletype.GetAllVehicleTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.VehicleTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetAllVehicleTypeImplRepository implements GetAllVehicleTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetAllVehicleTypeImplRepository.class);
    private final VehicleTypeSqlServerRepository vehicleTypeSqlServerRepository;

    @Override
    public List<VehicleTypeEntity> getAll() {
        try {
            return vehicleTypeSqlServerRepository.findAll();
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CORE: Error get all data for vehicle type with message " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
