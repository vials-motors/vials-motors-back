package co.com.vialsmotors.core.infrastructure.adapter.vehicletype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.vehicletype.GetVehicleTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.VehicleTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetVehicleTypeImplRepository implements GetVehicleTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(GetVehicleTypeImplRepository.class);
    private final VehicleTypeSqlServerRepository vehicleTypeSqlServerRepository;

    @Override
    public Optional<VehicleTypeEntity> getVehicleType(Long id) {
        try {
            LOGGER.info("INFO-MS-CORE: Get vehicle type with id: " + id);
            return vehicleTypeSqlServerRepository.findById(id);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
