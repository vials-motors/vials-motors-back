package co.com.vialsmotors.core.infrastructure.adapter.vehicletype;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.core.domain.port.vehicletype.SaveVehicleTypeRepository;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import co.com.vialsmotors.core.infrastructure.jpa.VehicleTypeSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SaveVehicleTypeImplRepository implements SaveVehicleTypeRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveVehicleTypeImplRepository.class);
    private final VehicleTypeSqlServerRepository vehicleTypeSqlServerRepository;

    @Override
    public void saveVehicleType(VehicleTypeEntity vehicleTypeEntity) {
        try {
            LOGGER.info("INFO-MS-CORE: Save vehicle type with id: " + vehicleTypeEntity.getIdVehicleType());
            this.vehicleTypeSqlServerRepository.save(vehicleTypeEntity);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
