package co.com.vialsmotors.core.infrastructure.apirest.client;

import co.com.vialsmotors.core.infrastructure.apirest.response.GetCredentialResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(name = "feignClientCredential", url = "${external.service.credential}")
public interface CredentialService {


    @GetMapping("/login/get-credential")
    ResponseEntity<GetCredentialResponse> getCredential(@RequestHeader Map<String, Object> headers);
}
