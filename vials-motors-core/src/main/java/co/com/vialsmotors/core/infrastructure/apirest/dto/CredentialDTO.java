package co.com.vialsmotors.core.infrastructure.apirest.dto;

public record CredentialDTO(String username, String password, String email, String customerID) {
}
