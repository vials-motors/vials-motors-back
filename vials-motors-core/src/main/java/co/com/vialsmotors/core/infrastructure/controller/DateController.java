package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.DateCommand;
import co.com.vialsmotors.core.application.command.UpdateDateByTechnicalCommand;
import co.com.vialsmotors.core.application.command.UpdateDateCommand;
import co.com.vialsmotors.core.application.handler.date.delete.DeleteDateHandler;
import co.com.vialsmotors.core.application.handler.date.list.*;
import co.com.vialsmotors.core.application.handler.date.save.SaveDateHandler;
import co.com.vialsmotors.core.application.handler.date.update.UpdateDateByCustomerHandler;
import co.com.vialsmotors.core.application.handler.date.update.UpdateDateByTechnicalHandler;
import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/date")
@CrossOrigin(origins = "http://localhost:4200")
public class DateController {

    private static final Logger LOGGER = Logger.getLogger(DateController.class);

    private final SaveDateHandler saveDateHandler;
    private final DeleteDateHandler deleteDateHandler;
    private final GetAllListReserveHandler getAllListReserveHandler;
    private final GetListReserveActiveForAdminAndTechnicalHandler getListReserveActiveForAdminAndTechnicalHandler;
    private final GetListActiveByClientHandler getListActiveByClientHandler;
    private final GetAllListReserveClientHandler getAllListReserveClientHandler;
    private final UpdateDateByCustomerHandler updateDateByCustomerHandler;
    private final UpdateDateByTechnicalHandler updateDateByTechnicalHandler;
    private final GetAvailableHoursForDayHandler getAvailableHoursForDayHandler;
    private final ObjectMapper objectMapper;

    @PostMapping(path = "/save")
    public ResponseEntity<GenericResponse> saveDate(@RequestBody DateCommand dateCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for save date with body request: " + objectMapper.writeValueAsString(dateCommand));
        return saveDateHandler.saveDate(dateCommand);
    }

    @PutMapping(path = "/delete")
    public ResponseEntity<GenericResponse> deleteDate(@RequestParam("deleteRequest") Integer deleteRequest, @RequestParam("day") String day,
                                                      @RequestParam("hour") String hour, @RequestParam("customerId") String customerId,
                                                      @RequestParam("license") String license){
        LOGGER.info("INFO-MS-CORE: Receiver request for delete date with day: " + day);
        LOGGER.info("INFO-MS-CORE: Receiver request for delete date with hour: " + hour);
        LOGGER.info("INFO-MS-CORE: Receiver request for delete date with Customer Id: " + customerId);
        return deleteDateHandler.deleteDate(deleteRequest, day, hour, customerId, license);
    }

    @GetMapping("/list-all")
    public ResponseEntity<ListGenericResponse<DateDTO>> getAllList(@RequestParam("day") String day){
        LOGGER.info("INFO-MS-CORE: Receiver request for get all list in day: " + day);
        return getAllListReserveHandler.getAllList(day);
    }

    @GetMapping("/list-all-active-internal")
    public ResponseEntity<ListGenericResponse<DateDTO>> getListActiveAdminAndTechnical(@RequestParam("day") String day){
        LOGGER.info("INFO-MS-CORE: Receiver request for get all list active in day for admin and technical: " + day);
        return getListReserveActiveForAdminAndTechnicalHandler.getListReserve(day);
    }

    @GetMapping("/list-active-customer")
    public ResponseEntity<ListGenericResponse<DateDTO>> getListByClient(@RequestParam("customerId") String customerId){
        LOGGER.info("INFO-MS-CORE: Receiver request for get list active by client with customer id: " + customerId);
        return getListActiveByClientHandler.getList(customerId);
    }

    @GetMapping("/list-customer")
    public ResponseEntity<ListGenericResponse<DateDTO>> getAllListByIdentificationNumber(@RequestParam("identificationNumber") String identificationNumber){
        LOGGER.info("INFO-MS-CORE: Receiver request for get list for client by identification number: " + identificationNumber);
        return getAllListReserveClientHandler.getList(identificationNumber);
    }

    @PutMapping("/update-by-customer")
    public ResponseEntity<GenericResponse> updateDateByCustomer(@RequestHeader("customerId") String customerId, @RequestHeader("day") String day,
                                                                @RequestHeader("hour") String hour,
                                                                @RequestHeader("license") String license,
                                                                @RequestBody UpdateDateCommand updateDateCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update date by customer with customer id: " + customerId);
        LOGGER.info("INFO-MS-CORE: Receiver request for update date by customer with body: " + objectMapper.writeValueAsString(updateDateCommand));
        return updateDateByCustomerHandler.updateHandler(customerId, day, hour, updateDateCommand, license);
    }

    @PutMapping("/update-by-technical")
    public ResponseEntity<GenericResponse> updateDateByTechnical(@RequestHeader("customerId") String customerId, @RequestHeader("day") String day,
                                                                 @RequestHeader("hour") String hour, @RequestHeader("newState") Long newState,
                                                                 @RequestHeader("license") String license,
                                                                 @RequestBody UpdateDateByTechnicalCommand updateDateByTechnicalCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update date by technical with body: " + objectMapper.writeValueAsString(updateDateByTechnicalCommand));
        LOGGER.info("INFO-MS-CORE: Receiver request for update date by technical with customerId: " + customerId);
        return updateDateByTechnicalHandler.updateDate(customerId, day, hour, newState, updateDateByTechnicalCommand, license);
    }

    @GetMapping(path = "/available-hour")
    public ResponseEntity<ListGenericResponse<String>> getAvailableHours(@RequestParam("day") String day){
        LOGGER.info("INFO-MS-CORE: Receiver request for get list available hours for day with day: " + day);
        return getAvailableHoursForDayHandler.getList(day);
    }
}
