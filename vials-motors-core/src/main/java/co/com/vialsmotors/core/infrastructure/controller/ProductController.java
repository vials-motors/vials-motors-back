package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.ProductCommand;
import co.com.vialsmotors.core.application.handler.product.delete.DeleteProductHandler;
import co.com.vialsmotors.core.application.handler.product.list.GetListProductHandler;
import co.com.vialsmotors.core.application.handler.product.save.SaveProductHandler;
import co.com.vialsmotors.core.application.handler.product.update.UpdateProductHandler;
import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

    private static final Logger LOGGER = Logger.getLogger(ProductController.class);

    private final SaveProductHandler saveProductHandler;
    private final GetListProductHandler getListProductHandler;
    private final UpdateProductHandler updateProductHandler;
    private final DeleteProductHandler deleteProductHandler;
    private final ObjectMapper objectMapper;

    @PostMapping(path = "/save")
    public ResponseEntity<GenericResponse> saveProduct(@RequestBody ProductCommand productCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Request receiver for save product in Product Controller with Body request: " + objectMapper.writeValueAsString(productCommand));
        return saveProductHandler.saveProduct(productCommand);
    }

    @GetMapping(path = "/get-list")
    public ResponseEntity<ListGenericResponse<ProductDTO>> getListProduct(){
        LOGGER.info("INFO-MS-CORE: Request receiver for getListProduct in ProductController");
        return getListProductHandler.getListProduct();
    }

    @PutMapping(path = "/update")
    public ResponseEntity<GenericResponse> updateProduct(@RequestBody ProductCommand productCommand, @PathParam("productCode") String productCode) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Request receiver for update product in Product Controller with Body request: " + objectMapper.writeValueAsString(productCommand));
        LOGGER.info("INFO-MS-CORE: Request receiver for update product in Product Controller with productCode " + productCode);
        return updateProductHandler.updateProduct(productCommand, productCode);
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<GenericResponse> deleteProduct(@RequestParam("productCode") String productCode){
        LOGGER.info("INFO-MS-CORE: Request receiver for delete product in Product Controller with productCode " + productCode);
        return deleteProductHandler.deleteProduct(productCode);
    }
}
