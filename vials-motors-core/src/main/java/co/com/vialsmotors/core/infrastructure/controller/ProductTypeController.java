package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.ProductTypeCommand;
import co.com.vialsmotors.core.application.handler.producttype.delete.DeleteProductTypeHandler;
import co.com.vialsmotors.core.application.handler.producttype.list.GetListProductTypeHandler;
import co.com.vialsmotors.core.application.handler.producttype.save.SaveProductTypeHandler;
import co.com.vialsmotors.core.application.handler.producttype.update.UpdateProductTypeHandler;
import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product-type")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductTypeController {

    private static final Logger LOGGER = Logger.getLogger(ProductTypeController.class);

    private final ObjectMapper objectMapper;
    private final SaveProductTypeHandler saveProductTypeHandler;
    private final GetListProductTypeHandler getListProductTypeHandler;
    private final UpdateProductTypeHandler updateProductTypeHandler;
    private final DeleteProductTypeHandler deleteProductTypeHandler;

    @PostMapping(value = "/save")
    public ResponseEntity<GenericResponse> saveProductType(@RequestBody ProductTypeCommand productTypeCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request by product type save with request: " + objectMapper.writeValueAsString(productTypeCommand));
        return saveProductTypeHandler.saveProductType(productTypeCommand);
    }

    @GetMapping
    public ResponseEntity<ListGenericResponse<ProductTypeDTO>> getProductTypeList(){
        LOGGER.info("INFO-MS-CORE: Receiver request for get list product type");
        return getListProductTypeHandler.getListProductType();
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateProductType(@RequestBody ProductTypeCommand productTypeCommand, @RequestParam("id") Long id) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update product type with id: " + id);
        LOGGER.info("INFO-MS-CORE: Receiver request for update product type with body: " + objectMapper.writeValueAsString(productTypeCommand));
        return updateProductTypeHandler.updateProducType(productTypeCommand, id);
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse> deleteProductType(@RequestParam("id") Long id){
        LOGGER.info("INFO-MS-CORE: Receiver request for delete product type with id: " + id);
        return deleteProductTypeHandler.deleteProductType(id);
    }
}
