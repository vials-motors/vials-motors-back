package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.ServiceCommand;
import co.com.vialsmotors.core.application.handler.provideservice.delete.DeleteServiceHandler;
import co.com.vialsmotors.core.application.handler.provideservice.list.GetAllServicesHandler;
import co.com.vialsmotors.core.application.handler.provideservice.save.SaveServiceHandler;
import co.com.vialsmotors.core.application.handler.provideservice.update.UpdateServicesHandler;
import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/service")
@CrossOrigin(origins = "http://localhost:4200")
public class ServiceController {

    private static final Logger LOGGER = Logger.getLogger(ServiceController.class);

    private final SaveServiceHandler saveServiceHandler;
    private final GetAllServicesHandler getAllServicesHandler;
    private final DeleteServiceHandler deleteServiceHandler;
    private final UpdateServicesHandler updateServicesHandler;
    private final ObjectMapper objectMapper;

    @PostMapping
    public ResponseEntity<GenericResponse> saveNewService(@RequestBody ServiceCommand serviceCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for save new service in Service controller with body request: " + objectMapper.writeValueAsString(serviceCommand));
        return saveServiceHandler.saveProvideService(serviceCommand);
    }

    @GetMapping
    public ResponseEntity<ListGenericResponse<ServiceDTO>> getAllServices(){
        LOGGER.info("INFO-MS-CORE: Receiver request for get all services");
        return getAllServicesHandler.getAll();
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse> deleteService(@RequestParam("codeService") String codeService){
        LOGGER.info("INFO-MS-CORE: Receiver request for delete service with code service: " + codeService);
        return deleteServiceHandler.deleteService(codeService);
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateService(@RequestBody ServiceCommand serviceCommand, @RequestParam("codeService") String codeService) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update service with code service: " + codeService);
        LOGGER.info("INFO-MS-CORE: Receiver request for update service with body: " + objectMapper.writeValueAsString(serviceCommand));
        return updateServicesHandler.updateService(serviceCommand, codeService);
    }
}
