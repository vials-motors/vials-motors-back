package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.ServiceTypeCommand;
import co.com.vialsmotors.core.application.handler.servicetype.delete.DeleteServiceTypeHandler;
import co.com.vialsmotors.core.application.handler.servicetype.list.GetAllServiceTypeHandler;
import co.com.vialsmotors.core.application.handler.servicetype.save.SaveServiceTypeHandler;
import co.com.vialsmotors.core.application.handler.servicetype.update.UpdateServiceTypeHandler;
import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/service-type")
@CrossOrigin(origins = "http://localhost:4200")
public class ServiceTypeController {

    private static final Logger LOGGER = Logger.getLogger(ServiceTypeController.class);

    private final GetAllServiceTypeHandler getAllServiceTypeHandler;
    private final SaveServiceTypeHandler saveServiceTypeHandler;
    private final DeleteServiceTypeHandler deleteServiceTypeHandler;
    private final UpdateServiceTypeHandler updateServiceTypeHandler;
    private final ObjectMapper objectMapper;

    @GetMapping
    public ResponseEntity<ListGenericResponse<ServiceTypeDTO>> getAll(){
        LOGGER.info("INFO-MS-CORE: Receiver request for get all data of service type");
        return getAllServiceTypeHandler.getAll();
    }

    @PostMapping
    public ResponseEntity<GenericResponse> saveServiceType(@RequestBody ServiceTypeCommand serviceTypeCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for save service type with body: " + objectMapper.writeValueAsString(serviceTypeCommand));
        return saveServiceTypeHandler.saveServiceType(serviceTypeCommand);
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse> deleteServiceType(@RequestParam("id") Long id){
        LOGGER.info("INFO-MS-CORE: Receiver request for delete service type with id: " + id);
        return deleteServiceTypeHandler.deleteServiceType(id);
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateServiceType(@RequestBody ServiceTypeCommand serviceTypeCommand, @RequestParam("id") Long id) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update service type with body: " + objectMapper.writeValueAsString(serviceTypeCommand));
        LOGGER.info("INFO-MS-CORE: Receiver request for update service type with id: " + id);
        return updateServiceTypeHandler.updateServiceType(serviceTypeCommand, id);
    }
}
