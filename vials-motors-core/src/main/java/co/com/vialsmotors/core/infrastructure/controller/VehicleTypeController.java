package co.com.vialsmotors.core.infrastructure.controller;

import co.com.vialsmotors.core.application.command.VehicleTypeCommand;
import co.com.vialsmotors.core.application.handler.vehicletype.delete.DeleteVehicleTypeHandler;
import co.com.vialsmotors.core.application.handler.vehicletype.list.GetAllVehicleTypeHandler;
import co.com.vialsmotors.core.application.handler.vehicletype.save.SaveVehicleTypeHandler;
import co.com.vialsmotors.core.application.handler.vehicletype.update.UpdateVehicleTypeHandler;
import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import co.com.vialsmotors.core.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/vehicle-type")
@CrossOrigin(origins = "http://localhost:4200")
public class VehicleTypeController {
    private static final Logger LOGGER = Logger.getLogger(VehicleTypeController.class);

    private final GetAllVehicleTypeHandler getAllVehicleTypeHandler;
    private final SaveVehicleTypeHandler saveVehicleTypeHandler;
    private final DeleteVehicleTypeHandler deleteVehicleTypeHandler;
    private final UpdateVehicleTypeHandler updateVehicleTypeHandler;
    private final ObjectMapper objectMapper;

    @GetMapping
    public ResponseEntity<ListGenericResponse<VehicleTypeDTO>> getAll(){
        LOGGER.info("INFO-MS-CORE: Receiver request for get list of vehicle type");
        return getAllVehicleTypeHandler.getAll();
    }

    @PostMapping
    public ResponseEntity<GenericResponse> saveVehicleType(@RequestBody VehicleTypeCommand vehicleTypeCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for save vehicle type with body: " + objectMapper.writeValueAsString(vehicleTypeCommand));
        return saveVehicleTypeHandler.saveVehicleType(vehicleTypeCommand);
    }

    @DeleteMapping
    public ResponseEntity<GenericResponse> deleteVehicleType(@RequestParam("id") Long id){
        LOGGER.info("INFO-MS-CORE: Receiver request for delete vehicle type with id: " + id);
        return deleteVehicleTypeHandler.deleteVehicleType(id);
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateVehicleType(@RequestParam("id") Long id, @RequestBody VehicleTypeCommand vehicleTypeCommand) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CORE: Receiver request for update vehicle type with body: " + objectMapper.writeValueAsString(vehicleTypeCommand));
        LOGGER.info("INFO-MS-CORE: Receiver request for update vehicle type with id: " + id);
        return updateVehicleTypeHandler.updateVehicleType(vehicleTypeCommand, id);
    }
}
