package co.com.vialsmotors.core.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "audit_date")
public class AuditDateEntity {

    @Id
    @Column(name = "id_audit_date")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAuditDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_initial_inspection")
    private InitialInspectionEntity initialInspectionEntity;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "audit_detail_list",
        joinColumns = @JoinColumn(name = "id_audit_date"),
        inverseJoinColumns = @JoinColumn(name = "id_details_date"))
    private List<DetailsDateEntity> detailsDate = new ArrayList<>();

    public AuditDateEntity(InitialInspectionEntity initialInspectionEntity, List<DetailsDateEntity> detailsDate) {
        this.initialInspectionEntity = initialInspectionEntity;
        this.detailsDate = detailsDate;
    }

    public AuditDateEntity withDetailsDate(List<DetailsDateEntity> detailsDate){
        this.detailsDate = detailsDate;
        return this;
    }

    public AuditDateEntity withInitialInspection(InitialInspectionEntity initialInspectionEntity){
        this.initialInspectionEntity = initialInspectionEntity;
        return this;
    }
}
