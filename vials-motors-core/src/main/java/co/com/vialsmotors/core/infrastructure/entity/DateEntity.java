package co.com.vialsmotors.core.infrastructure.entity;

import co.com.vialsmotors.core.infrastructure.utils.BuildDateUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "date", indexes = {
        @Index(name = "idx_date_entity_day_hour", columnList = """
                day,
                hour,
                status,
                id_customer,
                license_plate
                """)
})
public class DateEntity {

    @Id
    @Column(name = "id_date")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDate;

    @Column(name = "day", nullable = false)
    private String day;

    @Column(name = "hour", nullable = false)
    private String hour;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "id_service")
    private ServiceEntity serviceEntity;

    @Column(name = "id_customer")
    private String customerId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_audit_date")
    private AuditDateEntity auditDateEntity;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "license_plate")
    private VehicleEntity vehicleEntity;

    @Column(name = "day_date")
    private ZonedDateTime dayDate;

    public DateEntity(String day, String hour,Boolean status, ServiceEntity serviceEntity, String customerId,
                      AuditDateEntity auditDateEntity, VehicleEntity vehicleEntity) {
        this.day = day;
        this.hour = hour;
        this.status = status;
        this.serviceEntity = serviceEntity;
        this.customerId = customerId;
        this.auditDateEntity = auditDateEntity;
        this.vehicleEntity = vehicleEntity;
        this.dayDate = BuildDateUtils.buildDate(this.day, this.hour);
    }

    public DateEntity withAuditEntity(AuditDateEntity auditDateEntity){
        this.auditDateEntity = auditDateEntity;
        return this;
    }

    public DateEntity withStatus(Boolean status){
        this.status = status;
        return this;
    }

    public DateEntity withDay(String day){
        this.day =  day;
        return this;
    }

    public DateEntity withHour(String hour){
        this.hour = hour;
        return this;
    }

    public DateEntity withService(ServiceEntity service){
        this.serviceEntity = service;
        return this;
    }
}
