package co.com.vialsmotors.core.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "details_date")
public class DetailsDateEntity {

    @Id
    @Column(name = "id_details_date")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDetailsDate;

    @Column(name = "novelty_date")
    private String noveltyDate;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "id_technical")
    private String technicalCode;

    @ManyToOne
    @JoinColumn(name = "id_date_status")
    private DateStatusEntity dateStatusEntity;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "detail_input",
        joinColumns = @JoinColumn(name = "id_details_date"),
        inverseJoinColumns = @JoinColumn(name = "id_product"))
    private List<ProductEntity> productEntityList = new ArrayList<>();

    public DetailsDateEntity(String noveltyDate, ZonedDateTime updateDate, String technicalCode, DateStatusEntity dateStatusEntity,
                             List<ProductEntity> productEntityList) {
        this.noveltyDate = noveltyDate;
        this.updateDate = updateDate;
        this.technicalCode = technicalCode;
        this.dateStatusEntity = dateStatusEntity;
        this.productEntityList = productEntityList;
    }
}
