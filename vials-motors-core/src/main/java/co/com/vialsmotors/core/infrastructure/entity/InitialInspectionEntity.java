package co.com.vialsmotors.core.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "initial_inspection", indexes = {
        @Index(name = "idx_initial_inspection_entity", columnList = "customer_id")
})
public class InitialInspectionEntity {

    private static final String ACCEPT_TERMS_AND_CONDITION = "Terminos y condiciones aceptado por el usuario con customer id: ";

    @Id
    @Column(name = "id_initial_inspection")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInitialInspection;

    @Column(name = "customer_id", nullable = false)
    private String customerId;

    @Column(name = "mileage")
    private String mileage;

    @Column(name = "date_inspection")
    private ZonedDateTime dateInspection;

    @Column(name = "inspection_description")
    private String inspectionDescription;

    @Column(name = "full_level")
    private String fullLevel;

    @Column(name = "general_description")
    private String generalDescription;

    @Column(name = "terms_and_condition")
    private String termsAndCondition;

    public InitialInspectionEntity(String customerId) {
        this.customerId = customerId;
    }

    public InitialInspectionEntity withMileage(String mileage){
        this.mileage = mileage;
        return this;
    }

    public InitialInspectionEntity withDateInspection(ZonedDateTime dateInspection){
        this.dateInspection = dateInspection;
        return this;
    }

    public InitialInspectionEntity withInspectionDescription(String inspectionDescription){
        this.inspectionDescription = inspectionDescription;
        return this;
    }

    public InitialInspectionEntity withFullLevel(String fullLevel){
        this.fullLevel = fullLevel;
        return this;
    }

    public InitialInspectionEntity withGeneralDescription(String generalDescription){
        this.generalDescription = generalDescription;
        return this;
    }

    public InitialInspectionEntity withTermsAndCondition(){
        this.termsAndCondition = ACCEPT_TERMS_AND_CONDITION.concat(this.customerId);
        return this;
    }
}
