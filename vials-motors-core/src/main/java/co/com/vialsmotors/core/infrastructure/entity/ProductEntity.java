package co.com.vialsmotors.core.infrastructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "product", indexes = {
        @Index(name = "idx_product_entity", columnList = "product_code")
})
@Getter
@Setter
@NoArgsConstructor
public class ProductEntity {

    @Id
    @Column(name = "id_product")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProduct;

    @Column(name = "product_code", nullable = false, length = 250)
    private String productCode;

    @Column(name = "description", nullable = false, length = 250)
    private String description;

    @Column(name = "price", nullable = false)
    private Double price;

    @ManyToOne
    @JoinColumn(name = "id_product_type")
    private ProductTypeEntity productTypeEntity;

    public ProductEntity(String productCode, String description, Double price, ProductTypeEntity productTypeEntity) {
        this.productCode = productCode;
        this.description = description;
        this.price = price;
        this.productTypeEntity = productTypeEntity;
    }

    public ProductEntity withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public ProductEntity withDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductEntity withPrice(Double price) {
        this.price = price;
        return this;
    }

    public ProductEntity withProductTypeEntity(ProductTypeEntity productTypeEntity) {
        this.productTypeEntity = productTypeEntity;
        return this;
    }
}
