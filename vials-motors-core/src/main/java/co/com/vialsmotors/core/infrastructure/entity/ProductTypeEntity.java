package co.com.vialsmotors.core.infrastructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_type")
@Getter
@Setter
@NoArgsConstructor
public class ProductTypeEntity {

    @Id
    @Column(name = "id_product_type")
    private Long idProductType;

    @Column(name = "description", nullable = false)
    private String description;

    public ProductTypeEntity(Long idProductType, String description) {
        this.idProductType = idProductType;
        this.description = description;
    }

    public ProductTypeEntity withDescription(String description){
        this.description = description;
        return this;
    }
}
