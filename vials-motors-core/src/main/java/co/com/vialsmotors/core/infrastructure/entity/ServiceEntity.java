package co.com.vialsmotors.core.infrastructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "service_entity", indexes = {
        @Index(name = "idx_service_entity_id_service", columnList = "id_service"),
        @Index(name = "idx_service_entity", columnList = "code_service")
})
@Getter
@Setter
@NoArgsConstructor
public class ServiceEntity {

    @Id
    @Column(name = "id_service")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idService;

    @Column(name = "code_service", nullable = false)
    private String codeService;

    @Column(name = "description", nullable = false, length = 250)
    private String description;

    @Column(name = "amount")
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "id_service_type")
    private ServiceTypeEntity serviceTypeEntity;

    public ServiceEntity(String codeService, String description, Double amount, ServiceTypeEntity serviceTypeEntity) {
        this.codeService = codeService;
        this.description = description;
        this.amount = amount;
        this.serviceTypeEntity = serviceTypeEntity;
    }

    public ServiceEntity withCodeService(String codeService){
        this.codeService = codeService;
        return this;
    }

    public ServiceEntity withDescription(String description){
        this.description = description;
        return this;
    }

    public ServiceEntity withAmount(Double amount){
        this.amount = amount;
        return this;
    }

    public ServiceEntity withServiceType(ServiceTypeEntity serviceTypeEntity){
        this.serviceTypeEntity = serviceTypeEntity;
        return this;
    }
}
