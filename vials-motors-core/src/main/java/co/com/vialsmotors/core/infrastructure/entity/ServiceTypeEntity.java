package co.com.vialsmotors.core.infrastructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "service_type", indexes = {
        @Index(name = "idx_service_type_entity", columnList = "id_service_type")
})
@Getter
@Setter
@NoArgsConstructor
public class ServiceTypeEntity {

    @Id
    @Column(name = "id_service_type")
    private Long idServiceType;

    @Column(name = "description", nullable = false)
    private String description;

    public ServiceTypeEntity(Long idServiceType, String description) {
        this.idServiceType = idServiceType;
        this.description = description;
    }

    public ServiceTypeEntity withDescription(String description){
        this.description = description;
        return this;
    }
}
