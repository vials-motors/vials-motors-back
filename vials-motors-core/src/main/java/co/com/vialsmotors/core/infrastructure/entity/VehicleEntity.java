package co.com.vialsmotors.core.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "vehicle", indexes = {
        @Index(name = "idx_vehicle_entity", columnList = "license_plate")
})
public class VehicleEntity {

    @Id
    @Column(name = "id_vehicle")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVehicle;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "make")
    private String make;

    @Column(name = "reference")
    private String reference;

    @Column(name = "model")
    private String model;

    @ManyToOne
    @JoinColumn(name = "id_vehicle_type")
    private VehicleTypeEntity vehicleTypeEntity;

    public VehicleEntity(String licensePlate, String make, String reference, String model, VehicleTypeEntity vehicleTypeEntity) {
        this.licensePlate = licensePlate;
        this.make = make;
        this.reference = reference;
        this.model = model;
        this.vehicleTypeEntity = vehicleTypeEntity;
    }
}
