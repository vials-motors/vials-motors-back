package co.com.vialsmotors.core.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicle_type", indexes = {
        @Index(name = "idx_vehicle_type_entity", columnList = "id_vehicle_type")
})
public class VehicleTypeEntity {

    @Id
    @Column(name = "id_vehicle_type")
    private Long idVehicleType;

    @Column(name = "description", nullable = false)
    private String description;

    public VehicleTypeEntity withDescription(String description){
        this.description = description;
        return this;
    }
}
