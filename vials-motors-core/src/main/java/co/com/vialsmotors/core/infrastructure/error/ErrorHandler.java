package co.com.vialsmotors.core.infrastructure.error;

import co.com.vialsmotors.commons.domain.exception.*;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.*;
import co.com.vialsmotors.core.domain.response.GenericResponse;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = Logger.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: ";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler() {
        CODE_STATE.put(FieldException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(DecodedErrorTokenException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CallCredentialException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(GetUserNameTokenException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(IdentificationTypeException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(SqlServerDataBaseException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CallCustomerException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(ProductException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ProductTypeException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(ListProductException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ProductCodeException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ProductTypeExistException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ListProductTypeException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(ProvideServiceException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(DateExistException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(CoreErrorException.class.getSimpleName(), HttpStatus.OK.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleAllException(Exception exception){
        String nameException = exception.getClass().getSimpleName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof FieldException fieldException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(fieldException.getStatus(), fieldException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof DecodedErrorTokenException decodedErrorTokenException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(decodedErrorTokenException.getStatus(), decodedErrorTokenException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof CallCredentialException callCredentialException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(callCredentialException.getStatus(), callCredentialException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GetUserNameTokenException getUserNameTokenException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(getUserNameTokenException.getStatus(), getUserNameTokenException.getMessage(), TIMESTAMP_RESPONSE));
        } else if(code != null && exception instanceof IdentificationTypeException identificationTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(identificationTypeException.getStatus(), identificationTypeException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof SqlServerDataBaseException sqlServerDataBaseException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sqlServerDataBaseException.getStatus(), sqlServerDataBaseException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ProductException productException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(productException.getStatus(), productException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof ProductTypeException productTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(productTypeException.getStatus(), productTypeException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof ListProductException listProductException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(listProductException.getStatus(), listProductException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ProductCodeException productCodeException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(productCodeException.getStatus(), productCodeException.getMessage(), TIMESTAMP_RESPONSE));
        }else if(code != null && exception  instanceof ProductTypeExistException productTypeExistException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(productTypeExistException.getStatus(), productTypeExistException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof ListProductTypeException listProductTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(listProductTypeException.getStatus(), listProductTypeException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ProvideServiceException provideServiceException) {
             return ResponseEntity.status(code)
                     .body(new GenericResponse(provideServiceException.getStatus(), provideServiceException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof DateExistException dateExistException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(dateExistException.getStatus(), dateExistException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof CoreErrorException coreErrorException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(coreErrorException.getStatus(), coreErrorException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof CallCustomerException callCustomerException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(callCustomerException.getStatus(), callCustomerException.getMessage(), TIMESTAMP_RESPONSE));
        } else {
            LOGGER.info(MESSAGE_ERROR_GENERIC + exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_ERROR.getCode(), EResponse.INTERNAL_ERROR.getMessage(), TIMESTAMP_RESPONSE));
        }
    }
}
