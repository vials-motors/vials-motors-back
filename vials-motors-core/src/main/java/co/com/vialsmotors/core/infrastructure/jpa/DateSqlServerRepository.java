package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DateSqlServerRepository extends JpaRepository<DateEntity, Long> {

    @Query(value = """
            SELECT * FROM date JOIN vehicle ON date.license_plate = vehicle.id_vehicle
             WHERE date.hour = ?1 AND date.day = ?2 AND date.status = ?3  AND date.id_customer = ?4 AND vehicle.license_plate =?5
            """, nativeQuery = true)
    Optional<DateEntity> findByDayAndHourAndStatusAndCustomerId(String hour, String day, Boolean status, String customerId, String license);

    @Query(value = "SELECT * FROM date WHERE hour = ?1 AND day = ?2 AND status =?3 ", nativeQuery = true)
    List<DateEntity> getListByDayAndHourAndStatus(String hour, String day, Boolean status);

    @Query(value = "SELECT * FROM date WHERE day =?1", nativeQuery = true)
    List<DateEntity> getAllListDay(String day);

    @Query(value = "SELECT * FROM date WHERE day =?1 AND status =?2", nativeQuery = true)
    List<DateEntity> getAllListDayActiveByAdminAndTechnical(String day, Boolean status);

    @Query(value = "SELECT * FROM date WHERE id_customer =?1 AND status =?2", nativeQuery = true)
    List<DateEntity> getAllListActiveByClient(String customerId, Boolean status);

    @Query(value = "SELECT * FROM date WHERE id_customer =?1", nativeQuery = true)
    List<DateEntity> getAllListByClient(String customerId);

    @Query(value = """
            SELECT * FROM date JOIN vehicle ON date.license_plate = vehicle.id_vehicle
             WHERE date.id_customer = ?1 AND date.day = ?2 AND date.hour = ?3  AND date.status = ?4 AND vehicle.license_plate =?5
            """, nativeQuery = true)
    Optional<DateEntity> getDateEntityByCustomer(String customerId, String day, String hour, Boolean status, String license);
}
