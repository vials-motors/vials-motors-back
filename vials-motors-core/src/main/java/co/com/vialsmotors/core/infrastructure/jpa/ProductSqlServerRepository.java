package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductSqlServerRepository extends JpaRepository<ProductEntity, Long> {

    Optional<ProductEntity> findByProductCode(String productCode);
}
