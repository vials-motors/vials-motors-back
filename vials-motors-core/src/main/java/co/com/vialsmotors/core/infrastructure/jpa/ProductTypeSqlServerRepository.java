package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeSqlServerRepository extends JpaRepository<ProductTypeEntity, Long> {
}
