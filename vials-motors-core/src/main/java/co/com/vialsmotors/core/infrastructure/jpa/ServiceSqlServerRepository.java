package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ServiceSqlServerRepository extends JpaRepository<ServiceEntity, Long> {

    @Query(value = "SELECT * FROM service_entity WHERE code_service = ?1", nativeQuery = true)
    Optional<ServiceEntity> findByCodeService(String codeService);
}
