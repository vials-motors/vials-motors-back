package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleSqlServerRepository extends JpaRepository<VehicleEntity, Long> {
}
