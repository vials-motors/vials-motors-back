package co.com.vialsmotors.core.infrastructure.jpa;

import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleTypeSqlServerRepository extends JpaRepository<VehicleTypeEntity, Long> {
}
