package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.DateDTO;
import co.com.vialsmotors.core.domain.dto.NoveltyDateDTO;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.domain.exception.CoreErrorException;
import co.com.vialsmotors.core.domain.model.Date;
import co.com.vialsmotors.core.infrastructure.entity.DateEntity;
import co.com.vialsmotors.core.infrastructure.entity.DetailsDateEntity;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;
import co.com.vialsmotors.core.infrastructure.utils.ValidateStatusUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DateMapper {

    private static final String NOT_INSTANTIABLE = "The class Date Mapper is not instantiable";

    private DateMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static DateEntity convertModelToEntity(Date date, Boolean status, ServiceEntity service){
        return new DateEntity(date.day(),
                date.hour(),
                status,
                service,
                date.customerId(),
                null,
                VehicleMapper.convertModelToEntity(date.vehicle()));
    }

    public static List<DateDTO> convertListEntityToDto(List<DateEntity> entityList){
        return entityList.stream()
                .map(dataTemporal -> new DateDTO(dataTemporal.getDay(),
                        dataTemporal.getHour(), dataTemporal.getCustomerId(),
                        dataTemporal.getVehicleEntity().getLicensePlate(),
                        dataTemporal.getServiceEntity().getDescription(),
                        ValidateStatusUtils.validate(dataTemporal.getStatus()),
                        extractDescription(dataTemporal.getAuditDateEntity().getDetailsDate()
                                .stream()
                                .skip(dataTemporal.getAuditDateEntity().getDetailsDate().size() - 1)
                                .findFirst()),
                        dataTemporal.getVehicleEntity().getMake(),
                        dataTemporal.getVehicleEntity().getReference(),
                        dataTemporal.getVehicleEntity().getVehicleTypeEntity().getDescription(),
                        setList(dataTemporal.getAuditDateEntity().getDetailsDate())))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private static String extractDescription(Optional<DetailsDateEntity> detailsDateEntity){
        if (detailsDateEntity.isPresent())
            return detailsDateEntity.get().getDateStatusEntity().getDescription();
        throw new CoreErrorException(EResponse.ERROR_SERVICE.getMessage(), EResponse.ERROR_SERVICE.getCode());
    }

    private static List<NoveltyDateDTO> setList(List<DetailsDateEntity> list){
        return list.stream()
                .map(temporal -> new NoveltyDateDTO(temporal.getNoveltyDate(), temporal.getTechnicalCode(),
                        temporal.getUpdateDate(), temporal.getDateStatusEntity().getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
