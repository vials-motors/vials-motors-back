package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.ProductDTO;
import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.model.Product;
import co.com.vialsmotors.core.infrastructure.entity.ProductEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductMapper {

    private static final String NOT_INSTANTIABLE = "The class ProductMapper is not instantiable";

    private ProductMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ProductEntity convertModelToEntity(Product product){
        return new ProductEntity(product.productCode(), product.description(), product.price(),
                ProductTypeMapper.convertModelToEntity(product.productType()));
    }

    public static List<ProductDTO> convertListEntityToDTO(List<ProductEntity> data){
        return data.stream().
                map(dataTemporal -> new ProductDTO(dataTemporal.getProductCode(),
                        dataTemporal.getDescription(), dataTemporal.getPrice(),
                        new ProductTypeDTO(dataTemporal.getProductTypeEntity().getIdProductType(),
                                dataTemporal.getProductTypeEntity().getDescription())))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
