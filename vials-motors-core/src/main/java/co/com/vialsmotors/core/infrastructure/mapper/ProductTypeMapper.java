package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.ProductTypeDTO;
import co.com.vialsmotors.core.domain.model.ProductType;
import co.com.vialsmotors.core.infrastructure.entity.ProductTypeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class ProductTypeMapper is not instantiable";

    private ProductTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ProductTypeEntity convertModelToEntity(ProductType productType){
        return new ProductTypeEntity(productType.idProductType(), productType.description());
    }

    public static List<ProductTypeDTO> convertListEntityToDTO(List<ProductTypeEntity> data){
        return data.stream()
                .map(dataTemporal -> new ProductTypeDTO(dataTemporal.getIdProductType(),
                        dataTemporal.getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
