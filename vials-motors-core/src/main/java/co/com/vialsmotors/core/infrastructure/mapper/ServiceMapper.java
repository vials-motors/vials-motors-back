package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.ServiceDTO;
import co.com.vialsmotors.core.domain.model.Service;
import co.com.vialsmotors.core.infrastructure.entity.ServiceEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceMapper {

    private static final String NOT_INSTANTIABLE = "The class service mapper is not instantiable";

    private ServiceMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ServiceEntity convertModelToEntity(Service service){
        return new ServiceEntity(service.codeService(), service.description().toUpperCase(),
                service.amount(),
                ServiceTypeMapper.convertModelToEntity(service.serviceType()));
    }

    public static List<ServiceDTO> convertListEntityToDto(List<ServiceEntity> list){
        return list.stream()
                .map(dataTemporal -> new ServiceDTO(dataTemporal.getCodeService(),
                        dataTemporal.getDescription(), dataTemporal.getAmount(),
                        ServiceTypeMapper.convertEntityToDto(dataTemporal.getServiceTypeEntity())))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
