package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.ServiceTypeDTO;
import co.com.vialsmotors.core.domain.model.ServiceType;
import co.com.vialsmotors.core.infrastructure.entity.ServiceTypeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class service type mapper is not instantiable";

    private ServiceTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ServiceTypeEntity convertModelToEntity(ServiceType serviceType){
        return new ServiceTypeEntity(serviceType.idServiceType(), serviceType.description());
    }

    public static List<ServiceTypeDTO> convertListEntityToDto(List<ServiceTypeEntity> list){
        return list.stream()
                .map(dataTemporal -> new ServiceTypeDTO(dataTemporal.getIdServiceType(), dataTemporal.getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static ServiceTypeDTO convertEntityToDto(ServiceTypeEntity serviceType){
        return new ServiceTypeDTO(serviceType.getIdServiceType(), serviceType.getDescription());
    }
}
