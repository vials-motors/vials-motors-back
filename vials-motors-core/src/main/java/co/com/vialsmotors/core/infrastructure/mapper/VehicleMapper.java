package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.model.Vehicle;
import co.com.vialsmotors.core.infrastructure.entity.VehicleEntity;

public class VehicleMapper {

    private static final String NOT_INSTANTIABLE = "The class Vehicle Mapper is not instantiable";

    private VehicleMapper (){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static VehicleEntity convertModelToEntity(Vehicle vehicle){
        return new VehicleEntity(vehicle.licensePlate(),
                vehicle.make(),
                vehicle.reference(),
                vehicle.model(),
                VehicleTypeMapper.convertModelToEntity(vehicle.vehicleType()));
    }
}
