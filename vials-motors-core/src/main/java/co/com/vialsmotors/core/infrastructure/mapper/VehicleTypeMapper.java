package co.com.vialsmotors.core.infrastructure.mapper;

import co.com.vialsmotors.core.domain.dto.VehicleTypeDTO;
import co.com.vialsmotors.core.domain.model.VehicleType;
import co.com.vialsmotors.core.infrastructure.entity.VehicleTypeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class Vehicle Type Mapper is not instantiable";

    private VehicleTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static VehicleTypeEntity convertModelToEntity(VehicleType vehicleType){
        return new VehicleTypeEntity(vehicleType.idVehicleType(), vehicleType.description());
    }

    public static List<VehicleTypeDTO> convertListEntityToDTO(List<VehicleTypeEntity> list){
        return list.stream()
                .map(dataTemporal -> new VehicleTypeDTO(dataTemporal.getIdVehicleType(), dataTemporal.getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
