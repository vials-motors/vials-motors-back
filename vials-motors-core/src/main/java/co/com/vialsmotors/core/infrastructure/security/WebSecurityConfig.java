package co.com.vialsmotors.core.infrastructure.security;

import co.com.vialsmotors.core.infrastructure.security.jwt.AuthEntryPointJwt;
import co.com.vialsmotors.core.infrastructure.security.jwt.AuthTokenFilter;
import co.com.vialsmotors.core.infrastructure.security.jwt.JwtUtils;
import co.com.vialsmotors.core.infrastructure.security.userdetails.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity()
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig {

    private static final String SERVICE = "/service";
    public static final String SERVICE_TYPE = "/service-type";
    public static final String VEHICLE_TYPE = "/vehicle-type";
    public static final String PRODUCT_TYPE = "/product-type";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String TECHNICAL = "TECHNICAL";
    public static final String CLIENT = "CLIENT";

    private final AuthEntryPointJwt unauthorizedHandler;
    private final JwtUtils jwtUtils;
    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter(){
        return new AuthTokenFilter(jwtUtils, userDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/product/save").hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/product/get-list").hasAnyRole(ROLE_ADMIN, TECHNICAL)
                .antMatchers(HttpMethod.PUT, "/product/update").hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, "/product/delete").hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, "/product-type/save").hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, PRODUCT_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, PRODUCT_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, PRODUCT_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, SERVICE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, SERVICE).hasAnyRole(ROLE_ADMIN, CLIENT)
                .antMatchers(HttpMethod.DELETE, SERVICE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, SERVICE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, "/date/save").hasAnyRole(ROLE_ADMIN, CLIENT)
                .antMatchers(HttpMethod.PUT, "/date/delete").hasAnyRole(ROLE_ADMIN, CLIENT)
                .antMatchers(HttpMethod.GET, "/date/list-all").hasAnyRole(ROLE_ADMIN, CLIENT, TECHNICAL)
                .antMatchers(HttpMethod.GET, "/date/list-all-active-internal").hasAnyRole(ROLE_ADMIN, CLIENT, TECHNICAL)
                .antMatchers(HttpMethod.GET, "/date/list-active-customer").hasAnyRole(ROLE_ADMIN, CLIENT, TECHNICAL)
                .antMatchers(HttpMethod.GET, "/date/list-customer").hasAnyRole(ROLE_ADMIN, CLIENT, TECHNICAL)
                .antMatchers(HttpMethod.PUT, "/date/update-by-customer").hasAnyRole(CLIENT)
                .antMatchers(HttpMethod.PUT, "/date/update-by-technical").hasAnyRole(TECHNICAL)
                .antMatchers(HttpMethod.GET, "/date/available-hour").hasAnyRole(ROLE_ADMIN, CLIENT)
                .antMatchers(HttpMethod.GET, SERVICE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, SERVICE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, SERVICE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, SERVICE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, VEHICLE_TYPE).hasAnyRole(ROLE_ADMIN, CLIENT)
                .antMatchers(HttpMethod.POST, VEHICLE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, VEHICLE_TYPE).hasAnyRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, VEHICLE_TYPE).hasAnyRole(ROLE_ADMIN)
                .anyRequest().authenticated().and()
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class).build();
    }
}
