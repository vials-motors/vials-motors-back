package co.com.vialsmotors.core.infrastructure.security.jwt;

import co.com.vialsmotors.commons.domain.enums.EJwtMessage;
import co.com.vialsmotors.commons.domain.exception.DecodedErrorTokenException;
import co.com.vialsmotors.commons.domain.exception.GetUserNameTokenException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class JwtUtils {

    private static final Logger LOGGER = Logger.getLogger(JwtUtils.class);
    private static final String ISS = "Vials Motors";
    @Value("${key.private}")
    protected String privateKey;

    public String getUserNameFromJwtToken(String token){
        try {
            return decodedToken(token).getClaims().get("sub").asString();
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error in the class JWT Utils in getUserNameFromJwtToken Method: " + exception.getMessage());
            throw new GetUserNameTokenException(exception.getMessage(), EJwtMessage.GET_USERNAME_MESSAGE.getCode());
        }
    }

    public Boolean validateJwtToken(String authToken){
        try{
            Long time = Instant.now().getEpochSecond();
            DecodedJWT jwt = decodedToken(authToken);
            Long timeExo = jwt.getClaims().get("exp").asLong();
            String sub = jwt.getClaims().get("sub").asString();
            if (timeExo > time && sub != null){
                return true;
            }
        } catch (TokenExpiredException e) {
            LOGGER.error("JWT token is expired: " + e.getMessage());
        } catch (JWTVerificationException e) {
            LOGGER.error("Invalid JWT token: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error("JWT claims string is empty: " + e.getMessage());
        }
        return false;

    }

    private DecodedJWT decodedToken(String token) {
        try{
            Algorithm algorithm = Algorithm.HMAC512(privateKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISS)
                    .build();
            return verifier.verify(token);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error in the class JWT Utils in decodedToken Method: " + exception.getMessage());
            throw new DecodedErrorTokenException(exception.getMessage(), EJwtMessage.DECODED_TOKEN_ERROR_MESSAGE.getCode());
        }
    }
}
