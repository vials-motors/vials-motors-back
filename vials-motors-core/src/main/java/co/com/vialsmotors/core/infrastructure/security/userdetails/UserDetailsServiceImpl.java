package co.com.vialsmotors.core.infrastructure.security.userdetails;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.LoadUsernameException;
import co.com.vialsmotors.core.infrastructure.apirest.client.CredentialService;
import co.com.vialsmotors.core.infrastructure.apirest.response.GetCredentialResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UserDetailsServiceImpl {

    private static final Logger LOGGER = Logger.getLogger(UserDetailsServiceImpl.class);

    private final CredentialService credentialService;

    public UserDetails loadUserByUsername(String username, String token){
        try {
            ResponseEntity<GetCredentialResponse> response = credentialService.getCredential(buildHeaders(username, token));
            return UserDetailsImpl.build(Objects.requireNonNull(response.getBody()));
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error " + UserDetailsServiceImpl.class.getName() + " in method loadUserByUsername " + username + " " + exception.getMessage());
            throw new LoadUsernameException(exception.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL.getCode());
        }
    }

    private Map<String, Object> buildHeaders(String username, String token){
        Map<String, Object> headers = new HashMap<>();
        headers.put("username", username);
        headers.put("Authorization", token);
        return headers;
    }
}
