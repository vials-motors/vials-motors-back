package co.com.vialsmotors.core.infrastructure.utils;

import org.jboss.logging.Logger;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class BuildDateUtils {

    private static final String NOT_INSTANTIABLE = "The class build date utils is not instantiable";
    private static final Logger LOGGER = Logger.getLogger(BuildDateUtils.class);

    private BuildDateUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ZonedDateTime buildDate(String day, String hour){
        String date = day + "T" + hour + ":00.000-05:00";
        LOGGER.info("DATE " + date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        return ZonedDateTime.parse(date, formatter).withZoneSameInstant(ZoneId.of("UTC-5"));
    }
}
