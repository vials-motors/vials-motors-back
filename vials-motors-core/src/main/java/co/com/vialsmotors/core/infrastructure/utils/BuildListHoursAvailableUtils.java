package co.com.vialsmotors.core.infrastructure.utils;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BuildListHoursAvailableUtils {

    private static final String NOT_INSTANTIABLE = "The class Build list hour available is not instantiable";

    private BuildListHoursAvailableUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static List<String> generateList(Map<String, Long> dateForHours){
        List<String> hoursAvailable = new ArrayList<>();
        String initialHours = "07:00";
        String finishHours = "18:00";

        while (initialHours.compareTo(finishHours) < 0){
            if (!dateForHours.containsKey(initialHours) || dateForHours.get(initialHours) < 2){
                hoursAvailable.add(initialHours);
            }
            initialHours = sumOneHour(initialHours);
        }
        return hoursAvailable;
    }

    private static String sumOneHour(String hour){
        LocalTime localTime = LocalTime.parse(hour).plusHours(1);
        return localTime.toString();
    }
}
