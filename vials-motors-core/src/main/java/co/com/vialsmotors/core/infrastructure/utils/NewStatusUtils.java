package co.com.vialsmotors.core.infrastructure.utils;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EDateStatusEntity;
import co.com.vialsmotors.core.domain.enums.EResponse;
import co.com.vialsmotors.core.infrastructure.entity.DateStatusEntity;

public class NewStatusUtils {

    private static final String NOT_INSTANTIABLE = "The class NewStatusUtils is not instantiable";

    private NewStatusUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static DateStatusEntity build(Long newState){
        return switch (Integer.parseInt(newState.toString())){
          case 1 -> new DateStatusEntity(EDateStatusEntity.STATUS_REGISTER.getId(), EDateStatusEntity.STATUS_REGISTER.getDescription());
          case 2 -> new DateStatusEntity(EDateStatusEntity.STATUS_CANCEL.getId(), EDateStatusEntity.STATUS_CANCEL.getDescription());
          case 3 -> new DateStatusEntity(EDateStatusEntity.STATUS_RECEIVER.getId(), EDateStatusEntity.STATUS_RECEIVER.getDescription());
          case 4 -> new DateStatusEntity(EDateStatusEntity.STATUS_PROCESS.getId(), EDateStatusEntity.STATUS_PROCESS.getDescription());
          case 5 -> new DateStatusEntity(EDateStatusEntity.STATUS_FINISH.getId(), EDateStatusEntity.STATUS_FINISH.getDescription());
            default -> throw new FieldException(EResponse.NEW_STATE_INVALID.getMessage(), EResponse.NEW_STATE_INVALID.getCode());
        };
    }
}
