package co.com.vialsmotors.core.infrastructure.utils;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.core.domain.enums.EResponse;

public class ValidateDeleteDateUtils {

    private static final String NOT_INSTANTIABLE = "The class Validate Delete Date Utils is not instantiable";
    private static final String DELETE_BY_ADMIN = "Deleted by administrator";
    private static final String DELETE_BY_CLIENT = "Deleted by client";
    private ValidateDeleteDateUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String validate(Integer validateRequest){
        return switch (validateRequest){
            case 1 -> DELETE_BY_ADMIN;
            case 2 -> DELETE_BY_CLIENT;
            default -> throw new FieldException(EResponse.REQUEST_VALIDATE_ERROR.getMessage(), EResponse.REQUEST_VALIDATE_ERROR.getCode());
        };
    }
}
