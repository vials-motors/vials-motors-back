package co.com.vialsmotors.core.infrastructure.utils;

public class ValidateStatusUtils {

    public static String validate(Boolean status){
        if (status == true){
            return "ACTIVE";
        }else {
            return "INACTIVE";
        }
    }
}
