-- import.sql

--product_type
INSERT INTO product_type (id_product_type, description) VALUES (1, 'Liquidos');
INSERT INTO product_type (id_product_type, description) VALUES (2, 'Llantas');
INSERT INTO product_type (id_product_type, description) VALUES (3, 'Llantas');

--service_type
INSERT INTO service_type (id_service_type, description) VALUES (1, 'Liquidos');
INSERT INTO service_type (id_service_type, description) VALUES (2, 'Alineacion y Balanceo');

--input
INSERT INTO product (product_code, description, id_product_type, price) VALUES ('INPUT-0001', 'Galón liquido de frenos', 1, 80000)
INSERT INTO product (product_code, description, id_product_type, price) VALUES ('INPUT-0002', 'Galón liquido de aceite', 1, 100000)

--vehicle_type
INSERT INTO vehicle_type (id_vehicle_type, description) VALUES (1, 'AUTOMOVIL')

--date_status
INSERT INTO date_status (id_date_status, description) VALUES (1, 'REGISTRADO')
INSERT INTO date_status (id_date_status, description) VALUES (2, 'CANCELADO')
INSERT INTO date_status (id_date_status, description) VALUES (3, 'RECIBIDO')
INSERT INTO date_status (id_date_status, description) VALUES (4, 'PROCESO')
INSERT INTO date_status (id_date_status, description) VALUES (5, 'FINALIZADO')