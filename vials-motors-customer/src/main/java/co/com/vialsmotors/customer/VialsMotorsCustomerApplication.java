package co.com.vialsmotors.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class VialsMotorsCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VialsMotorsCustomerApplication.class, args);
	}

}
