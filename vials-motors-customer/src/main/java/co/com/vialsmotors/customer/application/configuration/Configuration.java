package co.com.vialsmotors.customer.application.configuration;

import co.com.vialsmotors.customer.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.customer.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.customer.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerByEmailOrIdentificationNumberRepository;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerRepository;
import co.com.vialsmotors.customer.domain.port.customer.GetListCustomerRepository;
import co.com.vialsmotors.customer.domain.port.customer.RegisterCustomerRepository;
import co.com.vialsmotors.customer.domain.service.delete.DeleteCustomerImplService;
import co.com.vialsmotors.customer.domain.service.getcustomer.impl.GetCustomerByIdentificationNumberImplService;
import co.com.vialsmotors.customer.domain.service.getcustomer.impl.GetCustomerImplService;
import co.com.vialsmotors.customer.domain.service.list.GetListCustomerForSpImplService;
import co.com.vialsmotors.customer.domain.service.list.GetListCustomerImplService;
import co.com.vialsmotors.customer.domain.service.register.RegisterCustomerImplService;
import co.com.vialsmotors.customer.domain.service.update.UpdateCustomerImplService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public RegisterCustomerImplService registerCustomerImplService(RegisterCustomerRepository registerCustomerRepository,
                                                                   RegisterCredentialPort registerCredentialPort,
                                                                   GetCustomerByEmailOrIdentificationNumberRepository getCustomerByEmailOrIdentificationNumberRepository){
        return new RegisterCustomerImplService(registerCustomerRepository, registerCredentialPort, getCustomerByEmailOrIdentificationNumberRepository);
    }

    @Bean
    public GetListCustomerImplService getListCustomerImplService(GetListCustomerRepository getListCustomerRepository){
        return new GetListCustomerImplService(getListCustomerRepository);
    }

    @Bean
    public UpdateCustomerImplService updateCustomerImplService(RegisterCustomerRepository registerCustomerRepository, UpdateCredentialPort updateCredentialPort){
        return new UpdateCustomerImplService(registerCustomerRepository, updateCredentialPort);
    }

    @Bean
    public GetCustomerImplService getCustomerImplService(GetCustomerRepository getCustomerRepository){
        return new GetCustomerImplService(getCustomerRepository);
    }

    @Bean
    public DeleteCustomerImplService deleteCredentialImplService(RegisterCustomerRepository registerCustomerRepository, DeleteCredentialPort deleteCredentialPort){
        return new DeleteCustomerImplService(registerCustomerRepository, deleteCredentialPort);
    }

    @Bean
    public GetCustomerByIdentificationNumberImplService getCustomerByIdentificationNumberImplService(GetCustomerRepository getCustomerRepository){
        return new GetCustomerByIdentificationNumberImplService(getCustomerRepository);
    }

    @Bean
    public GetListCustomerForSpImplService getListCustomerForSpImplService (GetListCustomerRepository getListCustomerRepository){
        return new GetListCustomerForSpImplService(getListCustomerRepository);
    }
}
