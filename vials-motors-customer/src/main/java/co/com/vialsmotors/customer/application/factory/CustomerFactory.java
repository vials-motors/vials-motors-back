package co.com.vialsmotors.customer.application.factory;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.domain.model.IdentificationType;
import co.com.vialsmotors.customer.domain.request.UpdateCustomerRequest;
import org.springframework.stereotype.Component;

@Component
public class CustomerFactory {

    public Customer execute(CustomerCommand customerCommand){
        return new Customer(customerCommand.getFirstName(), customerCommand.getSecondName(),
                customerCommand.getFirstLastName(), customerCommand.getSecondLastName(),
                customerCommand.getPhone(), customerCommand.getIdentificationNumber(),
                buildIdentificationType(customerCommand.getIdentificationType()), customerCommand.getPassword(),
                customerCommand.getEmail());
    }

    public UpdateCustomerRequest updateCustomerRequest(CustomerCommand customerCommand){
        return new UpdateCustomerRequest(customerCommand.getFirstName(),
                customerCommand.getSecondName(), customerCommand.getFirstLastName(),
                customerCommand.getSecondLastName(), customerCommand.getPhone(), customerCommand.getEmail(),
                customerCommand.getIdentificationNumber());
    }

    private IdentificationType buildIdentificationType(IdentificationTypeCommand identificationTypeCommand){
        return identificationTypeCommand == null ?
                null:new IdentificationType(identificationTypeCommand.getIdIdentificationType(),
                identificationTypeCommand.getDescription());
    }
}
