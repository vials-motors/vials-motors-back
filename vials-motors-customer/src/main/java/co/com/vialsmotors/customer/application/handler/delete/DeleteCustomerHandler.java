package co.com.vialsmotors.customer.application.handler.delete;

import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteCustomerHandler {

    ResponseEntity<GenericResponse> deteleCustomer(String identificationNumber, String email);
}
