package co.com.vialsmotors.customer.application.handler.delete;

import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.domain.service.delete.DeleteCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteCustomerImplHandler implements DeleteCustomerHandler {

    private static final Logger LOGGER = Logger.getLogger(DeleteCustomerImplHandler.class);
    private final DeleteCustomerService deleteCustomerService;

    @Override
    public ResponseEntity<GenericResponse> deteleCustomer(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-CUSTOMER: Delete customer in handler with identification number: " + identificationNumber);
        return deleteCustomerService.deleteCredential(identificationNumber,  email);
    }
}
