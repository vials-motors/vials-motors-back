package co.com.vialsmotors.customer.application.handler.getcustomer;

import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import org.springframework.http.ResponseEntity;

public interface GetCustomerHandler {

    ResponseEntity<CustomerResponse> getCustomer(String customerId);
}
