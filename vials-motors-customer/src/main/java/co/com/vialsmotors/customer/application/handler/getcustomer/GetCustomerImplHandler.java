package co.com.vialsmotors.customer.application.handler.getcustomer;

import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import co.com.vialsmotors.customer.domain.service.getcustomer.GetCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetCustomerImplHandler implements GetCustomerHandler{

    private static final Logger LOGGER = Logger.getLogger(GetCustomerImplHandler.class);

    private final GetCustomerService getCustomerService;

    @Override
    public ResponseEntity<CustomerResponse> getCustomer(String customerId) {
        LOGGER.info("INFO-MS-CUSTOMER: Get customer in Get Customer Impl Handler with customer id: " + customerId);
        return getCustomerService.getCustomer(customerId);
    }
}
