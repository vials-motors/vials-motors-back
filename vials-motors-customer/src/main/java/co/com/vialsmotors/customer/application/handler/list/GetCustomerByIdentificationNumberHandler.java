package co.com.vialsmotors.customer.application.handler.list;

import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import org.springframework.http.ResponseEntity;

public interface GetCustomerByIdentificationNumberHandler {

    ResponseEntity<CustomerResponse> getCustomer(String identificationNumber);
}
