package co.com.vialsmotors.customer.application.handler.list;

import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListCustomerForSpHandler {

    ResponseEntity<ListGenericResponse<CustomerSpDTO>> getList(String dateInit, String dateFinish);
}
