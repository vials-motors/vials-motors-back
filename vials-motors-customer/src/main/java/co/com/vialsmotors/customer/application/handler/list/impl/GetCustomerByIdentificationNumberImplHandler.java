package co.com.vialsmotors.customer.application.handler.list.impl;

import co.com.vialsmotors.customer.application.handler.list.GetCustomerByIdentificationNumberHandler;
import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import co.com.vialsmotors.customer.domain.service.getcustomer.GetCustomerByIdentificationNumberService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetCustomerByIdentificationNumberImplHandler implements GetCustomerByIdentificationNumberHandler {

    private static final Logger LOGGER = Logger.getLogger(GetCustomerByIdentificationNumberImplHandler.class);
    private final GetCustomerByIdentificationNumberService getCustomerByIdentificationNumberService;

    @Override
    public ResponseEntity<CustomerResponse> getCustomer(String identificationNumber) {
        LOGGER.info("INFO-MS-CUSTOMER: Get customer with identificationNumber: " + identificationNumber);
        return getCustomerByIdentificationNumberService.getCustomer(identificationNumber);
    }
}
