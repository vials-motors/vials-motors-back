package co.com.vialsmotors.customer.application.handler.list.impl;

import co.com.vialsmotors.customer.application.handler.list.GetListCustomerForSpHandler;
import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import co.com.vialsmotors.customer.domain.service.list.GetListCustomerForSpService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListCustomerForSpImplHandler implements GetListCustomerForSpHandler {

    private static final Logger LOGGER = Logger.getLogger(GetListCustomerForSpImplHandler.class);
    private final GetListCustomerForSpService getListCustomerForSpService;

    @Override
    public ResponseEntity<ListGenericResponse<CustomerSpDTO>> getList(String dateInit, String dateFinish) {
        LOGGER.info("INFO-MS-CUSTOMER: Get list for sp in handler");
        return getListCustomerForSpService.getListForSp(dateInit, dateFinish);
    }
}
