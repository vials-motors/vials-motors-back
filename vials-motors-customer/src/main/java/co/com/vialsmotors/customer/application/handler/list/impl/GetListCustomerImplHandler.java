package co.com.vialsmotors.customer.application.handler.list.impl;

import co.com.vialsmotors.customer.application.handler.list.GetListCustomerHandler;
import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import co.com.vialsmotors.customer.domain.service.list.GetListCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetListCustomerImplHandler implements GetListCustomerHandler {

    private static final Logger LOGGER = Logger.getLogger(GetListCustomerImplHandler.class);

    private final GetListCustomerService getListCustomerService;

    @Override
    public ResponseEntity<ListGenericResponse<CustomerDTO>> getListCustomer() {
        LOGGER.info("INFO-MS-CUSTOMER: Get List customer in getListCustomer Method for class " + GetListCustomerImplHandler.class.getName());
        return getListCustomerService.getListCustomer();
    }
}
