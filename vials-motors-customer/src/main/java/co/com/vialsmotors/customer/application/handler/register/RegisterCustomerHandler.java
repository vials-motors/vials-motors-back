package co.com.vialsmotors.customer.application.handler.register;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterCustomerHandler {

    ResponseEntity<GenericResponse> registerCustomer(CustomerCommand customerCommand);
}
