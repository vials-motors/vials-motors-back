package co.com.vialsmotors.customer.application.handler.register;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.factory.CustomerFactory;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.domain.service.register.RegisterCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RegisterCustomerImplHandler implements RegisterCustomerHandler {

    private static final Logger LOGGER = Logger.getLogger(RegisterCustomerImplHandler.class);

    private final CustomerFactory customerFactory;
    private final RegisterCustomerService registerCustomerService;

    @Override
    public ResponseEntity<GenericResponse> registerCustomer(CustomerCommand customerCommand) {
        LOGGER.info("MS-CUSTOMER: Register customer in " + RegisterCustomerImplHandler.class.getName());
        return registerCustomerService.registerCustomer(customerFactory.execute(customerCommand));
    }
}
