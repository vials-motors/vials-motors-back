package co.com.vialsmotors.customer.application.handler.update;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateCustomerHandler {

    ResponseEntity<GenericResponse> updateCustomer(CustomerCommand customerCommand, String identificationNumber, String email);
}
