package co.com.vialsmotors.customer.application.handler.update;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.factory.CustomerFactory;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.domain.service.update.UpdateCustomerService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateCustomerImplHandler implements UpdateCustomerHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateCustomerImplHandler.class);

    private final UpdateCustomerService updateCustomerService;
    private final CustomerFactory customerFactory;

    @Override
    public ResponseEntity<GenericResponse> updateCustomer(CustomerCommand customerCommand, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-CUSTOMER: Update Customer in Update Customer Impl Handler with identification number: " + identificationNumber);
        return updateCustomerService.updateCustomer(customerFactory.updateCustomerRequest(customerCommand), identificationNumber, email);
    }
}
