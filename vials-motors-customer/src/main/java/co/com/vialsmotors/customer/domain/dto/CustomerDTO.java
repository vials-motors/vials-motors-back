package co.com.vialsmotors.customer.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
public class CustomerDTO extends CustomerBaseDTO{

    private String customerID;

    public CustomerDTO(String firstName, String secondName, String firstLastName, String secondLastName, String phone, String identificationNumber, String email, String customerID) {
        super(firstName, secondName, firstLastName, secondLastName, phone, identificationNumber, email);
        this.customerID = customerID;
    }
}
