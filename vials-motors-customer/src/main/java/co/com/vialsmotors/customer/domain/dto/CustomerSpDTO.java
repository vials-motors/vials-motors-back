package co.com.vialsmotors.customer.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerSpDTO extends CustomerBaseDTO{

    private String status;
    private String descriptionIdentificationType;

    public CustomerSpDTO(String firstName, String secondName, String firstLastName, String secondLastName, String phone, String identificationNumber, String email, String status, String descriptionIdentificationType) {
        super(firstName, secondName, firstLastName, secondLastName, phone, identificationNumber, email);
        this.status = status;
        this.descriptionIdentificationType = descriptionIdentificationType;
    }
}
