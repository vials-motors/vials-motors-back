package co.com.vialsmotors.customer.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    MESSAGE_RESPONSE_SUCCESS("SUCC-00", "Usuario registrado correctamente"),
    GET_LIST_SUCCESSFULLY("SUCC-01", "Lista obtenida correctamente"),
    UPDATE_CUSTOMER_SUCCESS("SUCC-02", "Usuario actualizado correctamente"),
    GET_CUSTOMER_SUCCESSFULLY("SUCC-03", "Usuario obtenido correctamente"),
    DELETE_CUSTOMER_SUCCESSFULLY("SUCC-04", "Usuario eliminado correctamente"),
    MESSAGE_ERROR_REGISTER("ERR-00", "Un error ocurrio en el registro"),
    MESSAGE_ERROR_IDENTIFICATION_NUMBER("ERR-01", "El usuario ya existe"),
    EMPTY_LIST_ERROR("ERR-02", "La lista esta vacia"),
    FIND_CUSTOMER_ERROR("ERR-03", "El usuario no existe"),
    IDENTIFICATION_NUMBER_ERROR("ERR-04", "El numero de identificacion no es valido"),
    FIND_CUSTOMER_ID_ERROR("ERR-05", "El id del usuario es requerido"),
    GET_CUSTOMER_ERROR("ERR-06", "Error obteniendo el usuario"),
    FIELD_EMAIL_ERROR("ERR-07", "Email es requerido"),
    FIELD_PASSWORD_ERROR("ERR-08", "El campo contraseña es requerido"),
    FIELD_DATE_INVALID("ERR-09", "El campo fecha es requerido"),
    IDENTIFICATION_NUMBER_EXIST("ERR-10", "El numero de identificacion ya existe"),
    EMAIL_EXIST("ERR-11", "El email ya existe"),
    INTERNAL_ERROR("ERR-500", "Error interno de servidor");

    private final String code;
    private final String message;
}
