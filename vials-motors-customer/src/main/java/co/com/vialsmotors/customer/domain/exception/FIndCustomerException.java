package co.com.vialsmotors.customer.domain.exception;

import lombok.Getter;

@Getter
public class FIndCustomerException extends RuntimeException{

    private final String status;

    public FIndCustomerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
