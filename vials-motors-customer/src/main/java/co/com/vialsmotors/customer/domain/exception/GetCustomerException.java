package co.com.vialsmotors.customer.domain.exception;

import lombok.Getter;

@Getter
public class GetCustomerException extends RuntimeException{

    private final String status;

    public GetCustomerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
