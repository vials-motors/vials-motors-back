package co.com.vialsmotors.customer.domain.exception;

import lombok.Getter;

@Getter
public class IdentificationNumberException extends RuntimeException{

    private final String status;

    public IdentificationNumberException(String message, String status) {
        super(message);
        this.status = status;
    }
}
