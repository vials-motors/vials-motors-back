package co.com.vialsmotors.customer.domain.exception;

import lombok.Getter;

@Getter
public class ListCustomerException extends RuntimeException{

    private final String status;

    public ListCustomerException(String message, String status) {
        super(message);
        this.status = status;
    }
}
