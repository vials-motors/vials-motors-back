package co.com.vialsmotors.customer.domain.model;


import co.com.vialsmotors.commons.domain.enums.EFieldMessage;
import co.com.vialsmotors.customer.domain.validate.ValidateArgument;

public record IdentificationType(Long idIdentificationType, String description) {

    public IdentificationType{
        ValidateArgument.validateFieldLong(idIdentificationType, EFieldMessage.FIELD_ID_MESSAGE.getCode(), EFieldMessage.FIELD_ID_MESSAGE.getMessage());
        ValidateArgument.validateFieldString(description, EFieldMessage.FIELD_DESCRIPTION_MESSAGE.getCode(), EFieldMessage.FIELD_DESCRIPTION_MESSAGE.getMessage());
    }
}
