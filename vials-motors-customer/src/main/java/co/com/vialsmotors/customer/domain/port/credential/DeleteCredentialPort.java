package co.com.vialsmotors.customer.domain.port.credential;

public interface DeleteCredentialPort {

    Boolean deleteCredential(String customerId);
}
