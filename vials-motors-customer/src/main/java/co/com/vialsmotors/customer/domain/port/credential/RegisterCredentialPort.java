package co.com.vialsmotors.customer.domain.port.credential;

import co.com.vialsmotors.customer.infrastructure.apirest.dto.CredentialDTO;

public interface RegisterCredentialPort {

    Boolean registerCredential(CredentialDTO credentialDTO, Integer userType);
}
