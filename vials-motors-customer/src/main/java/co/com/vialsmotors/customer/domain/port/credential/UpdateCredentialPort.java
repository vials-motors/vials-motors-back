package co.com.vialsmotors.customer.domain.port.credential;

public interface UpdateCredentialPort {

    Boolean updateCredential(String username, String customerID);
}
