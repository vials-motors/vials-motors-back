package co.com.vialsmotors.customer.domain.port.customer;

import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;

import java.util.Optional;

public interface GetCustomerByEmailOrIdentificationNumberRepository {

    Optional<CustomerEntity> getCustomerByEmail(String email);
    Optional<CustomerEntity> getCustomerByIdentification(String identificationNumber);
}
