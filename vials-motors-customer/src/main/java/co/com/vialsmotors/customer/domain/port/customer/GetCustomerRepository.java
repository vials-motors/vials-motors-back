package co.com.vialsmotors.customer.domain.port.customer;

import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;

import java.util.Optional;

public interface GetCustomerRepository {

    Optional<CustomerEntity> getCustomerForIdCustomer(String customerId);
    Optional<CustomerEntity> getCustomerByIdentificationNumber(String identificationNumber);
}
