package co.com.vialsmotors.customer.domain.port.customer;

import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;

import java.time.ZonedDateTime;
import java.util.List;

public interface GetListCustomerRepository {

    List<CustomerEntity> getListCustomer();
    List<CustomerEntity> getListCustomerSp(ZonedDateTime dateInit, ZonedDateTime dateFinish);
}
