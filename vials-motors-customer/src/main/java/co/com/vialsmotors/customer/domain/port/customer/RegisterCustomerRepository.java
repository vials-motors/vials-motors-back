package co.com.vialsmotors.customer.domain.port.customer;

import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;

import java.util.Optional;

public interface RegisterCustomerRepository {

    void registerCustomer(CustomerEntity customerEntity);

    Optional<CustomerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email);
}
