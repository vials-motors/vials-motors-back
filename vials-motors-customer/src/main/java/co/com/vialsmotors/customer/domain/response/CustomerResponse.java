package co.com.vialsmotors.customer.domain.response;

import co.com.vialsmotors.customer.domain.dto.CustomerDTO;

import java.time.ZonedDateTime;

public record CustomerResponse (String status, String message, CustomerDTO customer, ZonedDateTime timestamp){
}
