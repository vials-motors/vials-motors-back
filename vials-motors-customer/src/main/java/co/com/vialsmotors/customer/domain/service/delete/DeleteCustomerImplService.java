package co.com.vialsmotors.customer.domain.service.delete;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.FIndCustomerException;
import co.com.vialsmotors.customer.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.customer.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.customer.domain.port.customer.RegisterCustomerRepository;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteCustomerImplService implements DeleteCustomerService {

    private static final Logger LOGGER = Logger.getLogger(DeleteCustomerImplService.class);
    private static final Boolean STATUS_TRUE_FOR_CUSTOMER = true;
    private static final Boolean STATUS_FALSE_FOR_CUSTOMER = false;
    private final RegisterCustomerRepository registerCustomerRepository;
    private final DeleteCredentialPort deleteCredentialPort;

    public DeleteCustomerImplService(RegisterCustomerRepository registerCustomerRepository, DeleteCredentialPort deleteCredentialPort) {
        this.registerCustomerRepository = registerCustomerRepository;
        this.deleteCredentialPort = deleteCredentialPort;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteCredential(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-CUSTOMER: Delete credential with identification number: " + identificationNumber);
        this.registerCustomerRepository.registerCustomer(callDeleteCredential(identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_CUSTOMER_SUCCESSFULLY.getCode(), EResponse.DELETE_CUSTOMER_SUCCESSFULLY.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isBlank())
            throw new IdentificationNumberException(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getCode());
    }

    private void validateEmail(String email){
        if (email == null || email.isBlank()){
            throw new FieldException(EResponse.FIELD_EMAIL_ERROR.getMessage(), EResponse.FIELD_EMAIL_ERROR.getCode());
        }
    }

    private CustomerEntity getCustomerEntity(String identificationNumber, String email){
        this.validateIdentificationNumber(identificationNumber);
        this.validateEmail(email);
        Optional<CustomerEntity> customerEntity = this.registerCustomerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_TRUE_FOR_CUSTOMER, email);
        if (customerEntity.isPresent())
            return customerEntity.get();
        throw new FIndCustomerException(EResponse.FIND_CUSTOMER_ERROR.getMessage(), EResponse.FIND_CUSTOMER_ERROR.getCode());
    }

    private CustomerEntity callDeleteCredential(String identificationNumber, String email){
        CustomerEntity customerEntity = getCustomerEntity(identificationNumber, email);
        Boolean callCredential = this.deleteCredentialPort.deleteCredential(customerEntity.getCustomerID());
        if (Boolean.FALSE.equals(callCredential)){
            LOGGER.error("ERROR-MS-CUSTOMER: Error call delete credential with callCredential: " + false);
            throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL_MESSAGE.getCode());
        }
        return changeStatus(customerEntity);
    }

    private CustomerEntity changeStatus(CustomerEntity customerEntity){
        return customerEntity.withStatus(STATUS_FALSE_FOR_CUSTOMER);
    }
}
