package co.com.vialsmotors.customer.domain.service.delete;

import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteCustomerService {

    ResponseEntity<GenericResponse> deleteCredential(String identificationNumber, String email);
}
