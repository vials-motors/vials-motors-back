package co.com.vialsmotors.customer.domain.service.getcustomer;

import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import org.springframework.http.ResponseEntity;

public interface GetCustomerService {

    ResponseEntity<CustomerResponse> getCustomer(String customerId);
}
