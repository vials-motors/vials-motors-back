package co.com.vialsmotors.customer.domain.service.getcustomer.impl;

import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.FieldException;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerRepository;
import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import co.com.vialsmotors.customer.domain.service.getcustomer.GetCustomerByIdentificationNumberService;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class GetCustomerByIdentificationNumberImplService implements GetCustomerByIdentificationNumberService {

    private static final Logger LOGGER = Logger.getLogger(GetCustomerByIdentificationNumberImplService.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final GetCustomerRepository getCustomerRepository;

    public GetCustomerByIdentificationNumberImplService(GetCustomerRepository getCustomerRepository) {
        this.getCustomerRepository = getCustomerRepository;
    }

    @Override
    public ResponseEntity<CustomerResponse> getCustomer(String identificationNumber) {
        this.validateIdentificationNumber(identificationNumber);
        Optional<CustomerEntity> customerEntity = this.getCustomerRepository.getCustomerByIdentificationNumber(identificationNumber);
        return customerEntity.map(entity -> ResponseEntity.ok(new CustomerResponse(EResponse.GET_CUSTOMER_SUCCESSFULLY.getCode(),
                EResponse.GET_CUSTOMER_SUCCESSFULLY.getMessage(),
                CustomerMapper.convertEntityToDto(entity),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())))).orElseGet(() -> ResponseEntity.ok(new CustomerResponse(EResponse.GET_CUSTOMER_ERROR.getCode(),
                EResponse.GET_CUSTOMER_ERROR.getMessage(), null, TIMESTAMP)));
    }


    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isBlank()){
            LOGGER.error("ERROR-MS-CUSTOMER: Error for identification number empty or null");
            throw new FieldException(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getCode());
        }
    }
}
