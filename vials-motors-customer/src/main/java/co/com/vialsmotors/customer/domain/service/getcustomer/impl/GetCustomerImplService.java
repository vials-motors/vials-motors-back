package co.com.vialsmotors.customer.domain.service.getcustomer.impl;

import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.CustomerIdException;
import co.com.vialsmotors.customer.domain.exception.FIndCustomerException;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerRepository;
import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import co.com.vialsmotors.customer.domain.service.getcustomer.GetCustomerService;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class GetCustomerImplService implements GetCustomerService {

    private static final Logger LOGGER = Logger.getLogger(GetCustomerImplService.class);

    private final GetCustomerRepository getCustomerRepository;

    public GetCustomerImplService(GetCustomerRepository getCustomerRepository) {
        this.getCustomerRepository = getCustomerRepository;
    }

    @Override
    public ResponseEntity<CustomerResponse> getCustomer(String customerId) {
        LOGGER.info("INFO-MS-CUSTOMER: Get customer in Get customer impl Service with customer id: " + customerId);
        if (customerId == null || customerId.isEmpty())
            throw new CustomerIdException(EResponse.FIND_CUSTOMER_ID_ERROR.getMessage(), EResponse.FIND_CUSTOMER_ID_ERROR.getCode());
        return ResponseEntity.ok(new CustomerResponse(EResponse.GET_CUSTOMER_SUCCESSFULLY.getCode(), EResponse.GET_CUSTOMER_SUCCESSFULLY.getMessage(),
                buildCustomerDto(customerId), ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private CustomerDTO buildCustomerDto(String customerId){
        Optional<CustomerEntity> customerEntity = getCustomerRepository.getCustomerForIdCustomer(customerId);
        if (customerEntity.isPresent())
            return CustomerMapper.convertEntityToDto(customerEntity.get());
        throw new FIndCustomerException(EResponse.FIND_CUSTOMER_ERROR.getMessage(), EResponse.FIND_CUSTOMER_ERROR.getCode());
    }
}
