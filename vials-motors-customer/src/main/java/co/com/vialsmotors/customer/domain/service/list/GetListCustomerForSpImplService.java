package co.com.vialsmotors.customer.domain.service.list;

import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.ListCustomerException;
import co.com.vialsmotors.customer.domain.port.customer.GetListCustomerRepository;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import co.com.vialsmotors.customer.infrastructure.util.GenerateDateUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListCustomerForSpImplService implements GetListCustomerForSpService{

    private static final Logger LOGGER = Logger.getLogger(GetListCustomerForSpImplService.class);
    private final GetListCustomerRepository getListCustomerRepository;

    public GetListCustomerForSpImplService(GetListCustomerRepository getListCustomerRepository) {
        this.getListCustomerRepository = getListCustomerRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<CustomerSpDTO>> getListForSp(String dateInit, String dateFinish) {
        this.validateDate(dateInit, dateFinish);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_SUCCESSFULLY.getCode(),
                EResponse.GET_LIST_SUCCESSFULLY.getMessage(),
                CustomerMapper.convertListEntityToListSpDTO(getList(dateInit, dateFinish)),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private List<CustomerEntity> getList(String dateInit, String dateFinish){
        LOGGER.info("INFO-MS-CUSTOMER: Get list with date init: " + dateInit + " and date finish: " + dateFinish);
        return this.getListCustomerRepository.getListCustomerSp(
                GenerateDateUtils.generateDate(dateInit),
                GenerateDateUtils.generateDate(dateFinish)
        );
    }

    private void validateDate(String dateInit, String dateFinish){
        LOGGER.info("INFO-MS-CUSTOMER: validate date init: " + dateInit + " and date finish: " + dateFinish);
        if (dateInit == null || dateFinish == null || dateFinish.isEmpty() || dateInit.isEmpty()){
            throw new ListCustomerException(EResponse.FIELD_DATE_INVALID.getMessage(), EResponse.FIELD_DATE_INVALID.getCode());
        }
    }
}
