package co.com.vialsmotors.customer.domain.service.list;

import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListCustomerForSpService {

    ResponseEntity<ListGenericResponse<CustomerSpDTO>> getListForSp(String dateInit, String dateFinish);
}
