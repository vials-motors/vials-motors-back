package co.com.vialsmotors.customer.domain.service.list;

import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.ListCustomerException;
import co.com.vialsmotors.customer.domain.port.customer.GetListCustomerRepository;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListCustomerImplService implements GetListCustomerService{

    private static final Logger LOGGER = Logger.getLogger(GetListCustomerImplService.class);

    private final GetListCustomerRepository getListCustomerRepository;

    public GetListCustomerImplService(GetListCustomerRepository getListCustomerRepository) {
        this.getListCustomerRepository = getListCustomerRepository;
    }

    @Override
    public ResponseEntity<ListGenericResponse<CustomerDTO>> getListCustomer() {
        LOGGER.info("INFO-MS-CUSTOMER: Get list in getListCustomerService");
        List<CustomerEntity> entityList = getListCustomerRepository.getListCustomer();
        validateList(entityList);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_LIST_SUCCESSFULLY.getCode(), EResponse.GET_LIST_SUCCESSFULLY.getMessage(),
                CustomerMapper.convertListEntityToDto(entityList), ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateList(List<CustomerEntity> customerEntityList){
        if (customerEntityList.isEmpty()){
            LOGGER.info("INFO-MS-CUSTOMER: The list customer is empty in validateList for class " + GetListCustomerImplService.class.getName());
            throw new ListCustomerException(EResponse.EMPTY_LIST_ERROR.getMessage(), EResponse.EMPTY_LIST_ERROR.getCode());
        }
    }
}
