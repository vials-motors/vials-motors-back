package co.com.vialsmotors.customer.domain.service.list;

import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

public interface GetListCustomerService {

    ResponseEntity<ListGenericResponse<CustomerDTO>> getListCustomer();
}
