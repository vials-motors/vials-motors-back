package co.com.vialsmotors.customer.domain.service.register;

import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.FIndCustomerException;
import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerByEmailOrIdentificationNumberRepository;
import co.com.vialsmotors.customer.domain.port.customer.RegisterCustomerRepository;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.infrastructure.apirest.dto.CredentialDTO;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class RegisterCustomerImplService implements RegisterCustomerService{

    private static final Logger LOGGER = Logger.getLogger(RegisterCustomerImplService.class);
    private static final Integer NUMBER_FOR_USER_TYPE = 2;
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private static final Boolean STATUS_FOR_CLIENT = true;
    private static final Boolean STATUS_FOR_CLIENT_INACTIVE = false;

    private final RegisterCustomerRepository registerCustomerRepository;
    private final RegisterCredentialPort registerCredentialPort;
    private final GetCustomerByEmailOrIdentificationNumberRepository getCustomerByEmailOrIdentificationNumberRepository;

    public RegisterCustomerImplService(RegisterCustomerRepository registerCustomerRepository, RegisterCredentialPort registerCredentialPort,
                                       GetCustomerByEmailOrIdentificationNumberRepository getCustomerByEmailOrIdentificationNumberRepository) {
        this.registerCustomerRepository = registerCustomerRepository;
        this.registerCredentialPort = registerCredentialPort;
        this.getCustomerByEmailOrIdentificationNumberRepository = getCustomerByEmailOrIdentificationNumberRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> registerCustomer(Customer customer) {
            CustomerEntity customerEntity = CustomerMapper.convertModelToEntity(customer, STATUS_FOR_CLIENT);
            this.validateEmail(customer.email());
            this.validateIdentificationNumber(customer.identificationNumber());
            this.validateIdentificationNumberAndRegister(customer.identificationNumber(), customerEntity, customer);
                return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESS.getCode(),
                        EResponse.MESSAGE_RESPONSE_SUCCESS.getMessage(), TIMESTAMP_RESPONSE));
    }

    private CredentialDTO buildCredentialDto(Customer customer, String customerID){
        return new CredentialDTO(customer.email(),
                customer.password(),  customerID);
    }

    private void validateIdentificationNumberAndRegister(String identificationNumber, CustomerEntity newCustomerEntity, Customer customer){
        Optional<CustomerEntity> customerEntity = registerCustomerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_FOR_CLIENT, customer.email());
        Optional<CustomerEntity> customerEntityInactive = registerCustomerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_FOR_CLIENT_INACTIVE, customer.email());
        if (customerEntity.isPresent()){
            throw new FIndCustomerException(EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getMessage(),
                    EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
        }else if (customerEntityInactive.isPresent()){
            callRegisterCredential(customer, customerEntityInactive.get().getCustomerID());
            registerCustomerRepository.registerCustomer(buildCustomerEntity(newCustomerEntity, customerEntityInactive.get()));
        }else {
            callRegisterCredential(customer, newCustomerEntity.getCustomerID());
            registerCustomerRepository.registerCustomer(newCustomerEntity);
        }
    }

    private void callRegisterCredential(Customer customer, String customerId){
        Boolean statusCall = registerCredentialPort.registerCredential(buildCredentialDto(customer, customerId), NUMBER_FOR_USER_TYPE);
        if (Boolean.FALSE.equals(statusCall)){
            throw new CallCredentialException(  EResponse.MESSAGE_ERROR_REGISTER.getMessage(), EResponse.MESSAGE_ERROR_REGISTER.getCode());
        }
    }

    private CustomerEntity buildCustomerEntity(CustomerEntity newCustomerEntity, CustomerEntity previosCustomerEntity){
        return previosCustomerEntity.withFirstName(newCustomerEntity.getFirstName())
                .withSecondName(newCustomerEntity.getSecondName())
                .withFirstLastName(newCustomerEntity.getFirstLastName())
                .withSecondLastName(newCustomerEntity.getSecondLastName())
                .withPhone(newCustomerEntity.getPhone())
                .withIdentificationNumber(newCustomerEntity.getIdentificationNumber())
                .withIdentificationType(newCustomerEntity.getIdentificationType())
                .withEmail(newCustomerEntity.getEmail())
                .withStatus(true);
    }

    private void validateEmail(String email){
        Optional<CustomerEntity> customerEntity = this.getCustomerByEmailOrIdentificationNumberRepository.getCustomerByEmail(email);
        if (customerEntity.isPresent()){
            LOGGER.error("ERROR-MS-CUSTOMER: Error because email exist");
            throw new FIndCustomerException(EResponse.EMAIL_EXIST.getMessage(), EResponse.EMAIL_EXIST.getCode());
        }
    }

    private void validateIdentificationNumber(String identificationNumber){
        Optional<CustomerEntity> customerEntity = this.getCustomerByEmailOrIdentificationNumberRepository.getCustomerByIdentification(identificationNumber);
        if (customerEntity.isPresent()){
            LOGGER.error("ERROR-MS-CUSTOMER: Error because identificationNumber exist");
            throw new FIndCustomerException(EResponse.IDENTIFICATION_NUMBER_EXIST.getMessage(), EResponse.IDENTIFICATION_NUMBER_EXIST.getCode());
        }
    }
}
