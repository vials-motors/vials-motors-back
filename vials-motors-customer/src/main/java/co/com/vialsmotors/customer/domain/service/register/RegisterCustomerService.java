package co.com.vialsmotors.customer.domain.service.register;

import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterCustomerService {

    ResponseEntity<GenericResponse> registerCustomer(Customer customer);
}
