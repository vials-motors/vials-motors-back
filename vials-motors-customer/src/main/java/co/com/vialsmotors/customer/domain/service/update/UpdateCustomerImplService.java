package co.com.vialsmotors.customer.domain.service.update;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.FIndCustomerException;
import co.com.vialsmotors.customer.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.customer.domain.port.customer.RegisterCustomerRepository;
import co.com.vialsmotors.customer.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.customer.domain.request.UpdateCustomerRequest;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateCustomerImplService implements UpdateCustomerService{

    private static final Logger LOGGER = Logger.getLogger(UpdateCustomerImplService.class);
    private static final Boolean STATUS_ACTIVE_FOR_CUSTOMER = true;

    private final RegisterCustomerRepository registerCustomerRepository;
    private final UpdateCredentialPort updateCredentialPort;

    public UpdateCustomerImplService(RegisterCustomerRepository registerCustomerRepository, UpdateCredentialPort updateCredentialPort) {
        this.registerCustomerRepository = registerCustomerRepository;
        this.updateCredentialPort = updateCredentialPort;
    }

    @Override
    public ResponseEntity<GenericResponse> updateCustomer(UpdateCustomerRequest customer, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-CUSTOMER: Update customer in Update Customer Impl Service with identification number: " + identificationNumber);
        this.validateIdentificationNumber(identificationNumber);
        this.registerCustomerRepository.registerCustomer(buildNewCustomerEntity(customer, identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_CUSTOMER_SUCCESS.getCode(), EResponse.UPDATE_CUSTOMER_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private CustomerEntity getCustomerEntity(String identificationNumber, String email) {
        Optional<CustomerEntity> customerEntity = registerCustomerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_ACTIVE_FOR_CUSTOMER, email);
        return customerEntity.orElse(null);
    }

    private CustomerEntity buildNewCustomerEntity(UpdateCustomerRequest newCustomer, String identificationNumber, String email){
        CustomerEntity customerEntity = getCustomerEntity(identificationNumber, email);
        this.validateCustomerEntity(customerEntity);
        this.validateAndCall(newCustomer, customerEntity);
        return customerEntity.withFirstName(newCustomer.getFirstName())
                .withSecondName(newCustomer.getSecondName())
                .withFirstLastName(newCustomer.getFirstLastName())
                .withSecondLastName(newCustomer.getSecondLastName())
                .withPhone(newCustomer.getPhone())
                .withEmail(newCustomer.getEmail())
                .withIdentificationNumber(newCustomer.getIdentificationNumber());
    }

    private Boolean validateCreateUsername(UpdateCustomerRequest newCustomer, CustomerEntity customerEntity){
        return !customerEntity.getEmail().equals(newCustomer.getEmail());
    }

    private Boolean callUpdateCredential(UpdateCustomerRequest newCustomer, String customerId){
        return updateCredentialPort.updateCredential(newCustomer.getEmail(), customerId);
    }

    private void validateCustomerEntity(CustomerEntity customerEntity){
        if (customerEntity == null ){
            throw new FIndCustomerException(EResponse.FIND_CUSTOMER_ERROR.getMessage(), EResponse.FIND_CUSTOMER_ERROR.getCode());
        }
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isBlank()) {
            throw new IdentificationNumberException(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getCode());
        }
    }

    private void validateAndCall(UpdateCustomerRequest newCustomer, CustomerEntity customerEntity){
        if (Boolean.TRUE.equals(validateCreateUsername(newCustomer, customerEntity))){
            LOGGER.info("INFO-MS-CUSTOMER: Update credentials with first name: " + newCustomer.getFirstName());
            if (Boolean.FALSE.equals(callUpdateCredential(newCustomer, customerEntity.getCustomerID()))){
                LOGGER.info("ERROR-MS-CUSTOMER: Error call update credential");
                throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL.getCode(), ECallMessage.ERROR_CALL_CREDENTIAL.getMessage());
            }
        }
    }
}
