package co.com.vialsmotors.customer.domain.service.update;

import co.com.vialsmotors.customer.domain.request.UpdateCustomerRequest;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateCustomerService {

    ResponseEntity<GenericResponse> updateCustomer(UpdateCustomerRequest customer, String identificationNumber, String email);
}
