package co.com.vialsmotors.customer.infrastructure.adapter.crendential;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.customer.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.customer.infrastructure.apirest.client.CredentialService;
import co.com.vialsmotors.customer.infrastructure.apirest.response.FeignResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class DeleteCredentialAdapter implements DeleteCredentialPort {

    private static final Logger LOGGER = Logger.getLogger(DeleteCredentialAdapter.class);
    private static final String STATUS_CHECK_REQUEST = "SUCC-02";
    private final CredentialService credentialService;

    @Override
    public Boolean deleteCredential(String customerId) {
        try {
            return validateResponse(credentialService.deleteCredential(generateHeaders(customerId)));
        }catch (Exception e){
            LOGGER.error("ERROR-MS-CUSTOMER: Error callback delete credential service with message: " + e.getMessage());
            throw new CallCredentialException(e.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL.getCode());
        }
    }

    private Map<String, Object> generateHeaders(String customerId){
        Map<String, Object> headers = new HashMap<>();
        headers.put("customerId", customerId);
        return headers;
    }

    private Boolean validateResponse(ResponseEntity<FeignResponse> response){
        return Objects.requireNonNull(response.getBody()).getCode().equals(STATUS_CHECK_REQUEST);
    }
}
