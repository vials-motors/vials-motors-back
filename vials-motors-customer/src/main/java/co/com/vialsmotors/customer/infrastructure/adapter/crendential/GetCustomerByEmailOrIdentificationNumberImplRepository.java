package co.com.vialsmotors.customer.infrastructure.adapter.crendential;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerByEmailOrIdentificationNumberRepository;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.jpa.CustomerSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetCustomerByEmailOrIdentificationNumberImplRepository implements GetCustomerByEmailOrIdentificationNumberRepository {

    private static final Logger LOGGER = Logger.getLogger(GetCustomerByEmailOrIdentificationNumberImplRepository.class);
    private static final Boolean STATUS_TRUE = Boolean.TRUE;
    private final CustomerSqlServerRepository customerSqlServerRepository;

    @Override
    public Optional<CustomerEntity> getCustomerByEmail(String email) {
        try {
            return this.customerSqlServerRepository.findByEmailAndStatus(email, STATUS_TRUE);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error get data with email with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<CustomerEntity> getCustomerByIdentification(String identificationNumber) {
        try {
            return this.customerSqlServerRepository.findByIdentificationNumberAndStatus(identificationNumber, STATUS_TRUE);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error get data with identification number with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
