package co.com.vialsmotors.customer.infrastructure.adapter.crendential;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.customer.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.customer.infrastructure.apirest.client.CredentialService;
import co.com.vialsmotors.customer.infrastructure.apirest.response.FeignResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UpdateCredentialAdapter implements UpdateCredentialPort {

    private static final Logger LOGGER = Logger.getLogger(UpdateCredentialAdapter.class);
    private static final String STATUS_CHECK_REQUEST = "SUCC-01";

    private final CredentialService credentialService;

    @Override
    public Boolean updateCredential(String username, String customerID) {
        try {
            return validateResponse(credentialService.updateCredential(generateHeaders(username, customerID)));
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error callback update credential service with message: " + exception.getMessage());
            throw new CallCredentialException(exception.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL.getMessage());
        }
    }

    private Map<String, Object> generateHeaders(String username, String customerId){
        Map<String, Object> headers = new HashMap<>();
        headers.put("username", username);
        headers.put("customerId", customerId);
        return headers;
    }

    private Boolean validateResponse(ResponseEntity<FeignResponse> response){
        return Objects.requireNonNull(response.getBody()).getCode().equals(STATUS_CHECK_REQUEST);
    }
}
