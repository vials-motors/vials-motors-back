package co.com.vialsmotors.customer.infrastructure.adapter.customer;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.customer.domain.port.customer.GetCustomerRepository;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.jpa.CustomerSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetCustomerImplRepository implements GetCustomerRepository {

    private static final Logger LOGGER = Logger.getLogger(GetCustomerImplRepository.class);

    private final CustomerSqlServerRepository customerSqlServerRepository;

    @Override
    public Optional<CustomerEntity> getCustomerForIdCustomer(String customerId) {
        try {
            return customerSqlServerRepository.findByCustomerId(customerId);
        }catch (Exception e) {
            LOGGER.error("ERROR-MS-CUSTOMER: Error get customer with customer id: " + customerId + " with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<CustomerEntity> getCustomerByIdentificationNumber(String identificationNumber) {
        try {
            return customerSqlServerRepository.findByIdentificationNumber(identificationNumber);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error get customer with identification Number: " + identificationNumber);
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
