package co.com.vialsmotors.customer.infrastructure.adapter.customer;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.customer.domain.port.customer.GetListCustomerRepository;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.jpa.CustomerSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListCustomerImplRepository implements GetListCustomerRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListCustomerImplRepository.class);

    private final CustomerSqlServerRepository customerSqlServerRepository;

    @Override
    public List<CustomerEntity> getListCustomer() {
        try {
            return customerSqlServerRepository.findAll();
        }catch (Exception exception){
            LOGGER.error("ERR-MS-CUSTOMER: Error in getListCustomer for class " + GetListCustomerImplRepository.class.getName());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    @Transactional
    public List<CustomerEntity> getListCustomerSp(ZonedDateTime dateInit, ZonedDateTime dateFinish) {
        try {
            return customerSqlServerRepository.getCustomerFluxSp(dateInit, dateFinish);
        }catch (Exception e){
            LOGGER.error("ERR-MS-CUSTOMER: Error in getListCustomer for class " + GetListCustomerImplRepository.class.getName());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
