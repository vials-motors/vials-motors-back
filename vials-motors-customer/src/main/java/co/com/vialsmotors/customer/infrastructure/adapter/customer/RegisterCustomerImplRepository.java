package co.com.vialsmotors.customer.infrastructure.adapter.customer;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.customer.domain.port.customer.RegisterCustomerRepository;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.jpa.CustomerSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class RegisterCustomerImplRepository implements RegisterCustomerRepository {

    private static final Logger LOGGER = Logger.getLogger(RegisterCustomerImplRepository.class);

    private final CustomerSqlServerRepository customerSqlServerRepository;

    @Override
    public void registerCustomer(CustomerEntity customerEntity) {
        try {
            customerSqlServerRepository.save(customerEntity);
        }catch (Exception exception) {
            LOGGER.error("ERROR-MS-CUSTOMER: Error in registerCustomer for class RegisterCustomerImplRepository: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<CustomerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email) {
        return customerSqlServerRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, status, email);
    }
}
