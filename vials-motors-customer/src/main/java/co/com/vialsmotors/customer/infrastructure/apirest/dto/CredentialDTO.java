package co.com.vialsmotors.customer.infrastructure.apirest.dto;

public record CredentialDTO ( String email, String password,  String customerID) {
}
