package co.com.vialsmotors.customer.infrastructure.controller;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.handler.delete.DeleteCustomerHandler;
import co.com.vialsmotors.customer.application.handler.getcustomer.GetCustomerHandler;
import co.com.vialsmotors.customer.application.handler.list.GetCustomerByIdentificationNumberHandler;
import co.com.vialsmotors.customer.application.handler.list.GetListCustomerForSpHandler;
import co.com.vialsmotors.customer.application.handler.list.GetListCustomerHandler;
import co.com.vialsmotors.customer.application.handler.register.RegisterCustomerHandler;
import co.com.vialsmotors.customer.application.handler.update.UpdateCustomerHandler;
import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.response.CustomerResponse;
import co.com.vialsmotors.customer.domain.response.GenericResponse;
import co.com.vialsmotors.customer.domain.response.ListGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {

    private static final Logger LOGGER = Logger.getLogger(CustomerController.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final RegisterCustomerHandler registerCustomerHandler;
    private final GetListCustomerHandler getListCustomerHandler;
    private final UpdateCustomerHandler updateCustomerHandler;
    private final GetCustomerHandler getCustomerHandler;
    private final DeleteCustomerHandler deleteCustomerHandler;
    private final GetListCustomerForSpHandler getListCustomerForSpHandler;
    private final GetCustomerByIdentificationNumberHandler getCustomerByIdentificationNumberHandler;


    @PostMapping
    public ResponseEntity<GenericResponse> addCustomer(@RequestBody CustomerCommand customerCommand) throws JsonProcessingException {
        LOGGER.info("MS-CUSTOMER: Register customer in " + CustomerController.class.getName() + " With " + OBJECT_MAPPER.writeValueAsString(customerCommand));
        return registerCustomerHandler.registerCustomer(customerCommand);
    }

    @GetMapping
    public ResponseEntity<ListGenericResponse<CustomerDTO>> getListCustomer(){
        LOGGER.info("INFO-MS-CUSTOMER: Received request for get list customer in CustomerController");
        return getListCustomerHandler.getListCustomer();
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateCustomer(@RequestBody CustomerCommand customerCommand,
                                                          @PathParam("identificationNumber") String identificationNumber,
                                                          @PathParam("email") String email) throws JsonProcessingException {
        LOGGER.info("INFO-MS-CUSTOMER: Request receiver for Update Customer with Body request: " + OBJECT_MAPPER.writeValueAsString(customerCommand));
        LOGGER.info("INFO-MS-CUSTOMER: Request receiver with identification Number: " + identificationNumber);
        return updateCustomerHandler.updateCustomer(customerCommand, identificationNumber, email);
    }

    @GetMapping(path = "/get-customer")
    public ResponseEntity<CustomerResponse> getCustomerByCustomerId(@PathParam("customerId") String customerId){
        LOGGER.info("INFO-MS-CUSTOMER: Request receiver for get customer by customer id: " + customerId);
        return getCustomerHandler.getCustomer(customerId);
    }

    @GetMapping(path = "/get-customer-identification")
    public ResponseEntity<CustomerResponse> getCustomerByIdentificationNumber(@RequestParam("identificationNumber") String identificationNumber){
        LOGGER.info("INFO-MS-CUSTOMER: Receiver request for get customer by identification number: " + identificationNumber);
        return getCustomerByIdentificationNumberHandler.getCustomer(identificationNumber);
    }

    @PutMapping("/delete")
    public ResponseEntity<GenericResponse> deleteCustomer(@PathParam("identificationNumber") String identificationNumber, @PathParam("email") String email){
        LOGGER.info("INFO-MS-CUSTOMER: Request receiver for delete customer by identification number " + identificationNumber);
        return deleteCustomerHandler.deteleCustomer(identificationNumber, email);
    }

    @GetMapping("/list-sp")
    public ResponseEntity<ListGenericResponse<CustomerSpDTO>> getListForSp(@RequestParam("dateInit") String dateInit, @RequestParam("dateFinish") String dateFinish){
        LOGGER.info("INFO-MS-CUSTOMER: Request receiver for get list customer for sp with date init: " + dateInit + " and date finish: " + dateFinish);
        return getListCustomerForSpHandler.getList(dateInit, dateFinish);
    }
}
