package co.com.vialsmotors.customer.infrastructure.jpa;

import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerSqlServerRepository extends JpaRepository<CustomerEntity, Long> {

    @Query(value = "SELECT * FROM customer where identification_number = ?1 and status = ?2 and email =?3", nativeQuery = true)
    Optional<CustomerEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email);

    @Query(value = "SELECT * FROM customer where customer_ID = ?1", nativeQuery = true)
    Optional<CustomerEntity> findByCustomerId(String customerId);

    @Query(value = "SELECT * FROM customer WHERE identification_number = ?1", nativeQuery = true)
    Optional<CustomerEntity> findByIdentificationNumber(String identificationNumber);

    @Query(value = "SELECT * FROM customer WHERE email =?1 AND status =?2", nativeQuery = true)
    Optional<CustomerEntity> findByEmailAndStatus(String email, Boolean status);

    @Query(value = "SELECT * FROM customer WHERE identification_number =?1 AND status =?2", nativeQuery = true)
    Optional<CustomerEntity> findByIdentificationNumberAndStatus(String identificationNumber, Boolean status);

    @Procedure("get_customer")
    List<CustomerEntity> getCustomerFluxSp(@Param("date_init") ZonedDateTime dateInit,
                                           @Param("date_finish") ZonedDateTime dateFinish);
}
