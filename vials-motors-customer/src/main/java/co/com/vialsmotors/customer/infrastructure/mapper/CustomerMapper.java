package co.com.vialsmotors.customer.infrastructure.mapper;

import co.com.vialsmotors.customer.domain.dto.CustomerDTO;
import co.com.vialsmotors.customer.domain.dto.CustomerSpDTO;
import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.util.CreateCustomerIdUtils;
import co.com.vialsmotors.customer.infrastructure.util.ValidateStatusUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerMapper {

    private static final String NOT_INSTANTIABLE = "The class " + CustomerMapper.class.getName() + " is not instantiable";

    private CustomerMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static CustomerEntity convertModelToEntity(Customer customer, Boolean status){
        return new CustomerEntity(customer.firstName(), customer.secondName(),
                customer.firstLastName(), customer.secondLastName(), customer.phone(),
                customer.identificationNumber(),
                CreateCustomerIdUtils.generateAlphanumericString(),
                IdentificationTypeMapper.convertModelToEntity(customer.identificationType()), customer.email(), status);
    }

    public static List<CustomerDTO> convertListEntityToDto(List<CustomerEntity> entityList){
        return entityList.stream()
                .map(entityTemporal -> new CustomerDTO(entityTemporal.getFirstName(), entityTemporal.getSecondName(),
                        entityTemporal.getFirstLastName(), entityTemporal.getSecondLastName(), entityTemporal.getPhone(),
                        entityTemporal.getIdentificationNumber(), entityTemporal.getEmail(), entityTemporal.getCustomerID()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static CustomerDTO convertEntityToDto(CustomerEntity entity){
        return new CustomerDTO(entity.getFirstName(), entity.getSecondName(),
                entity.getFirstLastName(), entity.getSecondLastName(), entity.getPhone(),
                entity.getIdentificationNumber(), entity.getEmail(), entity.getCustomerID());
    }

    public static List<CustomerSpDTO> convertListEntityToListSpDTO(List<CustomerEntity> list){
        return list.stream()
                .map(temporal -> new CustomerSpDTO(temporal.getFirstName(), temporal.getSecondName(),
                        temporal.getFirstLastName(), temporal.getSecondLastName(), temporal.getPhone(),
                        temporal.getIdentificationNumber(), temporal.getEmail(),
                        ValidateStatusUtils.validate(temporal.getStatus()),
                        temporal.getIdentificationType().getDescription()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}

