package co.com.vialsmotors.customer.infrastructure.mapper;

import co.com.vialsmotors.customer.domain.model.IdentificationType;
import co.com.vialsmotors.customer.infrastructure.entity.IdentificationTypeEntity;
import org.modelmapper.ModelMapper;

public class IdentificationTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class " + IdentificationTypeMapper.class.getName() + " is not instantiable";

    private IdentificationTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static IdentificationTypeEntity convertModelToEntity(IdentificationType identificationType){
        return new IdentificationTypeEntity(identificationType.idIdentificationType(), identificationType.description());
    }
}
