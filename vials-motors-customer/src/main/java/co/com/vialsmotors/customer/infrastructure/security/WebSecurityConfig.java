package co.com.vialsmotors.customer.infrastructure.security;

import co.com.vialsmotors.customer.infrastructure.security.jwt.AuthEntryPointJwt;
import co.com.vialsmotors.customer.infrastructure.security.jwt.AuthTokenFilter;
import co.com.vialsmotors.customer.infrastructure.security.jwt.JwtUtils;
import co.com.vialsmotors.customer.infrastructure.security.userdetails.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity()
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig {

    private static final String CUSTOMER = "/customer";
    public static final String ADMIN = "ADMIN";
    public static final String CLIENT = "CLIENT";
    public static final String TECHNICAL = "TECHNICAL";

    private final AuthEntryPointJwt unauthorizedHandler;
    private final JwtUtils jwtUtils;
    private final UserDetailsServiceImpl userDetailsService;


    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter(){
        return new AuthTokenFilter(jwtUtils, userDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers(HttpMethod.POST, CUSTOMER).permitAll()
                .antMatchers(HttpMethod.GET, CUSTOMER).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.PUT, CUSTOMER).hasAnyRole(CLIENT)
                .antMatchers(HttpMethod.GET, "/customer/get-customer").permitAll()
                .antMatchers(HttpMethod.PUT, "/customer/delete").hasAnyRole(ADMIN, CLIENT, TECHNICAL)
                .antMatchers(HttpMethod.GET, "/customer/get-customer-identification").permitAll()
                .antMatchers(HttpMethod.GET, "/customer/list-sp").permitAll()
                .anyRequest().authenticated().and()
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class).build();
    }
}
