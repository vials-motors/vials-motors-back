package co.com.vialsmotors.customer.infrastructure.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


public class GenerateDateUtils {

    private static final String NOT_INSTANTIABLE = "The class Generate Date Utils is not instantiable";

    private GenerateDateUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ZonedDateTime generateDate(String date){
        String newDate = date + "T" + "00:00:00.000-05:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        return ZonedDateTime.parse(newDate, formatter).withZoneSameInstant(ZoneId.of("UTC-5"));
    }
}
