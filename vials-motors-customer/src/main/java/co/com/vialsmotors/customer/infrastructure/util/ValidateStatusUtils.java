package co.com.vialsmotors.customer.infrastructure.util;

public class ValidateStatusUtils {

    private static final String NOT_INSTANTIABLE = "The class validate status utils is not instantiable";

    private ValidateStatusUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String validate(Boolean id){
       return Boolean.TRUE.equals(id) ? "ACTIVE":"INACTIVE";
    }
}
