package co.com.vialsmotors.customer.test.application;

import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.command.IdentificationTypeCommand;

public class CustomerCommandTestDataBuild {

    private static final String FIRST_NAME = "Maria";
    private static final String SECOND_NAME = "Alejandra";
    private static final String FIRST_LAST_NAME = "Agudelo";
    private static final String SECOND_LAST_NAME = "Mejia";
    private static final String PHONE = "11111111";
    private static final String IDENTIFICATION_NUMBER = "11111111";
    private static final IdentificationTypeCommand IDENTIFICATION_TYPE = new IdentificationTypeCommand(1L, "CC");
    private static final String PASSWORD = "sebas123";
    private static final String EMAIL = "maria@gmail.com";

    private String firstName;
    private final String secondName;
    private final String firstLastName;
    private final String secondLastName;
    private final String phone;
    private String identificationNumber;
    private IdentificationTypeCommand identificationType;
    private final String password;
    private String email;

    public CustomerCommandTestDataBuild (){
        this.firstName = FIRST_NAME;
        this.secondName = SECOND_NAME;
        this.firstLastName = FIRST_LAST_NAME;
        this.secondLastName = SECOND_LAST_NAME;
        this.phone = PHONE;
        this.identificationNumber = IDENTIFICATION_NUMBER;
        this.identificationType = IDENTIFICATION_TYPE;
        this.password = PASSWORD;
        this.email = EMAIL;
    }

    public CustomerCommandTestDataBuild withIdentificationNumber(String identificationNumber){
        this.identificationNumber = identificationNumber;
        return this;
    }

    public CustomerCommandTestDataBuild withIdentificationType(IdentificationTypeCommand identificationType){
        this.identificationType = identificationType;
        return this;
    }

    public CustomerCommandTestDataBuild withFistName(String fistName){
        this.firstName = fistName;
        return this;
    }

    public CustomerCommandTestDataBuild withEmail(String email){
        this.email = email;
        return this;
    }
    public CustomerCommand build(){
        return new CustomerCommand(this.firstName, this.secondName, firstLastName,
                this.secondLastName, this.phone, this.identificationNumber,
                this.identificationType, this.password, this.email);
    }
}
