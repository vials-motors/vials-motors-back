package co.com.vialsmotors.customer.test.domain.databuilder;

import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.domain.model.IdentificationType;

public class CustomerTestDataBuild {

    private static final String FIRST_NAME = "Juan";
    private static final String SECOND_NAME = "Sebastian";
    private static final String FIRST_LAST_NAME = "Agudelo";
    private static final String SECOND_LAST_NAME = "Mejia";
    private static final String PHONE = "123456789";
    private static final String IDENTIFICATION_NUMBER = "123456789";
    private static final IdentificationType IDENTIFICATION_TYPE = new IdentificationType(1L, "CC");
    private static final String PASSWORD = "sebas123";
    private static final String EMAIL = "sebas@gmail.com";

    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    private String phone;
    private String identificationNumber;
    private IdentificationType identificationType;
    private String password;
    private String email;

    public CustomerTestDataBuild (){
        this.firstName = FIRST_NAME;
        this.secondName = SECOND_NAME;
        this.firstLastName = FIRST_LAST_NAME;
        this.secondLastName = SECOND_LAST_NAME;
        this.phone = PHONE;
        this.identificationNumber = IDENTIFICATION_NUMBER;
        this.identificationType = IDENTIFICATION_TYPE;
        this.password = PASSWORD;
        this.email = EMAIL;
    }

    public CustomerTestDataBuild withFirstName(String firstName){
        this.firstName = firstName;
        return this;
    }

    public CustomerTestDataBuild withFirstLastName(String firstLastName){
        this.firstLastName = firstLastName;
        return this;
    }

    public CustomerTestDataBuild withSecondName(String secondName){
        this.secondName = secondName;
        return this;
    }

    public CustomerTestDataBuild withSecondLastName(String secondLastName){
        this.secondLastName = secondLastName;
        return this;
    }

    public CustomerTestDataBuild withPhone(String phone){
        this.phone = phone;
        return this;
    }

    public CustomerTestDataBuild withIdentificationNumber(String identificationNumber){
        this.identificationNumber = identificationNumber;
        return this;
    }

    public CustomerTestDataBuild withIdentificationType(IdentificationType identificationType){
        this.identificationType = identificationType;
        return this;
    }

    public CustomerTestDataBuild withPassword(String password){
        this.password = password;
        return this;
    }

    public CustomerTestDataBuild withEmail(String email){
        this.email = email;
        return this;
    }

    public Customer build(){
        return new Customer(this.firstName, this.secondName, this.firstLastName,
                this.secondLastName, this.phone, this.identificationNumber, this.identificationType,
                this.password, this.email);
    }
}
