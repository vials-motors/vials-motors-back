package co.com.vialsmotors.customer.test.domain.model;

import co.com.vialsmotors.commons.domain.enums.EFieldMessage;
import co.com.vialsmotors.commons.domain.exception.IdentificationTypeException;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.exception.FieldException;
import co.com.vialsmotors.customer.test.domain.databuilder.CustomerTestDataBuild;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    @Test
    void validateFistNameTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withFirstName(null).build());

        assertEquals(EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getMessage(), exception.getMessage());
    }

    @Test
    void validateFistLastNameTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withFirstLastName(null).build());

        assertEquals(EFieldMessage.FIELD_FIRST_LAST_NAME_MESSAGE.getMessage(), exception.getMessage());
    }

    @Test
    void validatePhoneTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withPhone(null).build());

        assertEquals(EFieldMessage.FIELD_PHONE_MESSAGE.getMessage(), exception.getMessage());
    }

    @Test
    void validateIdentificationNumberTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withIdentificationNumber(null).build());

        assertEquals(EFieldMessage.FIELD_IDENTIFICATION_NUMBER.getMessage(), exception.getMessage());
    }

    @Test
    void validateEmailTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withEmail(null).build());

        assertEquals(EResponse.FIELD_EMAIL_ERROR.getMessage(), exception.getMessage());
    }

    @Test
    void validatePasswordTest(){
        FieldException exception = assertThrows(FieldException.class, () ->
                new CustomerTestDataBuild().withPassword(null).build());

        assertEquals(EResponse.FIELD_PASSWORD_ERROR.getMessage(), exception.getMessage());
    }

    @Test
    void validateIdentificationTypeTest(){
        IdentificationTypeException exception = assertThrows(IdentificationTypeException.class, () ->
                new CustomerTestDataBuild().withIdentificationType(null).build());

        assertEquals(EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getMessage(), exception.getMessage());
    }
}
