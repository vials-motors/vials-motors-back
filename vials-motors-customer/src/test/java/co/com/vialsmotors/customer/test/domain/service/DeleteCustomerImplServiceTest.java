package co.com.vialsmotors.customer.test.domain.service;

import static org.junit.jupiter.api.Assertions.*;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.customer.VialsMotorsCustomerApplication;
import co.com.vialsmotors.customer.domain.enums.EResponse;

import co.com.vialsmotors.customer.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.customer.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.customer.domain.service.delete.DeleteCustomerService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = VialsMotorsCustomerApplication.class)
@AutoConfigureMockMvc
class DeleteCustomerImplServiceTest{

    @Autowired
    private DeleteCustomerService deleteCustomerService;
    @Mock
    private DeleteCredentialPort deleteCredentialPort;

    @Test
    void validateIdentificationNumberInvalidTest(){
        IdentificationNumberException identificationNumberException = assertThrows(IdentificationNumberException.class,
                () -> deleteCustomerService.deleteCredential(" ", "sagudelo@gmail.com"));

        assertEquals(identificationNumberException.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage());
    }

    @Test
    void validateEmailInvalidTest(){
        FieldException fieldException = assertThrows(FieldException.class,
                () -> deleteCustomerService.deleteCredential("123", " "));

        assertEquals(fieldException.getMessage(), EResponse.FIELD_EMAIL_ERROR.getMessage());
    }
}
