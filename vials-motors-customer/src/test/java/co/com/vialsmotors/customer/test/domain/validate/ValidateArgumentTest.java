package co.com.vialsmotors.customer.test.domain.validate;

import static org.junit.jupiter.api.Assertions.*;

import co.com.vialsmotors.commons.domain.exception.IdentificationTypeException;
import co.com.vialsmotors.customer.domain.exception.FieldException;
import co.com.vialsmotors.customer.domain.validate.ValidateArgument;
import org.junit.jupiter.api.Test;

class ValidateArgumentTest {

    @Test
    void validateIdentificationTypeValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateIdentificationType(new Object(), "status", "message"));
    }

    @Test
    void validateIdentificationTypeInvalidTest(){
        IdentificationTypeException exception = assertThrows(IdentificationTypeException.class,
                () -> ValidateArgument.validateIdentificationType(null, "status", "message"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }

    @Test
    void validateFieldStringValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateFieldString("String", "status", "message"));
    }

    @Test
    void validateFieldStringInvalidTest(){
        FieldException exception = assertThrows(FieldException.class,
                () -> ValidateArgument.validateFieldString(null, "status", "message"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }

    @Test
    void validateFieldLongValidTest(){
        assertDoesNotThrow(() -> ValidateArgument.validateFieldLong(1L, "status", "message"));
    }

    @Test
    void validateFieldLongInvalidTest(){
        FieldException exception = assertThrows(FieldException.class,
                () -> ValidateArgument.validateFieldLong(null, "status", "message"));

        assertEquals("message", exception.getMessage());
        assertEquals("status", exception.getStatus());
    }
}
