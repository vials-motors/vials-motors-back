package co.com.vialsmotors.customer.test.infrastructure.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.enums.EFieldMessage;
import co.com.vialsmotors.customer.VialsMotorsCustomerApplication;
import co.com.vialsmotors.customer.application.command.CustomerCommand;
import co.com.vialsmotors.customer.application.handler.delete.DeleteCustomerHandler;
import co.com.vialsmotors.customer.application.handler.getcustomer.GetCustomerHandler;
import co.com.vialsmotors.customer.application.handler.list.GetCustomerByIdentificationNumberHandler;
import co.com.vialsmotors.customer.application.handler.list.GetListCustomerForSpHandler;
import co.com.vialsmotors.customer.application.handler.list.GetListCustomerHandler;
import co.com.vialsmotors.customer.application.handler.register.RegisterCustomerHandler;
import co.com.vialsmotors.customer.application.handler.update.UpdateCustomerHandler;
import co.com.vialsmotors.customer.domain.enums.EResponse;
import co.com.vialsmotors.customer.domain.model.Customer;
import co.com.vialsmotors.customer.infrastructure.adapter.crendential.DeleteCredentialAdapter;
import co.com.vialsmotors.customer.infrastructure.adapter.crendential.RegisterCredentialAdapter;
import co.com.vialsmotors.customer.infrastructure.adapter.crendential.UpdateCredentialAdapter;
import co.com.vialsmotors.customer.infrastructure.entity.CustomerEntity;
import co.com.vialsmotors.customer.infrastructure.jpa.CustomerSqlServerRepository;
import co.com.vialsmotors.customer.infrastructure.mapper.CustomerMapper;
import co.com.vialsmotors.customer.test.application.CustomerCommandTestDataBuild;
import co.com.vialsmotors.customer.test.domain.databuilder.CustomerTestDataBuild;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

@SpringBootTest(classes = VialsMotorsCustomerApplication.class)
@AutoConfigureMockMvc
@Transactional
class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CustomerSqlServerRepository customerSqlServerRepository;
    @Autowired
    private RegisterCustomerHandler registerCustomerHandler;
    @Autowired
    private GetListCustomerHandler getListCustomerHandler;
    @Autowired
    private UpdateCustomerHandler updateCustomerHandler;
    @Autowired
    private GetCustomerHandler getCustomerHandler;
    @Autowired
    private DeleteCustomerHandler deleteCustomerHandler;
    @MockBean
    private GetListCustomerForSpHandler getListCustomerForSpHandler;
    @Autowired
    private GetCustomerByIdentificationNumberHandler getCustomerByIdentificationNumberHandler;
    @MockBean
    private RegisterCredentialAdapter credentialService;
    @MockBean
    private UpdateCredentialAdapter updateCredentialAdapter;
    @MockBean
    private DeleteCredentialAdapter deleteCredentialAdapter;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        customerSqlServerRepository.save(CustomerMapper.convertModelToEntity(new CustomerTestDataBuild().build(), true));
    }

    @Test
    void registerCustomer() throws Exception{
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().build();
        Mockito.when(credentialService.registerCredential(Mockito.any(), Mockito.any())).thenReturn(true);
        mockMvc.perform(post("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is(EResponse.MESSAGE_RESPONSE_SUCCESS.getCode())));
    }

    @Test
    void registerCustomerInactive() throws Exception{
        Customer customer = new CustomerTestDataBuild().build();
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().withIdentificationNumber(customer.identificationNumber()).withEmail(customer.email()).build();
        Mockito.when((deleteCredentialAdapter.deleteCredential(Mockito.any()))).thenReturn(true);
        deleteCustomerHandler.deteleCustomer(customer.identificationNumber(), customer.email());
        Mockito.when(credentialService.registerCredential(Mockito.any(), Mockito.any())).thenReturn(true);
        mockMvc.perform(post("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                        .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is(EResponse.MESSAGE_RESPONSE_SUCCESS.getCode())));
    }

    @Test
    void registerCustomerInvalid() throws Exception{
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().withIdentificationNumber("123456789").build();
        mockMvc.perform(post("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is(EResponse.IDENTIFICATION_NUMBER_EXIST.getCode())));
    }

    @Test
    void registerCustomerWithIdentificationTypeNull() throws Exception{
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().withIdentificationType(null).build();
        mockMvc.perform(post("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                        .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is( EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getCode())));
    }

    @Test
    void getCustomerList() throws Exception{
        mockMvc.perform(get("http://localhost:9001/customer"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[0].identificationNumber").value("123456789"))
                .andExpect(jsonPath("$.status", is(EResponse.GET_LIST_SUCCESSFULLY.getCode())))
                .andExpect(jsonPath("$.data").isArray());
    }

    @Test
    void updateCustomer() throws Exception{
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().withFistName("JHON").build();
        Mockito.when(updateCredentialAdapter.updateCredential(Mockito.any(), Mockito.any())).thenReturn(true);
        mockMvc.perform(put("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                .contentType(MediaType.APPLICATION_JSON)
                .param("identificationNumber", "123456789")
                .param("email", "sebas@gmail.com")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code", is(EResponse.UPDATE_CUSTOMER_SUCCESS.getCode())));
    }

    @Test
    void updateCustomerWithIdentificationNumberNull() throws Exception{
        CustomerCommand customerCommand = new CustomerCommandTestDataBuild().withFistName("JHON").build();
        Mockito.when(updateCredentialAdapter.updateCredential(Mockito.any(), Mockito.any())).thenReturn(true);
        mockMvc.perform(put("http://localhost:9001/customer").content(objectMapper.writeValueAsString(customerCommand))
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("identificationNumber", " ")
                        .param("email", "sebas@gmail.com")).andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code", is(EResponse.IDENTIFICATION_NUMBER_ERROR.getCode())));
    }

    @Test
    void getCustomerByCustomerId() throws Exception{
        CustomerEntity customerEntity = CustomerMapper.convertModelToEntity(new CustomerTestDataBuild().withIdentificationNumber("111111").withEmail("agudelo@gmail.com").build(), true);
        customerEntity.setCustomerID("ABCD123");
        customerSqlServerRepository.save(customerEntity);
        mockMvc.perform(get("http://localhost:9001/customer/get-customer")
                        .param("customerId", "ABCD123"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customer.identificationNumber").value("111111"))
                .andExpect(jsonPath("$.status", is(EResponse.GET_CUSTOMER_SUCCESSFULLY.getCode())));
    }

    @Test
    void getCustomerByCustomerIdInvalid() throws Exception{
        mockMvc.perform(get("http://localhost:9001/customer/get-customer")
                        .param("customerId", "INVALIDO"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(EResponse.FIND_CUSTOMER_ERROR.getMessage()))
                .andExpect(jsonPath("$.code", is(EResponse.FIND_CUSTOMER_ERROR.getCode())));
    }

    @Test
    void getCustomerByIdentificationNumber() throws Exception{
        mockMvc.perform(get("http://localhost:9001/customer/get-customer-identification")
                        .param("identificationNumber", "123456789"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customer.identificationNumber").value("123456789"))
                .andExpect(jsonPath("$.status", is(EResponse.GET_CUSTOMER_SUCCESSFULLY.getCode())));
    }

    @Test
    void getCustomerByIdentificationNumberWithIdentificationNumberInvalid() throws Exception{
        mockMvc.perform(get("http://localhost:9001/customer/get-customer-identification")
                        .param("identificationNumber", " "))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage())))
                .andExpect(jsonPath("$.code", is(EResponse.IDENTIFICATION_NUMBER_ERROR.getCode())));
    }

    @Test
    void deleteCustomerByIdentificationNumber() throws Exception{
        Mockito.when((deleteCredentialAdapter.deleteCredential(Mockito.any()))).thenReturn(true);
        mockMvc.perform(put("http://localhost:9001/customer/delete")
                .param("identificationNumber", "123456789")
                        .param("email", "sebas@gmail.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(EResponse.DELETE_CUSTOMER_SUCCESSFULLY.getMessage())))
                .andExpect(jsonPath("$.code", is(EResponse.DELETE_CUSTOMER_SUCCESSFULLY.getCode())));
    }

    @Test
    void deleteCustomerWithErrorCall() throws Exception{
        Mockito.when(deleteCredentialAdapter.deleteCredential(Mockito.any())).thenReturn(false);
        mockMvc.perform(put("http://localhost:9001/customer/delete")
                        .param("identificationNumber", "123456789")
                        .param("email", "sebas@gmail.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(ECallMessage.ERROR_CALL_CREDENTIAL.getMessage())));
    }

    @Test
    void deleteCustomerWithCustomrInvalid() throws Exception{
        mockMvc.perform(put("http://localhost:9001/customer/delete")
                        .param("identificationNumber", "NO")
                        .param("email", "sebas@gmail.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(EResponse.FIND_CUSTOMER_ERROR.getMessage())));
    }
}
