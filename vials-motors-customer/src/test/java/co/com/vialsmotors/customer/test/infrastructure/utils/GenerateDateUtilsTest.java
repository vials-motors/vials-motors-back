package co.com.vialsmotors.customer.test.infrastructure.utils;

import co.com.vialsmotors.customer.infrastructure.util.GenerateDateUtils;
import org.junit.jupiter.api.Test;

import javax.swing.text.DateFormatter;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GenerateDateUtilsTest {

    @Test
    void generateDateTest() {

        String date = "2022-01-01";
        String newDate= "2022-01-01T00:00:00.000-05:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(newDate, formatter).withZoneSameInstant(ZoneId.of("UTC-5"));

        ZonedDateTime dateExpect = GenerateDateUtils.generateDate(date);

        assertEquals(dateExpect, zonedDateTime);
    }
}
