package co.com.vialsmotors.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VialsMotorsEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(VialsMotorsEmailApplication.class, args);
	}

}
