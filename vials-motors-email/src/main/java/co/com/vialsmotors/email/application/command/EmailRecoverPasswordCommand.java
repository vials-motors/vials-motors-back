package co.com.vialsmotors.email.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailRecoverPasswordCommand extends EmailBaseCommand{

    private String otp;
}
