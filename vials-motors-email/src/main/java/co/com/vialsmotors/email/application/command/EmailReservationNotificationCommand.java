package co.com.vialsmotors.email.application.command;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailReservationNotificationCommand extends EmailBaseCommand{

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "UTC")
    private ZonedDateTime date;
}
