package co.com.vialsmotors.email.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailRevisionStatusCommand extends EmailBaseCommand{

    private String nameClient;
    private String status;
    private String novelty;
    private String extraObservation;
}
