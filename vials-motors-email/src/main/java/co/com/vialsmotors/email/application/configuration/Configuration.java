package co.com.vialsmotors.email.application.configuration;

import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.service.contactus.SendEmailContactUsImplService;
import co.com.vialsmotors.email.domain.service.recoverpassword.SendEmailRecoverPasswordImplService;
import co.com.vialsmotors.email.domain.service.reservationnotification.SendEmailReservationNotificationImplService;
import co.com.vialsmotors.email.domain.service.revisionstatus.SendEmailRevisionStatusImplService;
import co.com.vialsmotors.email.domain.service.welcomecustomer.SendEmailWelcomeCustomerImplService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public SendEmailWelcomeCustomerImplService sendEmailImplService(SendEmailPort sendEmailPort){
        return new SendEmailWelcomeCustomerImplService(sendEmailPort);
    }

    @Bean
    public SendEmailReservationNotificationImplService sendEmailReservationNotificationImplService(SendEmailPort sendEmailPort){
        return new SendEmailReservationNotificationImplService(sendEmailPort);
    }

    @Bean
    public SendEmailContactUsImplService sendEmailContactUsImplService(SendEmailPort sendEmailPort){
        return new SendEmailContactUsImplService(sendEmailPort);
    }

    @Bean
    public SendEmailRevisionStatusImplService sendEmailRevisionStatusImplService(SendEmailPort sendEmailPort){
        return new SendEmailRevisionStatusImplService(sendEmailPort);
    }

    @Bean
    public SendEmailRecoverPasswordImplService sendEmailRecoverPasswordImplService(SendEmailPort sendEmailPort){
        return new SendEmailRecoverPasswordImplService(sendEmailPort);
    }
}
