package co.com.vialsmotors.email.application.factory;

import co.com.vialsmotors.email.application.command.EmailContactUsCommand;
import co.com.vialsmotors.email.domain.model.EmailContactUs;
import org.springframework.stereotype.Component;

@Component
public class EmailContactUsFactory {

    public EmailContactUs execute(EmailContactUsCommand emailContactUsCommand){
        return new EmailContactUs(emailContactUsCommand.getEmail(), emailContactUsCommand.getMessage(), emailContactUsCommand.getPhone(),
                emailContactUsCommand.getName());
    }
}
