package co.com.vialsmotors.email.application.factory;

import co.com.vialsmotors.email.application.command.EmailRecoverPasswordCommand;
import co.com.vialsmotors.email.domain.model.EmailRecoverPassword;
import org.springframework.stereotype.Component;

@Component
public class EmailRecoverPasswordFactory {

    public EmailRecoverPassword execute(EmailRecoverPasswordCommand emailRecoverPasswordCommand){
        return new EmailRecoverPassword(emailRecoverPasswordCommand.getEmail(), emailRecoverPasswordCommand.getOtp());
    }
}
