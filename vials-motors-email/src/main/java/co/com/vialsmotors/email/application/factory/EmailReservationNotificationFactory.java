package co.com.vialsmotors.email.application.factory;

import co.com.vialsmotors.email.application.command.EmailReservationNotificationCommand;
import co.com.vialsmotors.email.domain.model.EmailReservationNotification;
import org.springframework.stereotype.Component;

@Component
public class EmailReservationNotificationFactory {

    public EmailReservationNotification execute(EmailReservationNotificationCommand emailReservationNotificationCommand){
        return new EmailReservationNotification(emailReservationNotificationCommand.getEmail(), emailReservationNotificationCommand.getDate());
    }
}
