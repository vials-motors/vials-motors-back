package co.com.vialsmotors.email.application.factory;

import co.com.vialsmotors.email.application.command.EmailRevisionStatusCommand;
import co.com.vialsmotors.email.domain.model.EmailRevisionStatus;
import org.springframework.stereotype.Component;

@Component
public class EmailRevisionStatusFactory {

    public EmailRevisionStatus execute(EmailRevisionStatusCommand emailRevisionStatusCommand){
        return new EmailRevisionStatus(emailRevisionStatusCommand.getEmail(), emailRevisionStatusCommand.getNameClient(),
                emailRevisionStatusCommand.getStatus(), buildStringObject(emailRevisionStatusCommand.getNovelty()),
                buildStringObject(emailRevisionStatusCommand.getExtraObservation()));
    }

    private String buildStringObject(String object){
        return object == null || object.isBlank() ? "No Aplica":object;
    }
}
