package co.com.vialsmotors.email.application.factory;

import co.com.vialsmotors.email.application.command.EmailWelcomeCustomerCommand;
import co.com.vialsmotors.email.domain.model.EmailWelcomeCustomer;
import org.springframework.stereotype.Component;

@Component
public class EmailWelcomeCustomerFactory {

    public EmailWelcomeCustomer execute(EmailWelcomeCustomerCommand emailWelcomeCustomerCommand){
        return new EmailWelcomeCustomer(emailWelcomeCustomerCommand.getEmail(), emailWelcomeCustomerCommand.getClientName());
    }
}
