package co.com.vialsmotors.email.application.handler.contactus;

import co.com.vialsmotors.email.application.command.EmailContactUsCommand;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailContactUsHandler {

    ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUsCommand emailContactUsCommand);
}
