package co.com.vialsmotors.email.application.handler.contactus;

import co.com.vialsmotors.email.application.command.EmailContactUsCommand;
import co.com.vialsmotors.email.application.factory.EmailContactUsFactory;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.domain.service.contactus.SendEmailContactUsService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendEmailContactUsImplHandler implements SendEmailContactUsHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailContactUsImplHandler.class);

    private final SendEmailContactUsService sendEmailContactUsService;
    private final EmailContactUsFactory emailContactUsFactory;

    @Override
    public ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUsCommand emailContactUsCommand) {
        LOGGER.info("INFO-MS-EMAIL: Send Email Contact Us in {}", SendEmailContactUsImplHandler.class.getSimpleName());
        return sendEmailContactUsService.sendEmailContactUs(emailContactUsFactory.execute(emailContactUsCommand));
    }
}
