package co.com.vialsmotors.email.application.handler.recoverpassword;

import co.com.vialsmotors.email.application.command.EmailRecoverPasswordCommand;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailRecoverPasswordHandler {

    ResponseEntity<GenericResponse> sendEmail(EmailRecoverPasswordCommand emailRecoverPasswordCommand);
}
