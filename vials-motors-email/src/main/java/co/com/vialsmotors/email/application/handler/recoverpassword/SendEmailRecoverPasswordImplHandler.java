package co.com.vialsmotors.email.application.handler.recoverpassword;

import co.com.vialsmotors.email.application.command.EmailRecoverPasswordCommand;
import co.com.vialsmotors.email.application.factory.EmailRecoverPasswordFactory;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.domain.service.recoverpassword.SendEmailRecoverPasswordService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendEmailRecoverPasswordImplHandler implements SendEmailRecoverPasswordHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailRecoverPasswordImplHandler.class);
    private final SendEmailRecoverPasswordService sendEmailRecoverPasswordService;
    private final EmailRecoverPasswordFactory emailRecoverPasswordFactory;

    @Override
    public ResponseEntity<GenericResponse> sendEmail(EmailRecoverPasswordCommand emailRecoverPasswordCommand) {
        LOGGER.info("INFO-MS-EMAIL: Send email for recover password in handler");
        return sendEmailRecoverPasswordService.sendEmailRecoverPassword(emailRecoverPasswordFactory.execute(emailRecoverPasswordCommand));
    }
}
