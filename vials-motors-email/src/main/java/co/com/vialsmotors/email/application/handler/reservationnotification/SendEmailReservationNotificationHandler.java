package co.com.vialsmotors.email.application.handler.reservationnotification;

import co.com.vialsmotors.email.application.command.EmailReservationNotificationCommand;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailReservationNotificationHandler {

    ResponseEntity<GenericResponse> sendEmailReservationNotification(EmailReservationNotificationCommand emailReservationNotificationCommand);
}
