package co.com.vialsmotors.email.application.handler.reservationnotification;

import co.com.vialsmotors.email.application.command.EmailReservationNotificationCommand;
import co.com.vialsmotors.email.application.factory.EmailReservationNotificationFactory;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.domain.service.reservationnotification.SendEmailReservationNotificationService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendEmailReservationNotificationImplHandler implements SendEmailReservationNotificationHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailReservationNotificationImplHandler.class);

    private final SendEmailReservationNotificationService sendEmailReservationNotificationService;
    private final EmailReservationNotificationFactory emailReservationNotificationFactory;

    @Override
    public ResponseEntity<GenericResponse> sendEmailReservationNotification(EmailReservationNotificationCommand emailReservationNotificationCommand) {
        LOGGER.info("INFO-MS-EMAIL: Send email reservation notification for SendEmailReservationNotificationImplHandler");
        return sendEmailReservationNotificationService.sendEmail(emailReservationNotificationFactory.execute(emailReservationNotificationCommand));
    }
}
