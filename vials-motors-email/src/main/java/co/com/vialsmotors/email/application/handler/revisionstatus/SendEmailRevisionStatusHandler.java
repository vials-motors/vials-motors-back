package co.com.vialsmotors.email.application.handler.revisionstatus;

import co.com.vialsmotors.email.application.command.EmailRevisionStatusCommand;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailRevisionStatusHandler {

    ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatusCommand emailRevisionStatusCommand);
}
