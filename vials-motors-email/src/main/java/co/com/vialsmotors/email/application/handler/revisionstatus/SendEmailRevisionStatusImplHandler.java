package co.com.vialsmotors.email.application.handler.revisionstatus;

import co.com.vialsmotors.email.application.command.EmailRevisionStatusCommand;
import co.com.vialsmotors.email.application.factory.EmailRevisionStatusFactory;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.domain.service.revisionstatus.SendEmailRevisionStatusService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendEmailRevisionStatusImplHandler implements SendEmailRevisionStatusHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailRevisionStatusImplHandler.class);

    private final SendEmailRevisionStatusService sendEmailRevisionStatusService;
    private final EmailRevisionStatusFactory emailRevisionStatusFactory;

    @Override
    public ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatusCommand emailRevisionStatusCommand) {
        LOGGER.info("INFO-MS-EMAIL: Send email revision status in {}", SendEmailRevisionStatusImplHandler.class.getSimpleName());
        return sendEmailRevisionStatusService.sendEmailRevisionStatus(emailRevisionStatusFactory.execute(emailRevisionStatusCommand));
    }
}
