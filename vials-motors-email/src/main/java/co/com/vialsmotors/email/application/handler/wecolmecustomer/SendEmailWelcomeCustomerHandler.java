package co.com.vialsmotors.email.application.handler.wecolmecustomer;

import co.com.vialsmotors.email.application.command.EmailWelcomeCustomerCommand;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailWelcomeCustomerHandler {

    ResponseEntity<GenericResponse> sendEmail(EmailWelcomeCustomerCommand emailWelcomeCustomerCommand);
}
