package co.com.vialsmotors.email.application.handler.wecolmecustomer;

import co.com.vialsmotors.email.application.command.EmailWelcomeCustomerCommand;
import co.com.vialsmotors.email.application.factory.EmailWelcomeCustomerFactory;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.domain.service.welcomecustomer.SendEmailWelcomeCustomerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendEmailWelcomeCustomerImplHandler implements SendEmailWelcomeCustomerHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailWelcomeCustomerImplHandler.class);

    private final SendEmailWelcomeCustomerService sendEmailWelcomeCustomerService;
    private final EmailWelcomeCustomerFactory emailWelcomeCustomerFactory;

    @Override
    public ResponseEntity<GenericResponse> sendEmail(EmailWelcomeCustomerCommand emailWelcomeCustomerCommand) {
        LOGGER.info("INFO-MS-EMAIL: Send Email in class: {}", SendEmailWelcomeCustomerImplHandler.class.getName());
        return sendEmailWelcomeCustomerService.sendEmailWelcomeCustomer(emailWelcomeCustomerFactory.execute(emailWelcomeCustomerCommand));
    }
}
