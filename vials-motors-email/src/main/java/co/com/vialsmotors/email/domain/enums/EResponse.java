package co.com.vialsmotors.email.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    MESSAGE_RESPONSE_SUCCESSFULLY("SUCC-00", "Correo enviado correctamente"),
    FIELD_EMAIL_NULL_EMPTY_MESSAGE("ERR-00", "El campo email es requerido"),
    FIELD_TEXT_NULL_EMPTY_MESSAGE("ERR-01", "El campo texto es requerido"),
    PATTERN_INVALID_MESSAGE("ERR-02", "El patron es requerido"),
    ERROR_SEND_EMAIL("ERR-03", "Error enviando correo"),
    FIELD_NAME_CLIENT_MESSAGE("ERR-04", "El campo nombre de cliente es requerido"),
    FIELD_MESSAGE_CONTACT("ERR-05", "El campo mensaje es requerido"),
    FIELD_STATUS_MESSAGE("ERR-06", "El campo estado es requerido"),
    INTERNAL_ERROR("ERR-500", "Error interno de servidor ");

    private final String code;
    private final String message;
}
