package co.com.vialsmotors.email.domain.exception;

import lombok.Getter;

@Getter
public class FieldException extends RuntimeException{

    private final String status;

    public FieldException(String message, String status) {
        super(message);
        this.status = status;
    }
}
