package co.com.vialsmotors.email.domain.exception;

import lombok.Getter;

@Getter
public class PatternEmailException extends RuntimeException{

    private final String status;

    public PatternEmailException(String message, String status) {
        super(message);
        this.status = status;
    }
}
