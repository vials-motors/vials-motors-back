package co.com.vialsmotors.email.domain.exception;

import lombok.Getter;

@Getter
public class SendEmailException extends RuntimeException{

    private final String status;

    public SendEmailException(String message, String status) {
        super(message);
        this.status = status;
    }
}
