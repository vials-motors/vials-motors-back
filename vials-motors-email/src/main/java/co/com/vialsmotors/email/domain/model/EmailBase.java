package co.com.vialsmotors.email.domain.model;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.validate.ValidateArgument;
import lombok.Getter;

@Getter
public class EmailBase {

    private final String email;

    public EmailBase(String email) {
        ValidateArgument.validatePatternEmail(email, EResponse.PATTERN_INVALID_MESSAGE.getMessage(), EResponse.PATTERN_INVALID_MESSAGE.getCode());
        ValidateArgument.validateStringField(email, EResponse.FIELD_EMAIL_NULL_EMPTY_MESSAGE.getMessage(), EResponse.FIELD_EMAIL_NULL_EMPTY_MESSAGE.getCode());
        this.email = email;
    }
}
