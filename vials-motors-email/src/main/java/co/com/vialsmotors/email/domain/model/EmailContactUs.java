package co.com.vialsmotors.email.domain.model;

import lombok.Getter;

@Getter
public class EmailContactUs extends EmailBase{

    private final String message;
    private final String phone;
    private final String name;

    public EmailContactUs(String email, String message, String phone, String name) {
        super(email);
        this.message = message;
        this.phone = phone;
        this.name = name;
    }
}
