package co.com.vialsmotors.email.domain.model;

import lombok.Getter;

@Getter
public class EmailRecoverPassword extends EmailBase{

    private final String otp;

    public EmailRecoverPassword(String email, String otp) {
        super(email);
        this.otp = otp;
    }
}
