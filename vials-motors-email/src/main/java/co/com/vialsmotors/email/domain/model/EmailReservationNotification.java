package co.com.vialsmotors.email.domain.model;

import lombok.Getter;

import java.time.ZonedDateTime;

@Getter
public class EmailReservationNotification extends EmailBase{

    private final ZonedDateTime date;

    public EmailReservationNotification(String email, ZonedDateTime date) {
        super(email);
        this.date = date;
    }
}
