package co.com.vialsmotors.email.domain.model;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.validate.ValidateArgument;
import lombok.Getter;

@Getter
public class EmailRevisionStatus extends EmailBase{

    private final String nameClient;
    private final String status;
    private final String novelty;
    private final String extraObservation;

    public EmailRevisionStatus(String email,String nameClient, String status, String novelty, String extraObservation) {
        super(email);
        ValidateArgument.validateStringField(nameClient, EResponse.FIELD_NAME_CLIENT_MESSAGE.getMessage(), EResponse.FIELD_NAME_CLIENT_MESSAGE.getCode());
        ValidateArgument.validateStringField(status, EResponse.FIELD_STATUS_MESSAGE.getMessage(), EResponse.FIELD_STATUS_MESSAGE.getCode());
        this.nameClient = nameClient;
        this.status = status;
        this.novelty = novelty;
        this.extraObservation = extraObservation;
    }
}
