package co.com.vialsmotors.email.domain.model;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.validate.ValidateArgument;
import lombok.Getter;

@Getter
public class EmailWelcomeCustomer extends EmailBase{

    private final String clientName;

    public EmailWelcomeCustomer(String email, String clientName) {
        super(email);
        ValidateArgument.validateStringField(clientName, EResponse.FIELD_NAME_CLIENT_MESSAGE.getMessage(), EResponse.FIELD_NAME_CLIENT_MESSAGE.getCode());
        this.clientName = clientName;
    }
}
