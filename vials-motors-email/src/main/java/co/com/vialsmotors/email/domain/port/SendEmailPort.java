package co.com.vialsmotors.email.domain.port;

import co.com.vialsmotors.email.domain.model.*;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailPort {

    ResponseEntity<GenericResponse> sendEmailReservationNotification(EmailReservationNotification emailReservationNotification);
    ResponseEntity<GenericResponse> sendEmailCustomer(EmailWelcomeCustomer emailWelcomeCustomer);
    ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUs emailContactUs);
    ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatus emailRevisionStatus);
    ResponseEntity<GenericResponse> sendEmailRecoverPassword(EmailRecoverPassword emailRecoverPassword);
}
