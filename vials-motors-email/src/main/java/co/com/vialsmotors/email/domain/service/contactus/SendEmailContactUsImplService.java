package co.com.vialsmotors.email.domain.service.contactus;

import co.com.vialsmotors.email.domain.model.EmailContactUs;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class SendEmailContactUsImplService implements SendEmailContactUsService{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailContactUsImplService.class);

    private final SendEmailPort sendEmailPort;

    public SendEmailContactUsImplService(SendEmailPort sendEmailPort) {
        this.sendEmailPort = sendEmailPort;
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUs emailContactUs) {
        LOGGER.info("INFO-MS-EMAIL: Send email Contact Us in Send email Contact Us Impl Service");
        return sendEmailPort.sendEmailContactUs(emailContactUs);
    }
}
