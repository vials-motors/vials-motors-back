package co.com.vialsmotors.email.domain.service.contactus;

import co.com.vialsmotors.email.domain.model.EmailContactUs;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailContactUsService {

    ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUs emailContactUs);
}
