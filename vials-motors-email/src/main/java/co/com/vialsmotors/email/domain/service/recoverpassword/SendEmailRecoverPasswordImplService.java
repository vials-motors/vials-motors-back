package co.com.vialsmotors.email.domain.service.recoverpassword;

import co.com.vialsmotors.email.domain.model.EmailRecoverPassword;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class SendEmailRecoverPasswordImplService implements SendEmailRecoverPasswordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailRecoverPasswordImplService.class);
    private final SendEmailPort sendEmailPort;

    public SendEmailRecoverPasswordImplService(SendEmailPort sendEmailPort) {
        this.sendEmailPort = sendEmailPort;
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailRecoverPassword(EmailRecoverPassword emailRecoverPassword) {
        LOGGER.info("INFO-MS-EMAIL: Send email for recover password");
        return this.sendEmailPort.sendEmailRecoverPassword(emailRecoverPassword);
    }
}
