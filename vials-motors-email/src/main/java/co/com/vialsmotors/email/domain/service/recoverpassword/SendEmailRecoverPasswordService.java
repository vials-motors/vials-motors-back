package co.com.vialsmotors.email.domain.service.recoverpassword;

import co.com.vialsmotors.email.domain.model.EmailRecoverPassword;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailRecoverPasswordService {

    ResponseEntity<GenericResponse> sendEmailRecoverPassword(EmailRecoverPassword emailRecoverPassword);
}
