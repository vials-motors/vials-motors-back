package co.com.vialsmotors.email.domain.service.reservationnotification;

import co.com.vialsmotors.email.domain.model.EmailReservationNotification;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class SendEmailReservationNotificationImplService implements SendEmailReservationNotificationService{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailReservationNotificationImplService.class);

    private final SendEmailPort sendEmailPort;

    public SendEmailReservationNotificationImplService(SendEmailPort sendEmailPort) {
        this.sendEmailPort = sendEmailPort;
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmail(EmailReservationNotification emailReservationNotification) {
        LOGGER.info("INFO-MS-EMAIL: Send Email of reservation Notification");
        return sendEmailPort.sendEmailReservationNotification(emailReservationNotification);
    }
}
