package co.com.vialsmotors.email.domain.service.reservationnotification;

import co.com.vialsmotors.email.domain.model.EmailReservationNotification;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailReservationNotificationService {

    ResponseEntity<GenericResponse> sendEmail(EmailReservationNotification emailReservationNotification);
}
