package co.com.vialsmotors.email.domain.service.revisionstatus;

import co.com.vialsmotors.email.domain.model.EmailRevisionStatus;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class SendEmailRevisionStatusImplService implements SendEmailRevisionStatusService{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailRevisionStatusImplService.class);

    private final SendEmailPort sendEmailPort;

    public SendEmailRevisionStatusImplService(SendEmailPort sendEmailPort) {
        this.sendEmailPort = sendEmailPort;
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatus emailRevisionStatus) {
        LOGGER.info("INFO-MS-EMAIL: Send email in {}", SendEmailRevisionStatusImplService.class.getSimpleName());
        return sendEmailPort.sendEmailRevisionStatus(emailRevisionStatus);
    }
}
