package co.com.vialsmotors.email.domain.service.revisionstatus;

import co.com.vialsmotors.email.domain.model.EmailRevisionStatus;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailRevisionStatusService {

    ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatus emailRevisionStatus);
}
