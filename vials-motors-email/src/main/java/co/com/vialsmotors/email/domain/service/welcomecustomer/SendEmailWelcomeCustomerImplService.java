package co.com.vialsmotors.email.domain.service.welcomecustomer;

import co.com.vialsmotors.email.domain.model.EmailWelcomeCustomer;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class SendEmailWelcomeCustomerImplService implements SendEmailWelcomeCustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailWelcomeCustomerImplService.class);

    private final SendEmailPort sendEmailPort;

    public SendEmailWelcomeCustomerImplService(SendEmailPort sendEmailPort) {
        this.sendEmailPort = sendEmailPort;
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailWelcomeCustomer(EmailWelcomeCustomer emailWelcomeCustomer) {
        LOGGER.info("INFO-MS-EMAIL: Send email in class: {}", SendEmailWelcomeCustomerImplService.class.getName());
        return sendEmailPort.sendEmailCustomer(emailWelcomeCustomer);
    }
}
