package co.com.vialsmotors.email.domain.service.welcomecustomer;

import co.com.vialsmotors.email.domain.model.EmailWelcomeCustomer;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SendEmailWelcomeCustomerService {

    ResponseEntity<GenericResponse> sendEmailWelcomeCustomer(EmailWelcomeCustomer emailWelcomeCustomer);
}
