package co.com.vialsmotors.email.domain.validate;

import co.com.vialsmotors.email.domain.exception.FieldException;
import co.com.vialsmotors.email.domain.exception.PatternEmailException;

import java.util.regex.Pattern;

public class ValidateArgument {

    private static final String NOT_INSTANTIABLE = "The class ValidateArgument is not instantiable";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-])+(@[A-Za-z0-9-])+(.[A-Za-z0-9])+(.[A-Za-z]{2,})$";
    private static final Pattern PATTERN = Pattern.compile(EMAIL_PATTERN);

    private ValidateArgument() {
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static void validateStringField(String field, String message, String status){
        if (field == null || field.isBlank())
            throw new FieldException(message, status);
    }

    public static void validatePatternEmail(String email, String message, String status){
        if (Boolean.FALSE.equals(validatePattern(email))){
            throw new PatternEmailException(message, status);
        }
    }

    private static Boolean validatePattern(String pattern){
        return PATTERN.matcher(pattern).matches();
    }
}
