package co.com.vialsmotors.email.infrastructure.adapter;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.exception.SendEmailException;
import co.com.vialsmotors.email.domain.model.*;
import co.com.vialsmotors.email.domain.port.SendEmailPort;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import co.com.vialsmotors.email.infrastructure.utils.BuildMimeMessageUtils;
import co.com.vialsmotors.email.infrastructure.utils.BuildVariablesUtils;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.TimeZone;

@Component
@RequiredArgsConstructor
public class SendEmailImplAdapter implements SendEmailPort{

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailImplAdapter.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    private final BuildMimeMessageUtils buildMimeMessageUtils;

    @Value("${spring.mail.username}")
    protected String emailFrom;

    @Override
    public ResponseEntity<GenericResponse> sendEmailReservationNotification(EmailReservationNotification emailReservationNotification) {
        LOGGER.info("INFO-MS-EMAIL: Send Email");
        try {
            this.buildMimeMessageUtils.buildBodyMessage("ReservationNotification.html",
                    BuildVariablesUtils.variablesForReservationNotification(emailReservationNotification),
                    emailFrom, emailReservationNotification.getEmail());

            return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getCode(),
                    EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getMessage(),
                    TIMESTAMP));
        }catch (Exception exception) {
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailCustomer(EmailWelcomeCustomer emailWelcomeCustomer) {
        try {
            this.buildMimeMessageUtils.buildBodyMessage("WelcomeCustomer.html",
                    BuildVariablesUtils.variablesForWelcomeCustomer(emailWelcomeCustomer), emailFrom,
                    emailWelcomeCustomer.getEmail());

            return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getCode(),
                    EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getMessage(),
                    TIMESTAMP));
        }catch (Exception exception){
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailContactUs(EmailContactUs emailContactUs) {
        try {
            this.buildMimeMessageUtils.buildBodyMessage("ContactUs.html",
                    BuildVariablesUtils.variablesForContactUs(emailContactUs),
                    emailFrom, "sagudelo312@gmail.com");

            return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getCode(),
                    EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getMessage(),
                    TIMESTAMP));
        }catch (Exception exception){
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailRevisionStatus(EmailRevisionStatus emailRevisionStatus) {
        try {
            this.buildMimeMessageUtils.buildBodyMessage("RevisionStatus.html",
                    BuildVariablesUtils.variablesForRevisionStatus(emailRevisionStatus),
                    emailFrom, emailRevisionStatus.getEmail());

            return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getCode(),
                    EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getMessage(),
                    TIMESTAMP));
        }catch (Exception exception){
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }

    @Override
    public ResponseEntity<GenericResponse> sendEmailRecoverPassword(EmailRecoverPassword emailRecoverPassword) {
        try {
            this.buildMimeMessageUtils.buildBodyMessage("RecoverPassword.html",
                    BuildVariablesUtils.variablesForRecoverPassword(emailRecoverPassword),
                    emailFrom, emailRecoverPassword.getEmail());
            return ResponseEntity.ok(new GenericResponse(EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getCode(),
                    EResponse.MESSAGE_RESPONSE_SUCCESSFULLY.getMessage(), TIMESTAMP));
        }catch (Exception exception){
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }
}
