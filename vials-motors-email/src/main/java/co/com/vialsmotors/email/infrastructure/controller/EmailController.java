package co.com.vialsmotors.email.infrastructure.controller;

import co.com.vialsmotors.email.application.command.*;
import co.com.vialsmotors.email.application.handler.contactus.SendEmailContactUsHandler;
import co.com.vialsmotors.email.application.handler.recoverpassword.SendEmailRecoverPasswordHandler;
import co.com.vialsmotors.email.application.handler.reservationnotification.SendEmailReservationNotificationHandler;
import co.com.vialsmotors.email.application.handler.revisionstatus.SendEmailRevisionStatusHandler;
import co.com.vialsmotors.email.application.handler.wecolmecustomer.SendEmailWelcomeCustomerHandler;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/send-email")
@CrossOrigin(origins = "http://localhost:4200")
public class EmailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

    private final SendEmailWelcomeCustomerHandler sendEmailWelcomeCustomerHandler;
    private final SendEmailReservationNotificationHandler sendEmailReservationNotificationHandler;
    private final SendEmailContactUsHandler sendEmailContactUsHandler;
    private final SendEmailRevisionStatusHandler sendEmailRevisionStatusHandler;
    private final SendEmailRecoverPasswordHandler sendEmailRecoverPasswordHandler;
    private final ObjectMapper objectMapper;

    @PostMapping(path = "/welcome-customer")
    public ResponseEntity<GenericResponse> sendEmail(@RequestBody EmailWelcomeCustomerCommand emailWelcomeCustomerCommand){
        LOGGER.info("INFO-MS-EMAIL: Request receiver in EmailController for sendEmail with body request");
        return sendEmailWelcomeCustomerHandler.sendEmail(emailWelcomeCustomerCommand);
    }

    @PostMapping(path = "/reservation-notification")
    public ResponseEntity<GenericResponse> sendEmailReservationNotification(@RequestBody EmailReservationNotificationCommand emailReservationNotificationCommand){
        LOGGER.info("INFO-MS-EMAIL: Request receiver in email controller for sendEmailReservationNotification with body request");
        return sendEmailReservationNotificationHandler.sendEmailReservationNotification(emailReservationNotificationCommand);
    }

    @PostMapping(path = "/contact-us")
    public ResponseEntity<GenericResponse> sendEmailContactUs(@RequestBody EmailContactUsCommand emailContactUsCommand){
        LOGGER.info("INFO-MS-EMAIL: Request receiver in email Controller for sendEmailContactUs with body request ");
        return sendEmailContactUsHandler.sendEmailContactUs(emailContactUsCommand);
    }

    @PostMapping(path = "/revision-status")
    public ResponseEntity<GenericResponse> sendEmailRevisionStatus(@RequestBody EmailRevisionStatusCommand emailRevisionStatusCommand){
        LOGGER.info("INFO-MS-EMAIL: Request receiver in email Controller for sendEmailRevisionStatus with body request ");
        return sendEmailRevisionStatusHandler.sendEmailRevisionStatus(emailRevisionStatusCommand);
    }

    @PostMapping(path = "/recover-password")
    public ResponseEntity<GenericResponse> sendEmailRecoverPassword(@RequestBody EmailRecoverPasswordCommand emailRecoverPasswordCommand){
        LOGGER.info("INFO-MS-EMAIL: Request receiver in email controller for sendEmailRecoverPassword with body request");
        return sendEmailRecoverPasswordHandler.sendEmail(emailRecoverPasswordCommand);
    }
}
