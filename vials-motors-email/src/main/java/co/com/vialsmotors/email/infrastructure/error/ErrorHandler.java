package co.com.vialsmotors.email.infrastructure.error;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.exception.FieldException;
import co.com.vialsmotors.email.domain.exception.PatternEmailException;
import co.com.vialsmotors.email.domain.exception.SendEmailException;
import co.com.vialsmotors.email.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: {}";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler() {
        CODE_STATE.put(FieldException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(PatternEmailException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(SendEmailException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleException(Exception exception){
        String nameException = exception.getClass().getSimpleName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof FieldException fieldException){
            LOGGER.info(MESSAGE_ERROR_GENERIC, fieldException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(fieldException.getStatus(), fieldException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof PatternEmailException patternEmailException){
            LOGGER.info(MESSAGE_ERROR_GENERIC, patternEmailException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(patternEmailException.getStatus(), patternEmailException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof SendEmailException sendEmailException){
            LOGGER.info(MESSAGE_ERROR_GENERIC, sendEmailException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sendEmailException.getStatus(), sendEmailException.getMessage(), TIMESTAMP_RESPONSE));
        }else {
            LOGGER.info(MESSAGE_ERROR_GENERIC, exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_ERROR.getCode(),
                            EResponse.INTERNAL_ERROR.getMessage() + exception.getMessage(), TIMESTAMP_RESPONSE));
        }

    }
}
