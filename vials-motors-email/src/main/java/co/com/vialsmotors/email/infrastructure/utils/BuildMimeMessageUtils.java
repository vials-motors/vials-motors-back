package co.com.vialsmotors.email.infrastructure.utils;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.exception.SendEmailException;
import co.com.vialsmotors.email.infrastructure.thymeleaf.ThymeleafService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class BuildMimeMessageUtils {

    private static final String SUBJECT = "INFO Vials Motors";

    private final JavaMailSender javaMailSender;
    private final ThymeleafService thymeleafService;


    public void buildBodyMessage(String template, Map<String, Object> variables, String emailFrom, String emailTo){
        try {
            MimeMessage message =  javaMailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

            helper.setFrom(emailFrom);
            helper.setText(thymeleafService.createContent(template, variables), true);
            helper.setTo(emailTo);
            helper.setSubject(SUBJECT);

            javaMailSender.send(message);

        }catch (Exception exception){
            throw new SendEmailException(EResponse.ERROR_SEND_EMAIL.getMessage() + exception.getMessage(), EResponse.ERROR_SEND_EMAIL.getCode());
        }
    }
}
