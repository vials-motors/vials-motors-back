package co.com.vialsmotors.email.infrastructure.utils;

import co.com.vialsmotors.email.domain.model.*;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class BuildVariablesUtils {

    private static final String NOT_INSTANTIABLE = "The class BuildVariableUtils is not instantiable";
    private static final String VARIABLE_CLIENT_NAME = "nameClient";

    private BuildVariablesUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static Map<String, Object> variablesForWelcomeCustomer(EmailWelcomeCustomer emailWelcomeCustomer){
        Map<String, Object> variables = new HashMap<>();
        variables.put(VARIABLE_CLIENT_NAME, emailWelcomeCustomer.getClientName());
        return variables;
    }

    public static Map<String, Object> variablesForReservationNotification(EmailReservationNotification emailReservationNotification){
        Map<String, Object> variables = new HashMap<>();
        variables.put("date", DateFormatterUtils.dateFormatter(emailReservationNotification.getDate()));
        return variables;
    }

    public static Map<String, Object> variablesForContactUs(EmailContactUs emailContactUs){
        Map<String, Object> variables = new HashMap<>();
        variables.put("emailContact", emailContactUs.getEmail());
        variables.put(VARIABLE_CLIENT_NAME, emailContactUs.getName());
        variables.put("phone", emailContactUs.getPhone());
        variables.put("messageContact", emailContactUs.getMessage());
        return variables;
    }

    public static Map<String, Object> variablesForRevisionStatus(EmailRevisionStatus emailRevisionStatus){
        Map<String, Object> variables = new HashMap<>();
        variables.put(VARIABLE_CLIENT_NAME, emailRevisionStatus.getNameClient());
        variables.put("status", SetStatusUtils.setStatus(emailRevisionStatus.getStatus()));
        variables.put("extra", emailRevisionStatus.getExtraObservation());
        variables.put("novelty", emailRevisionStatus.getNovelty());
        return variables;
    }

    public static Map<String, Object> variablesForRecoverPassword(EmailRecoverPassword emailRecoverPassword){
        Map<String, Object> variables = new HashMap<>();
        variables.put("otp", URLEncoder.encode(emailRecoverPassword.getOtp(), StandardCharsets.UTF_8));
        return variables;
    }
}
