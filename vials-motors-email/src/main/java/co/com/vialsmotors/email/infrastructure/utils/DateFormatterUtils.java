package co.com.vialsmotors.email.infrastructure.utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatterUtils {

    private static final String NOT_INSTANTIABLE = "The class DateFormatterUtils is not instantiable";

    private DateFormatterUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String dateFormatter(ZonedDateTime dateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return dateTime.format(formatter);
    }
}
