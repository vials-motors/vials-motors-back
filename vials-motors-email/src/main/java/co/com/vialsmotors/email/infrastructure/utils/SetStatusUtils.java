package co.com.vialsmotors.email.infrastructure.utils;

import co.com.vialsmotors.email.domain.enums.EResponse;
import co.com.vialsmotors.email.domain.exception.FieldException;

public class SetStatusUtils {

    private static final String NOT_INSTANTIABLE = "The class set status utils is not instantiable";

    private SetStatusUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String setStatus(String status){
        return switch (status){
            case "1" -> "Cita registrada";
            case "2" -> "Cita cancelada";
            case "3" -> "Vehiculo ingresado";
            case "4" -> "Vehiculo en proceso";
            case "5" -> "Cita finalizada";
            default -> throw new FieldException(EResponse.FIELD_STATUS_MESSAGE.getMessage(), EResponse.FIELD_STATUS_MESSAGE.getCode());
        };
    }
}
