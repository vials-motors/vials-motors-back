package co.com.vialsmotors.login.application.configuration;

import co.com.vialsmotors.login.domain.port.*;
import co.com.vialsmotors.login.domain.service.credential.CredentialImplService;
import co.com.vialsmotors.login.domain.service.delete.DeleteCredentialImplService;
import co.com.vialsmotors.login.domain.service.getcredential.GetCredentialImplService;
import co.com.vialsmotors.login.domain.service.login.LoginImplService;
import co.com.vialsmotors.login.domain.service.otp.save.SaveOtpImplService;
import co.com.vialsmotors.login.domain.service.otp.validate.ValidateOtpImplService;
import co.com.vialsmotors.login.domain.service.update.email.UpdateEmailImplService;
import co.com.vialsmotors.login.domain.service.update.password.UpdatePasswordImplService;
import co.com.vialsmotors.login.infrastructure.mapper.GenericMapper;
import co.com.vialsmotors.login.infrastructure.security.bean.PasswordEncodeBean;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public CredentialImplService credentialImplService(CredentialRepository credentialRepository, GenericMapper genericMapper){
        return new CredentialImplService(credentialRepository, genericMapper);
    }

    @Bean
    public LoginImplService loginImplService(LoginRepository loginRepository){
        return new LoginImplService(loginRepository);
    }

    @Bean
    public GetCredentialImplService getCredentialImplService(GetCredentialRepository getCredentialRepository){
        return new GetCredentialImplService(getCredentialRepository);
    }

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    @Bean
    public UpdateEmailImplService updateUsernameImplService(UpdateCredentialRepository updateCredentialRepository){
        return new UpdateEmailImplService(updateCredentialRepository);
    }

    @Bean
    public DeleteCredentialImplService deleteCredentialImplService(DeleteCredentialRepository deleteCredentialRepository, UpdateCredentialRepository updateCredentialRepository){
        return new DeleteCredentialImplService(deleteCredentialRepository, updateCredentialRepository);
    }

    @Bean
    public SaveOtpImplService saveOtpImplService(FindEmailRepository findEmailRepository, SaveOtpRepository saveOtpRepository, SendEmailRecoverPasswordPort sendEmailRecoverPasswordPort){
        return new SaveOtpImplService(findEmailRepository, saveOtpRepository, sendEmailRecoverPasswordPort);
    }

    @Bean
    public ValidateOtpImplService validateOtpImplService(GetOtpRepository getOtpRepository, DeleteOtpRepository deleteOtpRepository){
        return new ValidateOtpImplService(getOtpRepository, deleteOtpRepository);
    }

    @Bean
    public UpdatePasswordImplService updatePasswordImplService(FindEmailRepository findEmailRepository, UpdateCredentialRepository updateCredentialRepository, PasswordEncodeBean passwordEncodeBean){
        return new UpdatePasswordImplService(findEmailRepository, updateCredentialRepository, passwordEncodeBean);
    }
}
