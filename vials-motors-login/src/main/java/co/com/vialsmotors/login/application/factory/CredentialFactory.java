package co.com.vialsmotors.login.application.factory;

import co.com.vialsmotors.login.application.command.CredentialCommand;
import co.com.vialsmotors.login.domain.model.Credential;
import org.springframework.stereotype.Component;

@Component
public class CredentialFactory {

    public Credential executeFactory(CredentialCommand credentialCommand){
        return new Credential(credentialCommand.getEmail(),
                 credentialCommand.getPassword(),
                 credentialCommand.getCustomerID());
    }
}
