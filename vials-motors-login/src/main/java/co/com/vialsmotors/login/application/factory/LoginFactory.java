package co.com.vialsmotors.login.application.factory;

import co.com.vialsmotors.login.application.command.LoginCommand;
import co.com.vialsmotors.login.domain.model.Login;
import org.springframework.stereotype.Component;

@Component
public class LoginFactory {

    public Login execute(LoginCommand loginCommand){
        return new Login(loginCommand.getEmail(), loginCommand.getPassword());
    }
}
