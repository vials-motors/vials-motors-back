package co.com.vialsmotors.login.application.handler.credential;

import co.com.vialsmotors.login.application.command.CredentialCommand;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface CredentialHandler {

    ResponseEntity<GenericResponse> executeRegisterCredential(CredentialCommand credentialCommand, Integer userType);
}
