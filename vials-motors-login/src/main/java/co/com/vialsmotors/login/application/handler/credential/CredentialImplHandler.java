package co.com.vialsmotors.login.application.handler.credential;

import co.com.vialsmotors.login.application.command.CredentialCommand;
import co.com.vialsmotors.login.application.factory.CredentialFactory;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.service.credential.CredentialService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component@RequiredArgsConstructor
public class CredentialImplHandler implements CredentialHandler{

    private static final Logger LOGGER = Logger.getLogger(CredentialImplHandler.class);
    private final CredentialService credentialService;
    private final CredentialFactory credentialFactory;

    @Override
    public ResponseEntity<GenericResponse> executeRegisterCredential(CredentialCommand credentialCommand, Integer userType) {
        LOGGER.info("MS-LOGIN: Executing credential Handler with username: " + credentialCommand.getEmail());
        return this.credentialService.executeRegisterCredential(credentialFactory.executeFactory(credentialCommand), userType);
    }
}
