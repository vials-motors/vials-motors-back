package co.com.vialsmotors.login.application.handler.delete;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteCredentialHandler {

    ResponseEntity<GenericResponse> deleteCredential(String customerId);
}
