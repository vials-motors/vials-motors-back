package co.com.vialsmotors.login.application.handler.delete;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.service.delete.DeleteCredentialService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteCredentialImplHandler implements DeleteCredentialHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteCredentialImplHandler.class);
    private final DeleteCredentialService deleteCredentialService;

    @Override
    public ResponseEntity<GenericResponse> deleteCredential(String customerId) {
        LOGGER.info("INFO-MS-LOGIN: Delete Credential in handler with customer id: " + customerId);
        return deleteCredentialService.deleteCredential(customerId);
    }
}
