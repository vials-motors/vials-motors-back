package co.com.vialsmotors.login.application.handler.getcredential;

import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import org.springframework.http.ResponseEntity;

public interface GetCredentialHandler {

    ResponseEntity<CredentialDTO> getCredential(String username);
}
