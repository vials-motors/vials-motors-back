package co.com.vialsmotors.login.application.handler.getcredential;

import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.service.getcredential.GetCredentialService;
import co.com.vialsmotors.login.domain.validate.ValidateArgument;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetCredentialImplHandler implements GetCredentialHandler{

    private static final Logger LOGGER = Logger.getLogger(GetCredentialImplHandler.class);

    private final GetCredentialService getCredentialService;

    @Override
    public ResponseEntity<CredentialDTO> getCredential(String username) {
        LOGGER.info("MS-LOGIN: Get Credential in " + GetCredentialImplHandler.class.getName());
        ValidateArgument.validateStringObject(username, EFieldException.USERNAME_EXCEPTION.getCode(), EFieldException.USERNAME_EXCEPTION.getMessage());
        return getCredentialService.getCredential(username);
    }
}
