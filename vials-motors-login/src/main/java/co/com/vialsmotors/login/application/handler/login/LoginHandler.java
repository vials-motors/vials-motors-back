package co.com.vialsmotors.login.application.handler.login;

import co.com.vialsmotors.login.application.command.LoginCommand;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface LoginHandler {

    ResponseEntity<JwtResponse> executeLogin(LoginCommand loginCommand);
}
