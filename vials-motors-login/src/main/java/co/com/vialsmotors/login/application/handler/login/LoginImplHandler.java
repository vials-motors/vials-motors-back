package co.com.vialsmotors.login.application.handler.login;

import co.com.vialsmotors.login.application.command.LoginCommand;
import co.com.vialsmotors.login.application.factory.LoginFactory;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import co.com.vialsmotors.login.domain.service.login.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoginImplHandler implements LoginHandler{

    private final LoginFactory loginFactory;
    private final LoginService loginService;

    @Override
    public ResponseEntity<JwtResponse> executeLogin(LoginCommand loginCommand) {
        return this.loginService.executeLogin(loginFactory.execute(loginCommand));
    }
}
