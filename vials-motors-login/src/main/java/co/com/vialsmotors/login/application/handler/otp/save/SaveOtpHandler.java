package co.com.vialsmotors.login.application.handler.otp.save;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveOtpHandler {

    ResponseEntity<GenericResponse> saveOtp(String email);
}
