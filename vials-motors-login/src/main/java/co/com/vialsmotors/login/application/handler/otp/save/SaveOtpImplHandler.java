package co.com.vialsmotors.login.application.handler.otp.save;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.service.otp.save.SaveOtpService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SaveOtpImplHandler implements SaveOtpHandler{

    private static final Logger LOGGER = Logger.getLogger(SaveOtpImplHandler.class);
    private final SaveOtpService saveOtpService;

    @Override
    public ResponseEntity<GenericResponse> saveOtp(String email) {
        LOGGER.info("INFO-MS-LOGIN: Save Otp in handler with email: " + email);
        return saveOtpService.saveOtp(email);
    }
}
