package co.com.vialsmotors.login.application.handler.otp.validate;

import co.com.vialsmotors.login.domain.response.ValidateResponse;
import org.springframework.http.ResponseEntity;

public interface ValidateOtpHandler {

    ResponseEntity<ValidateResponse> validateOtp(String otp);
}
