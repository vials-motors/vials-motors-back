package co.com.vialsmotors.login.application.handler.otp.validate;

import co.com.vialsmotors.login.domain.response.ValidateResponse;
import co.com.vialsmotors.login.domain.service.otp.validate.ValidateOtpService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ValidateOtpImplHandler implements ValidateOtpHandler {

    private static final Logger LOGGER = Logger.getLogger(ValidateOtpImplHandler.class);
    private final ValidateOtpService validateOtpService;

    @Override
    public ResponseEntity<ValidateResponse> validateOtp(String otp) {
        LOGGER.info("INFO-MS-LOGIN: Validate otp in handler with otp: " + otp);
        return validateOtpService.validateOtp(otp);
    }
}
