package co.com.vialsmotors.login.application.handler.update.email;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateUsernameHandler {

    ResponseEntity<GenericResponse> updateUsername(String username, String customerID);
}
