package co.com.vialsmotors.login.application.handler.update.email;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.service.update.email.UpdateEmailService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateUsernameImplHandler implements UpdateUsernameHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateUsernameImplHandler.class);
    private final UpdateEmailService updateEmailService;

    @Override
    public ResponseEntity<GenericResponse> updateUsername(String username, String customerID) {
        LOGGER.info("INFO-MS-LOGIN: Update username in Update username impl handler with customer id: " + customerID);
        return updateEmailService.updateEmail(username, customerID);
    }
}
