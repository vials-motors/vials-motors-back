package co.com.vialsmotors.login.application.handler.update.password;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdatePasswordHandler {

    ResponseEntity<GenericResponse> updatePassword(String email, String newPassword, String authorization);
}
