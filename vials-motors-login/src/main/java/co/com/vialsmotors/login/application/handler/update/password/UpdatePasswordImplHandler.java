package co.com.vialsmotors.login.application.handler.update.password;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.service.update.password.UpdatePasswordService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdatePasswordImplHandler implements UpdatePasswordHandler {

    private static final Logger LOGGER = Logger.getLogger(UpdatePasswordImplHandler.class);
    private final UpdatePasswordService updatePasswordService;

    @Override
    public ResponseEntity<GenericResponse> updatePassword(String email, String newPassword, String authorization) {
        LOGGER.info("INFO-MS-LOGIN: Update password in handler for email: " + email);
        return this.updatePasswordService.updatePassword(email, newPassword, authorization);
    }
}
