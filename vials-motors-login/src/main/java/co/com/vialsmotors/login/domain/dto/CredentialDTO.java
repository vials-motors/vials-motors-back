package co.com.vialsmotors.login.domain.dto;

import co.com.vialsmotors.login.infrastructure.entity.RoleEntity;

import java.util.Set;

public record CredentialDTO (String email, Set<RoleEntity> roles ){
}
