package co.com.vialsmotors.login.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EErrorDataBase {

    ERROR_USERNAME("DB-ERR-00", "El nombre de usuario ya existe"),
    ERROR_DATABASE("DB-ERR-01", "Error en la comunicacion con la base de datos");

    private final String code;
    private final String message;
}
