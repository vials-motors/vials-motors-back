package co.com.vialsmotors.login.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EFieldException {

    USERNAME_EXCEPTION("FIELD-ERR-00", "El campo nombre de usuario es nulo o vacio"),
    EMAIL_EXCEPTION("FIELD-ERR-02", "El campo email es requerido"),
    PASSWORD_EXCEPTION("FIELD-ERR-01", "El campo password es requerido"),
    UNIQUE_ID_EXCEPTION("FIELD-ERR-03", "El campo unique id es requerido"),
    CUSTOMER_ID_EXCEPTION("FIELD-ERR-04", "El campo customer id es requerido"),
    CODE_OTP_ERROR("FIELD-ERR-05", "El codigo OTP es requerido");

    private final String code;
    private final String message;
}
