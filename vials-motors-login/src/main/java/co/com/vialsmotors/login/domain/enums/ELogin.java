package co.com.vialsmotors.login.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ELogin {

    SUCCESS_LOGIN("LOGIN-SUCCESS-00","Login correcto"),
    LOGIN_BEARER("0", "Bearer "),
    ERROR_LOGIN("LOGIN-ERR-00", "Error creando el token ");

    private final String code;
    private final String message;
}
