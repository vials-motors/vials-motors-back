package co.com.vialsmotors.login.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    SUCCESSFULLY_REGISTER_CREDENTIAL("SUCC-00", "Las crendenciales son registradas correctamente"),
    SUCCESSFULLY_UPDATE_CREDENTIAL("SUCC-01", "La crendencial es actualizada"),
    SUCCESSFULLY_DELETE_CREDENTIAL("SUCC-02", "La credencial es eliminada"),
    SUCCESSFULLY_SAVE_OTP("SUCC-03", "Correo enviado para recuperar contraseña"),
    SUCCESSFULLY_VALIDATE_OTP("SUCC-04", "El otp es correcto"),
    UPDATE_PASSWORD_SUCCESS("SUCC-05", "Actualizacion de contraseña correcta"),
    CREDENTIAL_EXCEPTION_MESSAGE("ERR-00", "Las credenciales para el usuario no existe"),
    OTP_NOT_EXIST("ERR-01", "El codigo otp no existe"),
    OTP_EXPIRATION_ERROR("ERR-02", "Otp expirado"),
    UNAUTHORIZED_ERROR("ERR-401", "No esta autorizado"),
    SEND_EMAIL_ERROR("ERR-503", "El llamado para envio de correo fallo"),
    INTERNAL_ERROR("ERR-500", "Error interno de servidor");

    private final String code;
    private final String message;
}
