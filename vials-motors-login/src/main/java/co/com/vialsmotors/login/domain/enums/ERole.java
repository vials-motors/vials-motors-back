package co.com.vialsmotors.login.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ERole {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_CLIENT("ROLE_CLIENT"),
    ROLE_TECHNICAL("ROLE_TECHNICAL");

    private final String role;
}
