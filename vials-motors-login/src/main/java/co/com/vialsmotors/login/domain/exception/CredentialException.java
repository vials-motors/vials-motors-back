package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class CredentialException extends RuntimeException{

    private final String status;

    public CredentialException(String message, String status) {
        super(message);
        this.status = status;
    }
}
