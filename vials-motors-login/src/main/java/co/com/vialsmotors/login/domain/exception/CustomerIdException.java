package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class CustomerIdException extends RuntimeException{

    private final String status;

    public CustomerIdException(String message, String status) {
        super(message);
        this.status = status;
    }
}
