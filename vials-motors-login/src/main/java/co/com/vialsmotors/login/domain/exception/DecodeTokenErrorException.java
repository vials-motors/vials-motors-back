package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class DecodeTokenErrorException extends RuntimeException {

    private final String status;

    public DecodeTokenErrorException(String message, String status) {
        super(message);
        this.status = status;
    }
}
