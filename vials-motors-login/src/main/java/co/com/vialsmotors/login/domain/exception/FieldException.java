package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class FieldException extends RuntimeException {

    private final String codeException;

    public FieldException(String codeException, String message) {
        super(message);
        this.codeException = codeException;
    }
}
