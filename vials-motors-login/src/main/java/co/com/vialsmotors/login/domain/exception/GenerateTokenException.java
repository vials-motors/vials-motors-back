package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class GenerateTokenException extends RuntimeException{

    private final String status;

    public GenerateTokenException(String message, String status) {
        super(message);
        this.status = status;
    }
}
