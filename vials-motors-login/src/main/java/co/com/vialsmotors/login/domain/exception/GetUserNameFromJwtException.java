package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class GetUserNameFromJwtException extends RuntimeException {

    private final String status;

    public GetUserNameFromJwtException(String message, String status) {
        super(message);
        this.status = status;
    }
}
