package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class LoginException extends RuntimeException{

    private final String status;

    public LoginException(String message, String status) {
        super(message);
        this.status = status;
    }
}
