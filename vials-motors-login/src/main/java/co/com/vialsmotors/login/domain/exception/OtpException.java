package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class OtpException extends RuntimeException{

    private final String code;

    public OtpException(String message, String code) {
        super(message);
        this.code = code;
    }
}
