package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class SendEmailException extends RuntimeException{

    private final String code;

    public SendEmailException(String message, String code) {
        super(message);
        this.code = code;
    }
}
