package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class SqlServerDatabaseException extends RuntimeException {

    private final String status;

    public SqlServerDatabaseException(String message, String status) {
        super(message);
        this.status = status;
    }
}
