package co.com.vialsmotors.login.domain.exception;

import lombok.Getter;

@Getter
public class UsernameException extends RuntimeException{

    private final String status;

    public UsernameException(String message, String status) {
        super(message);
        this.status = status;
    }
}
