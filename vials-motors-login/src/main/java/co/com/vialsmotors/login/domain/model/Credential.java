package co.com.vialsmotors.login.domain.model;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.validate.ValidateArgument;
import lombok.Getter;

@Getter
public class Credential extends Login {

    private final String customerID;



    public Credential(String email, String password, String customerID) {
        super(email, password);
        ValidateArgument.validateStringObject(email, EFieldException.USERNAME_EXCEPTION.getCode(), EFieldException.USERNAME_EXCEPTION.getMessage());
        ValidateArgument.validateStringObject(password, EFieldException.PASSWORD_EXCEPTION.getCode(), EFieldException.PASSWORD_EXCEPTION.getMessage());
        ValidateArgument.validateStringObject(customerID, EFieldException.UNIQUE_ID_EXCEPTION.getCode(), EFieldException.UNIQUE_ID_EXCEPTION.getMessage());
        this.customerID = customerID;
    }
}
