package co.com.vialsmotors.login.domain.model;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.validate.ValidateArgument;
import lombok.Getter;

@Getter
public class Login {

    private static final String USERNAME_INVALID = "The username is invalid";
    private static final String PASSWORD_INVALID = "The password is invalid";

    private final String email;
    private final String password;

    public Login (String email, String password){
        ValidateArgument.validateStringObject(email, EFieldException.EMAIL_EXCEPTION.getCode(), EFieldException.EMAIL_EXCEPTION.getMessage());
        ValidateArgument.validateStringObject(password, EFieldException.PASSWORD_EXCEPTION.getCode(), EFieldException.PASSWORD_EXCEPTION.getMessage());
        this.email = email;
        this.password = password;
    }
}
