package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import org.springframework.http.ResponseEntity;

public interface CredentialRepository {

    ResponseEntity<GenericResponse> addCredential(CredentialEntity credentialEntity, Integer userType);
}
