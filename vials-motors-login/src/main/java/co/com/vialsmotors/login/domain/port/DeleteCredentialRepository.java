package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteCredentialRepository {
    ResponseEntity<GenericResponse> deleteCredential(Long id);
}
