package co.com.vialsmotors.login.domain.port;

public interface DeleteOtpRepository {

    void deleteOtp(Long id);
}
