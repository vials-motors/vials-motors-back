package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;

import java.util.Optional;

public interface GetCredentialRepository {

    Optional<CredentialEntity> getCredential(String email);
}
