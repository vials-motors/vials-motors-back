package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;

import java.util.Optional;

public interface GetOtpRepository {

    Optional<OtpEntity> getOtp(String otp);
}
