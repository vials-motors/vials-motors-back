package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.domain.model.Login;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface LoginRepository {

    ResponseEntity<JwtResponse> executeLogin(Login login);
}
