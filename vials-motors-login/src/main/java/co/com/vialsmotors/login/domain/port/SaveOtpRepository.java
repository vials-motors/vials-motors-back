package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;

public interface SaveOtpRepository {

    void saveOtp(OtpEntity otpEntity);
}
