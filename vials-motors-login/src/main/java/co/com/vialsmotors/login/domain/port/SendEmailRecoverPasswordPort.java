package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.apirest.dto.SendEmailRecoverPasswordDTO;

public interface SendEmailRecoverPasswordPort {

    void sendEmail(SendEmailRecoverPasswordDTO sendEmailRecoverPasswordDTO);
}
