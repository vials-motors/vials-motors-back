package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;

public interface UpdateCredentialPasswordRepository {

    void updatePassword(CredentialEntity credentialEntity);
}
