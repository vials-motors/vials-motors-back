package co.com.vialsmotors.login.domain.port;

import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;

import java.util.Optional;

public interface UpdateCredentialRepository {

    void updateCredential(CredentialEntity credentialEntity);
    Optional<CredentialEntity> getCredentialByCustomerId(String customerID);
}
