package co.com.vialsmotors.login.domain.response;

import java.time.ZonedDateTime;

public record JwtResponse (String code, String tokenValue, Long id, String tokenType, String email, String customerId, String rol, ZonedDateTime timestamp) {
}
