package co.com.vialsmotors.login.domain.response;

import java.time.ZonedDateTime;

public record ValidateResponse(String code, String message, String email, ZonedDateTime timestamp) {
}
