package co.com.vialsmotors.login.domain.service.credential;

import co.com.vialsmotors.login.domain.model.Credential;
import co.com.vialsmotors.login.domain.port.CredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.mapper.GenericMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

public class CredentialImplService implements CredentialService{

    private static final Logger LOGGER = Logger.getLogger(CredentialImplService.class);
    private final CredentialRepository credentialRepository;
    private final GenericMapper genericMapper;

    public CredentialImplService(CredentialRepository credentialRepository, GenericMapper genericMapper) {
        this.credentialRepository = credentialRepository;
        this.genericMapper = genericMapper;
    }

    @Override
    public ResponseEntity<GenericResponse> executeRegisterCredential(Credential credential, Integer userType) {
        LOGGER.info("MS-LOGIN: Service execute register credential with username: " + credential.getEmail());
        return credentialRepository.addCredential(genericMapper.map(credential, CredentialEntity.class), userType );
    }
}
