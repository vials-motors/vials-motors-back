package co.com.vialsmotors.login.domain.service.credential;

import co.com.vialsmotors.login.domain.model.Credential;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface CredentialService {

    ResponseEntity<GenericResponse> executeRegisterCredential(Credential credential, Integer userType);
}
