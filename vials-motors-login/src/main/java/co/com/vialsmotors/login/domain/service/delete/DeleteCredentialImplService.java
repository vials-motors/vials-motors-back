package co.com.vialsmotors.login.domain.service.delete;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.exception.CustomerIdException;
import co.com.vialsmotors.login.domain.port.DeleteCredentialRepository;
import co.com.vialsmotors.login.domain.port.UpdateCredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public class DeleteCredentialImplService implements DeleteCredentialService{

    private static final Logger LOGGER = Logger.getLogger(DeleteCredentialImplService.class);
    private final DeleteCredentialRepository deleteCredentialRepository;
    private final UpdateCredentialRepository updateCredentialRepository;

    public DeleteCredentialImplService(DeleteCredentialRepository deleteCredentialRepository, UpdateCredentialRepository updateCredentialRepository) {
        this.deleteCredentialRepository = deleteCredentialRepository;
        this.updateCredentialRepository = updateCredentialRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteCredential(String customerId) {
        LOGGER.info("INFO-MS-LOGIN: Delete credential in Service with customer id: " + customerId);
        this.validateCustomerId(customerId);
        return this.deleteCredentialRepository.deleteCredential(this.getCredential(customerId).getId());
    }

    private CredentialEntity getCredential(String customerId){
        Optional<CredentialEntity> credentialEntity = this.updateCredentialRepository.getCredentialByCustomerId(customerId);
        if (credentialEntity.isPresent())
            return credentialEntity.get();
        LOGGER.error("ERROR-MS-LOGIN: Error in delete credential for credential not exist");
        throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty())
            throw new CustomerIdException(EFieldException.CUSTOMER_ID_EXCEPTION.getMessage(), EFieldException.CUSTOMER_ID_EXCEPTION.getCode());
    }
}
