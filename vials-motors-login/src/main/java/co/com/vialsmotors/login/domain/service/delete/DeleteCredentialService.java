package co.com.vialsmotors.login.domain.service.delete;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteCredentialService {

    ResponseEntity<GenericResponse> deleteCredential(String customerId);
}
