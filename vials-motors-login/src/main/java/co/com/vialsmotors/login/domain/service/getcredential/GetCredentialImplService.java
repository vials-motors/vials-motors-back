package co.com.vialsmotors.login.domain.service.getcredential;

import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.port.GetCredentialRepository;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.mapper.CredentialMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public class GetCredentialImplService implements GetCredentialService{

    private static final Logger LOGGER = Logger.getLogger(GetCredentialImplService.class);
    private final GetCredentialRepository getCredentialRepository;

    public GetCredentialImplService(GetCredentialRepository getCredentialRepository) {
        this.getCredentialRepository = getCredentialRepository;
    }

    @Override
    public ResponseEntity<CredentialDTO> getCredential(String username) {
        LOGGER.info("MS-LOGIN: Get Credential in GetCredentialImplService.class");
        return ResponseEntity.ok(CredentialMapper.convertEntityToDTO(getCredentialEntityOptional(username)));
    }

    private CredentialEntity getCredentialEntityOptional(String username){
        Optional<CredentialEntity> credentialEntity = this.getCredentialRepository.getCredential(username);
        if (credentialEntity.isEmpty()){
            throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
        }
        return credentialEntity.get();
    }
}
