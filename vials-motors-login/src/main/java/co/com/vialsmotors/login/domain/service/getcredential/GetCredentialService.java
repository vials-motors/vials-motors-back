package co.com.vialsmotors.login.domain.service.getcredential;

import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import org.springframework.http.ResponseEntity;

public interface GetCredentialService {

    ResponseEntity<CredentialDTO> getCredential(String username);
}
