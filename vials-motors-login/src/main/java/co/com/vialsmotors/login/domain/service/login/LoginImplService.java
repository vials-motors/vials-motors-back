package co.com.vialsmotors.login.domain.service.login;

import co.com.vialsmotors.login.domain.model.Login;
import co.com.vialsmotors.login.domain.port.LoginRepository;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public class LoginImplService implements LoginService{

    private final LoginRepository loginRepository;

    public LoginImplService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    @Override
    public ResponseEntity<JwtResponse> executeLogin(Login login) {
        return loginRepository.executeLogin(login);
    }
}
