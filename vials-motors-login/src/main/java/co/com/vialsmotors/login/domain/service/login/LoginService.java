package co.com.vialsmotors.login.domain.service.login;

import co.com.vialsmotors.login.domain.model.Login;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface LoginService {

    ResponseEntity<JwtResponse> executeLogin(Login login);
}
