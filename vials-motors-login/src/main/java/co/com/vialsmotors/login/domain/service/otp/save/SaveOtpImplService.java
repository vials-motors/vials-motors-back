package co.com.vialsmotors.login.domain.service.otp.save;

import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.port.FindEmailRepository;
import co.com.vialsmotors.login.domain.port.SaveOtpRepository;
import co.com.vialsmotors.login.domain.port.SendEmailRecoverPasswordPort;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.apirest.dto.SendEmailRecoverPasswordDTO;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import co.com.vialsmotors.login.infrastructure.util.GenerateOtpUtils;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class SaveOtpImplService implements SaveOtpService{

    private static final Logger LOGGER = Logger.getLogger(SaveOtpImplService.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private static final Long PLUS_FIVE_MINUTES = 5L;
    private final FindEmailRepository findEmailRepository;
    private final SaveOtpRepository saveOtpRepository;
    private final SendEmailRecoverPasswordPort sendEmailRecoverPasswordPort;

    public SaveOtpImplService(FindEmailRepository findEmailRepository, SaveOtpRepository saveOtpRepository, SendEmailRecoverPasswordPort sendEmailRecoverPasswordPort) {
        this.findEmailRepository = findEmailRepository;
        this.saveOtpRepository = saveOtpRepository;
        this.sendEmailRecoverPasswordPort = sendEmailRecoverPasswordPort;
    }

    @Override
    public ResponseEntity<GenericResponse> saveOtp(String email) {
        LOGGER.info("INFO-MS-LOGIN: Save and send email for otp");
        CredentialEntity credentialEntity = getCredential(email);
        String code = GenerateOtpUtils.generate();
        sendEmail(email, code);
        this.saveOtpRepository.saveOtp(buildOtp(email, code, credentialEntity));
        return ResponseEntity.ok(new GenericResponse(EResponse.SUCCESSFULLY_SAVE_OTP.getCode(),
                EResponse.SUCCESSFULLY_SAVE_OTP.getMessage(), TIMESTAMP));
    }

    private CredentialEntity getCredential(String email){
        LOGGER.info("INFO-MS-LOGIN: Get credential with email: " + email);
        Optional<CredentialEntity> credentialEntity = this.findEmailRepository.getCredential(email);
        if (credentialEntity.isEmpty())
            throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
        return credentialEntity.get();
    }

    private OtpEntity buildOtp(String email, String code, CredentialEntity credentialEntity){
        LOGGER.info("INFO-MS-LOGIN: Build new Otp with email: " + email + " and otp: " + code);
        return new OtpEntity(code,
                credentialEntity,
                TIMESTAMP.plusMinutes(PLUS_FIVE_MINUTES));
    }

    private void sendEmail(String email, String code){
        this.sendEmailRecoverPasswordPort.sendEmail(new SendEmailRecoverPasswordDTO(email, code));
    }
}
