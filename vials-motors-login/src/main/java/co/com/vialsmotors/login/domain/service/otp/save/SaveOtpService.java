package co.com.vialsmotors.login.domain.service.otp.save;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface SaveOtpService {

    ResponseEntity<GenericResponse> saveOtp(String email);
}
