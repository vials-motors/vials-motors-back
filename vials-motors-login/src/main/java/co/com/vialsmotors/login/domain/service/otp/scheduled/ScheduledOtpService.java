package co.com.vialsmotors.login.domain.service.otp.scheduled;

import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import co.com.vialsmotors.login.infrastructure.jpa.OtpSqlServerRepository;
import org.jboss.logging.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

@Service
public class ScheduledOtpService {

    private static final Logger LOGGER = Logger.getLogger(ScheduledOtpService.class);
    private final OtpSqlServerRepository otpSqlServerRepository;

    public ScheduledOtpService(OtpSqlServerRepository otpSqlServerRepository) {
        this.otpSqlServerRepository = otpSqlServerRepository;
    }

    @Scheduled(fixedRate = 600000)
    public void execute(){
        LOGGER.info("INFO-MS-LOGIN: Execute scheduled for delete opt");
        this.deleteOtp(this.otpSqlServerRepository.findAll()
                .stream()
                .filter(temporal -> temporal.getExpiration().isBefore(ZonedDateTime.now(TimeZone
                        .getTimeZone("America/Bogota").toZoneId())))
                .toList());
    }

    private void deleteOtp(List<OtpEntity> list){
        LOGGER.info("INFO-MS-LOGIN: Delete otp");
        list.forEach(temporal -> this.otpSqlServerRepository.deleteById(temporal.getIdOtp()));
    }
}
