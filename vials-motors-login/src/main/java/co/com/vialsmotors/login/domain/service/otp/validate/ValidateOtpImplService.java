package co.com.vialsmotors.login.domain.service.otp.validate;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.FieldException;
import co.com.vialsmotors.login.domain.exception.OtpException;
import co.com.vialsmotors.login.domain.port.DeleteOtpRepository;
import co.com.vialsmotors.login.domain.port.GetOtpRepository;
import co.com.vialsmotors.login.domain.response.ValidateResponse;
import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class ValidateOtpImplService implements ValidateOtpService{

    private static final Logger LOGGER = Logger.getLogger(ValidateOtpImplService.class);
    private static final ZonedDateTime TIMESTAMP = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final GetOtpRepository getOtpRepository;
    private final DeleteOtpRepository deleteOtpRepository;

    public ValidateOtpImplService(GetOtpRepository getOtpRepository, DeleteOtpRepository deleteOtpRepository) {
        this.getOtpRepository = getOtpRepository;
        this.deleteOtpRepository = deleteOtpRepository;
    }

    @Override
    public ResponseEntity<ValidateResponse> validateOtp(String otp) {
        this.validateFieldOtp(otp);
        OtpEntity otpEntity = getOtp(otp);
        this.validateDate(otpEntity.getExpiration());
        this.deleteOtp(otpEntity.getIdOtp());
        return ResponseEntity.ok(new ValidateResponse(EResponse.SUCCESSFULLY_VALIDATE_OTP.getCode(),
                EResponse.SUCCESSFULLY_VALIDATE_OTP.getMessage(), otpEntity.getEmail().getEmail(), TIMESTAMP));
    }

    private OtpEntity getOtp(String otp){
        Optional<OtpEntity> otpEntity = this.getOtpRepository.getOtp(otp);
        if (otpEntity.isEmpty()){
            LOGGER.info("INFO-MS-LOGIN: The code otp not exit");
            throw new OtpException(EResponse.OTP_NOT_EXIST.getMessage(), EResponse.OTP_NOT_EXIST.getCode());
        }
        return otpEntity.get();
    }

    private void deleteOtp(Long id){
        this.deleteOtpRepository.deleteOtp(id);
    }

    private void validateDate(ZonedDateTime expiration){
        if (expiration.isBefore(TIMESTAMP)){
            LOGGER.info("INFO-MS-LOGIN: Expiration of date");
            throw new OtpException(EResponse.OTP_EXPIRATION_ERROR.getMessage(), EResponse.OTP_EXPIRATION_ERROR.getCode());
        }
    }

    private void validateFieldOtp(String otp){
        if (otp == null || otp.isEmpty()){
            LOGGER.error("ERROR-MS-LOGIN: Error for otp invalid");
            throw new FieldException(EFieldException.CODE_OTP_ERROR.getCode(), EFieldException.CODE_OTP_ERROR.getMessage());
        }
    }
}
