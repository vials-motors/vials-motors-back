package co.com.vialsmotors.login.domain.service.otp.validate;

import co.com.vialsmotors.login.domain.response.ValidateResponse;
import org.springframework.http.ResponseEntity;

public interface ValidateOtpService {

    ResponseEntity<ValidateResponse> validateOtp(String otp);
}
