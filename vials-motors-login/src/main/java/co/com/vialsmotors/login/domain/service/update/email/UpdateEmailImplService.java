package co.com.vialsmotors.login.domain.service.update.email;

import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.port.UpdateCredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.validate.ValidateArgument;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateEmailImplService implements UpdateEmailService {

    private static final Logger LOGGER = Logger.getLogger(UpdateEmailImplService.class);
    private final UpdateCredentialRepository updateCredentialRepository;

    public UpdateEmailImplService(UpdateCredentialRepository updateCredentialRepository) {
        this.updateCredentialRepository = updateCredentialRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> updateEmail(String email, String customerID) {
       LOGGER.info("INFO-MS-LOGIN: Update credential in update credential impl service with customer id: " + customerID);
       ValidateArgument.validateCustomerAndEmailField(email, customerID);
       this.updateCredentialRepository.updateCredential(buildNewCredential(email, customerID));
       return ResponseEntity.ok(new GenericResponse(EResponse.SUCCESSFULLY_UPDATE_CREDENTIAL.getCode(), EResponse.SUCCESSFULLY_UPDATE_CREDENTIAL.getMessage(),
               ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private CredentialEntity getCredential(String customerId){
        Optional<CredentialEntity> credentialEntity = this.updateCredentialRepository.getCredentialByCustomerId(customerId);
        return credentialEntity.orElse(null);
    }

    private CredentialEntity buildNewCredential(String username, String customerID){
        CredentialEntity credentialEntity = getCredential(customerID);
        if (credentialEntity == null ){
            throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
        }
        return credentialEntity.withEmail(username);
    }
}
