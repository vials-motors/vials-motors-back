package co.com.vialsmotors.login.domain.service.update.email;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateEmailService {

    ResponseEntity<GenericResponse> updateEmail(String email, String customerID);
}
