package co.com.vialsmotors.login.domain.service.update.password;

import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.exception.UnauthorizedException;
import co.com.vialsmotors.login.domain.port.FindEmailRepository;
import co.com.vialsmotors.login.domain.port.UpdateCredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.security.bean.PasswordEncodeBean;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdatePasswordImplService implements UpdatePasswordService{

    private static final Logger LOGGER = Logger.getLogger(UpdatePasswordImplService.class);
    private final FindEmailRepository findEmailRepository;
    private final UpdateCredentialRepository updateCredentialRepository;
    private final PasswordEncodeBean passwordEncodeBean;

    @Value("${key.authorization}")
    protected String keyAuthorization;

    public UpdatePasswordImplService(FindEmailRepository findEmailRepository, UpdateCredentialRepository updateCredentialRepository, PasswordEncodeBean passwordEncodeBean) {
        this.findEmailRepository = findEmailRepository;
        this.updateCredentialRepository = updateCredentialRepository;
        this.passwordEncodeBean = passwordEncodeBean;
    }

    @Override
    public ResponseEntity<GenericResponse> updatePassword(String email, String newPassword, String authorization) {
        this.validateAuthorization(authorization);
        CredentialEntity credentialEntity = getCredential(email);
        this.updateCredential(updateForNewPassword(newPassword,credentialEntity));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_PASSWORD_SUCCESS.getCode(),
                EResponse.UPDATE_PASSWORD_SUCCESS.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private CredentialEntity getCredential(String email){
        Optional<CredentialEntity> credentialEntity = this.findEmailRepository.getCredential(email);
        if (credentialEntity.isEmpty()){
            LOGGER.info("INFO-MS-LOGIN: Error for credential entity not exist");
            throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
        }
        return credentialEntity.get();
    }

    private CredentialEntity updateForNewPassword(String newPassword, CredentialEntity credentialEntity){
        return credentialEntity.withPassword(passwordEncodeBean.passwordEncoder().encode(newPassword));
    }

    private void updateCredential(CredentialEntity credentialEntity){
        this.updateCredentialRepository.updateCredential(credentialEntity);
    }

    private void validateAuthorization(String authorization){
        if (authorization == null || authorization.isEmpty() || !authorization.equals(keyAuthorization)){
            throw new UnauthorizedException(EResponse.UNAUTHORIZED_ERROR.getMessage(), EResponse.UNAUTHORIZED_ERROR.getCode());
        }
    }
}
