package co.com.vialsmotors.login.domain.service.update.password;

import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdatePasswordService {

    ResponseEntity<GenericResponse> updatePassword(String email, String newPassword, String authorization);
}
