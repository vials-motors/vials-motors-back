package co.com.vialsmotors.login.domain.validate;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.exception.FieldException;

public class ValidateArgument {

    private static final String NOT_INSTANTIABLE = "The class is not instantiable";

    private ValidateArgument () {
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static void validateStringObject(String objectString, String code, String message){
        if (objectString == null || objectString.isBlank())
            throw new FieldException(code, message);
    }

    public static void validateCustomerAndEmailField(String email, String customerId){
        if (email == null || email.isBlank()){
            throw new FieldException(EFieldException.USERNAME_EXCEPTION.getCode(), EFieldException.USERNAME_EXCEPTION.getCode());
        }else if (customerId == null || customerId.isBlank()){
            throw new FieldException(EFieldException.CUSTOMER_ID_EXCEPTION.getCode(), EFieldException.CUSTOMER_ID_EXCEPTION.getCode());
        }
    }
}
