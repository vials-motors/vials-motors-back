package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.UsernameException;
import co.com.vialsmotors.login.domain.port.CredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.entity.RoleEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import co.com.vialsmotors.login.infrastructure.security.bean.PasswordEncodeBean;
import co.com.vialsmotors.login.infrastructure.util.ValidateRoleUtil;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

@Repository
@RequiredArgsConstructor
public class CredentialImplRepository implements CredentialRepository {

    private static final Logger LOGGER = Logger.getLogger(CredentialImplRepository.class);
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final CredentialSqlServerRepository credentialSqlServerRepository;
    private final PasswordEncodeBean passwordEncodeBean;

    @Override
    public ResponseEntity<GenericResponse> addCredential(CredentialEntity credentialEntity, Integer userType) {
        try {
            if (!this.validateEmail(credentialEntity.getEmail())){
                this.credentialSqlServerRepository.save(buildCredentialEntity(credentialEntity, userType));
                return ResponseEntity.ok(new GenericResponse(EResponse.SUCCESSFULLY_REGISTER_CREDENTIAL.getCode(),
                        EResponse.SUCCESSFULLY_REGISTER_CREDENTIAL.getMessage(), TIMESTAMP_RESPONSE));
            }
            return ResponseEntity.ok(new GenericResponse(EErrorDataBase.ERROR_USERNAME.getCode(),
                    EErrorDataBase.ERROR_USERNAME.getMessage(), TIMESTAMP_RESPONSE));
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error in CredentialImplRepository with call database for add credential: " + e.getMessage());
            throw new UsernameException(e.getMessage(), EErrorDataBase.ERROR_USERNAME.getCode());
        }
    }

    private CredentialEntity buildCredentialEntity(CredentialEntity credentialEntity, Integer userType){
        return credentialEntity
                .withRoles(setRoles(userType))
                .withPassword(passwordEncodeBean.passwordEncoder().encode(credentialEntity.getPassword()));
    }

    private boolean validateEmail(String email){
        return this.credentialSqlServerRepository.findByEmail(email).isPresent();
    }

    private Set<RoleEntity> setRoles(Integer userType){
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(ValidateRoleUtil.generateRoleWithUserType(userType));
        return roleEntities;
    }
}
