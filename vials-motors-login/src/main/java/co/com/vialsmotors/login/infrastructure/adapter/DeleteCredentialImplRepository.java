package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.DeleteCredentialRepository;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.TimeZone;

@Repository
@RequiredArgsConstructor
public class DeleteCredentialImplRepository implements DeleteCredentialRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteCredentialImplRepository.class);
    private final CredentialSqlServerRepository credentialSqlServerRepository;

    @Override
    public ResponseEntity<GenericResponse> deleteCredential(Long id) {
        try {
            this.credentialSqlServerRepository.deleteById(id);
            return ResponseEntity.ok(new GenericResponse(EResponse.SUCCESSFULLY_DELETE_CREDENTIAL.getCode(), EResponse.SUCCESSFULLY_DELETE_CREDENTIAL.getMessage(),
                    ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error delete credential in database with message: " + e.getMessage());
            throw  new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
