package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.DeleteOtpRepository;
import co.com.vialsmotors.login.infrastructure.jpa.OtpSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeleteOtpImplRepository implements DeleteOtpRepository {

    private static final Logger LOGGER = Logger.getLogger(DeleteOtpImplRepository.class);
    private final OtpSqlServerRepository otpSqlServerRepository;

    @Override
    public void deleteOtp(Long id) {
        try {
            this.otpSqlServerRepository.deleteById(id);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error delete otp token with message: " + e.getMessage());
            throw new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
