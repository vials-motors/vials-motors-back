package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.FindEmailRepository;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class FindEmailImplRepository implements FindEmailRepository {

    private static final Logger LOGGER = Logger.getLogger(FindEmailImplRepository.class);
    private final CredentialSqlServerRepository credentialSqlServerRepository;

    @Override
    public Optional<CredentialEntity> getCredential(String email) {
        try {
            return this.credentialSqlServerRepository.findByEmail(email);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error getting credential with message: " + e.getMessage());
            throw new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
