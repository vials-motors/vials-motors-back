package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.CredentialException;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.GetCredentialRepository;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetCredentialImplRepository implements GetCredentialRepository {

    private static final Logger LOGGER = Logger.getLogger(GetCredentialImplRepository.class);

    private final CredentialSqlServerRepository credentialSqlServerRepository;

    @Override
    public Optional<CredentialEntity> getCredential(String email) {
        try {
            Optional<CredentialEntity> getCredentialEntity = credentialSqlServerRepository.findByEmail(email);
            if (getCredentialEntity.isEmpty()){
                throw new CredentialException(EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
            }
            return getCredentialEntity;
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-LOGIN: Error in " + GetCredentialImplRepository.class.getName() + " in getCredential Method: " + exception.getMessage());
            validateException(exception);
        }
        return Optional.empty();
    }

    private void validateException(Exception e){
        if (e instanceof CredentialException credentialException){
            throw new CredentialException(credentialException.getMessage(), EResponse.CREDENTIAL_EXCEPTION_MESSAGE.getCode());
        }
        throw new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
    }
}
