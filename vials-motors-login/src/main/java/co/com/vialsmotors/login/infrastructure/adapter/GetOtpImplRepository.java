package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.GetOtpRepository;
import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import co.com.vialsmotors.login.infrastructure.jpa.OtpSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetOtpImplRepository implements GetOtpRepository {

    private static final Logger LOGGER = Logger.getLogger(GetOtpImplRepository.class);
    private final OtpSqlServerRepository otpSqlServerRepository;

    @Override
    public Optional<OtpEntity> getOtp(String otp) {
        try {
            return otpSqlServerRepository.findByOtp(otp);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error get otp entity with message: " + e.getMessage());
            throw new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
