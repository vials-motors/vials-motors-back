package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.ELogin;
import co.com.vialsmotors.login.domain.exception.LoginException;
import co.com.vialsmotors.login.domain.model.Login;
import co.com.vialsmotors.login.domain.port.LoginRepository;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import co.com.vialsmotors.login.infrastructure.security.jwt.JwtUtils;
import co.com.vialsmotors.login.infrastructure.security.userdetails.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.TimeZone;

@Repository
@RequiredArgsConstructor
public class LoginImplRepository implements LoginRepository {

    private static final Logger LOGGER = Logger.getLogger(LoginImplRepository.class);
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    @Override
    public ResponseEntity<JwtResponse> executeLogin(Login login) {
        try{
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            return ResponseEntity.ok(new JwtResponse(ELogin.SUCCESS_LOGIN.getCode(), generateJwt(authentication),
                    userDetails.getId(), ELogin.LOGIN_BEARER.getMessage(), userDetails.getEmail(), userDetails.getCustomerId(), getRol(userDetails), TIMESTAMP_RESPONSE));
        }catch (Exception e){
            LOGGER.error(ELogin.ERROR_LOGIN.getMessage() + e.getMessage());
            throw new LoginException(e.getMessage(), ELogin.ERROR_LOGIN.getCode());
        }
    }

    private String generateJwt(Authentication authentication){
        return jwtUtils.generateJwtToken(authentication);
    }

    private String getRol(UserDetailsImpl userDetails){
        return userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList()
                .get(0);
    }
}
