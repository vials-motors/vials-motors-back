package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.SaveOtpRepository;
import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import co.com.vialsmotors.login.infrastructure.jpa.OtpSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class SaveOtpImplRepository implements SaveOtpRepository {

    private static final Logger LOGGER = Logger.getLogger(SaveOtpImplRepository.class);
    private final OtpSqlServerRepository otpSqlServerRepository;

    @Override
    public void saveOtp(OtpEntity otpEntity) {
        try {
            this.otpSqlServerRepository.save(otpEntity);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-LOGIN: Error save Otp with message: " + e.getMessage());
            throw new SqlServerDatabaseException(e.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
