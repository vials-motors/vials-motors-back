package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.SendEmailException;
import co.com.vialsmotors.login.domain.port.SendEmailRecoverPasswordPort;
import co.com.vialsmotors.login.infrastructure.apirest.client.EmailService;
import co.com.vialsmotors.login.infrastructure.apirest.dto.SendEmailRecoverPasswordDTO;
import co.com.vialsmotors.login.infrastructure.apirest.response.FeignResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class SendEmailRecoverPasswordAdapter implements SendEmailRecoverPasswordPort {

    private static final Logger LOGGER = Logger.getLogger(SendEmailRecoverPasswordAdapter.class);
    private static final String STATUS_OK = "SUCC-00";
    private final EmailService emailService;

    @Override
    public void sendEmail(SendEmailRecoverPasswordDTO sendEmailRecoverPasswordDTO) {
        try{
            ResponseEntity<FeignResponse> response = this.emailService.sendEmailRecoverPassword(sendEmailRecoverPasswordDTO);
            this.validateResponse(Objects.requireNonNull(response.getBody()).getCode());
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-LOGIN: Error calling the mail sending microservice with the following message: " + exception.getMessage());
            throw new SendEmailException(exception.getMessage(), EResponse.SEND_EMAIL_ERROR.getCode());
        }
    }

    private void validateResponse(String codeResponse){
        if (!codeResponse.equals(STATUS_OK))
            throw new SendEmailException(EResponse.SEND_EMAIL_ERROR.getMessage(), EResponse.SEND_EMAIL_ERROR.getCode());
    }
}
