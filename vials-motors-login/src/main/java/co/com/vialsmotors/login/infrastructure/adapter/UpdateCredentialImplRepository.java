package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.UpdateCredentialRepository;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UpdateCredentialImplRepository implements UpdateCredentialRepository {

    private static final Logger LOGGER = Logger.getLogger(UpdateCredentialImplRepository.class);
    private final CredentialSqlServerRepository credentialSqlServerRepository;

    @Override
    public void updateCredential(CredentialEntity credentialEntity) {
        try {
            this.credentialSqlServerRepository.save(credentialEntity);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-LOGIN: Error update credential in Update Credential Impl Repository with message: " + exception.getMessage());
            throw new SqlServerDatabaseException(exception.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<CredentialEntity> getCredentialByCustomerId(String customerID) {
        try {
            return this.credentialSqlServerRepository.findByCustomerID(customerID);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-LOGIN: Error in find by customer id with message " + exception.getMessage());
            throw new SqlServerDatabaseException(exception.getMessage(), EErrorDataBase.ERROR_USERNAME.getCode());
        }
    }
}
