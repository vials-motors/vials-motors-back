package co.com.vialsmotors.login.infrastructure.adapter;

import co.com.vialsmotors.login.domain.enums.EErrorDataBase;
import co.com.vialsmotors.login.domain.exception.SqlServerDatabaseException;
import co.com.vialsmotors.login.domain.port.UpdateCredentialPasswordRepository;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class UpdateCredentialPasswordImplRepository implements UpdateCredentialPasswordRepository {

    private static final Logger LOGGER = Logger.getLogger(UpdateCredentialPasswordImplRepository.class);
    private final CredentialSqlServerRepository credentialSqlServerRepository;

    @Override
    public void updatePassword(CredentialEntity credentialEntity) {
        try {
            this.credentialSqlServerRepository.save(credentialEntity);
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-LOGIN: Error while update login: " + exception.getMessage());
            throw new SqlServerDatabaseException(exception.getMessage(), EErrorDataBase.ERROR_DATABASE.getCode());
        }
    }
}
