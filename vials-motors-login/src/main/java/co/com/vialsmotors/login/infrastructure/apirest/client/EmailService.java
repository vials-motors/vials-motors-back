package co.com.vialsmotors.login.infrastructure.apirest.client;

import co.com.vialsmotors.login.infrastructure.apirest.dto.SendEmailRecoverPasswordDTO;
import co.com.vialsmotors.login.infrastructure.apirest.response.FeignResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "feignClientEmail", url = "${external.service.email}")
public interface EmailService {

    @PostMapping("/send-email/recover-password")
    ResponseEntity<FeignResponse> sendEmailRecoverPassword(@RequestBody SendEmailRecoverPasswordDTO sendEmailRecoverPasswordDTO);
}
