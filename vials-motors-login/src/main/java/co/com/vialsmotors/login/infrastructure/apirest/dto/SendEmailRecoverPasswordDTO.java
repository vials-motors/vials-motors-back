package co.com.vialsmotors.login.infrastructure.apirest.dto;

import co.com.vialsmotors.login.domain.enums.EFieldException;
import co.com.vialsmotors.login.domain.validate.ValidateArgument;

public record SendEmailRecoverPasswordDTO(String email, String otp) {

    public SendEmailRecoverPasswordDTO{
        ValidateArgument.validateStringObject(email, EFieldException.EMAIL_EXCEPTION.getCode(), EFieldException.EMAIL_EXCEPTION.getMessage());
        ValidateArgument.validateStringObject(otp, EFieldException.CODE_OTP_ERROR.getCode(), EFieldException.CODE_OTP_ERROR.getMessage());
    }
}
