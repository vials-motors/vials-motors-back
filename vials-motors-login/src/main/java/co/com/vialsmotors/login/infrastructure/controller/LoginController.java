package co.com.vialsmotors.login.infrastructure.controller;

import co.com.vialsmotors.login.application.command.CredentialCommand;
import co.com.vialsmotors.login.application.command.LoginCommand;
import co.com.vialsmotors.login.application.handler.credential.CredentialHandler;
import co.com.vialsmotors.login.application.handler.delete.DeleteCredentialHandler;
import co.com.vialsmotors.login.application.handler.getcredential.GetCredentialHandler;
import co.com.vialsmotors.login.application.handler.login.LoginHandler;
import co.com.vialsmotors.login.application.handler.update.email.UpdateUsernameHandler;
import co.com.vialsmotors.login.application.handler.update.password.UpdatePasswordHandler;
import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.response.JwtResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/login")
@CrossOrigin(origins = "${cors}")
public class LoginController {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    private final CredentialHandler credentialHandler;
    private final LoginHandler loginHandler;
    private final GetCredentialHandler getCredentialHandler;
    private final UpdateUsernameHandler updateUsernameHandler;
    private final DeleteCredentialHandler deleteCredentialHandler;
    private final UpdatePasswordHandler updatePasswordHandler;

    @PostMapping
    public ResponseEntity<JwtResponse> executeLogin(@RequestBody LoginCommand loginCommand){
        return this.loginHandler.executeLogin(loginCommand);
    }

    @PostMapping(path = "/register-credential")
    public ResponseEntity<GenericResponse> executeRegisterCredential(@RequestBody CredentialCommand credentialCommand, @RequestHeader("user-type") Integer userType){
        return this.credentialHandler.executeRegisterCredential(credentialCommand, userType);
    }

    @GetMapping(path = "/get-credential")
    public ResponseEntity<CredentialDTO> getUserCredential(@RequestHeader("username") String username){
        return getCredentialHandler.getCredential(username);
    }

    @PutMapping(path = "/update")
    public ResponseEntity<GenericResponse> updateCredential(@RequestHeader("username") String username, @RequestHeader("customerId") String customerId){
        LOGGER.info("INFO-MS-LOGIN: Receiver request for update credential with customer id: " + customerId);
        return updateUsernameHandler.updateUsername(username, customerId);
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<GenericResponse> deleteCredential(@RequestHeader("customerId") String customerId){
        LOGGER.info("INFO-MS-LOGIN: Receiver request for delete credentila with customer id: " + customerId);
        return deleteCredentialHandler.deleteCredential(customerId);
    }

    @PutMapping(path = "/update-password")
    public ResponseEntity<GenericResponse> updatePassword(@RequestHeader("email") String email, @RequestHeader("newPassword") String newPassword,
                                                          @RequestHeader("authorization") String authorization){
        LOGGER.info("INFO-MS-LOGIN: Receiver request for update password with email: " + email);
        return this.updatePasswordHandler.updatePassword(email, newPassword, authorization);
    }

    @GetMapping(path = "/test")
    public String test(){
        return "Test Login";
    }
}
