package co.com.vialsmotors.login.infrastructure.controller;

import co.com.vialsmotors.login.application.handler.otp.save.SaveOtpHandler;
import co.com.vialsmotors.login.application.handler.otp.validate.ValidateOtpHandler;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import co.com.vialsmotors.login.domain.response.ValidateResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/otp")
@CrossOrigin(origins = "http://localhost:4200")
public class OtpController {

    private static final Logger LOGGER = Logger.getLogger(OtpController.class);

    private final SaveOtpHandler saveOtpHandler;
    private final ValidateOtpHandler validateOtpHandler;

    @PostMapping
    public ResponseEntity<GenericResponse> saveOtp(@RequestParam("email") String email){
        LOGGER.info("INFO-MS-LOGIN: Receiver request for saveOtp in Otp controller with email: " + email);
        return saveOtpHandler.saveOtp(email);
    }

    @PostMapping(value = "/validate")
    public ResponseEntity<ValidateResponse> validateOtp(@RequestParam("otp") String otp){
        LOGGER.info("INFO-MS-LOGIN: Receiver request for validateOtp in Otp controller with otp: " + otp);
        return this.validateOtpHandler.validateOtp(otp);
    }
}
