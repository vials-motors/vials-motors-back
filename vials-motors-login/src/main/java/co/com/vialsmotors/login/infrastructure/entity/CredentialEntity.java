package co.com.vialsmotors.login.infrastructure.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users",
        uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
        @UniqueConstraint(columnNames = "customer_id")
        })
public class CredentialEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "credential_id")
    private Long id;

    @Column(length = 100, nullable = false, unique = false)
    private String email;

    @Column(length = 250, nullable = false)
    private String password;

    @Column(name = "customer_id", nullable = false)
    private String customerID;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
               joinColumns = @JoinColumn(name = "credential_id"),
               inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<RoleEntity> roles = new HashSet<>();

    public CredentialEntity withRoles(Set<RoleEntity> setRoles){
        this.roles = setRoles;
        return this;
    }

    public CredentialEntity withPassword(String password){
        this.password = password;
        return this;
    }

    public CredentialEntity withEmail(String email){
        this.email = email;
        return this;
    }
}
