package co.com.vialsmotors.login.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "otp", indexes = {
        @Index(name = "idx_otp_entity_id_otp_otp",
                columnList = "id_otp, otp")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OtpEntity {

    @Id
    @Column(name = "id_otp")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOtp;

    @Column(name = "otp", nullable = false)
    private String otp;

    @OneToOne
    @JoinColumn(name = "credential_id")
    private CredentialEntity email;

    @Column(name = "expiration")
    private ZonedDateTime expiration;

    public OtpEntity(String otp, CredentialEntity email, ZonedDateTime expiration) {
        this.otp = otp;
        this.email = email;
        this.expiration = expiration;
    }
}
