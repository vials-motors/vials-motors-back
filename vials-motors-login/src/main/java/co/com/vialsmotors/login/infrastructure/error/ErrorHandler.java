package co.com.vialsmotors.login.infrastructure.error;

import co.com.vialsmotors.login.domain.enums.EResponse;
import co.com.vialsmotors.login.domain.exception.*;
import co.com.vialsmotors.login.domain.response.GenericResponse;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = Logger.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: ";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler() {
        CODE_STATE.put(FieldException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(UsernameException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(GetUserNameFromJwtException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(DecodeTokenErrorException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(GenerateTokenException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(LoginException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(SqlServerDatabaseException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CredentialException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(CustomerIdException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(SendEmailException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(UnauthorizedException.class.getSimpleName(), HttpStatus.UNAUTHORIZED.value());
        CODE_STATE.put(OtpException.class.getSimpleName(), HttpStatus.OK.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleAllException(Exception exception){

        String nameException = exception.getClass().getSimpleName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof FieldException fieldException){
            LOGGER.info(MESSAGE_ERROR_GENERIC + fieldException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(fieldException.getCodeException(), fieldException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof UsernameException usernameException){
            LOGGER.info(MESSAGE_ERROR_GENERIC + usernameException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(usernameException.getStatus(), usernameException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof LoginException loginException) {
            LOGGER.info(MESSAGE_ERROR_GENERIC + loginException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(loginException.getStatus(), loginException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof SqlServerDatabaseException sqlServerDatabaseException) {
            LOGGER.info(MESSAGE_ERROR_GENERIC + sqlServerDatabaseException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sqlServerDatabaseException.getStatus(), sqlServerDatabaseException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GetUserNameFromJwtException getUsernameException) {
            LOGGER.info(MESSAGE_ERROR_GENERIC + getUsernameException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(getUsernameException.getStatus(), getUsernameException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof DecodeTokenErrorException decodeTokenErrorException) {
            LOGGER.info(MESSAGE_ERROR_GENERIC + decodeTokenErrorException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(decodeTokenErrorException.getStatus(), decodeTokenErrorException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GenerateTokenException generateTokenException) {
            LOGGER.info(MESSAGE_ERROR_GENERIC + generateTokenException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(generateTokenException.getStatus(), generateTokenException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof CredentialException credentialException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(credentialException.getStatus(), credentialException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof CustomerIdException customerIdException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(customerIdException.getStatus(), customerIdException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof SendEmailException sendEmailException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sendEmailException.getCode(), sendEmailException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof UnauthorizedException unauthorizedException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(unauthorizedException.getCode(), unauthorizedException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof OtpException otpException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(otpException.getCode(), otpException.getMessage(), TIMESTAMP_RESPONSE));
        } else {
            LOGGER.info(MESSAGE_ERROR_GENERIC + exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_ERROR.getCode(), EResponse.INTERNAL_ERROR.getMessage(), TIMESTAMP_RESPONSE));
        }
    }
}
