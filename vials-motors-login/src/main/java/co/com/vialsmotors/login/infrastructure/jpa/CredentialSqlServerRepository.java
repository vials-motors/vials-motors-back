package co.com.vialsmotors.login.infrastructure.jpa;

import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CredentialSqlServerRepository extends JpaRepository<CredentialEntity, Long> {

    Optional<CredentialEntity> findByEmail(String email);

    @Query(value = "SELECT * FROM users WHERE customer_id = ?1", nativeQuery = true)
    Optional<CredentialEntity> findByCustomerID(String customerId);

}
