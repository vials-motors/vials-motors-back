package co.com.vialsmotors.login.infrastructure.jpa;

import co.com.vialsmotors.login.infrastructure.entity.OtpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface OtpSqlServerRepository extends JpaRepository<OtpEntity, Long> {

    @Query(value = "SELECT * FROM otp WHERE otp =?1", nativeQuery = true)
    Optional<OtpEntity> findByOtp(String otp);
}
