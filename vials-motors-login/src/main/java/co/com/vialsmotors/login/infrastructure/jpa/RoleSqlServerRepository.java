package co.com.vialsmotors.login.infrastructure.jpa;

import co.com.vialsmotors.login.domain.enums.ERole;
import co.com.vialsmotors.login.infrastructure.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleSqlServerRepository extends JpaRepository<RoleEntity, Long> {

    Optional<RoleEntity> findByName(ERole name);
}
