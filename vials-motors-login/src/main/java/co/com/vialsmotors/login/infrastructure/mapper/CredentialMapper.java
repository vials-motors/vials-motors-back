package co.com.vialsmotors.login.infrastructure.mapper;

import co.com.vialsmotors.login.domain.dto.CredentialDTO;
import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;

public class CredentialMapper {

    private static final String NOT_INSTANTIABLE = "The class CredentialMapper is not instantiable";

    private CredentialMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static CredentialDTO convertEntityToDTO(CredentialEntity credentialEntity){
        return new CredentialDTO(credentialEntity.getEmail(), credentialEntity.getRoles());
    }
}

