package co.com.vialsmotors.login.infrastructure.security.jwt;

import co.com.vialsmotors.login.domain.exception.DecodeTokenErrorException;
import co.com.vialsmotors.login.domain.exception.GenerateTokenException;
import co.com.vialsmotors.login.domain.exception.GetUserNameFromJwtException;
import co.com.vialsmotors.login.infrastructure.security.userdetails.UserDetailsImpl;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtils {

    private static final Logger LOGGER = Logger.getLogger(JwtUtils.class);
    private static final String ISS = "Vials Motors";
    private static final String AUD = "Universidad Catolica de Oriente";
    @Value("${key.private}")
    protected String privateKey;

    public String generateJwtToken(Authentication authentication) {
        try {
            Algorithm algorithm = Algorithm.HMAC512(privateKey);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            Long time = Instant.now().getEpochSecond();
            Long timeOfExpirationIdAndRefresh = Instant.now().plus(30, ChronoUnit.DAYS).getEpochSecond();

            Map<String, Object> headerClaim = new HashMap<>();
            headerClaim.put("alg", "RSA256");
            headerClaim.put("typ", "JWT");

            return JWT.create()
                    .withHeader(headerClaim)
                    .withClaim("sub", userDetails.getEmail())
                    .withClaim("iss", ISS)
                    .withClaim("aud", AUD)
                    .withClaim("iat", time)
                    .withClaim("exp", timeOfExpirationIdAndRefresh)
                    .sign(algorithm);
        }catch (GenerateTokenException e){
            throw new GenerateTokenException("JWT-ERR-00", e.getMessage());
        }
    }
    public String getEmailFromJwtToken(String token){
        try {
            DecodedJWT jwt = decodeToken(token);
            return jwt.getClaims().get("sub").asString();
        }
        catch(JWTVerificationException exception) {
            throw new GetUserNameFromJwtException("JWT-ERR-01",exception.getMessage());
        }
    }
    public boolean validateJwtToken(String authToken){
        try {
            Long time = Instant.now().getEpochSecond();
            DecodedJWT jwt = decodeToken(authToken);
            if (jwt.getClaims().get("exp").asLong() > time && jwt.getClaims().get("sub").asString() != null){
                return true;
            }
        } catch (TokenExpiredException e) {
            LOGGER.error("JWT token is expired: " + e.getMessage());
        } catch (JWTVerificationException e) {
            LOGGER.error("Invalid JWT token: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error("JWT claims string is empty: " + e.getMessage());
        }
        return false;
    }
    private DecodedJWT decodeToken(String token) {
        try {

            Algorithm algorithm = Algorithm.HMAC512(privateKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISS)
                    .build();
            return verifier.verify(token);
        }catch (Exception e){
            throw new DecodeTokenErrorException("JWT-ERR-02", e.getMessage());
        }
    }

}
