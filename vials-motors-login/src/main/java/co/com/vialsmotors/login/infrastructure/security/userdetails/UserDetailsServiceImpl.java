package co.com.vialsmotors.login.infrastructure.security.userdetails;

import co.com.vialsmotors.login.infrastructure.entity.CredentialEntity;
import co.com.vialsmotors.login.infrastructure.jpa.CredentialSqlServerRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private final CredentialSqlServerRepository credentialSqlServerRepository;

    public UserDetailsServiceImpl(CredentialSqlServerRepository credentialSqlServerRepository) {
        this.credentialSqlServerRepository = credentialSqlServerRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CredentialEntity credentialEntity = credentialSqlServerRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        return UserDetailsImpl.build(credentialEntity);
    }
}
