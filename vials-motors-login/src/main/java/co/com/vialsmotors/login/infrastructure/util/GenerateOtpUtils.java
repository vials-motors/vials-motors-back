package co.com.vialsmotors.login.infrastructure.util;

import java.security.SecureRandom;
import java.util.stream.Collectors;

public class GenerateOtpUtils {

    private static final String NOT_INSTANTIABLE = "The class GenerateOtpUtils is not instantiable";
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int CODE_LENGTH = 10;

    private GenerateOtpUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String generate(){
        SecureRandom random = new SecureRandom();
        return random.ints(CODE_LENGTH, 0, CHARACTERS.length())
                .mapToObj(CHARACTERS::charAt)
                .map(Object::toString)
                .collect(Collectors.joining());
    }
}
