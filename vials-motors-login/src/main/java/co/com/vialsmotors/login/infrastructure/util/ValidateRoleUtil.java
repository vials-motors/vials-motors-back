package co.com.vialsmotors.login.infrastructure.util;

import co.com.vialsmotors.login.domain.enums.ERole;
import co.com.vialsmotors.login.infrastructure.entity.RoleEntity;

public class ValidateRoleUtil {

    private static final String NOT_INSTANTIABLE = "The class ValidateRoleUtil is not instantiable";

    private ValidateRoleUtil(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static RoleEntity generateRoleWithUserType(Integer userType){
        return switch (userType) {
            case 1 -> new RoleEntity(1L, ERole.ROLE_ADMIN);
            case 2 -> new RoleEntity(2L, ERole.ROLE_CLIENT);
            default -> new RoleEntity(3L, ERole.ROLE_TECHNICAL);
        };
    }
}
