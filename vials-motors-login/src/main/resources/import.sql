-- import.sql

--Roles
INSERT INTO roles (role_id, name) VALUES (1, 'ROLE_ADMIN');
INSERT INTO roles (role_id, name) VALUES (2, 'ROLE_CLIENT');
INSERT INTO roles (role_id, name) VALUES (3, 'ROLE_TECHNICAL');
