package co.com.vialsmotors.login.domain.validate;

import co.com.vialsmotors.login.domain.exception.FieldException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidateArgumentTest {

    @Test
    void validateStringObjectWithStringNullTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateStringObject(null, "code", "message"));
    }
    @Test
    void validateStringObjectWithStringBlankTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateStringObject("", "code", "message"));
    }
    @Test
    void validateCustomerAndUsernameFieldWithUsernameNullTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateCustomerAndEmailField(null, "customer"));
    }
    @Test
    void validateCustomerAndUsernameFieldWithUsernameEmptyTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateCustomerAndEmailField("", "customer"));
    }
    @Test
    void validateCustomerAndUsernameFieldWithCustomerEmptyTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateCustomerAndEmailField("username", " "));
    }
    @Test
    void validateCustomerAndUsernameFieldWithCustomerNullTest(){
        assertThrows(FieldException.class, () -> ValidateArgument.validateCustomerAndEmailField("username", null));
    }

}
