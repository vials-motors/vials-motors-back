package co.com.vialsmotors.reporting.application.configuration;

import co.com.vialsmotors.reporting.domain.port.GetConsolidateRepository;
import co.com.vialsmotors.reporting.domain.port.GetFlowServicesRepository;
import co.com.vialsmotors.reporting.domain.port.GetListCustomerForSpPort;
import co.com.vialsmotors.reporting.domain.service.GenerateExcelFlowServicesService;
import co.com.vialsmotors.reporting.domain.service.GenerateExcelFluxService;
import co.com.vialsmotors.reporting.domain.service.impl.*;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public GetConsolidateImplService getConsolidateImplService(GetConsolidateRepository getConsolidateRepository){
        return new GetConsolidateImplService(getConsolidateRepository);
    }

    @Bean
    public GetReportFluxClientImplService getReportFluxClientImplService (GetListCustomerForSpPort getListCustomerForSpPort, GenerateExcelFluxService generateExcelFluxService){
        return new GetReportFluxClientImplService(getListCustomerForSpPort, generateExcelFluxService);
    }

    @Bean
    public GenerateExcelFluxImplService generateExcelFluxImplService(){
        return new GenerateExcelFluxImplService();
    }

    @Bean
    public GetReportFlowServicesImplService getReportFlowServicesImplService(GetFlowServicesRepository getFlowServicesRepository,
                                                                             GenerateExcelFlowServicesService generateExcelFlowServicesService){
        return new GetReportFlowServicesImplService(getFlowServicesRepository, generateExcelFlowServicesService);
    }

    @Bean
    public GenerateExcelFlowServicesImplService generateExcelFlowServicesImplService(){
        return new GenerateExcelFlowServicesImplService();
    }
}
