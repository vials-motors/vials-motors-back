package co.com.vialsmotors.reporting.application.handler;

import co.com.vialsmotors.reporting.domain.dto.FlowServiceDTO;
import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface GetReportFlowServicesHandler {

    ResponseEntity<ListGenericResponse<FlowServiceDTO>> getReport(String dateInit, String dateFinish, Integer excel) throws IOException;
}
