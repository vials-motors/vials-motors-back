package co.com.vialsmotors.reporting.application.handler;

import co.com.vialsmotors.reporting.domain.dto.FlowServiceDTO;
import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.domain.service.GetReportFlowServicesService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class GetReportFlowServicesImplHandler implements GetReportFlowServicesHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(GetReportFlowServicesImplHandler.class);
    private final GetReportFlowServicesService getReportFlowServicesService;

    @Override
    public ResponseEntity<ListGenericResponse<FlowServiceDTO>> getReport(String dateInit, String dateFinish, Integer excel) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Get report in handler with date init: {}", dateInit);
        return getReportFlowServicesService.getReport(dateInit, dateFinish, excel);
    }
}
