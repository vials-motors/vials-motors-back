package co.com.vialsmotors.reporting.application.handler;

import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface GetReportFluxClientHandler {

    ResponseEntity<ListGenericResponse<CustomerSpResponse>> getReport(String initDate, String finishDate, Integer excel) throws IOException;
}
