package co.com.vialsmotors.reporting.application.handler;

import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.domain.service.GetReportFluxClientService;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class GetReportFluxClientImplHandler implements GetReportFluxClientHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger(GetReportFluxClientImplHandler.class);
    private final GetReportFluxClientService getReportFluxClientService;

    @Override
    public ResponseEntity<ListGenericResponse<CustomerSpResponse>> getReport(String initDate, String finishDate, Integer excel) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Generate Report of client flux with excel: {}", excel);
        return getReportFluxClientService.getResponse(initDate, finishDate, excel);
    }
}
