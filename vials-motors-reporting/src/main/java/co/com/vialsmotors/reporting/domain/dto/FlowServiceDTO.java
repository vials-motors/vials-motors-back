package co.com.vialsmotors.reporting.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FlowServiceDTO {

    private Long serviceId;
    private String codeService;
    private String description;
    private String descriptionServiceType;
    private Integer totalService;
    private Double totalAmount;
}
