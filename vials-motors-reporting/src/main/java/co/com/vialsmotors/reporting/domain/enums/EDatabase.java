package co.com.vialsmotors.reporting.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EDatabase {

    ERROR_DATABASE("DB-80000");

    private final String code;
}
