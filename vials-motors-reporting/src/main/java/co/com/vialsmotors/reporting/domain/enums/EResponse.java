package co.com.vialsmotors.reporting.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    GET_REPORT_CLIENT_FLUX_SUCCESS("SUCC-00", "Obteniendo data para el flujo de clientes"),
    GET_REPORT_SERVICE_FLOW_SUCCESS("SUCC-01", "Data de flujo de servicio obtenida"),
    ERROR_GENERATE_EXCEL("ERR-8000", "Error generando excel"),
    ERROR_DATE_MESSAGE("ERR-01", "La fecha inicial no puede ser mayor a la fecha final"),
    FIELD_DATE_ERROR("ERR-02", "Las fechas son requeridas"),
    FIELD_EXCEL_ERROR("ERR-03", "El campo excel es requerido"),
    ERROR_CALL_MICROSERVICE("ERR-503", "Error de comunicacion interno"),
    ERROR_GENERATE_DATE("ERR-503", "Error generando la fecha por formatos invalidos"),
    INTERNAL_SERVER("ERR-500", "Error interno de servidor");

    private final String code;
    private final String message;
}
