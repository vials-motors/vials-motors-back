package co.com.vialsmotors.reporting.domain.exception;

import lombok.Getter;

@Getter
public class CallMicroserviceException extends RuntimeException{

    private final String code;

    public CallMicroserviceException(String message, String code) {
        super(message);
        this.code = code;
    }
}
