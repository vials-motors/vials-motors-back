package co.com.vialsmotors.reporting.domain.exception;

import lombok.Getter;

@Getter
public class GenerateDateException extends RuntimeException{

    private final String code;

    public GenerateDateException(String message, String code) {
        super(message);
        this.code = code;
    }
}
