package co.com.vialsmotors.reporting.domain.exception;

import lombok.Getter;

@Getter
public class ReportingException extends RuntimeException{

    private final String code;

    public ReportingException(String message, String code) {
        super(message);
        this.code = code;
    }
}
