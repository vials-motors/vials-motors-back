package co.com.vialsmotors.reporting.domain.exception;

import lombok.Getter;

@Getter
public class SqlDatabaseException extends RuntimeException{

    private final String code;

    public SqlDatabaseException(String message, String code) {
        super(message);
        this.code = code;
    }
}
