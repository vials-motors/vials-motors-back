package co.com.vialsmotors.reporting.domain.port;

import co.com.vialsmotors.reporting.infrastructure.entity.ConsolidateEntity;

import java.time.ZonedDateTime;
import java.util.List;

public interface GetConsolidateRepository {

    List<ConsolidateEntity> getConsolidate(ZonedDateTime dateInit, ZonedDateTime dateFinish);
}
