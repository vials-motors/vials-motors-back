package co.com.vialsmotors.reporting.domain.port;

import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;

import java.time.ZonedDateTime;
import java.util.List;

public interface GetFlowServicesRepository {

    List<FlowServiceEntity> getListServices(ZonedDateTime dateInit, ZonedDateTime dateFinish);
}
