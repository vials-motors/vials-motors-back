package co.com.vialsmotors.reporting.domain.port;

import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;

import java.util.List;

public interface GetListCustomerForSpPort {

    List<CustomerSpResponse> getList(String dateInit, String dateFinish);
}
