package co.com.vialsmotors.reporting.domain.response;

import java.time.ZonedDateTime;
import java.util.List;

public record ListGenericResponse<T> (String code, String message, List<T> data, ZonedDateTime timestamp) {
}
