package co.com.vialsmotors.reporting.domain.service;

import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;

import java.io.IOException;
import java.util.List;

public interface GenerateExcelFlowServicesService {

    void generateExcel(List<FlowServiceEntity> flowServiceEntities) throws IOException;
}
