package co.com.vialsmotors.reporting.domain.service;

import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;

import java.io.IOException;
import java.util.List;

public interface GenerateExcelFluxService {

    void generateExcel(List<CustomerSpResponse> customerSpResponses) throws IOException;
}
