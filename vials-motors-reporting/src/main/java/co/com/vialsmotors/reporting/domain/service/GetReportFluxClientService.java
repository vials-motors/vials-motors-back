package co.com.vialsmotors.reporting.domain.service;

import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface GetReportFluxClientService {

    ResponseEntity<ListGenericResponse<CustomerSpResponse>> getResponse(String initDate, String finishDate, Integer excel) throws IOException;
}
