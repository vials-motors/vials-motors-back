package co.com.vialsmotors.reporting.domain.service.impl;

import co.com.vialsmotors.reporting.domain.service.GenerateExcelFlowServicesService;
import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;
import co.com.vialsmotors.reporting.infrastructure.utils.GenerateExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

public class GenerateExcelFlowServicesImplService implements GenerateExcelFlowServicesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateExcelFlowServicesImplService.class);
    private static final String SHEET_NAME = "services-flow";
    @Value("${path.download}")
    protected String path;

    @Override
    public void generateExcel(List<FlowServiceEntity> flowServiceEntities) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Get report excel for services flow");
        GenerateExcelUtils<FlowServiceEntity> generateExcelUtils = new GenerateExcelUtils<>(flowServiceEntities);
        byte [] exelByte = generateExcelUtils.generateExcel(setHeader(), SHEET_NAME);
        File file = new File(path, generateName());
        FileCopyUtils.copy(exelByte, file);
    }

    private String[] setHeader(){
        return new String[]{"serviceId", "codeService", "descriptionServiceType",
        "totalService", "totalAmount"};
    }

    private String generateName(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
        String formatter = zonedDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return "services-flow-" + formatter + ".xlsx";
    }
}
