package co.com.vialsmotors.reporting.domain.service.impl;

import co.com.vialsmotors.reporting.domain.service.GenerateExcelFluxService;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import co.com.vialsmotors.reporting.infrastructure.utils.GenerateExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

public class GenerateExcelFluxImplService implements GenerateExcelFluxService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateExcelFluxImplService.class);
    private static final String SHEET_NAME = "customer-flux";

    @Value("${path.download}")
    protected String path;

    @Override
    public void generateExcel(List<CustomerSpResponse> customerSpResponses) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Get report excel for customer flux");
        GenerateExcelUtils<CustomerSpResponse> generateExcelUtils = new GenerateExcelUtils<>(customerSpResponses);
        byte [] excelByte = generateExcelUtils.generateExcel(setHeader(), SHEET_NAME);
        File file = new File(path, generateName());
        FileCopyUtils.copy(excelByte, file);
    }

    private String[] setHeader(){
        return new String[]{"firstName", "secondName", "firstLastName",
        "secondLastName", "phone", "identificationNumber", "email",
        "status", "descriptionIdentificationType"};
    }

    private String generateName(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
        String formatter = zonedDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return "customer-flux-" + formatter + ".xlsx";
    }
}
