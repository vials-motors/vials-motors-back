package co.com.vialsmotors.reporting.domain.service.impl;

import co.com.vialsmotors.reporting.domain.port.GetConsolidateRepository;
import co.com.vialsmotors.reporting.infrastructure.entity.ConsolidateEntity;
import co.com.vialsmotors.reporting.infrastructure.utils.GenerateExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

public class GetConsolidateImplService{

    private static final Logger LOGGER = LoggerFactory.getLogger(GetConsolidateImplService.class);
    private static final String SHEET_NAME = "consolidate";
    private static final String UTC_5 = "UTC-5";
    private final GetConsolidateRepository getConsolidateRepository;

    public GetConsolidateImplService(GetConsolidateRepository getConsolidateRepository) {
        this.getConsolidateRepository = getConsolidateRepository;
    }

    @Value("${path.download}")
    protected String path;

    @Scheduled(cron = "0 1 0 * * ?")
    public void getConsolidate() throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Get consolidate in service");
        GenerateExcelUtils<ConsolidateEntity> generateExcelUtils = new GenerateExcelUtils<>(getListConsolidate());
        byte [] excelByte = generateExcelUtils.generateExcel(setHeader(), SHEET_NAME);
        File file = new File(path, generateName());
        FileCopyUtils.copy(excelByte, file);
    }

    private String[] setHeader(){
        return new String[]{"id", "dia", "hora", "descripcion de servicio", "descripcion tipo de servicio", "placa", "costo"};
    }

    private List<ConsolidateEntity> getListConsolidate(){
        return getConsolidateRepository.getConsolidate(getYesterdayMidnight(), getYesterdayEndOfDay());
    }

    private String generateName(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
        String formatter = zonedDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return "consolidate-" + formatter + ".xlsx";
    }

    private ZonedDateTime getYesterdayMidnight() {
        LocalDateTime yesterdayMidnight = LocalDateTime.now().minusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        return yesterdayMidnight.atZone(ZoneId.of(UTC_5));
    }

    private ZonedDateTime getYesterdayEndOfDay() {
        LocalDateTime yesterdayEndOfDay = LocalDateTime.now().minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(999_000_000);
        return yesterdayEndOfDay.atZone(ZoneId.of(UTC_5));
    }
}
