package co.com.vialsmotors.reporting.domain.service.impl;

import co.com.vialsmotors.reporting.domain.dto.FlowServiceDTO;
import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.ReportingException;
import co.com.vialsmotors.reporting.domain.port.GetFlowServicesRepository;
import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.domain.service.GenerateExcelFlowServicesService;
import co.com.vialsmotors.reporting.domain.service.GetReportFlowServicesService;
import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;
import co.com.vialsmotors.reporting.infrastructure.mapper.GenericMapper;
import co.com.vialsmotors.reporting.infrastructure.utils.GenerateDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetReportFlowServicesImplService implements GetReportFlowServicesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetReportFlowServicesImplService.class);

    private static final String FORMAT_HOUR_INIT = "00:00:00.000-05:00";
    private static final String FORMAT_HOUR_FINISH = "23:59:59.999-05:00";
    private final GetFlowServicesRepository getFlowServicesRepository;
    private final GenerateExcelFlowServicesService generateExcelFlowServicesService;

    public GetReportFlowServicesImplService(GetFlowServicesRepository getFlowServicesRepository, GenerateExcelFlowServicesService generateExcelFlowServicesService) {
        this.getFlowServicesRepository = getFlowServicesRepository;
        this.generateExcelFlowServicesService = generateExcelFlowServicesService;
    }

    @Override
    public ResponseEntity<ListGenericResponse<FlowServiceDTO>> getReport(String dateInit, String dateFinish, Integer excel) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Generate report in service");
        this.validateDateInit(dateInit);
        this.validateDateFinish(dateFinish);
        this.validateDateExcel(excel);
        ZonedDateTime dateInitTime = GenerateDateUtils.generateDate(dateInit, FORMAT_HOUR_INIT);
        ZonedDateTime dateFinishTime = GenerateDateUtils.generateDate(dateFinish, FORMAT_HOUR_FINISH);
        validateDate(dateInitTime, dateFinishTime);
        List<FlowServiceEntity> flowServiceEntity = this.getFlowServicesRepository.getListServices(dateInitTime, dateFinishTime);
        this.generateExcel(excel, flowServiceEntity);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_REPORT_SERVICE_FLOW_SUCCESS.getMessage(),
                EResponse.GET_REPORT_SERVICE_FLOW_SUCCESS.getCode(),
                GenericMapper.convertListEntityToDto(flowServiceEntity),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));

    }

    private void validateDateInit(String dateInit){
        if (dateInit == null || dateInit.isEmpty()){
            throw new ReportingException(EResponse.FIELD_DATE_ERROR.getMessage(), EResponse.FIELD_DATE_ERROR.getCode());
        }
    }

    private void validateDateFinish(String dateFinish){
        if (dateFinish == null || dateFinish.isEmpty()){
            throw new ReportingException(EResponse.FIELD_DATE_ERROR.getMessage(), EResponse.ERROR_DATE_MESSAGE.getCode());
        }
    }

    private void validateDateExcel(Integer excel){
        if (excel == null){
            throw new ReportingException(EResponse.FIELD_EXCEL_ERROR.getMessage(), EResponse.FIELD_EXCEL_ERROR.getCode());
        }
    }

    private void validateDate(ZonedDateTime dateInit, ZonedDateTime dateFinish){
        if (dateInit.isAfter(dateFinish)){
            throw new ReportingException(EResponse.ERROR_DATE_MESSAGE.getMessage(), EResponse.ERROR_DATE_MESSAGE.getCode());
        }
    }

    private void generateExcel(Integer excel, List<FlowServiceEntity> list) throws IOException {
        if (excel == 1){
            this.generateExcelFlowServicesService.generateExcel(list);
        }
    }
}
