package co.com.vialsmotors.reporting.domain.service.impl;

import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.ReportingException;
import co.com.vialsmotors.reporting.domain.port.GetListCustomerForSpPort;
import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.domain.service.GenerateExcelFluxService;
import co.com.vialsmotors.reporting.domain.service.GetReportFluxClientService;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import co.com.vialsmotors.reporting.infrastructure.utils.GenerateDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetReportFluxClientImplService implements GetReportFluxClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetReportFluxClientImplService.class);
    private static final String FORMAT_HOUR = "00:00:00.000-05:00";
    private final GetListCustomerForSpPort getListCustomerForSpPort;
    private final GenerateExcelFluxService generateExcelFluxService;

    public GetReportFluxClientImplService(GetListCustomerForSpPort getListCustomerForSpPort, GenerateExcelFluxService generateExcelFluxService) {
        this.getListCustomerForSpPort = getListCustomerForSpPort;
        this.generateExcelFluxService = generateExcelFluxService;
    }

    @Override
    public ResponseEntity<ListGenericResponse<CustomerSpResponse>> getResponse(String initDate, String finishDate, Integer excel) throws IOException {
        this.validateFieldInitDate(initDate);
        this.validateFieldFinishDate(finishDate);
        this.validateDate(initDate, finishDate);
        this.validateExcel(excel);
        List<CustomerSpResponse> customerSpResponseList = getListCustomerForSpPort.getList(initDate, finishDate);
        this.generateExcel(excel, customerSpResponseList);
        return ResponseEntity.ok(new ListGenericResponse<>(EResponse.GET_REPORT_CLIENT_FLUX_SUCCESS.getCode(),
                EResponse.GET_REPORT_CLIENT_FLUX_SUCCESS.getCode(), customerSpResponseList,
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateDate (String initDate, String finishDate){
        ZonedDateTime initDateTemporal = GenerateDateUtils.generateDate(initDate,FORMAT_HOUR);
        ZonedDateTime finishDateTemporal = GenerateDateUtils.generateDate(finishDate, FORMAT_HOUR);
        if (initDateTemporal.isAfter(finishDateTemporal)){
            LOGGER.error("ERROR-MS-REPORTING: Error because the start date is greater than the end date");
            throw new ReportingException(EResponse.ERROR_DATE_MESSAGE.getMessage(), EResponse.ERROR_DATE_MESSAGE.getCode());
        }
    }

    private void validateFieldInitDate(String dateInit){
        if (dateInit == null || dateInit.isEmpty()){
            throw new ReportingException(EResponse.FIELD_DATE_ERROR.getMessage(), EResponse.FIELD_DATE_ERROR.getCode());
        }
    }

    private void validateFieldFinishDate(String finishDate){
        if (finishDate == null || finishDate.isEmpty()){
            throw new ReportingException(EResponse.FIELD_DATE_ERROR.getMessage(), EResponse.FIELD_DATE_ERROR.getCode());
        }
    }

    private void generateExcel(Integer excel, List<CustomerSpResponse> list) throws IOException {
        if (excel == 1){
            this.generateExcelFluxService.generateExcel(list);
        }
    }

    private void validateExcel(Integer excel){
        if (excel == null ){
            throw new ReportingException(EResponse.FIELD_EXCEL_ERROR.getMessage(), EResponse.FIELD_EXCEL_ERROR.getCode());
        }
    }
}
