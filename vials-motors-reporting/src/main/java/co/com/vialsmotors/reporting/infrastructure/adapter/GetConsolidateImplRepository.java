package co.com.vialsmotors.reporting.infrastructure.adapter;

import co.com.vialsmotors.reporting.domain.enums.EDatabase;
import co.com.vialsmotors.reporting.domain.exception.SqlDatabaseException;
import co.com.vialsmotors.reporting.domain.port.GetConsolidateRepository;
import co.com.vialsmotors.reporting.infrastructure.entity.ConsolidateEntity;
import co.com.vialsmotors.reporting.infrastructure.jpa.ConsolidateSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetConsolidateImplRepository implements GetConsolidateRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetConsolidateImplRepository.class);
    private final ConsolidateSqlServerRepository consolidateSqlServerRepository;

    @Override
    @Transactional
    public List<ConsolidateEntity> getConsolidate(ZonedDateTime dateInit, ZonedDateTime dateFinish) {
        try {
            LOGGER.info("INFO-MS-REPORTING: Get consolidate sp");
            return consolidateSqlServerRepository.getConsolidateSp(dateInit, dateFinish);
        }catch (Exception e){
            throw new SqlDatabaseException(e.getMessage(), EDatabase.ERROR_DATABASE.getCode());
        }
    }
}
