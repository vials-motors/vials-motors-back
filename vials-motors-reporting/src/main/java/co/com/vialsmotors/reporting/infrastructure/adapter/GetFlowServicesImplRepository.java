package co.com.vialsmotors.reporting.infrastructure.adapter;

import co.com.vialsmotors.reporting.domain.enums.EDatabase;
import co.com.vialsmotors.reporting.domain.exception.ReportingException;
import co.com.vialsmotors.reporting.domain.port.GetFlowServicesRepository;
import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;
import co.com.vialsmotors.reporting.infrastructure.jpa.FlowServiceSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetFlowServicesImplRepository implements GetFlowServicesRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetFlowServicesImplRepository.class);
    private final FlowServiceSqlServerRepository flowServiceSqlServerRepository;

    @Override
    @Transactional
    public List<FlowServiceEntity> getListServices(ZonedDateTime dateInit, ZonedDateTime dateFinish) {
        try {
            LOGGER.info("INFO-MS-REPORTING: Get flow service");
            return flowServiceSqlServerRepository.getFlowService(dateInit, dateFinish);
        }catch (Exception e){
            throw new ReportingException(e.getMessage(), EDatabase.ERROR_DATABASE.getCode());
        }
    }
}
