package co.com.vialsmotors.reporting.infrastructure.adapter;

import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.CallMicroserviceException;
import co.com.vialsmotors.reporting.domain.port.GetListCustomerForSpPort;
import co.com.vialsmotors.reporting.infrastructure.apirest.client.CustomerClient;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.FeignResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class GetListCustomerForSpImplPort implements GetListCustomerForSpPort {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetListCustomerForSpImplPort.class);
    private final CustomerClient customerClient;


    @Override
    public List<CustomerSpResponse> getList(String dateInit, String dateFinish) {
        return this.extracList(dateInit, dateFinish);
    }

    private ResponseEntity<FeignResponse> callCustomer(String dateInit, String dateFinish){
        try {
            LOGGER.info("INFO-MS-REPORTING: Get list customer for sp");
            return customerClient.getCustomerSp(dateInit, dateFinish);
        }catch (Exception e){
            throw new CallMicroserviceException(e.getMessage(), EResponse.ERROR_CALL_MICROSERVICE.getCode());
        }
    }

    private List<CustomerSpResponse> extracList(String dateInit, String dateFinish){
        return Objects.requireNonNull(callCustomer(dateInit, dateFinish).getBody()).getData();
    }
}
