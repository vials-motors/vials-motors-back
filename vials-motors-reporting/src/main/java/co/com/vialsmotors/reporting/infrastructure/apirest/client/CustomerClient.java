package co.com.vialsmotors.reporting.infrastructure.apirest.client;

import co.com.vialsmotors.reporting.infrastructure.apirest.response.FeignResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "feignClientCustomer", url = "${external.service.customer}")
public interface CustomerClient {

    @GetMapping("/customer/list-sp")
    ResponseEntity<FeignResponse> getCustomerSp(@RequestParam("dateInit") String dateInit,
                                                @RequestParam("dateFinish") String dateFinish);
}
