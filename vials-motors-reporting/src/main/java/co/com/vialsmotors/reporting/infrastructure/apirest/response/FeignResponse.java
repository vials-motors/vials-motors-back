package co.com.vialsmotors.reporting.infrastructure.apirest.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeignResponse {

    private String status;
    private String message;
    private List<CustomerSpResponse> data;
    ZonedDateTime timestamp;

}
