package co.com.vialsmotors.reporting.infrastructure.controller;

import co.com.vialsmotors.reporting.application.handler.GetReportFlowServicesHandler;
import co.com.vialsmotors.reporting.application.handler.GetReportFluxClientHandler;
import co.com.vialsmotors.reporting.domain.dto.FlowServiceDTO;
import co.com.vialsmotors.reporting.domain.response.ListGenericResponse;
import co.com.vialsmotors.reporting.infrastructure.apirest.response.CustomerSpResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/report")
@CrossOrigin(origins = "http://localhost:4200")
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

    private final GetReportFluxClientHandler getReportFluxClientHandler;
    private final GetReportFlowServicesHandler getReportFlowServicesHandler;

    @GetMapping(path = "/customer-flux")
    public ResponseEntity<ListGenericResponse<CustomerSpResponse>> getReportCustomer(@RequestParam("dateInit") String dateInit,
                                                                                     @RequestParam("dateFinish") String dateFinish,
                                                                                     @RequestParam("excel") Integer excel) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report with date init: {}", dateInit);
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report with date finish: {}", dateFinish);
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report with excel: {}", excel);
        return getReportFluxClientHandler.getReport(dateInit, dateFinish, excel);
    }

    @GetMapping(path = "/services-flow")
    public ResponseEntity<ListGenericResponse<FlowServiceDTO>> getReportServices(@RequestParam("dateInit") String dateInit,
                                                                                 @RequestParam("dateFinish") String dateFinish,
                                                                                 @RequestParam("excel") Integer excel) throws IOException {
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report of flow services with date init: {}", dateInit);
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report of flow services with date finish: {}", dateFinish);
        LOGGER.info("INFO-MS-REPORTING: Receiver request for generate report of flow services with excel: {}", excel);
        return getReportFlowServicesHandler.getReport(dateInit, dateFinish, excel);
    }

}
