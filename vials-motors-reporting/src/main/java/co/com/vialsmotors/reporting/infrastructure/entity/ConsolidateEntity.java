package co.com.vialsmotors.reporting.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsolidateEntity {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "day")
    private String day;

    @Column(name = "hour")
    private String hour;

    @Column(name = "description_service")
    private String descriptionService;

    @Column(name = "description_service_type")
    private String descriptionServiceType;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "amount")
    private Double amount;
}
