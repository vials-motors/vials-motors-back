package co.com.vialsmotors.reporting.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlowServiceEntity {

    @Id
    @Column(name = "id_service")
    private Long serviceId;

    @Column(name = "code_service")
    private String codeService;

    @Column(name = "description")
    private String description;

    @Column(name = "description_service_type")
    private String descriptionServiceType;

    @Column(name = "total_service")
    private Integer totalService;

    @Column(name = "total_amount")
    private Double totalAmount;
}
