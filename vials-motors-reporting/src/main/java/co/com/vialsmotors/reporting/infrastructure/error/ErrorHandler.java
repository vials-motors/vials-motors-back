package co.com.vialsmotors.reporting.infrastructure.error;

import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.*;
import co.com.vialsmotors.reporting.domain.response.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: {}";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler(){
        CODE_STATE.put(GenerateExcelException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(SqlDatabaseException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CallMicroserviceException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(GenerateDateException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(ReportingException.class.getSimpleName(), HttpStatus.OK.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleAllException(Exception exception){
        String nameException = exception.getClass().getCanonicalName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof GenerateExcelException generateExcelException){
            LOGGER.info(MESSAGE_ERROR_GENERIC, generateExcelException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(generateExcelException.getCode(), generateExcelException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof SqlDatabaseException sqlDatabaseException){
            LOGGER.info(MESSAGE_ERROR_GENERIC, sqlDatabaseException.getMessage());
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sqlDatabaseException.getCode(), sqlDatabaseException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof CallMicroserviceException callMicroserviceException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(callMicroserviceException.getCode(),callMicroserviceException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GenerateDateException generateDateException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(generateDateException.getCode(), generateDateException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ReportingException reportingException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(reportingException.getCode(), reportingException.getMessage(), TIMESTAMP_RESPONSE));
        } else {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_SERVER.getCode(),
                            EResponse.INTERNAL_SERVER.getMessage(), TIMESTAMP_RESPONSE));
        }
    }
}
