package co.com.vialsmotors.reporting.infrastructure.jpa;

import co.com.vialsmotors.reporting.infrastructure.entity.ConsolidateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;

public interface ConsolidateSqlServerRepository extends JpaRepository<ConsolidateEntity, Long> {

    @Procedure("consolidate")
    List<ConsolidateEntity> getConsolidateSp(@Param("date_init") ZonedDateTime dateInit,
                                             @Param("date_finish") ZonedDateTime dateFinish);
}
