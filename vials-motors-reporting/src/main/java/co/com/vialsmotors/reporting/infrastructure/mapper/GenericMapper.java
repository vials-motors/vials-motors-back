package co.com.vialsmotors.reporting.infrastructure.mapper;

import co.com.vialsmotors.reporting.domain.dto.FlowServiceDTO;
import co.com.vialsmotors.reporting.infrastructure.entity.FlowServiceEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GenericMapper {

    private static final String NOT_INSTANTIABLE = "The class generic mapper is not instantiable";

    private GenericMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static List<FlowServiceDTO> convertListEntityToDto(List<FlowServiceEntity> list){
        return list.stream()
                .map(temporal -> new FlowServiceDTO(temporal.getServiceId(),
                        temporal.getCodeService(), temporal.getDescription(),
                        temporal.getDescriptionServiceType(), temporal.getTotalService(),
                        temporal.getTotalAmount()))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
