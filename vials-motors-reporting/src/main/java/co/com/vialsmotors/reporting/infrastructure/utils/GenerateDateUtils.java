package co.com.vialsmotors.reporting.infrastructure.utils;

import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.GenerateDateException;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class GenerateDateUtils {

    private static final String NOT_INSTANTIABLE = "The class Generate Date Utils is not instantiable";

    private GenerateDateUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static ZonedDateTime generateDate(String date, String formatHour){
        try {
            String newDate = date + "T" + formatHour;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
            return ZonedDateTime.parse(newDate, formatter).withZoneSameInstant(ZoneId.of("UTC-5"));
        }catch (Exception e){
            throw new GenerateDateException(EResponse.ERROR_GENERATE_DATE.getMessage(), EResponse.ERROR_GENERATE_DATE.getCode());
        }
    }
}
