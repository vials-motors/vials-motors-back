package co.com.vialsmotors.reporting.infrastructure.utils;

import co.com.vialsmotors.reporting.domain.enums.EResponse;
import co.com.vialsmotors.reporting.domain.exception.GenerateExcelException;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.util.List;

@RequiredArgsConstructor
public class GenerateExcelUtils<T> {

    private final List<T> dataList;

    public byte[] generateExcel(String[] headers, String sheetName){
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet(sheetName);
            createHeaderRow(headers, sheet, workbook);
            populateDataRows(headers, sheet);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        }catch (Exception e){
            throw new GenerateExcelException(e.getMessage(), EResponse.ERROR_GENERATE_EXCEL.getCode());
        }
    }

    private void populateDataRows(String[] headers, Sheet sheet) {
        int rowNum = 1;
        for (T data : dataList) {
            Row row = sheet.createRow(rowNum++);
            int cellNum = 0;
            for (String fieldName : headers) {
                cellNum = setCellValue(data, fieldName, row, cellNum);
            }
        }
    }

    private static void createHeaderRow(String[] headers, Sheet sheet, Workbook workbook) {
        Row headerRow = sheet.createRow(0);
        CellStyle headerStyle = createHeaderStyle(workbook);
        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerStyle);
        }
    }

    private int setCellValue(T data, String fieldName, Row row, int cellNum) {
        try {
            Object value = data.getClass().getMethod("get" + capitalize(fieldName)).invoke(data);
            if (value != null) {
                if (value instanceof String string) {
                    row.createCell(cellNum++).setCellValue(string);
                } else if (value instanceof Long l) {
                    row.createCell(cellNum++).setCellValue(l);
                } else if (value instanceof Double d) {
                    row.createCell(cellNum++).setCellValue(d);
                } else if (value instanceof Integer i) {
                    row.createCell(cellNum++).setCellValue(i);
                }
            } else {
                row.createCell(cellNum++).setCellValue("");
            }
        } catch (Exception e) {
            throw new GenerateExcelException(e.getMessage(), EResponse.ERROR_GENERATE_EXCEL.getCode());
        }
        return cellNum;
    }

    private String capitalize(String capitalize){
        return Character.toUpperCase(capitalize.charAt(0)) + capitalize.substring(1);
    }

    private static CellStyle createHeaderStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();

        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        style.setFont(font);
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;
    }
}
