package co.com.vialsmotors.technical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class VialsMotorsTechnicalApplication {

	public static void main(String[] args) {
		SpringApplication.run(VialsMotorsTechnicalApplication.class, args);
	}

}
