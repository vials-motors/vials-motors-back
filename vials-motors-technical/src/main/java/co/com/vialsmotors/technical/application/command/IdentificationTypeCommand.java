package co.com.vialsmotors.technical.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IdentificationTypeCommand {

    private Long idIdentificationType;
    private String description;
}
