package co.com.vialsmotors.technical.application.configuration;

import co.com.vialsmotors.technical.domain.port.GetListTechnicalRepository;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByCustomerIdRepository;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByIdentificationNumberOrEmailRepository;
import co.com.vialsmotors.technical.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.technical.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.technical.domain.port.RegisterTechnicalRepository;
import co.com.vialsmotors.technical.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.technical.domain.service.delete.DeleteTechnicalImplService;
import co.com.vialsmotors.technical.domain.service.get.GetTechnicalByCustomerIdImplService;
import co.com.vialsmotors.technical.domain.service.list.GetListTechnicalImplService;
import co.com.vialsmotors.technical.domain.service.register.RegisterTechnicalImplService;
import co.com.vialsmotors.technical.domain.service.update.UpdateTechnicalImplService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public RegisterTechnicalImplService registerTechnicalImplService (RegisterTechnicalRepository registerTechnicalRepository,
                                                                      RegisterCredentialPort registerCredentialPort,
                                                                      GetTechnicalByIdentificationNumberOrEmailRepository getTechnicalByIdentificationNumberOrEmailRepository){
        return new RegisterTechnicalImplService(registerTechnicalRepository, registerCredentialPort, getTechnicalByIdentificationNumberOrEmailRepository);
    }

    @Bean
    public GetListTechnicalImplService getListTechnicalImplService(GetListTechnicalRepository getListTechnicalRepository){
        return new GetListTechnicalImplService(getListTechnicalRepository);
    }

    @Bean
    public UpdateTechnicalImplService updateTechnicalImplService(RegisterTechnicalRepository registerTechnicalRepository, UpdateCredentialPort updateCredentialPort){
        return new UpdateTechnicalImplService(registerTechnicalRepository, updateCredentialPort);
    }

    @Bean
    public DeleteTechnicalImplService deleteTechnicalImplService(DeleteCredentialPort deleteCredentialPort, RegisterTechnicalRepository registerTechnicalRepository){
        return new DeleteTechnicalImplService(deleteCredentialPort, registerTechnicalRepository);
    }

    @Bean
    public GetTechnicalByCustomerIdImplService getTechnicalByCustomerIdImplService(GetTechnicalByCustomerIdRepository getTechnicalByCustomerIdRepository){
        return new GetTechnicalByCustomerIdImplService(getTechnicalByCustomerIdRepository);
    }
}
