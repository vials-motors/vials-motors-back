package co.com.vialsmotors.technical.application.factory;

import co.com.vialsmotors.technical.application.command.IdentificationTypeCommand;
import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.domain.model.IdentificationType;
import co.com.vialsmotors.technical.domain.model.Technical;
import co.com.vialsmotors.technical.domain.request.UpdateTechnicalRequest;
import org.springframework.stereotype.Component;

@Component
public class TechnicalFactory {

    public Technical execute(TechnicalCommand technicalCommand){
        return new Technical(technicalCommand.getFirstName(), technicalCommand.getSecondName(),
                technicalCommand.getFirstLastName(), technicalCommand.getSecondLastName(),
                technicalCommand.getPhone(), technicalCommand.getIdentificationNumber(),
                buildIdentificationType(technicalCommand.getIdentificationType()), technicalCommand.getPassword(),
                technicalCommand.getEmail());
    }

    public UpdateTechnicalRequest updateTechnicalRequest(TechnicalCommand technicalCommand){
        return new UpdateTechnicalRequest(technicalCommand.getFirstName(), technicalCommand.getSecondName(), technicalCommand.getFirstLastName(),
                technicalCommand.getSecondLastName(), technicalCommand.getPhone(), technicalCommand.getIdentificationNumber(), technicalCommand.getEmail());
    }

    private IdentificationType buildIdentificationType(IdentificationTypeCommand identificationTypeCommand){
        return identificationTypeCommand == null ? null:new IdentificationType(identificationTypeCommand.getIdIdentificationType(),
                identificationTypeCommand.getDescription());
    }
}
