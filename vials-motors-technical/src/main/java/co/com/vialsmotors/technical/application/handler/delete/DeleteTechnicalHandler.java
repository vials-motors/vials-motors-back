package co.com.vialsmotors.technical.application.handler.delete;

import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteTechnicalHandler {

    ResponseEntity<GenericResponse> deleteTechnical(String identificationNumber, String email);
}
