package co.com.vialsmotors.technical.application.handler.delete;

import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.domain.service.delete.DeleteTechnicalService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteTechnicalImplHandler implements DeleteTechnicalHandler{

    private static final Logger LOGGER = Logger.getLogger(DeleteTechnicalImplHandler.class);
    private final DeleteTechnicalService deleteTechnicalService;

    @Override
    public ResponseEntity<GenericResponse> deleteTechnical(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-TECHNICAL: Delete technical in handler with identification number " + identificationNumber);
        return deleteTechnicalService.deleteTechnical(identificationNumber, email);
    }
}
