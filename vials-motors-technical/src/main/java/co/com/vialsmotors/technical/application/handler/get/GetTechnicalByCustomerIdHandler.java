package co.com.vialsmotors.technical.application.handler.get;

import co.com.vialsmotors.technical.domain.response.TechnicalResponse;
import org.springframework.http.ResponseEntity;

public interface GetTechnicalByCustomerIdHandler {

    ResponseEntity<TechnicalResponse> getTechnical(String customerId);
}
