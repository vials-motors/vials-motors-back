package co.com.vialsmotors.technical.application.handler.get;

import co.com.vialsmotors.technical.domain.response.TechnicalResponse;
import co.com.vialsmotors.technical.domain.service.get.GetTechnicalByCustomerIdService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetTechnicalByCustomerImplHandler implements GetTechnicalByCustomerIdHandler{

    private static final Logger LOGGER = Logger.getLogger(GetTechnicalByCustomerImplHandler.class);
    private final GetTechnicalByCustomerIdService getTechnicalByCustomerIdService;

    @Override
    public ResponseEntity<TechnicalResponse> getTechnical(String customerId) {
        LOGGER.info("INFO-MS-TECHNICAL: Get technical with customer id: " + customerId);
        return getTechnicalByCustomerIdService.getTechnical(customerId);
    }
}
