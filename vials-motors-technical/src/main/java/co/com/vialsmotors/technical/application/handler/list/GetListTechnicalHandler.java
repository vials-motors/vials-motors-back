package co.com.vialsmotors.technical.application.handler.list;

import co.com.vialsmotors.technical.domain.response.GenericListResponse;
import org.springframework.http.ResponseEntity;


public interface GetListTechnicalHandler {

    ResponseEntity<GenericListResponse> getListTechnical();
}
