package co.com.vialsmotors.technical.application.handler.list;

import co.com.vialsmotors.technical.domain.response.GenericListResponse;
import co.com.vialsmotors.technical.domain.service.list.GetListTechnicalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class GetListTechnicalImplHandler implements GetListTechnicalHandler{

    private final GetListTechnicalService getListTechnicalService;

    @Override
    public ResponseEntity<GenericListResponse> getListTechnical() {
        return getListTechnicalService.getListTechnical();
    }
}
