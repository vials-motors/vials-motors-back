package co.com.vialsmotors.technical.application.handler.register;

import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterTechnicalHandler {

    ResponseEntity<GenericResponse> registerTechnical(TechnicalCommand technicalCommand);
}
