package co.com.vialsmotors.technical.application.handler.register;

import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.application.factory.TechnicalFactory;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.domain.service.register.RegisterTechnicalService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RegisterTechnicalImplHandler implements RegisterTechnicalHandler{

    private static final Logger LOGGER = Logger.getLogger(RegisterTechnicalImplHandler.class);

    private final RegisterTechnicalService registerTechnicalService;
    private final TechnicalFactory technicalFactory;

    @Override
    public ResponseEntity<GenericResponse> registerTechnical(TechnicalCommand technicalCommand) {
        LOGGER.info("MS-TECHNICAL: Register Technical in class RegisterTechnicalImplHandler with method registerTechnical");
        return registerTechnicalService.registerTechnical(technicalFactory.execute(technicalCommand));
    }
}
