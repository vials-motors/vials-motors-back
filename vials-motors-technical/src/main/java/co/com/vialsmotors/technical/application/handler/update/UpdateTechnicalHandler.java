package co.com.vialsmotors.technical.application.handler.update;

import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateTechnicalHandler {

    ResponseEntity<GenericResponse> updateTechnical(TechnicalCommand newTechnicalCommand, String identificationNumber, String email);
}
