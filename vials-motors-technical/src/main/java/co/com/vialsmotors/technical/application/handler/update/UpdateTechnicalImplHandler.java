package co.com.vialsmotors.technical.application.handler.update;

import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.application.factory.TechnicalFactory;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.domain.service.update.UpdateTechnicalService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateTechnicalImplHandler implements UpdateTechnicalHandler{

    private static final Logger LOGGER = Logger.getLogger(UpdateTechnicalImplHandler.class);

    private final UpdateTechnicalService updateTechnicalService;
    private final TechnicalFactory technicalFactory;

    @Override
    public ResponseEntity<GenericResponse> updateTechnical(TechnicalCommand newTechnicalCommand, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-TECHNICAL: Update technical in Update technical impl handler with identification number: " + identificationNumber);
        return updateTechnicalService.updateTechnical(technicalFactory.updateTechnicalRequest(newTechnicalCommand), identificationNumber, email);
    }
}
