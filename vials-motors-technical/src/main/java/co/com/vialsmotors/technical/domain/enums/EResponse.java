package co.com.vialsmotors.technical.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EResponse {

    REGISTER_TECHNICAL_SUCCESSFULLY("SUCC-00", "Tecnico registrado correctamente"),
    LIST_TECHNICAL_SUCCESSFULLY("SUCC-01", "Lista obtenida correctamente"),
    UPDATE_TECHNICAL_SUCCESSFULLY("SUCC-02", "Tecnico actualizado correctamente"),
    DELETE_TECHNICAL_SUCCESSFULLY("SUCC-03", "Tecnico eliminado correctamente"),
    GET_TECHNICAL_SUCCESSFULLY("SUCC-04", "Tecnico obtenido correctamente"),
    MESSAGE_ERROR_REGISTER("ERR-00", "Un error ocurrio en el registro"),
    MESSAGE_ERROR_IDENTIFICATION_NUMBER("ERR-01", "El tecnico ya existe"),
    LIST_EMPTY_MESSAGE("ERR-02", "La lista de tecnicos es vacio"),
    TECHNICAL_NOT_EXIST("ERR-03", "El tecnico no existe"),
    FIELD_IDENTIFICATION_NUMBER("ERR-04", "El campo numero de identificacion es requerido"),
    FIELD_EMAIL_ERROR("ERR-05", "El correo es requerido"),
    FIELD_PASSWORD_ERROR("ERR-06", "El campo contraseña es requerido"),
    FIELD_CUSTOMER_ID_ERROR("ERR-07", "El campo customer id es requerido"),
    IDENTIFICATION_NUMBER_ERROR("ERR-08", "El numero de identificacion ya existe"),
    EMAIL_ERROR("ERR-09", "El correo ya existe"),
    INTERNAL_ERROR("ERR-500", "Error interno de servidor ");

    private final String code;
    private final String message;
}
