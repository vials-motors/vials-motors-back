package co.com.vialsmotors.technical.domain.exception;

import lombok.Getter;

@Getter
public class FindTechnicalException extends RuntimeException{

    private final String status;

    public FindTechnicalException(String message, String status) {
        super(message);
        this.status = status;
    }
}
