package co.com.vialsmotors.technical.domain.exception;

import lombok.Getter;

@Getter
public class ListTechnicalException extends RuntimeException{

    private final String status;

    public ListTechnicalException(String message, String status) {
        super(message);
        this.status = status;
    }
}
