package co.com.vialsmotors.technical.domain.model;

import co.com.vialsmotors.commons.domain.enums.EFieldMessage;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.validate.ValidateArgument;

public record Technical(String firstName, String secondName, String firstLastName, String secondLastName, String phone,
                        String identificationNumber, IdentificationType identificationType, String password, String email) {

    public Technical{
        ValidateArgument.validateFieldString(firstName, EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getCode(), EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getMessage());
        ValidateArgument.validateFieldString(firstLastName, EFieldMessage.FIELD_FIRST_LAST_NAME_MESSAGE.getCode(), EFieldMessage.FIELD_FIRST_NAME_MESSAGE.getMessage());
        ValidateArgument.validateFieldString(phone, EFieldMessage.FIELD_PHONE_MESSAGE.getCode(), EFieldMessage.FIELD_PHONE_MESSAGE.getMessage());
        ValidateArgument.validateFieldString(identificationNumber, EFieldMessage.FIELD_IDENTIFICATION_NUMBER.getCode(), EFieldMessage.FIELD_IDENTIFICATION_NUMBER.getMessage());
        ValidateArgument.validateFieldString(email, EResponse.FIELD_EMAIL_ERROR.getCode(), EResponse.FIELD_EMAIL_ERROR.getMessage());
        ValidateArgument.validateFieldString(password, EResponse.FIELD_PASSWORD_ERROR.getCode(), EResponse.FIELD_PASSWORD_ERROR.getMessage());
        ValidateArgument.validateIdentificationType(identificationType, EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getCode(), EFieldMessage.IDENTIFICATION_TYPE_MESSAGE.getMessage());
    }
}
