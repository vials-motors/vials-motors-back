package co.com.vialsmotors.technical.domain.port;

import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;

import java.util.List;

public interface GetListTechnicalRepository {

    List<TechnicalEntity> getTechnicalList();
}
