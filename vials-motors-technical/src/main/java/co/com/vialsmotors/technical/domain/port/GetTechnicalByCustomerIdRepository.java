package co.com.vialsmotors.technical.domain.port;

import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;

import java.util.Optional;

public interface GetTechnicalByCustomerIdRepository {

    Optional<TechnicalEntity> getTechnical(String customerId);
}
