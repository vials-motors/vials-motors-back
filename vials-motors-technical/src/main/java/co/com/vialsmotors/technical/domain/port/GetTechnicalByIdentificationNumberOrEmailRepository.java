package co.com.vialsmotors.technical.domain.port;

import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;

import java.util.Optional;

public interface GetTechnicalByIdentificationNumberOrEmailRepository {

    Optional<TechnicalEntity> getTechnicalByIdentificationNumber(String identificationNumber);
    Optional<TechnicalEntity> getTechnicalByEmail(String email);
}
