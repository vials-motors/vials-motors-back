package co.com.vialsmotors.technical.domain.port;

import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;

import java.util.Optional;

public interface RegisterTechnicalRepository {

    void registerTechnical(TechnicalEntity technicalEntity);
    Optional<TechnicalEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email);
}
