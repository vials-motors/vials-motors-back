package co.com.vialsmotors.technical.domain.port.credential;

public interface DeleteCredentialPort {

    Boolean deleteCredential(String customerId);
}
