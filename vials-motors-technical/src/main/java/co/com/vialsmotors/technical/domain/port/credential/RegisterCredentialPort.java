package co.com.vialsmotors.technical.domain.port.credential;

import co.com.vialsmotors.technical.infrastructure.apirest.dto.CredentialDTO;

public interface RegisterCredentialPort {

    Boolean registerCredential(CredentialDTO credentialDTO, Integer userType);
}
