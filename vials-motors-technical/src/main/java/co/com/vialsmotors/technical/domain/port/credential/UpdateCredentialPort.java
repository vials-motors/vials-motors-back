package co.com.vialsmotors.technical.domain.port.credential;

public interface UpdateCredentialPort {

    Boolean updateCredential(String username, String customerID);
}
