package co.com.vialsmotors.technical.domain.response;

import co.com.vialsmotors.technical.domain.dto.TechnicalDTO;

import java.time.ZonedDateTime;
import java.util.List;

public record GenericListResponse (String status, String message, List<TechnicalDTO> data, ZonedDateTime timestamp){
}
