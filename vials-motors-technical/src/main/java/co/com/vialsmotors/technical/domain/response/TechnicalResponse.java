package co.com.vialsmotors.technical.domain.response;

import co.com.vialsmotors.technical.domain.dto.TechnicalDTO;

import java.time.ZonedDateTime;

public record TechnicalResponse(String code, String message, TechnicalDTO data, ZonedDateTime timestamp) {
}
