package co.com.vialsmotors.technical.domain.service.delete;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.FindTechnicalException;
import co.com.vialsmotors.technical.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.technical.domain.port.RegisterTechnicalRepository;
import co.com.vialsmotors.technical.domain.port.credential.DeleteCredentialPort;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class DeleteTechnicalImplService implements DeleteTechnicalService{

    private static final Logger LOGGER = Logger.getLogger(DeleteTechnicalImplService.class);
    private static final Boolean STATUS_TRUE_FOR_TECHNICAL = true;
    private static final Boolean STATUS_FALSE_FOR_TECHNICAL = false;
    private final DeleteCredentialPort deleteCredentialPort;
    private final RegisterTechnicalRepository registerTechnicalRepository;

    public DeleteTechnicalImplService(DeleteCredentialPort deleteCredentialPort, RegisterTechnicalRepository registerTechnicalRepository) {
        this.deleteCredentialPort = deleteCredentialPort;
        this.registerTechnicalRepository = registerTechnicalRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> deleteTechnical(String identificationNumber, String email) {
        LOGGER.info("INFO-MS-TECHNICAL: Delete credential with identification number: " + identificationNumber);
        this.registerTechnicalRepository.registerTechnical(callDeleteCredential(identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.DELETE_TECHNICAL_SUCCESSFULLY.getCode(), EResponse.DELETE_TECHNICAL_SUCCESSFULLY.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isEmpty())
            throw new IdentificationNumberException(EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
    }

    private void validateEmail(String email){
        if (email == null || email.isEmpty()){
            throw new FieldException(EResponse.FIELD_EMAIL_ERROR.getMessage(), EResponse.FIELD_EMAIL_ERROR.getCode());
        }
    }

    private TechnicalEntity getManagerEntity(String identificationNumber, String email){
        this.validateIdentificationNumber(identificationNumber);
        this.validateEmail(email);
        Optional<TechnicalEntity> managerEntity = this.registerTechnicalRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS_TRUE_FOR_TECHNICAL, email);
        if (managerEntity.isPresent())
            return managerEntity.get();
        throw new FindTechnicalException(EResponse.TECHNICAL_NOT_EXIST.getMessage(), EResponse.TECHNICAL_NOT_EXIST.getCode());
    }

    private TechnicalEntity callDeleteCredential(String identificationNumber, String email){
        TechnicalEntity technicalEntity = getManagerEntity(identificationNumber, email);
        Boolean callCredential = this.deleteCredentialPort.deleteCredential(technicalEntity.getCustomerID());
        if (Boolean.FALSE.equals(callCredential)){
            LOGGER.error("ERROR-MS-ADMIN: Error call delete credential with callCredential: " + false);
            throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL_MESSAGE.getCode());
        }
        return changeStatus(technicalEntity);
    }

    private TechnicalEntity changeStatus(TechnicalEntity technicalEntity){
        return technicalEntity.withStatus(STATUS_FALSE_FOR_TECHNICAL);
    }
}
