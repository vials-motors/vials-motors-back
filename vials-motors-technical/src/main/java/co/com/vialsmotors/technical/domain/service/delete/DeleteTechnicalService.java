package co.com.vialsmotors.technical.domain.service.delete;

import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface DeleteTechnicalService {

    ResponseEntity<GenericResponse> deleteTechnical(String identificationNumber, String email);
}
