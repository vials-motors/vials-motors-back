package co.com.vialsmotors.technical.domain.service.get;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.FindTechnicalException;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByCustomerIdRepository;
import co.com.vialsmotors.technical.domain.response.TechnicalResponse;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.mapper.TechnicalMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class GetTechnicalByCustomerIdImplService implements GetTechnicalByCustomerIdService{

    private static final Logger LOGGER = Logger.getLogger(GetTechnicalByCustomerIdImplService.class);
    private final GetTechnicalByCustomerIdRepository getTechnicalByCustomerIdRepository;

    public GetTechnicalByCustomerIdImplService(GetTechnicalByCustomerIdRepository getTechnicalByCustomerIdRepository) {
        this.getTechnicalByCustomerIdRepository = getTechnicalByCustomerIdRepository;
    }

    @Override
    public ResponseEntity<TechnicalResponse> getTechnical(String customerId) {
        this.validateCustomerId(customerId);
        return ResponseEntity.ok(new TechnicalResponse(EResponse.GET_TECHNICAL_SUCCESSFULLY.getCode(),
                EResponse.GET_TECHNICAL_SUCCESSFULLY.getMessage(),
                TechnicalMapper.convertEntityToModel(this.getTechnicalOfOptional(customerId)),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private void validateCustomerId(String customerId){
        if (customerId == null || customerId.isEmpty()){
            LOGGER.error("ERROR-MS-TECHNICAL: The customer id is null or empty");
            throw new FieldException(EResponse.FIELD_CUSTOMER_ID_ERROR.getMessage(), EResponse.FIELD_CUSTOMER_ID_ERROR.getCode());
        }
    }

    private TechnicalEntity getTechnicalOfOptional(String customerId){
        Optional<TechnicalEntity> technicalEntity = this.getTechnicalByCustomerIdRepository.getTechnical(customerId);
        if (technicalEntity.isEmpty()){
            LOGGER.error("ERROR-MS-TECHNICAL: Error because technical not exist");
            throw new FindTechnicalException(EResponse.TECHNICAL_NOT_EXIST.getMessage(), EResponse.TECHNICAL_NOT_EXIST.getCode());
        }
        return technicalEntity.get();
    }
}
