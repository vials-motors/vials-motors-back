package co.com.vialsmotors.technical.domain.service.get;

import co.com.vialsmotors.technical.domain.response.TechnicalResponse;
import org.springframework.http.ResponseEntity;

public interface GetTechnicalByCustomerIdService {

    ResponseEntity<TechnicalResponse> getTechnical(String customerId);
}
