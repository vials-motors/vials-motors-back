package co.com.vialsmotors.technical.domain.service.list;

import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.ListTechnicalException;
import co.com.vialsmotors.technical.domain.port.GetListTechnicalRepository;
import co.com.vialsmotors.technical.domain.response.GenericListResponse;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.mapper.TechnicalMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public class GetListTechnicalImplService implements GetListTechnicalService {

    private static final Logger LOGGER = Logger.getLogger(GetListTechnicalImplService.class);

    private final GetListTechnicalRepository getListTechnicalRepository;

    public GetListTechnicalImplService(GetListTechnicalRepository getListTechnicalRepository) {
        this.getListTechnicalRepository = getListTechnicalRepository;
    }

    @Override
    public ResponseEntity<GenericListResponse> getListTechnical() {
        LOGGER.info("Get List in method getListTechnical for class " + GetListTechnicalImplService.class.getName());
        List<TechnicalEntity> listTechnicalEntity = getListTechnicalRepository.getTechnicalList();
        validateListEntity(listTechnicalEntity);
        return ResponseEntity.ok(new GenericListResponse(EResponse.LIST_TECHNICAL_SUCCESSFULLY.getCode(),
                EResponse.LIST_TECHNICAL_SUCCESSFULLY.getMessage(),
                TechnicalMapper.convertListEntityToListDto(listTechnicalEntity),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));

    }

    private void validateListEntity(List<TechnicalEntity> entityList) {
        if (entityList.isEmpty()) {
            LOGGER.info("INFO-MS-TECHNICAL: The list entity is empty in method validateListEntity for class " + GetListTechnicalImplService.class.getName());
            throw new ListTechnicalException(EResponse.LIST_EMPTY_MESSAGE.getMessage(), EResponse.LIST_EMPTY_MESSAGE.getCode());
        }
    }
}
