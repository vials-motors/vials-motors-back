package co.com.vialsmotors.technical.domain.service.list;

import co.com.vialsmotors.technical.domain.response.GenericListResponse;
import org.springframework.http.ResponseEntity;


public interface GetListTechnicalService {

    ResponseEntity<GenericListResponse> getListTechnical();
}
