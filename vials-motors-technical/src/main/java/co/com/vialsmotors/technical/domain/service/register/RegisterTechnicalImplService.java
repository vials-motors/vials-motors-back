package co.com.vialsmotors.technical.domain.service.register;

import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.FindTechnicalException;
import co.com.vialsmotors.technical.domain.model.Technical;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByIdentificationNumberOrEmailRepository;
import co.com.vialsmotors.technical.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.technical.domain.port.RegisterTechnicalRepository;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.infrastructure.apirest.dto.CredentialDTO;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.mapper.TechnicalMapper;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class RegisterTechnicalImplService implements RegisterTechnicalService{

    private static final Logger LOGGER = Logger.getLogger(RegisterTechnicalImplService.class);
    private static final Boolean STATUS_FOR_TECHNICAL_ACTIVE = true;
    private static final Boolean STATUS_FOR_TECHNICAL_INACTIVE = false;
    private static final Integer NUMBER_FOR_USER_TYPE = 3;
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());
    private final RegisterTechnicalRepository registerTechnicalRepository;
    private final RegisterCredentialPort registerCredentialPort;
    private final GetTechnicalByIdentificationNumberOrEmailRepository getTechnicalByIdentificationNumberOrEmailRepository;

    public RegisterTechnicalImplService(RegisterTechnicalRepository registerTechnicalRepository, RegisterCredentialPort registerCredentialPort, GetTechnicalByIdentificationNumberOrEmailRepository getTechnicalByIdentificationNumberOrEmailRepository) {
        this.registerTechnicalRepository = registerTechnicalRepository;
        this.registerCredentialPort = registerCredentialPort;
        this.getTechnicalByIdentificationNumberOrEmailRepository = getTechnicalByIdentificationNumberOrEmailRepository;
    }

    @Override
    public ResponseEntity<GenericResponse> registerTechnical(Technical technical) {
        LOGGER.info("MS-TECHNICAL: Register Technical in class RegisterTechnicalImplService in method registerTechnical");
        this.getTechnicalWithIdentificationNumber(technical.identificationNumber());
        this.getTechnicalWithEmail(technical.email());
        TechnicalEntity technicalEntity = TechnicalMapper.convertModelToEntity(technical, STATUS_FOR_TECHNICAL_ACTIVE);
        this.validateIdentificationNumberAndRegister(technicalEntity, technical);
        return ResponseEntity.ok(new GenericResponse(EResponse.REGISTER_TECHNICAL_SUCCESSFULLY.getCode(),
                        EResponse.REGISTER_TECHNICAL_SUCCESSFULLY.getMessage(),
                        TIMESTAMP_RESPONSE));

    }

    private CredentialDTO buildCredentialDto(Technical technical, String customerID){
        return new CredentialDTO(technical.email(), technical.password(), customerID);
    }

    private void validateIdentificationNumberAndRegister(TechnicalEntity newTechnicalEntity, Technical technical){
        Optional<TechnicalEntity> managerEntity = registerTechnicalRepository.findByIdentificationNumberAndStatusAndEmail(technical.identificationNumber(), STATUS_FOR_TECHNICAL_ACTIVE, technical.email());
        Optional<TechnicalEntity> managerEntityInactive = registerTechnicalRepository.findByIdentificationNumberAndStatusAndEmail(technical.identificationNumber(), STATUS_FOR_TECHNICAL_INACTIVE, technical.email());
        if (managerEntity.isPresent()){
            throw new FindTechnicalException(EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
        } else if (managerEntityInactive.isPresent()) {
            callRegisterCredential(technical, managerEntityInactive.get().getCustomerID());
            registerTechnicalRepository.registerTechnical(buildTechnicalEntity(newTechnicalEntity, managerEntityInactive.get()));
        }else{
            callRegisterCredential(technical, newTechnicalEntity.getCustomerID());
            registerTechnicalRepository.registerTechnical(newTechnicalEntity);
        }
    }

    private void callRegisterCredential(Technical technical, String customerId){
        Boolean statusCall = registerCredentialPort.registerCredential(buildCredentialDto(technical, customerId), NUMBER_FOR_USER_TYPE);
        if (Boolean.FALSE.equals(statusCall)){
            throw new CallCredentialException(EResponse.MESSAGE_ERROR_REGISTER.getMessage(), EResponse.MESSAGE_ERROR_IDENTIFICATION_NUMBER.getCode());
        }
    }

    private TechnicalEntity buildTechnicalEntity(TechnicalEntity newTechnicalEntity, TechnicalEntity previusTechnicalEntity){
        return previusTechnicalEntity.withFirstName(newTechnicalEntity.getFirstName())
                .withSecondName(newTechnicalEntity.getSecondName())
                .withFirstLastName(newTechnicalEntity.getFirstLastName())
                .withSecondLastName(newTechnicalEntity.getSecondLastName())
                .withPhone(newTechnicalEntity.getPhone())
                .withIdentificationNumber(newTechnicalEntity.getIdentificationNumber())
                .withIdentificationType(newTechnicalEntity.getIdentificationType())
                .withEmail(newTechnicalEntity.getEmail())
                .withStatus(true);
    }

    private void getTechnicalWithIdentificationNumber(String identificationNumber){
        Optional<TechnicalEntity> technicalEntity = this.getTechnicalByIdentificationNumberOrEmailRepository.getTechnicalByIdentificationNumber(identificationNumber);
        if (technicalEntity.isPresent()){
            throw new FindTechnicalException(EResponse.IDENTIFICATION_NUMBER_ERROR.getMessage(), EResponse.IDENTIFICATION_NUMBER_ERROR.getCode());
        }
    }

    private void getTechnicalWithEmail(String email){
        Optional<TechnicalEntity> technicalEntity = this.getTechnicalByIdentificationNumberOrEmailRepository.getTechnicalByEmail(email);
        if (technicalEntity.isPresent()){
            throw new FindTechnicalException(EResponse.EMAIL_ERROR.getMessage(), EResponse.EMAIL_ERROR.getCode());
        }
    }
}
