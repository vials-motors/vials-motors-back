package co.com.vialsmotors.technical.domain.service.register;

import co.com.vialsmotors.technical.domain.model.Technical;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface RegisterTechnicalService {

    ResponseEntity<GenericResponse> registerTechnical(Technical technical);
}
