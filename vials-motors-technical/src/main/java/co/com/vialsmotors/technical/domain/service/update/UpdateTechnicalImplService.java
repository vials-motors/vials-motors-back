package co.com.vialsmotors.technical.domain.service.update;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.FindTechnicalException;
import co.com.vialsmotors.technical.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.technical.domain.port.RegisterTechnicalRepository;
import co.com.vialsmotors.technical.domain.port.credential.UpdateCredentialPort;
import co.com.vialsmotors.technical.domain.request.UpdateTechnicalRequest;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.TimeZone;

public class UpdateTechnicalImplService implements UpdateTechnicalService {

    private static final Logger LOGGER = Logger.getLogger(UpdateTechnicalImplService.class);
    private static final Boolean STATUS = true;

    private final RegisterTechnicalRepository registerTechnicalRepository;
    private final UpdateCredentialPort updateCredentialPort;

    public UpdateTechnicalImplService(RegisterTechnicalRepository registerTechnicalRepository, UpdateCredentialPort updateCredentialPort) {
        this.registerTechnicalRepository = registerTechnicalRepository;
        this.updateCredentialPort = updateCredentialPort;
    }

    @Override
    public ResponseEntity<GenericResponse> updateTechnical(UpdateTechnicalRequest newTechnical, String identificationNumber, String email) {
        LOGGER.info("INFO-MS-TECHNICAL: Update technical in Update Technical Impl Service with identification number " + identificationNumber);
        this.validateIdentificationNumber(identificationNumber);
        registerTechnicalRepository.registerTechnical(buildNewTechnical(newTechnical, identificationNumber, email));
        return ResponseEntity.ok(new GenericResponse(EResponse.UPDATE_TECHNICAL_SUCCESSFULLY.getCode(), EResponse.UPDATE_TECHNICAL_SUCCESSFULLY.getMessage(),
                ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId())));
    }

    private TechnicalEntity getTechnicalEntity(String identificationNumber, String email){
        Optional<TechnicalEntity> technicalEntity = registerTechnicalRepository.findByIdentificationNumberAndStatusAndEmail(identificationNumber, STATUS, email);
        return technicalEntity.orElse(null);
    }

    private TechnicalEntity buildNewTechnical(UpdateTechnicalRequest newTechnical, String identificationNumber, String email){
        TechnicalEntity technicalEntity = getTechnicalEntity(identificationNumber, email);
        this.validateTechnicalEntity(technicalEntity);
        this.validateAndCall(newTechnical, technicalEntity);
        return technicalEntity.withFirstName(newTechnical.getFirstName())
                .withSecondName(newTechnical.getSecondName())
                .withFirstLastName(newTechnical.getFirstLastName())
                .withSecondLastName(newTechnical.getSecondLastName())
                .withPhone(newTechnical.getPhone())
                .withEmail(newTechnical.getEmail())
                .withIdentificationNumber(newTechnical.getIdentificationNumber());
    }

    private void validateTechnicalEntity(TechnicalEntity technicalEntity){
        if (technicalEntity == null){
            throw new FindTechnicalException(EResponse.TECHNICAL_NOT_EXIST.getMessage(), EResponse.TECHNICAL_NOT_EXIST.getCode());
        }
    }

    private void validateIdentificationNumber(String identificationNumber){
        if (identificationNumber == null || identificationNumber.isEmpty()){
            throw new IdentificationNumberException(EResponse.FIELD_IDENTIFICATION_NUMBER.getMessage(), EResponse.FIELD_IDENTIFICATION_NUMBER.getCode());
        }
    }

    private Boolean callUpdateCredential(UpdateTechnicalRequest technical, String customerId){
        return updateCredentialPort.updateCredential(technical.getEmail(), customerId);
    }

    private Boolean validateCreateUsername(UpdateTechnicalRequest technical, TechnicalEntity technicalEntity){
        return !technicalEntity.getEmail().equals(technical.getEmail());
    }

    private void validateAndCall(UpdateTechnicalRequest technical, TechnicalEntity technicalEntity){
        if (Boolean.TRUE.equals(validateCreateUsername(technical, technicalEntity))){
            LOGGER.info("INFO-MS-TECHNICAL: Update credential with first name: " + technical.getFirstName());
            if (Boolean.FALSE.equals(callUpdateCredential(technical, technicalEntity.getCustomerID()))){
                LOGGER.error("ERROR-MS-TECHNICAL: Error call update credential");
                throw new CallCredentialException(ECallMessage.ERROR_CALL_CREDENTIAL_MESSAGE.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL_MESSAGE.getCode());
            }
        }
    }

}
