package co.com.vialsmotors.technical.domain.service.update;

import co.com.vialsmotors.technical.domain.request.UpdateTechnicalRequest;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface UpdateTechnicalService {

    ResponseEntity<GenericResponse> updateTechnical(UpdateTechnicalRequest newTechnical, String identificationNumber, String email);
}
