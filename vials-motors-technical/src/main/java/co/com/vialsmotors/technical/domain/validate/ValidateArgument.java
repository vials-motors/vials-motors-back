package co.com.vialsmotors.technical.domain.validate;

import co.com.vialsmotors.commons.domain.exception.FieldException;
import co.com.vialsmotors.commons.domain.exception.IdentificationTypeException;

public class ValidateArgument {

    private static final String NOT_INSTANTIABLE = "The class validate argument is not instantiable";

    private ValidateArgument(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static void validateIdentificationType(Object object, String status, String message){
        if (object == null)
            throw new IdentificationTypeException(message, status);
    }

    public static void validateFieldString(String object, String status, String message){
        if (object == null || object.isEmpty())
            throw new FieldException(message, status);
    }

    public static void validateFieldLong(Long object, String status, String message){
        if (object == null)
            throw new FieldException(message, status);
    }
}
