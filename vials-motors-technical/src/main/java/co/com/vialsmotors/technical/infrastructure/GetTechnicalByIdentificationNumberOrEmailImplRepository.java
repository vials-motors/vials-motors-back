package co.com.vialsmotors.technical.infrastructure;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByIdentificationNumberOrEmailRepository;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.jpa.TechnicalSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GetTechnicalByIdentificationNumberOrEmailImplRepository implements GetTechnicalByIdentificationNumberOrEmailRepository {

    private static final Logger LOGGER = Logger.getLogger(GetTechnicalByIdentificationNumberOrEmailImplRepository.class);
    private static final Boolean STATUS = Boolean.TRUE;
    private final TechnicalSqlServerRepository technicalSqlServerRepository;

    @Override
    public Optional<TechnicalEntity> getTechnicalByIdentificationNumber(String identificationNumber) {
       try {
           LOGGER.info("INFO-MS-TECHNICAL: Error get technical with identification number: " + identificationNumber);
           return technicalSqlServerRepository.findByIdentificationNumberAndStatus(identificationNumber, STATUS);
       }catch (Exception e){
           throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
       }
    }

    @Override
    public Optional<TechnicalEntity> getTechnicalByEmail(String email) {
        try {
            LOGGER.info("INFO-MS-TECHNICAL: Error get technical with email: " + email);
            return technicalSqlServerRepository.findByEmailAndStatus(email, STATUS);
        }catch (Exception e){
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
