package co.com.vialsmotors.technical.infrastructure.adapter;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.technical.domain.port.GetListTechnicalRepository;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.jpa.TechnicalSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class GetListTechnicalImplRepository implements GetListTechnicalRepository {

    private static final Logger LOGGER = Logger.getLogger(GetListTechnicalImplRepository.class);

    private final TechnicalSqlServerRepository technicalSqlServerRepository;

    @Override
    public List<TechnicalEntity> getTechnicalList() {
        try {
            return technicalSqlServerRepository.findAll();
        }catch (Exception exception) {
            LOGGER.error("ERR-MS-TECHNICAL: Error en GetListTechnicalImplRepository in method getTechnicalList with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
