package co.com.vialsmotors.technical.infrastructure.adapter;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.technical.domain.port.GetTechnicalByCustomerIdRepository;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.jpa.TechnicalSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
@RequiredArgsConstructor
public class GetTechnicalByCustomerIdImplRepository implements GetTechnicalByCustomerIdRepository {

    private static final Logger LOGGER = Logger.getLogger(GetTechnicalByCustomerIdImplRepository.class);
    private final TechnicalSqlServerRepository technicalSqlServerRepository;

    @Override
    public Optional<TechnicalEntity> getTechnical(String customerId) {
        try {
            return technicalSqlServerRepository.findByCustomerId(customerId);
        }catch (Exception e){
            LOGGER.error("ERROR-MS-TECHNICAL: Error get technical with customer id: " + customerId + " And with message: " + e.getMessage());
            throw new SqlServerDataBaseException(e.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }
}
