package co.com.vialsmotors.technical.infrastructure.adapter;

import co.com.vialsmotors.commons.domain.enums.EDatabaseResponse;
import co.com.vialsmotors.commons.domain.exception.SqlServerDataBaseException;
import co.com.vialsmotors.technical.domain.port.RegisterTechnicalRepository;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.jpa.TechnicalSqlServerRepository;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class RegisterTechnicalImplRepository implements RegisterTechnicalRepository {

    private static final Logger LOGGER = Logger.getLogger(RegisterTechnicalImplRepository.class);

    private final TechnicalSqlServerRepository technicalSqlServerRepository;

    @Override
    public void registerTechnical(TechnicalEntity technicalEntity) {
        try {
            technicalSqlServerRepository.save(technicalEntity);
        }catch (Exception exception) {
            LOGGER.error("ERROR-MS-TECHNICAL: Error in " + RegisterTechnicalImplRepository.class.getName() + " with message: " + exception.getMessage());
            throw new SqlServerDataBaseException(exception.getMessage(), EDatabaseResponse.ERROR_DATABASE.getCode());
        }
    }

    @Override
    public Optional<TechnicalEntity> findByIdentificationNumberAndStatusAndEmail(String identificationNumber, Boolean status, String email) {
        return technicalSqlServerRepository.findByIdentificationNumberAndStatus(identificationNumber, status);
    }
}
