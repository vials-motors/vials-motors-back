package co.com.vialsmotors.technical.infrastructure.adapter.credential;

import co.com.vialsmotors.commons.domain.enums.ECallMessage;
import co.com.vialsmotors.commons.domain.exception.CallCredentialException;
import co.com.vialsmotors.technical.domain.port.credential.RegisterCredentialPort;
import co.com.vialsmotors.technical.infrastructure.apirest.client.CredentialService;
import co.com.vialsmotors.technical.infrastructure.apirest.dto.CredentialDTO;
import co.com.vialsmotors.technical.infrastructure.apirest.dto.FeignResponse;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class RegisterCredentialAdapter implements RegisterCredentialPort {

    private static final Logger LOGGER = Logger.getLogger(RegisterCredentialAdapter.class);

    private final CredentialService credentialService;

    @Override
    public Boolean registerCredential(CredentialDTO credentialDTO, Integer userType) {
            ResponseEntity<FeignResponse> response = credentialService.registerCredential(generateHeaders(userType), credentialDTO);
            return validateResponse(response);
    }

    private Map<String, Object> generateHeaders(Integer userType){
        Map<String, Object> headers =  new HashMap<>();
        headers.put("user-type", userType);
        return headers;
    }

    private Boolean validateResponse(ResponseEntity<FeignResponse> response){
        try{
            validateHttpStatus(response);
            return Objects.requireNonNull(response.getBody()).getCode().equals("SUCC-00");
        }catch (Exception exception){
            LOGGER.error("ERROR-MS-CUSTOMER: Error in validate response for the class Register credential Adapter: " + exception.getMessage());
            throw new CallCredentialException(exception.getMessage(), ECallMessage.ERROR_CALL_CREDENTIAL.getCode());
        }
    }

    private void validateHttpStatus(ResponseEntity<FeignResponse> response){
        if (response.getStatusCodeValue() == 401){
            throw new CallCredentialException("401 FROM LOGIN", "ERR-0000");
        }
    }
}
