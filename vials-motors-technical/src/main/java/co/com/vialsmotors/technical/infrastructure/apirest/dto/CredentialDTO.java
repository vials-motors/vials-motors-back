package co.com.vialsmotors.technical.infrastructure.apirest.dto;

public record CredentialDTO(String email, String password, String customerID) {
}
