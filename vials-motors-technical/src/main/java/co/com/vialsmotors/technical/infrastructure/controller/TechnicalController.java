package co.com.vialsmotors.technical.infrastructure.controller;

import co.com.vialsmotors.technical.application.command.TechnicalCommand;
import co.com.vialsmotors.technical.application.handler.delete.DeleteTechnicalHandler;
import co.com.vialsmotors.technical.application.handler.get.GetTechnicalByCustomerIdHandler;
import co.com.vialsmotors.technical.application.handler.list.GetListTechnicalHandler;
import co.com.vialsmotors.technical.application.handler.register.RegisterTechnicalHandler;
import co.com.vialsmotors.technical.application.handler.update.UpdateTechnicalHandler;
import co.com.vialsmotors.technical.domain.response.GenericListResponse;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import co.com.vialsmotors.technical.domain.response.TechnicalResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/technical")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class TechnicalController {

    private static final Logger LOGGER = Logger.getLogger(TechnicalController.class);

    private final RegisterTechnicalHandler registerTechnicalHandler;
    private final GetListTechnicalHandler getListTechnicalHandler;
    private final UpdateTechnicalHandler updateTechnicalHandler;
    private final DeleteTechnicalHandler deleteTechnicalHandler;
    private final GetTechnicalByCustomerIdHandler getTechnicalByCustomerIdHandler;
    private final ObjectMapper objectMapper;

    @PostMapping
    public ResponseEntity<GenericResponse> addTechnical(@RequestBody TechnicalCommand technicalCommand) throws JsonProcessingException {
        LOGGER.info("MS-TECHNICAL: Register Technical in Technical Controller class with body request: " + objectMapper.writeValueAsString(technicalCommand));
        return registerTechnicalHandler.registerTechnical(technicalCommand);
    }

    @GetMapping
    public ResponseEntity<GenericListResponse> getTechnicalList(){
        LOGGER.info("MS-TECHNICAL: Get Technical List in controller class");
        return getListTechnicalHandler.getListTechnical();
    }

    @PutMapping
    public ResponseEntity<GenericResponse> updateTechnical(@RequestBody TechnicalCommand technicalCommand,
                                                           @RequestParam("identificationNumber") String identificationNumber,
                                                           @RequestParam("email") String email) throws JsonProcessingException {
        LOGGER.info("MS-TECHNICAL: Update Technical in Technical Controller class with body request: " + objectMapper.writeValueAsString(technicalCommand));
        LOGGER.info("MS-TECHNICAL: Update Technical in Technical Controller class with identification Number: " + identificationNumber);
        return updateTechnicalHandler.updateTechnical(technicalCommand, identificationNumber, email);
    }

    @PutMapping(value = "/delete")
    public ResponseEntity<GenericResponse> deleteTechnical(@RequestParam("identificationNumber") String identificationNumber, @RequestParam("email") String email){
        LOGGER.info("MS-TECHNICAL: Delete technical in Technical controller with identification number " + identificationNumber);
        return deleteTechnicalHandler.deleteTechnical(identificationNumber, email);
    }

    @GetMapping(path = "/get-technical")
    public ResponseEntity<TechnicalResponse> getTechnicalByCustomerId(@RequestParam("customerId") String customerId){
        LOGGER.info("INFO-MS-TECHNICAL: Get technical in technical repository with customer id: " + customerId);
        return getTechnicalByCustomerIdHandler.getTechnical(customerId);
    }
}
