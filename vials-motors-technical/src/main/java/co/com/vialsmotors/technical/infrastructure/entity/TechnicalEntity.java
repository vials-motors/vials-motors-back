package co.com.vialsmotors.technical.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "technical", uniqueConstraints = {
        @UniqueConstraint(columnNames = "customer_ID"),
        @UniqueConstraint(columnNames = "identification_number")
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TechnicalEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customer")
    private Long idCustomer;

    @Column(name = "firs_name", nullable = false, length = 250)
    private String firstName;

    @Column(name = "second_name", length = 250)
    private String secondName;

    @Column(name = "first_last_name", nullable = false, length = 250)
    private String firstLastName;

    @Column(name = "second_last_name", length = 250)
    private String secondLastName;

    @Column(length = 100, nullable = false)
    private String phone;

    @Column(name = "identification_number", nullable = false, unique = true, length = 250)
    private String identificationNumber;

    @Column(name = "customer_ID", nullable = false, unique = true, length = 250)
    private String customerID;

    @ManyToOne
    @JoinColumn(name = "id_identification_type")
    private IdentificationTypeEntity identificationType;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "status", nullable = false)
    private Boolean status;

    public TechnicalEntity(String firstName, String secondName, String firstLastName, String secondLastName,
                           String phone, String identificationNumber, String customerID,
                           IdentificationTypeEntity identificationType, String email, Boolean status) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.phone = phone;
        this.identificationNumber = identificationNumber;
        this.customerID = customerID;
        this.identificationType = identificationType;
        this.email = email;
        this.status = status;
    }

    public TechnicalEntity withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TechnicalEntity withSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public TechnicalEntity withFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
        return this;
    }

    public TechnicalEntity withSecondLastName(String secondLastName){
        this.secondLastName = secondLastName;
        return this;
    }

    public TechnicalEntity withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public TechnicalEntity withIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
        return this;
    }

    public TechnicalEntity withEmail(String email) {
        this.email = email;
        return this;
    }

    public TechnicalEntity withIdentificationType(IdentificationTypeEntity identificationType){
        this.identificationType = identificationType;
        return this;
    }

    public TechnicalEntity withStatus(Boolean status){
        this.status = status;
        return this;
    }
}
