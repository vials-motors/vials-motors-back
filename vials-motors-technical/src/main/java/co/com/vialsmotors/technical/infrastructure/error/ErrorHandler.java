package co.com.vialsmotors.technical.infrastructure.error;

import co.com.vialsmotors.commons.domain.exception.*;
import co.com.vialsmotors.technical.domain.enums.EResponse;
import co.com.vialsmotors.technical.domain.exception.FindTechnicalException;
import co.com.vialsmotors.technical.domain.exception.IdentificationNumberException;
import co.com.vialsmotors.technical.domain.exception.ListTechnicalException;
import co.com.vialsmotors.technical.domain.response.GenericResponse;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ErrorHandler {

    private static final Logger LOGGER = Logger.getLogger(ErrorHandler.class);
    private static final String MESSAGE_ERROR_GENERIC = "MS-LOGIN: Error Handler with message: ";
    private static final ConcurrentHashMap<String, Integer> CODE_STATE = new ConcurrentHashMap<>();
    private static final ZonedDateTime TIMESTAMP_RESPONSE = ZonedDateTime.now(TimeZone.getTimeZone("America/Bogota").toZoneId());

    public ErrorHandler() {
        CODE_STATE.put(FieldException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(DecodedErrorTokenException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(CallCredentialException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(GetUserNameTokenException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(IdentificationTypeException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(SqlServerDataBaseException.class.getSimpleName(), HttpStatus.SERVICE_UNAVAILABLE.value());
        CODE_STATE.put(ListTechnicalException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(FindTechnicalException.class.getSimpleName(), HttpStatus.OK.value());
        CODE_STATE.put(IdentificationNumberException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        CODE_STATE.put(MissingServletRequestParameterException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse> handleAllException(Exception exception){
        String nameException = exception.getClass().getSimpleName();
        Integer code = CODE_STATE.get(nameException);

        if (code != null && exception instanceof FieldException fieldException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(fieldException.getStatus(), fieldException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof DecodedErrorTokenException decodedErrorTokenException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(decodedErrorTokenException.getStatus(), decodedErrorTokenException.getMessage(), TIMESTAMP_RESPONSE));
        }else if (code != null && exception instanceof CallCredentialException callCredentialException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(callCredentialException.getStatus(), callCredentialException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof GetUserNameTokenException getUserNameTokenException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(getUserNameTokenException.getStatus(), getUserNameTokenException.getMessage(), TIMESTAMP_RESPONSE));
        } else if(code != null && exception instanceof IdentificationTypeException identificationTypeException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(identificationTypeException.getStatus(), identificationTypeException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof SqlServerDataBaseException sqlServerDataBaseException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(sqlServerDataBaseException.getStatus(), sqlServerDataBaseException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof ListTechnicalException listTechnicalException){
            return ResponseEntity.status(code)
                    .body(new GenericResponse(listTechnicalException.getStatus(), listTechnicalException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof IdentificationNumberException identificationNumberException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(identificationNumberException.getStatus(), identificationNumberException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof FindTechnicalException findTechnicalException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(findTechnicalException.getStatus(), findTechnicalException.getMessage(), TIMESTAMP_RESPONSE));
        } else if (code != null && exception instanceof MissingServletRequestParameterException missingServletRequestParameterException) {
            return ResponseEntity.status(code)
                    .body(new GenericResponse(code.toString(), missingServletRequestParameterException.getMessage(), TIMESTAMP_RESPONSE));
        } else {
            LOGGER.info(MESSAGE_ERROR_GENERIC + exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericResponse(EResponse.INTERNAL_ERROR.getCode(), EResponse.INTERNAL_ERROR.getMessage(), TIMESTAMP_RESPONSE));
        }
    }
}
