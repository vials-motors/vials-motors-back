package co.com.vialsmotors.technical.infrastructure.jpa;

import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TechnicalSqlServerRepository extends JpaRepository <TechnicalEntity, Long>{

    @Query(value = "SELECT * FROM technical WHERE identification_number = ?1 AND status = ?2", nativeQuery = true)
    Optional<TechnicalEntity> findByIdentificationNumberAndStatus(String identificationNumber, Boolean status);

    @Query(value = "SELECT * FROM technical WHERE customer_ID =?1", nativeQuery = true)
    Optional<TechnicalEntity> findByCustomerId(String customerId);

    @Query(value = "SELECT * FROM technical WHERE email =?1 AND status =?2", nativeQuery = true)
    Optional<TechnicalEntity> findByEmailAndStatus(String email, Boolean status);
}
