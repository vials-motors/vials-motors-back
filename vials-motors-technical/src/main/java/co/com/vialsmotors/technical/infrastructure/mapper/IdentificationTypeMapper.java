package co.com.vialsmotors.technical.infrastructure.mapper;

import co.com.vialsmotors.technical.domain.model.IdentificationType;
import co.com.vialsmotors.technical.infrastructure.entity.IdentificationTypeEntity;

public class IdentificationTypeMapper {

    private static final String NOT_INSTANTIABLE = "The class " + IdentificationTypeMapper.class.getName() + " is not instantiable";

    private IdentificationTypeMapper(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static IdentificationTypeEntity convertModelToEntity(IdentificationType identificationType){
        return new IdentificationTypeEntity(identificationType.idIdentificationType(), identificationType.description());
    }
}
