package co.com.vialsmotors.technical.infrastructure.mapper;

import co.com.vialsmotors.technical.domain.dto.TechnicalDTO;
import co.com.vialsmotors.technical.domain.model.Technical;
import co.com.vialsmotors.technical.infrastructure.entity.TechnicalEntity;
import co.com.vialsmotors.technical.infrastructure.utils.CreateCustomerIdUtils;
import co.com.vialsmotors.technical.infrastructure.utils.ValidateStatusUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TechnicalMapper {

    private static final String NOT_INSTANTIABLE = "The class " + TechnicalMapper.class.getName() + " is not instantiable";

    private TechnicalMapper() {
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static TechnicalEntity convertModelToEntity(Technical technical, Boolean status){
        return new TechnicalEntity(technical.firstName(), technical.secondName(),
                technical.firstLastName(), technical.secondLastName(),
                technical.phone(), technical.identificationNumber(),
                CreateCustomerIdUtils.generateAlphanumericString(),
                IdentificationTypeMapper.convertModelToEntity(technical.identificationType()),
                technical.email(), status);
    }

    public static List<TechnicalDTO> convertListEntityToListDto(List<TechnicalEntity> entityList){
        return entityList.stream()
                .map(entityTemporal -> new TechnicalDTO(entityTemporal.getFirstName(),
                        entityTemporal.getSecondName(), entityTemporal.getFirstLastName(),
                        entityTemporal.getSecondLastName(), entityTemporal.getPhone(),
                        entityTemporal.getIdentificationNumber(), entityTemporal.getEmail(),
                        ValidateStatusUtils.validateStatus(entityTemporal.getStatus()),
                        entityTemporal.getCustomerID()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static TechnicalDTO convertEntityToModel(TechnicalEntity technicalEntity){
        return new TechnicalDTO(technicalEntity.getFirstName(), technicalEntity.getSecondName(),
                technicalEntity.getFirstLastName(), technicalEntity.getSecondLastName(), technicalEntity.getPhone(),
                technicalEntity.getIdentificationNumber(), technicalEntity.getEmail(),
                ValidateStatusUtils.validateStatus(technicalEntity.getStatus()),
                technicalEntity.getCustomerID());
    }
}
