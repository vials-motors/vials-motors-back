package co.com.vialsmotors.technical.infrastructure.security;

import co.com.vialsmotors.technical.infrastructure.security.jwt.AuthEntryPointJwt;
import co.com.vialsmotors.technical.infrastructure.security.jwt.AuthTokenFilter;
import co.com.vialsmotors.technical.infrastructure.security.jwt.JwtUtils;
import co.com.vialsmotors.technical.infrastructure.security.userdetails.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity()
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig {

    private static final String TECHNICAL = "/technical";
    public static final String ADMIN = "ADMIN";
    public static final String CLIENT = "CLIENT";

    private final AuthEntryPointJwt unauthorizedHandler;
    private final JwtUtils jwtUtils;
    private final UserDetailsServiceImpl userDetailsService;


    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter(){
        return new AuthTokenFilter(jwtUtils, userDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers(HttpMethod.POST, TECHNICAL).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.GET, TECHNICAL).hasAnyRole(ADMIN, CLIENT)
                .antMatchers(HttpMethod.PUT, TECHNICAL).hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.PUT, "/technical/delete").hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.GET, "/technical/get-technical").permitAll()
                .anyRequest().authenticated().and()
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class).build();
    }
}
