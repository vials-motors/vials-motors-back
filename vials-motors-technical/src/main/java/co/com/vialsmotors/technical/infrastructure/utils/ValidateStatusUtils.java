package co.com.vialsmotors.technical.infrastructure.utils;

public class ValidateStatusUtils {

    private static final String NOT_INSTANTIABLE = "The class Validate status utils is not instantiable";

    private ValidateStatusUtils(){
        throw new UnsupportedOperationException(NOT_INSTANTIABLE);
    }

    public static String validateStatus(Boolean status){
        return Boolean.TRUE.equals(status) ? "ACTIVE":"INACTIVE";
    }
}
